<?php

return [
    'default' => 'vinylheaven-theme',

    'themes' => [
        // 'default' => [
        //     'views_path' => 'resources/themes/default/views',
        //     'assets_path' => 'public/themes/default/assets',
        //     'name' => 'Default'
        // ],
        'vinylheaven-seller-portal-theme' => [
            'views_path' => 'resources/themes/vinylheaven-seller-portal-theme/views',
            'assets_path' => 'public/themes/vinylheaven-seller-portal-theme/assets',
            'name' => 'VinylHeaven Seller Portal Theme'
        ],
        'vinylheaven-theme' => [
            'views_path' => 'resources/themes/vinylheaven-theme/views',
            'assets_path' => 'public/themes/vinylheaven-theme/assets',
            'name' => 'VinylHeaven Theme'
        ],
        // 'velocity' => [
        //     'views_path' => 'resources/themes/velocity/views',
        //     'assets_path' => 'public/themes/velocity/assets',
        //     'name' => 'Velocity',
        //     'parent' => 'default'
        // ],
        'vinylexpress' => [
            'views_path' => 'resources/themes/vinylexpress/views',
            'assets_path' => 'public/themes/vinylexpress/assets',
            'name' => 'VinylExpress',
            'parent' => 'default'
        ]
    ]
];
