<?php

return [
    'paypal' => [
        'slug' => 'paypal',
        'title' => 'Paypal Standard',
        'description' => 'Paypal standard payment',
        'class' => 'VinylHeaven\Payment\Methods\Paypal',
        'return_route' => 'shop.checkout.process.paypal.payment',
        'cancel_route' => '',
    ],
    'bank' => [
        'slug' => 'bank',
        'title' => 'Bank Transfer',
        'description' => 'Standard bank transfer',
        'class' => 'VinylHeaven\Payment\Methods\Bank',
        'return_route' => 'shop.checkout.process.bank.payment',
        'cancel_route' => '',
    ],
    'cash' => [
        'slug' => 'cash',
        'title' => 'Cash Payment',
        'description' => 'Pay with cash',
        'class' => 'VinylHeaven\Payment\Methods\Cash',
        'return_route' => 'shop.checkout.process.cash.payment',
        'cancel_route' => '',
    ],
];