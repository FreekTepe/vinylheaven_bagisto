<?php

    return [
        'bymail' => [
            'code'              => 'bymail',
            'title'             => 'Send By Mail',
            'description'       => 'Send your order by mail',
            'active'            => true,
            'default_rate'      => '6.95',
            'for_vinyl_heaven'  => true,
            'class'             => 'VinylHeaven\Shipping\Carriers\ByMail',
        ],
    
        'pickup'     => [
            'code'              => 'pickup',
            'title'             => 'Pickup',
            'description'       => 'Pickup your order at the store',
            'active'            => true,
            'default_rate'      => '0',
            'for_vinyl_heaven'  => true,
            'class'             => 'VinylHeaven\Shipping\Carriers\Pickup',
        ]
    ];
