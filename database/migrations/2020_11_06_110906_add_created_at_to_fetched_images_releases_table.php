<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCreatedAtToFetchedImagesReleasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fetched_image_releases', function (Blueprint $table) {
            $table->unsignedBigInteger('id', true)->change();
            $table->dateTime('created_at');
            $table->dropColumn('last_updated');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fetched_image_releases', function (Blueprint $table) {
            //
        });
    }
}
