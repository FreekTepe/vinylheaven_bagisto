<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use VinylHeaven\CustomAttributeType\Models\ArtistGroup;

$factory->define(ArtistGroup::class, function (Faker $faker) {
    return [
        "name" => $faker->firstNameMale,
        "profile" => $faker->paragraph(5, true),
        "data_quality" => $faker->paragraph(5, true)
    ];
});
