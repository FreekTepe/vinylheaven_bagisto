<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use \VinylHeaven\CustomAttributeType\Models\Artist;
use \VinylHeaven\CustomAttributeType\Models\ArtistGroup;
use \VinylHeaven\CustomAttributeType\Models\ArtistSolo;

$factory->define(Artist::class, function (Faker $faker) {

    // generate either a ArtistGroup or ArtistSolo randomly
    if (rand(0, 1)) {
        $artistGroup = factory(ArtistGroup::class, 1)->create()->first();

        // link some more solo artists to group
        $artists = factory(ArtistSolo::class, 3)->create();
        $artistGroup->artists()->attach($artists->pluck('id'));

        return [
            'artistable_type' => 'group',
            'artistable_id' => $artistGroup->id
        ];
    } else {
        $artist = factory(ArtistSolo::class, 1)->create()->first();
        return [
            'artistable_type' => 'solo',
            'artistable_id' => $artist->id
        ];
    }
});
