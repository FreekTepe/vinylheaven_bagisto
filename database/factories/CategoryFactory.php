<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Webkul\Category\Models\Category;

$factory->define(Category::class, function (Faker $faker) {
    $now = Carbon::now();
    return [
        'position'   => '0',
        'image'      => NULL,
        'status'     => '1',
        '_lft'       => '0',
        '_rgt'       => '0',
        'parent_id'  => NULL,
        'created_at' => $now,
        'updated_at' => $now,
    ];
});
