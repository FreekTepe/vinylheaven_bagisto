<?php

use Illuminate\Database\Seeder;
use VinylHeaven\Shipping\Models\Description;

class FormatDescriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $used_descriptions = [
            'LP',
            '16"',
            '12"',
            '11"',
            '10"',
            '9"',
            '7"',
            '6½"',
            '6"',
            '5½"',
            '5"',
            '4"',
            '3½"',
            '3"',
            '2"',
            '21cm',
            '25cm',
            '27cm',
            '29cm',
            '35cm',
            '50cm',
            '3" Cine Reel',
            '5" Cine Reel',
            '7" Cine Reel',
            '2 Minute',
            '3 Minute',
            '4 Minute',
            'Concert',
            'Salon',
            'Mini',
            'Business Card',
            'Shape',
            'Minimax',
            'CD-ROM',
            'CDi',
            'CD+G',
            'HDCD',
            'VCD',
            'AVCD',
            'SVCD',
            '8"',
            'Blu-ray Audio',
            'DVD-Audio',
            'DVD-Data',
            'DVD-Video',
            '3.5"',
            '5.25"',
            'CD-Record',
            'VinylDisc',
            'Album',
            'Mini-Album',
            'EP',
            'Maxi-Single',
            'Single',
            'Compilation',
            'Card Backed',
            'Deluxe Edition',
            'Styrene',
            'Transcription',
            '16mm',
            '35mm'
        ];


        $unused_descriptions = [
            '8 ⅓ RPM',
            '16 ⅔ RPM',
            '33 ⅓ RPM',
            '45 RPM',
            '78 RPM',
            '80 RPM',
            '90 RPM',
            '15/16 ips',
            '1 ⅞ ips',
            '15 ips',
            '3 ¾ ips',
            '30 ips',
            '7 ½ ips',
            '½"',
            '¼"',
            '⅛"',
            '2-Track Mono',
            '2-Track Stereo',
            '4-Track Mono',
            '4-Track Stereo',
            'Hybrid',
            'Multichannel',
            'AAC',
            'AIFC',
            'AIFF',
            'ALAC',
            'AMR',
            'APE',
            'AVI',
            'DFF',
            'Disc Image',
            'DSF',
            'FLAC',
            'FLV',
            'MOV',
            'MP2',
            'MP3',
            'MPEG Video',
            'MPEG-4 Video',
            'ogg-vorbis',
            'Opus',
            'SHN',
            'SWF',
            'TTA',
            'WAV',
            'WavPack',
            'WMA',
            'WMV',
            'MP3 Surround',
            'DualDisc',
            'DVDplus',
            'Double Sided',
            'Single Sided',
            'Advance',
            'Stereo',
            'Mono',
            'Quadraphonic',
            'Ambisonic',
            'Club Edition',
            'Copy Protected',
            'Enhanced',
            'Etched',
            'Jukebox',
            'Limited',
            'Mispress',
            'Misprint',
            'Mixed',
            'Mixtape',
            'Numbered',
            'Partially Mixed',
            'Partially Unofficial',
            'Picture Disc',
            'Promo',
            'Reissue',
            'Remastered',
            'Repress',
            'Sampler',
            'Special Edition',
            'Test Pressing',
            'Unofficial Release',
            'White Label',
            'NTSC',
            'PAL',
            'SECAM'
        ];


        // Seed all genres and sub-genres/styles from config.genres
        foreach ($used_descriptions as $used_description) {
            Description::create(['name' => $used_description, 'used' => true]);
        }

        foreach ($unused_descriptions as $unused_description) {
            Description::create(['name' => $unused_description, 'used' => false]);
        }
    }
}
