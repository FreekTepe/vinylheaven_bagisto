# deploy vinylheaven on development server linode web dev 1

echo "Change folder to bagisto.rockhouse.nl project root"
cd /home/webadmin/bagisto/

#echo "Pulling latest from VinylHeaven bagisto repo"
#git pull git@bitbucket.org:vinylheaven/bagisto.git master
#echo "Pulling latest from VinylHeaven bagisto repo done"

echo "Execute composer update"
/bin/composer update

echo "Execute the needed command for ElasticSearch"
php artisan elastic:update-mapping "VinylHeaven\Product\Models\Product"

echo "Run database migrations"
php artisan migrate

echo "Setting the jwt secret"
php artisan jwt:secret

echo "Compiling the needed Vuejs components"
NODE_MODULE_DIR="/var/www/html/bagisto/bagisto/node_module"
if [ -d NODE_MODULE_DIR ]; then
    npm run dev
else
    npm install
    npm run dev
fi

echo "Finished deploy"