<?php

namespace VinylHeaven\Shipping\Repositories;

use VinylHeaven\Seller\Models\Seller;
use Webkul\Core\Eloquent\Repository;
use VinylHeaven\Shipping\Models\Policy;
use Illuminate\Database\Eloquent\Builder;
use VinylHeaven\Shipping\Models\Description;

class ShippingPriceRepository extends Repository
{

    protected $shippingPolicyRepository;

    public function __construct(
        ShippingPolicyRepository $shippingPolicyRepository
    )
    {
        $this->shippingPolicyRepository = $shippingPolicyRepository;
    }

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'VinylHeaven\Shipping\Models\Policy'; // ?? which model to use ?? (Repository breaks without model declaration)
    }

    public function getShippingPrice($marketplace_product, $ip = null, $customer = null)
    {

        // if(!$ip && !$customer){
        //     return null;
        // }

        // $country_code = null;
        // if($customer){
        //     if($customer->default_address){
        //         // extract country code from customer model
        //         $country_code = strtolower($customer->default_address->country);
        //     }
        // }else if($ip){
        //     // extract country code from ip address
        //     $country_code = strtolower(geoip($ip)['iso_code']);
        // }else{
        //     return null;
        // }

        // if(!$country_code){
        //     return null;
        // }

        $country_code = 'nl';

        $seller = Seller::find($marketplace_product->marketplace_seller_id);
        if(!$seller){
            return null;
        }

        $width = $marketplace_product->width;
        $height = $marketplace_product->height;
        $length = $marketplace_product->length;
        $weight = $marketplace_product->weight;

        if(!$width || !$height || !$length || !$weight){
            return null;
        }
        
        // Find seller policies that match the given country code
        $policy_matches = Policy::whereHas('countries', function (Builder $query) use ($country_code) {
            $query->where('code', $country_code);
        })->get();

        if(!$policy_matches || empty($policy_matches)){
            return null;
        }

        // Try to find a matching range within policy, we can return the price defined in this range as our shipping price!
        $shipping_price = null;
        foreach ($policy_matches as $policy) {
            foreach($policy->ranges as $range){
                if(
                    $width <= $range->max_width &&
                    $height <= $range->max_height &&
                    $length <= $range->max_length &&
                    $weight <= $range->weight_to &&
                    $weight >= $range->weight_from
                ){
                    
                    $shipping_price = $range->price;
                    break;
                }
            }
        }

        return $shipping_price;
    }
}
