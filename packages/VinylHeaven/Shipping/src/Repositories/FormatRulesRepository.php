<?php

namespace VinylHeaven\Shipping\Repositories;

use Webkul\Core\Eloquent\Repository;
use Illuminate\Database\Eloquent\Builder;
use VinylHeaven\Shipping\Models\Description;

class FormatRulesRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'VinylHeaven\Shipping\Models\Rule';
    }

    // Takes format json (as given from our rockhouse api, tries to match agains our format rules, and returns corresponding width,length,height & weight
    // Returns false, if no match is found.
    public function attachFormatRulesToProduct($product, $formats)
    {
        // loop trough formats and try to match them with rules
        foreach ($formats as $format) {

            // Retrieve posts with at least one comment containing words like foo%...
            $query = $this->model
                ->whereHas('format', function (Builder $query) use ($format) {
                    $query->where('name', $format['name']);
                });
                
            if(!is_null($format['descriptions'])){
                foreach ($this->filterUnusedDescriptions($format['descriptions']) as $description) {
                    $query->whereHas('descriptions', function (Builder $query) use ($description) {
                        $query->where('name', $description);
                    });
                }
            }
            
            $rule_match = $query->first();
            
            if (!is_null($rule_match)) {
                // attach rule_match->id to 
                $product->rules()->attach($rule_match->id);
            }
        }
    }

    // takes the format descriptions and filters out the unused descriptions in our system, so remove -> (WHERE description.used == false)
    public function filterUnusedDescriptions($descriptions)
    {
        $unused_descriptions = Description::where('used', false)->get()->pluck('name')->toArray();

        $filtered_descriptions = array();
        foreach ($descriptions as $description) {
            if (!in_array($description, $unused_descriptions)) {
                array_push($filtered_descriptions, $description);
            }
        }

        return $filtered_descriptions;
    }
}
