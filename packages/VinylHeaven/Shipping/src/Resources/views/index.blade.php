@extends('marketplace::shop.layouts.account')

@section('page_title')
Shipping Policies
@endsection

@section('content')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-box1 icon-gradient bg-tempting-azure"></i>
            </div>
            <div>Shipping policies
                <div class="page-title-subheading">Add shipping prices based on weight/dimension and order region.</div>
            </div>
        </div>
        <div class="page-title-actions">
            <div class="d-inline-block">
                <a href="{{ route('shipping.policies.create') }}" class="btn-shadow btn btn-info" style="color: white;">
                    Create
                </a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <pre-select-countries :sellerid="{{$seller->id}}"/>
    </div>
</div>
{!! app('VinylHeaven\Shipping\DataGrids\ShippingPoliciesDataGrid')->render() !!}

@endsection