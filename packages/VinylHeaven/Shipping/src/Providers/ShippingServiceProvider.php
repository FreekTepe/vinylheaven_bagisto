<?php

namespace VinylHeaven\Shipping\Providers;

use Illuminate\Support\ServiceProvider;
use VinylHeaven\Shipping\Classes\Shipping;

class ShippingServiceProvider extends ServiceProvider
{
    public function register()
    {
        // $this->app->bind('shipping', function () {
        //     return new Shipping();
        // });
    }

    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/../Http/Routes/api.php');
        $this->loadRoutesFrom(__DIR__ . '/../Http/Routes/web.php');
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
        $this->loadViewsFrom(__DIR__ . '/../Resources/views', 'shipping');
    }
}
