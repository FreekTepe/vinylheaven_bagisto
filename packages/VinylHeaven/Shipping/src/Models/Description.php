<?php

namespace VinylHeaven\Shipping\Models;

use Illuminate\Database\Eloquent\Model;
use VinylHeaven\Shipping\Models\Rule;

class Description extends Model
{
    protected $fillable = ['name', 'used'];

    public function rules()
    {
        return $this->belongsToMany(Rule::class);
    }
}
