<?php

namespace VinylHeaven\Shipping\Models;

use Illuminate\Database\Eloquent\Model;
use VinylHeaven\Shipping\Models\Rule;

class Format extends Model
{
    protected $fillable = ['name'];

    public function rules()
    {
        return $this->hasMany(Rule::class);
    }
}
