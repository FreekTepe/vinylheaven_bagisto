<?php

namespace VinylHeaven\Shipping\Models;

use Illuminate\Database\Eloquent\Model;
use VinylHeaven\Shipping\Models\Policy;

class Range extends Model
{

    protected $fillable = ['policy_id', 'max_width', 'max_length', 'max_height', 'weight_from', 'weight_to', 'price', 'base_weight'];

    public function policy()
    {
        return $this->belongsTo(Policy::class, 'policy_id', 'id');
    }


}