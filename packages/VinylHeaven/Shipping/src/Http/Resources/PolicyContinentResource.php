<?php

namespace VinylHeaven\Shipping\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use VinylHeaven\Shipping\Http\Resources\ContinentCountriesResource;

class PolicyContinentResource extends JsonResource
{

    protected $policy;

    public function __construct($policy)
    {
        $this->policy = $policy;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'selected' => $this->policy->countries->pluck('id'),
            'name' => $this->name,
            'code' => $this->code,
            'countries' => ContinentCountriesResource::collection($this->countries()->get())
        ];
    }
}
