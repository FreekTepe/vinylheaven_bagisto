<?php

namespace VinylHeaven\Shipping\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ContinentCountriesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "full_name" => $this->full_name,
            "code" => $this->code,
            "emoji" => $this->emoji
        ];
    }
}
