<?php





Route::group(['middleware' => ['web', 'theme', 'locale', 'currency'/*, 'customer', 'set.theme.sellerdashboard'*/]], function () {
    Route::namespace('\VinylHeaven\Shipping\Http\Controllers')->group(function () {

        Route::prefix('/shipping')->group(function () {
            Route::get('format/rules', 'RuleController@index')->defaults('_config', [
                'view' => 'vendor.admin.shipping.rules'
            ])->name('shipping.format.rules');
        });
    });
});


Route::group(['middleware' => ['web', 'theme', 'locale', 'currency', 'customer', 'set.theme.sellerdashboard']], function () {
    Route::namespace('\VinylHeaven\Shipping\Http\Controllers')->group(function () {
        Route::prefix('marketplace/shipping')->group(function () {
            Route::get('/policies', 'ShippingPoliciesController@index')->defaults('_config', [
                'view' => 'shipping::index'
            ])->name('shipping.policies');
            Route::get('/policies/create', 'ShippingPoliciesController@create')->defaults('_config', [
                'view' => 'shipping::create'
            ])->name('shipping.policies.create');
            Route::post('/policies', 'ShippingPoliciesController@store')->defaults('_config', [
                'view' => 'shipping::index'
            ])->name('shipping.policies.store');
            Route::get('/policies/edit/{id}', 'ShippingPoliciesController@edit')->defaults('_config', [
                'view' => 'shipping::edit'
            ])->name('shipping.policies.edit');
            Route::put('/policies/update', 'ShippingPoliciesController@update')->defaults('_config', [
                'view' => 'shipping::edit'
            ])->name('shipping.policies.update');
        });
    });
});
