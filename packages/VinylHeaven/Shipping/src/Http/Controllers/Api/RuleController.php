<?php

namespace VinylHeaven\Shipping\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use VinylHeaven\Shipping\Http\Resources\RuleResource;
use VinylHeaven\Shipping\Models\Rule;
use VinylHeaven\Shipping\Models\Format;
use VinylHeaven\Shipping\Http\Resources\FormatResource;
use VinylHeaven\Shipping\Models\Description;
use VinylHeaven\Shipping\Http\Resources\DescriptionResource;

class RuleController extends Controller
{

    // return all rules to be rendered in table
    public function all()
    {
        return RuleResource::collection(Rule::orderBy('created_at', 'DESC')->get());
    }

    public function allFormats()
    {
        return FormatResource::collection(Format::all());
    }

    public function allDescriptions()
    {
        return DescriptionResource::collection(Description::where('used', true)->get());
    }

    // store a new rule
    public function store(Request $request)
    {
        \DB::beginTransaction();
        try {
            $rule = Rule::create($request->except(['descriptions']));
            $rule->descriptions()->sync($request->descriptions); // array of description ids [1,2,3,4,5,6]

            \DB::commit();
            return new RuleResource($rule);
        } catch (\Throwable $th) {

            \DB::rollBack();
            return response()->json(['data' => $th->getMessage()], 500);
        }
    }

    // Update a rule
    public function update(Request $request, $id)
    {

        $rule = Rule::findOrFail($id);

        \DB::beginTransaction();
        try {
            $rule->update($request->except(['descriptions']));
            $rule->descriptions()->sync($request->descriptions); // array of description ids [1,2,3,4,5,6]
            \DB::commit();
            return response()->json(['data' => 'success'], 200);
        } catch (\Throwable $th) {

            \DB::rollBack();
            return response()->json(['data' => $th->getMessage()], 500);
        }
    }

    public function destroy($id)
    {
        $rule = Rule::findOrFail($id);
        \DB::beginTransaction();
        try {
            $rule->delete();
            \DB::commit();
            return response()->json(['data' => 'success'], 200);
        } catch (\Throwable $th) {
            \DB::rollBack();
            return response()->json(['data' => $th->getMessage()], 500);
        }
    }
}
