<?php

namespace VinylHeaven\Shipping\Http\Controllers\Api;

use Khsing\World\World;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use VinylHeaven\Seller\Repositories\SellerRepository;
use VinylHeaven\Shipping\Models\Range;
use VinylHeaven\Shipping\Models\Policy;
use VinylHeaven\Response\Http\Resources\ResponseResource;
use VinylHeaven\Shipping\Http\Resources\RangeResource;

class ShippingRangesController extends Controller
{

    /**
     * SellerRepository object
     *
     * @var Object
     */
    protected $sellerRepository;

    public function __construct(SellerRepository $sellerRepository)
    {
        $this->sellerRepository = $sellerRepository;
    }


    public function getByPolicyId($policy_id)
    {
        $policy = Policy::findOrFail($policy_id);
        return RangeResource::collection($policy->ranges);
    }

    public function store(Request $request)
    {
        \DB::beginTransaction();
        try {
            Range::create($request->all());
            \DB::commit();
            return (new ResponseResource('success', 'Well done.', 'Range is created!', 3000, true))->response()->setStatusCode(200);
        } catch (\Throwable $th) {
            \DB::rollBack();
            return (new ResponseResource('warning', 'Oops..', 'Something went wrong while trying to create your range.', null, true))->response()->setStatusCode(500);
        }
    }

    public function update(Request $request, $id)
    {
        \DB::beginTransaction();
        try {
            $range = Range::findOrFail($id);
            $range->update($request->all());
            \DB::commit();
            return (new ResponseResource('success', 'Well done.', 'Range is created!', 3000, true))->response()->setStatusCode(200);
        } catch (\Throwable $th) {
            \DB::rollBack();
            return (new ResponseResource('warning', 'Oops..', 'Something went wrong while trying to create your range.', null, true))->response()->setStatusCode(500);
        }
    }

    public function delete($id)
    {
        \DB::beginTransaction();
        try {
            $range = Range::findOrFail($id);
            $range->delete();
            \DB::commit();
            return (new ResponseResource('success', 'Well done.', 'Range is deleted!', 3000, true))->response()->setStatusCode(200);
        } catch (\Throwable $th) {
            \DB::rollBack();
            return (new ResponseResource('warning', 'Oops..', 'Something went wrong while trying to delete your range.', null, true))->response()->setStatusCode(500);
        }
    }
}
