<?php

namespace VinylHeaven\Shipping\Http\Controllers\Api;

use Khsing\World\World;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use VinylHeaven\Seller\Models\Seller;
use VinylHeaven\Shipping\Models\Policy;
use VinylHeaven\Response\Http\Resources\ResponseResource;
use VinylHeaven\Shipping\Http\Resources\ContinentResource;
use VinylHeaven\Shipping\Http\Resources\PolicyContinentResource;
use VinylHeaven\Shipping\Http\Resources\ContinentCountriesResource;

class WorldController extends Controller
{

    public function getContinentCountryTree()
    {
        return ContinentResource::collection(World::Continents());
    }

    public function getContinentCountryTreeBySellerId($seller_id)
    {
        $seller = Seller::findOrFail($seller_id);
        $continents = World::Continents();
        foreach ($continents as $continent) {
            $continent->selected = $seller->countries()->where('continent_id', $continent->id)->get();
        }
        return ContinentResource::collection($continents);
    }


    public function getContinentCountryTreeByPolicyId($id)
    {
        $policy = Policy::findOrFail($id);
        $continents = World::Continents();

        foreach ($continents as $continent) {
            $continent->selected = $policy->countries()->where('continent_id', $continent->id)->get();
        }

        return ContinentResource::collection($continents);

        dd($continents);
        die;

        $policyContinentResource = new PolicyContinentResource($policy);
        return $policyContinentResource->collection(World::Continents());
        // return PolicyContinentResource::collection(World::Continents());
    }


    public function linkCountriesToSeller(Request $request, $seller_id)
    {
        try {
            $seller = Seller::findOrFail($seller_id);
            $seller->countries()->sync($request->country_ids);
            return (new ResponseResource('success', 'You list of countries is updated.', 'Go ahead and create some policies.', 2500, false))->response()->setStatusCode(200);
        } catch (\Throwable $th) {
            return (new ResponseResource('warning', 'Oops...', 'Something went wrong while updating your shipping countries.', null, true))->response()->setStatusCode(422);
        }
    }
}
