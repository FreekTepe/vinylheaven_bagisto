<?php

namespace VinylHeaven\Shipping\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Khsing\World\World;
use VinylHeaven\Shipping\Models\Policy;
use VinylHeaven\Seller\Repositories\SellerRepository;


class ShippingPoliciesController extends Controller
{
    protected $_config;
    protected $sellerRepository;


    public function __construct(SellerRepository $sellerRepository)
    {
        $this->_config = request('_config');
        $this->sellerRepository = $sellerRepository;

    }

    public function index()
    {
        // dd(World::Continents()->first()->countries()->get());
        $seller = $this->sellerRepository->findOneByField('customer_id', auth()->guard('customer')->user()->id);
        return view($this->_config['view'], compact('seller'));
    }

    public function create()
    {
        return view($this->_config['view']);
    }

    public function store(Request $request)
    {
        dd('ShippingPoliciesController@store');
    }

    public function edit($id)
    {
        $policy = Policy::findOrFail($id);
        $countries = $policy->countries->pluck('id');

        return view($this->_config['view'], compact('policy', 'countries'));
    }

    public function update(Request $request, $id)
    {
        dd('ShippingPoliciesController@update');
        // $shippingPolicy = 'bla bla bla';
        // $shippingPolicy->update($request->all());
        // return view($this->_config['view'], compact('shippingPolicy'));
    }

}