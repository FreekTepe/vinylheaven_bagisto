<?php

namespace VinylHeaven\Response\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ResponseResource extends JsonResource
{

    protected $swal;


    public function __construct($type, $title, $text, $timer = null, $showConfirmButton = false, $data = null)
    {
        $this->type = $type;
        $this->title = $title;
        $this->text = $text;
        $this->timer = $timer;
        $this->showConfirmButton = $showConfirmButton;
        $this->data = $data;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'swal' => [
                'type' => $this->type,
                'title' => $this->title,
                'text' => $this->text,
                'showConfirmButton' => $this->showConfirmButton,
                'timer' => $this->timer
            ],
            'data' => $this->data
        ];
    }
}
