<?php
    namespace VinylHeaven\MediaLibrary\RepsonsiveImages;

    use Illuminate\Support\Collection;

    use Spatie\MediaLibrary\ResponsiveImages\WidthCalculator\WidthCalculator;
    use Spatie\MediaLibrary\Support\ImageFactory;

    class ProductImagesOptimizedWidthCalculator extends WidthCalculator
    {
        public function calculateWidthsFromFile(string $imagePath): Collection
        {
            $image = ImageFactory::load($imagePath);

            $width = $image->getWidth();
            $height = $image->getHeight();
            $fileSize = filesize($imagePath);

            return $this->calculateWidths($fileSize, $width, $height);
        }

        public function calculateWidths(int $fileSize, int $width, int $height): Collection
        {

        }
    }