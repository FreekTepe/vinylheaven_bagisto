<?php

namespace VinylHeaven\Search\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;

use Webkul\Marketplace\Repositories\SellerRepository;
use Webkul\Shop\Http\Controllers\SearchController as SearchControllerBaseController;
use Webkul\Attribute\Models\AttributeFamily;

use GuzzleHttp\Client;
use GuzzleHttp\Subscriber\Oauth\Oauth1;

use VinylHeaven\Product\Models\Product as Product;
use VinylHeaven\Product\DiscogsAPI\Discogs;

class SearchController extends SearchControllerBaseController
{
    public $search_query;

    public $search_query_count;

    public $limit;
    public $offset;
    public $sort;
    public $order;
    public $term;
    public $condition;
    public $sleeve_condition;
    public $artist;
    public $format;
    public $genre;
    public $price_min;
    public $price_max;

    public function discogsapitest()
    {
        $release_id = '249504';
        $client = new Client(); //GuzzleHttp\Client

        $result = $client->request('GET', env('DISCOGS_API') . "/releases/" . $release_id . "?token=" . env('DISCOGS_TOKEN'), []);
        $status = $result->getStatusCode();

        if (!$status == 200) {
            abort($status);
        }

        $results = json_decode($result->getBody()->getContents(), true);


        dd('DISCOGSAPI RESULTS: ', $results);




        // $userAgent = 'bartsdiscogsapitest';  // specify recognizable user-agent

		// $server = new Discogs([
		// 	'identifier'   => env('DISCOGS_KEY'),
		// 	'secret'       => env('DISCOGS_SECRET'),
		// 	'callback_uri' => 'http://vinylheaven.local/'
		// ], null, $userAgent);

		// // no temporary token? redirect then
		// if ( !isset($_GET['oauth_token']) ) {
		// 	$tempCredentials = $server->getTemporaryCredentials();
		// 	$_SESSION['tempCredentials'] = serialize($tempCredentials);
		// 	header('Location: '.$server->getAuthorizationUrl($tempCredentials));
		// }

		// // ok got temporary token
		// // nb: you may save it in db
		// $token = $server->getTokenCredentials(
		// 	unserialize($_SESSION['tempCredentials']),
		// 	$_GET['oauth_token'],
		// 	$_GET['oauth_verifier']
		// );

		// // build guzzle client to make requests
		// $client = new Client([
		// 	'base_url' => 'http://api.discogs.com',
		// 	'defaults' => [
		// 		'headers' => ['User-Agent' => $userAgent ],
		// 		'auth'    => 'oauth',
		// 	],
		// ]);

		// // attach oauth subscriber to grab images
		// $client->getEmitter()->attach(new Oauth1([
		// 	'consumer_key'    => env('DISCOGS_KEY'),
		// 	'consumer_secret' => env('DISCOGS_SECRET'),
		// 	'token'           => $token->getIdentifier(),
		// 	'token_secret'    => $token->getSecret(),
		// ]));

		// // make calls
		// $results = $client->get('/database/search', [ 'query' => [
		// 	'q'    => 'Justin Bieber',
		// 	'type' => 'artist',
		// ]])->json();

        // dd('discogsAPI call result: ', $results);
        // get image body
		// $rawImage = $client
		// 	->get('http://path-to-restricted-discogs-cover')
		// 	->getBody();
    }

    /**
     * Index to handle the view loaded with the search results
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $params = request()->input();
        $products = [];

        $this->limit = isset($params['limit']) ? $params['limit'] : 33;
        $this->offset = isset($params['page']) ? ($this->limit * $params['page']) : 0;
        $this->sort = isset($params['sort']) ? $params['sort'] : 'created_at';
        $this->sort = ($this->sort == 'name') ? 'name.sort' : $this->sort;
        $this->sort = ($this->sort == 'price') ? 'marketplace_products.price' : $this->sort;
        $this->order = isset($params['order']) ? $params['order'] : 'asc';
        $this->term = isset($params['term']) ? $params['term'] : '';
        $this->term = empty($this->term) ? '*' : $this->term;

        $this->applyTermQuery();

        if (isset($params['condition'])) {
            $this->condition = $this->sanitizeUrlString($params['condition']);
            $this->applyConditionFilter();
        }

        if (isset($params['sleeve_condition'])) {
            $this->sleeve_condition = $this->sanitizeUrlString($params['sleeve_condition']);
            $this->applySleeveConditionFilter();
        }

        if (request()->filled('artist')) {
            $this->artist = request()->get('artist');
            $this->applyArtistFilter();
        }

        if (request()->filled('format')) {
            $this->format = request()->get('format');
            $this->applyFormatsFilter();
        }

        if (isset($params['genre'])) {
            $this->genre = $this->sanitizeUrlString($params['genre']);
            $this->applyGenreFilter();
        }

        if (isset($params['price_min']) || isset($params['price_max'])) {
            if (isset($params['price_min'])) {
                $this->price_min = $params['price_min'];
            }
            if (isset($params['price_max'])) {
                $this->price_max = $params['price_max'];
            }
            $this->applyPriceRange();
        }

        $hashQuery = md5(json_encode($this->search_query));

        if (Cache::store('redis')->has('search:query:' . $hashQuery)) {
            $results = Cache::store('redis')->get("search:query:" . $hashQuery);
            $results_count = $results['hits']['total']['value'];
        } else {

            $results = Product::searchRaw($this->search_query);
            $results_count = $results['hits']['total']['value'];

            Cache::store('redis')->put("search:query:" . $hashQuery, $results, now()->addMinutes(5));
        }

        if (Cache::store('redis')->has('search:products_temp' . $hashQuery)) {
            $products = Cache::store('redis')->get('search:products_temp' .  $hashQuery);
        } else {
            foreach($results['hits']['hits'] as $item_index => $item) {
                $product_temp = Product::with(['variants', 'variants.inventories'])->findOrFail($item['_id']);
                $products[] = $product_temp;
            }
            Cache::store('redis')->put('search:products_temp' .  $hashQuery, $products, now()->addMinutes(5));
        }

        return view($this->_config['view'])
                ->with('results', count($products) ? $products : null)
                ->with('products_count', $results_count)
                ->with('route_name', Route::currentRouteName());
    }

    public function applyTermQuery()
    {
        $this->search_query = [
            'query' => [
                'bool' => [
                    'must' => $this->getSearchQueryMust(),
                ]
            ],
            'sort' => [
                $this->sort => [
                    'order' => $this->order
                ]
            ],
            'from' => $this->offset,
            'size' => $this->limit
        ];
    }

    public function getSearchQueryMust()
    {
        if ($this->term != '*') {
            return [
                [
                    'nested' => [
                        'path' => 'marketplace_products',
                        'query' => [
                            'bool' => [
                                'must' => [
                                    [
                                        'exists' => [
                                            'field' => 'marketplace_products.condition'
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    'match_phrase_prefix' => [
                        'name.autocomplete' => $this->term
                    ]
                ],
            ];
        } else {
            return [
                [
                    'nested' => [
                        'path' => 'marketplace_products',
                        'query' => [
                            'bool' => [
                                'must' => [
                                    [
                                        'exists' => [
                                            'field' => 'marketplace_products.condition'
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    'simple_query_string' => [
                        'query' => $this->term,
                        'fields' => ['name'],
                        'default_operator' => 'and'
                    ]
                ],
            ];
        }
        return false;
    }

    public function sanitizeUrlString($url_string)
    {
        return explode(',', $url_string);
    }

    public function applyConditionFilter()
    {
        $condition_should_queries = [];

        foreach ($this->condition as $condition) {
            $condition_should_queries[] = ['match' => [
                'marketplace_products.condition' => $this->getElasticSearchConditionGrading($condition)
            ]];
        }


        $this->search_query['query']['bool']['must'][] = [
            'nested' => [
                'path' => 'marketplace_products',
                'query' => [
                    'bool' => [
                        'should' => $condition_should_queries
                    ]
                ]
            ]
        ];
        //dd($raw_search_query, json_encode($raw_search_query), $raw_pipo_search_query, json_encode($raw_pipo_search_query));
    }

    public function applySleeveConditionFilter()
    {
        $sleeve_condition_should_queries = [];

        foreach ($this->sleeve_condition as $condition) {
            $condition_should_queries[] = ['match' => [
                'marketplace_products.sleeve_condition' => $this->getElasticSearchConditionGrading($condition)
            ]];
        }


        $this->search_query['query']['bool']['must'][] = [
            'nested' => [
                'path' => 'marketplace_products',
                'query' => [
                    'bool' => [
                        'should' => $condition_should_queries
                    ]
                ]
            ]
        ];
        //dd($raw_search_query, json_encode($raw_search_query), $raw_pipo_search_query, json_encode($raw_pipo_search_query));
    }

    public function applyArtistFilter()
    {
        $this->search_query['query']['bool']['must'][] = [
            'nested' => [
                'path' => 'artists',
                'query' => [
                    'bool' => [
                        'must' => [
                            [
                                'match' => [
                                    'artists.artist_id' => request()->get('artist' )
                                ]
                            ]
                        ]
                    ]
                ],
                'inner_hits' => [
                    'highlight' => [
                        'fields' => [
                            'artists.artist_id' => [
                                'pre_tags' => [
                                    '<em>'
                                ],
                                'post_tags' => [
                                    '</em>'
                                ]
                            ]
                        ],
                    ],
                ]
            ]
        ];
    }

    public function applyFormatsFilter()
    {
        /**
         * hier de formats filter we hebben vier soorten filters
         * vinyl, cd, cassette, overige
         */
        if ($this->format == 'other') {
            $this->applyOtherFormatFilter();
        } else {
            $this->applyFormatFilter();
        }
    }

    public function applyFormatFilter()
    {
        $this->search_query['query']['bool']['must'][] = [
            'nested' => [
                'path' => 'formats',
                'query' => [
                    'bool' => [
                        'should' => [
                            [
                                'match' => [
                                    'formats.name' => $this->format,
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        if ($this->formatHasAliases($this->format)) {
            $format = $this->getFormat($this->format);
            foreach($format->aliases as $alias) {
                $this->search_query['query']['bool']['must'][2]['nested']['query']['bool']['should'][] = [
                    'nested' => [
                        'path' => 'formats.descriptions',
                        'query' => [
                            'bool' => [
                                'must' => [
                                    [
                                        'match' => [
                                            'formats.descriptions.name' => $alias->slug
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                ];
            }
        }
    }

    public function applyOtherFormatFilter()
    {
        $this->search_query['query']['bool']['must'][] = [
            'nested' => [
                'path' => 'formats',
                'query' => [
                    'bool' => [
                        'must_not' => []
                    ]
                ]
            ]
        ];

        foreach($this->getAllFormats() as $format) {
            if ($format->slug != 'other') {
                $this->search_query['query']['bool']['must'][2]['nested']['query']['bool']['must_not'][] = [
                    'match' => [
                        'formats.name' => $format->slug,
                    ]
                ];

                if ($this->formatHasAliases($format->slug)) {
                    $format = $this->getFormat($format->slug);
                    foreach($format->aliases as $alias) {
                        $this->search_query['query']['bool']['must'][2]['nested']['query']['bool']['must_not'][] = [
                            'nested' => [
                                'path' => 'formats.descriptions',
                                'query' => [
                                    'bool' => [
                                        'must_not' => [
                                            [
                                                'match' => [
                                                    'formats.descriptions.name' => $alias->slug
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                        ];
                    }
                }
            }
        }
    }

    public function formatHasAliases($format)
    {
        return !empty($this->getAllFormatsArray()[$format]['aliases']);
    }

    public function getFormat($format)
    {
        return !is_bool($this->getFormatArray($format)) ? json_decode(json_encode($this->getFormatArray($format))) : false;
    }

    public function getFormatArray($format)
    {
        return isset($this->getAllFormatsArray()[$format]) ? $this->getAllFormatsArray()[$format] : false;
    }

    public function getAllFormats()
    {
        return json_decode(json_encode($this->getAllFormatsArray()));
    }

    public function getAllFormatsArray()
    {
        return [
            'vinyl' => [
                'slug' => 'vinyl',
                'name' => 'Vinyl',
                'label_class' => '',
                'aliases' => [
                    'lp' => [
                        'slug' => 'lp',
                        'name' => 'LP',
                    ],
                    '12"' => [
                        'slug' => '12"',
                        'name' => '12"',
                    ],
                    '7"' => [
                        'slug' => '7"',
                        'name' => '7"',
                    ],
                ],
                'childs' => [],
            ],
            'cd' => [
                'slug' => 'cd',
                'name' => 'Cd',
                'label_class' => '',
                'aliases' => [],
                'childs' => [],
            ],
            'cassette' => [
                'slug' => 'cassette',
                'name' => 'Cassette',
                'label_class' => '',
                'aliases' => [],
                'childs' => [],
            ],
            'other' => [
                'slug' => 'other',
                'name' => 'Other',
                'label_class' => '',
                'aliases' => [],
                'childs' => [],
            ],
        ];
    }

    public function applyGenreFilter()
    {
        $genre_should_queries = [
            'bool' => [
                'should' => []
            ]
        ];

        foreach ($this->genre as $genre) {
            $genre_should_queries['bool']['should'][] = ['match' => [
                'root_category_id' => $genre
            ]];
        }

        $this->search_query['query']['bool']['must'][] = $genre_should_queries;
        //dd($this->genre, $this->search_query, json_encode($this->search_query));
    }

    public function applyPriceRange()
    {
        if (!empty($this->price_min) && empty($this->price_max)) {
            $this->search_query['query']['bool']['must'][] = [
                'nested' => [
                    'path' => 'marketplace_products',
                    'query' => [
                        'bool' => [
                            'must' => [
                                'range' => [
                                    'marketplace_products.price' => [
                                        'gte' => $this->price_min
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];
        }

        if (empty($this->price_min) && !empty($this->price_max)) {
            $this->search_query['query']['bool']['must'][] = [
                'nested' => [
                    'path' => 'marketplace_products',
                    'query' => [
                        'bool' => [
                            'must' => [
                                'range' => [
                                    'marketplace_products.price' => [
                                        'lt' => $this->price_max
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];
        }

        if (!empty($this->price_min) && !empty($this->price_max)) {
            $this->search_query['query']['bool']['must'][] = [
                'nested' => [
                    'path' => 'marketplace_products',
                    'query' => [
                        'bool' => [
                            'must' => [
                                'range' => [
                                    'marketplace_products.price' => [
                                        'gte' => $this->price_min,
                                        'lt' => $this->price_max
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];
        }
        //dd($raw_search_query, json_encode($raw_search_query));
    }

    public function getElasticSearchConditionGrading($condition)
    {
        return stristr($this->getConditionGrading($condition), '+') ? str_replace('+', 'P', $this->getConditionGrading($condition)) : $this->getConditionGrading($condition);
    }

    public function getConditionGrading($condition)
    {
        if (stristr($condition, 'M') && !stristr($condition, 'NM') && !stristr($condition, 'M-')) {
            return 'M';
        } elseif (stristr($condition, 'NM') || stristr($condition, 'M-')) {
            return 'NM';
        } elseif (stristr($condition, 'VG+')) {
            return 'VG+';
        } elseif (stristr($condition, 'VG')) {
            return 'VG';
        } elseif (stristr($condition, 'G+')) {
            return 'G+';
        } elseif (stristr($condition, 'G')) {
            return 'G';
        } elseif (stristr($condition, 'P')) {
            return 'P';
        }

        return '?';
    }

    public function autocomplete()
    {
        $data = request()->input();
        $autocomplete_releases = [];
        $autocomplete_artists = [];

        $autocomplete_search_query = [
            'query' => [
                'bool' => [
                    'must' => [
                        [
                            'match_phrase_prefix' => [
                                'name.autocomplete' => $data['autocomplete_search_term']
                            ]
                        ],
                        [
                            'nested' => [
                                'path' => 'marketplace_products',
                                'query' => [
                                    'bool' => [
                                        'must' => [
                                            [
                                                'exists' => [
                                                    'field' => 'marketplace_products.condition'
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ],
                    ]
                ]
            ],
            'sort' => [
                'name.sort' => [
                    'order' => 'asc'
                ],
            ],
            'from' => 0,
            'size' => 3
        ];

        $autocomplete_artists_search_query = [
            'query' => [
                'bool' => [
                    'must' => [
                        [
                            'nested' => [
                                'path' => 'artists',
                                'query' => [
                                    'match_phrase_prefix' => [
                                        'artists.name.autocomplete' => $data['autocomplete_search_term']
                                    ]
                                ],
                                'inner_hits' => [
                                    'highlight' => [
                                        'fields' => [
                                            'artists.name' => [
                                                'pre_tags' => [
                                                    '<em>'
                                                ],
                                                'post_tags' => [
                                                    '</em>'
                                                ]
                                            ]
                                        ],
                                    ],
                                ]
                            ]
                        ],
                        [
                            'nested' => [
                                'path' => 'marketplace_products',
                                'query' => [
                                    'bool' => [
                                        'must' => [
                                            [
                                                'exists' => [
                                                    'field' => 'marketplace_products.condition'
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ],
                    ]
                ]
            ],
            'sort' => [
                'artists.name.sort' => [
                    'order' => 'asc'
                ],
            ],
            'from' => 0,
            'size' => 3
        ];

        $releases = Product::searchRaw($autocomplete_search_query);
        $artists = Product::searchRaw($autocomplete_artists_search_query);

        foreach($releases['hits']['hits'] as $item_index => $item) {
            $product = Product::with(['variants', 'variants.inventories'])->findOrFail($item['_id']);
            $autocomplete_releases[] = [
                'name' => $product->name,
                'url' => $product->url_key,
                'primary_image' => $product->getPrimaryImageThumb()
            ];
        }
        foreach($artists['hits']['hits'] as $item_index => $item) {
            $autocomplete_artists[] = [
                'product_id' => $item['_source']['id'],
                'name' => $item['inner_hits']['artists']['hits']['hits'][0]['_source']['name'],
                'artist_id' => $item['inner_hits']['artists']['hits']['hits'][0]['_source']['artist_id'],
                'url' => '/search?term=&artist=' . $item['inner_hits']['artists']['hits']['hits'][0]['_source']['artist_id'],
                'primary_image' => asset('themes/vinylexpress/assets/images/Default-Product-Image.png')
            ];
        }


        $return_response = [
            'status' => 'success',
            'message' => 'We have successfully made an ajax call',
            'result' => [
                'releases' => $autocomplete_releases,
                'artists' => $autocomplete_artists
            ]
        ];

        return response()->json($return_response);
    }
}