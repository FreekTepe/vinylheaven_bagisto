<?php

namespace VinylHeaven\Extender\Classes;

use Illuminate\Support\Facades\Route;

class Extender
{
    public function divertRoutes($data)
    {
        foreach ($data as $routeName => $controllerMethod) {
            $route = Route::getRoutes()->getByName($routeName);
            if (empty($route)) {
                dd('route is missing no action: ', $routeName, $controllerMethod);
            }
            $routeAction = array_merge($route->getAction(), [
                'uses'       => $controllerMethod,
                'controller' => $controllerMethod,
            ]);
            $route->setAction($routeAction);
            $route->controller = false;
        }
    }

    public function applyMiddleware($data)
    {
        foreach ($data as $routeName => $middleware) {
            $route = Route::getRoutes()->getByName($routeName);
            if (empty($route)) {
                dd('middleware is missing no action: ', $routeName, $middleware);
            }
            $route->middleware($middleware);
        }
    }

    public function extendCoreModels($data)
    {
        foreach ($data as $core => $extender) {
            app()->concord->registerModel(
                $core,
                $extender
            );
        }
    }
}
