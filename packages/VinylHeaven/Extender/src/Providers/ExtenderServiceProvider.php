<?php
    namespace VinylHeaven\Extender\Providers;

    use Illuminate\Support\ServiceProvider;
    use VinylHeaven\Extender\Classes\Extender;

    class ExtenderServiceProvider extends ServiceProvider
    {
        public function register()
        {
            $this->app->bind('extender', function () {
                return new Extender();
            });
        }

        public function boot()
        {
            // Extend Webkul core models
            // Core Model => Overriding Model 
            \Extender::extendCoreModels([
                \Webkul\Product\Models\Product::class => \VinylHeaven\Product\Models\Product::class,
                \Webkul\Product\Models\ProductAttributeValue::class => \VinylHeaven\Product\Models\ProductAttributeValue::class,
                \Webkul\Marketplace\Models\Product::class => \VinylHeaven\Marketplace\Models\MarketplaceProduct::class,
                \Webkul\Customer\Models\Customer::class => \VinylHeaven\Customer\Models\Customer::class,
                \Webkul\Marketplace\Models\Seller::class => \VinylHeaven\Seller\Models\Seller::class,
                \Webkul\Checkout\Models\Cart::class => \VinylHeaven\Checkout\Models\Cart::class,
                \Webkul\Sales\Models\Order::class => \VinylHeaven\Order\Models\Order::class,
                \Webkul\Category\Models\Category::class => \VinylHeaven\Category\Models\Category::class,
                \Webkul\Checkout\Models\CartItem::class => \VinylHeaven\Checkout\Models\CartItem::class,
            ]);

            // divert Webkul routes to custom controller methods
            /**
             * webkul route name => controller@ method()
             */
            $this->app->booted(function () {
                \Extender::divertRoutes([
                    'shop.home.index'                       => 'VinylHeaven\Shop\Http\Controllers\HomeController@index',
                    'shop.productOrCategory.index'          => 'VinylHeaven\Catalog\Http\Controllers\CatalogController@productOrCategory',
                    'shop.search.index'                     => 'VinylHeaven\Search\Http\Controllers\SearchController@index',
                    'shop.checkout.onepage.index'           => 'VinylHeaven\Checkout\Http\Controllers\OnepageController@index',
                    'shop.checkout.save-order'              => 'VinylHeaven\Checkout\Http\Controllers\OnepageController@saveOrder',
                    'cart.add'                              => 'VinylHeaven\Checkout\Http\Controllers\CartController@add',
                    'customer.wishlist.index'               => 'VinylHeaven\Customer\Http\Controllers\WishlistController@index',
                    'marketplace.account.products.search'   => 'VinylHeaven\Product\Http\Controllers\AssignProductController@index',
                    'marketplace.account.seller.update'     => 'VinylHeaven\Seller\Http\Controllers\SellerController@update',
                    'marketplace.account.shipments.store'   => 'VinylHeaven\Shipment\Http\Controllers\ShipmentController@store',
                    'marketplace.account.dashboard.index'   => 'VinylHeaven\Theme\Http\Controllers\DashboardController@index',
                    'marketplace.sellers.product.index'     => 'VinylHeaven\Catalog\Http\Controllers\CatalogController@sellerProduct',
                    'marketplace.seller_central.index'      => 'VinylHeaven\Marketplace\Http\Controllers\MarketplaceRedirectController@redirectMarketplaceUrl',
                    'marketplace.seller.show'               => 'VinylHeaven\Seller\Http\Controllers\SellerController@show'
                ]);

                // here we can apply middleware to a list bagisto routes
                \Extender::applyMiddleware([
                    'marketplace.account.products.edit-assign' => 'set.theme.sellerdashboard',
                    'shop.marketplace.tablerate.rates.index' => 'set.theme.sellerdashboard',
                    'shop.marketplace.tablerate.super_set_rates.index' => 'set.theme.sellerdashboard',
                    'marketplace.account.products.index' => 'set.theme.sellerdashboard',
                    'customer.wishlist.index' => 'set.theme.sellerdashboard',
                    'marketplace.account.seller.edit' => 'set.theme.sellerdashboard',
                    'marketplace.account.orders.index' => 'set.theme.sellerdashboard',
                    'marketplace.account.transactions.index' => 'set.theme.sellerdashboard',
                    'marketplace.account.reviews.index' => 'set.theme.sellerdashboard',
                    'customer.profile.index' => 'set.theme.sellerdashboard',
                    'customer.profile.edit' => 'set.theme.sellerdashboard',
                    'customer.address.index' => 'set.theme.sellerdashboard',
                    'customer.address.create' => 'set.theme.sellerdashboard',
                    'customer.address.edit' => 'set.theme.sellerdashboard',
                    'customer.orders.index' => 'set.theme.sellerdashboard',
                    'marketplace.account.orders.view' => 'set.theme.sellerdashboard',
                    'customer.orders.view' => 'set.theme.sellerdashboard'
                ]);
            });

            // Here we extend the $attributeTypeFields attribute that gets referenced statically in Webkul\Product\Repositories\ProductAttributeValueRepository
            \Webkul\Product\Models\ProductAttributeValue::$attributeTypeFields = [
                'text'        => 'text_value',
                'textarea'    => 'text_value',
                'price'       => 'float_value',
                'boolean'     => 'boolean_value',
                'select'      => 'integer_value',
                'multiselect' => 'text_value',
                'datetime'    => 'datetime_value',
                'date'        => 'date_value',
                'file'        => 'text_value',
                'image'       => 'text_value',
                'checkbox'    => 'text_value',
                'tracklist'   => 'json_value',
                'artists'     => 'boolean_value',
                'labels'      => 'boolean_value',
                'companies'   => 'json_value',
                'identifiers' => 'json_value',
                'formats'     => 'json_value'
            ];
        }
    }
