<?php
    namespace VinylHeaven\Category\Repositories;

    use Webkul\Category\Repositories\CategoryRepository as CategoryBaseRepository;
    use Webkul\Category\Models\CategoryTranslation;


    class CategoryRepository extends CategoryBaseRepository
    {
        public function getIdByName($name)
        {
            // this needs to be the function for getting the correct category and not the style category
            // if ($category_translation = CategoryTranslation::where('name', $name)->whereNull('parent_id')->first()) {
            //     return $category_translation->category_id;
            // }

            if ($category_translation = CategoryTranslation::where('name', $name)->first()) {
                return $category_translation->category_id;
            }
            return null;
        }

        public function getIdsByNames($names)
        {
            $ids_array = array();
            foreach ($names as $name) {
                if ($id = $this->getIdByName($name)) {
                    array_push($ids_array, $id);
                }
            }
            return $ids_array;
        }
    }