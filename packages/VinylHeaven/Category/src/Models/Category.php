<?php
    namespace VinylHeaven\Category\Models;

    use Illuminate\Support\Facades\Cache;
    use Illuminate\Support\Facades\Storage;
    use Illuminate\Database\Eloquent\Builder;

    use Kalnoy\Nestedset\NodeTrait;
    use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

    use Webkul\Category\Models\Category as BaseCategory;
    use Webkul\Core\Eloquent\TranslatableModel;
    use Webkul\Category\Contracts\Category as CategoryContract;
    use Webkul\Attribute\Models\AttributeProxy;
    use Webkul\Category\Repositories\CategoryRepository;

    use VinylHeaven\Product\Models\Product;

    class Category extends BaseCategory
    {
        /**
         * The products that belong to the category.
         */
        public function products()
        {
            return $this->belongsToMany(Product::class, 'product_categories');
        }

        public function getMarketplaceProductsAttribute()
        {
            return $this->products()->has('marketplace_product')->get();
        }
    }