<?php
    namespace VinylHeaven\Customer\Models;

    use Webkul\Customer\Models\Customer as CustomerBaseModel;

    class Customer extends CustomerBaseModel
    {
        public function seller()
        {
            return $this->hasOne('VinylHeaven\Seller\Models\Seller');
        }

        public function isSeller()
        {
            return ($this->seller()->count() == 0) ? false : true;
        }
    }