<?php
    namespace VinylHeaven\Shop\Http\Controllers;

    use Illuminate\Support\Facades\Cache;
    use Illuminate\Support\Facades\Route;

    use Webkul\Shop\Http\Controllers\HomeController as HomeBaseController;

    class HomeController extends HomeBaseController
    {
        /**
         * loads the home page for the storefront
         *
         * @return \Illuminate\View\View
         */
        public function index()
        {
            // get the current channel and local
            $currentChannel = core()->getCurrentChannel();
            $currentLocale = core()->getCurrentLocale();

            //get all of the slider data
            $sliderData = $this->sliderRepository
                ->where('channel_id', $currentChannel->id)
                ->where('locale', $currentLocale->code)
                ->get()
                ->toArray();

            if (Cache::store('redis')->has('home:new_products')) {
                $new_products = Cache::store('redis')->get('home:new_products');
            } else {
                // get all of the 12 new products that are available on our platform
                $new_products = app('VinylHeaven\Marketplace\Models\MarketplaceProduct')
                    ->orderBy('created_at', 'desc')
                    ->where('qty', '>=', 1)
                    ->offset(0)
                    ->limit(50)
                    ->get();

                Cache::store('redis')->put('home:new_products', $new_products, now()->addMinutes(1));
            }

            // check if there are enough new products to show for the default amount of 12 random choosen new products
            // otherwise the random method will throw an exception for requesting to many items for there are less items available
            $random_amount = 12;
            if ($new_products->count() < 12) {
                $random_amount = $new_products->count();
            }

            // get the random choosen new products
            $new_products = $new_products->random($random_amount);

            // get the totals of our products
            $total_products = app('VinylHeaven\Product\Repositories\ProductRepository')->getTotalProducts();

            // return the rendered view
            return view($this->_config['view'])
                ->with('sliderData', compact('sliderData'))
                ->with('new_marketplace_products', $new_products)
                ->with('total_products', $total_products)
                ->with('route_name', Route::currentRouteName());
        }
    }