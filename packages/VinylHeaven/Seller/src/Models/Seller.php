<?php

namespace VinylHeaven\Seller\Models;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Khsing\World\Models\Country;

use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\File;

use Webkul\Marketplace\Models\Seller as SellerBaseModel;

use VinylHeaven\Shipping\Models\Policy;
use VinylHeaven\Marketplace\Models\MarketplaceProduct;
use VinylHeaven\ReleaseImport\Models\FailedCsvRowImports;
use VinylHeaven\Payment\Facades\VinylHeavenPaymentFacade as Payment;

class Seller extends SellerBaseModel implements HasMedia
{
    use InteractsWithMedia;

    public $max_rating = 5;

    // Register file collections to be associated to seller
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('dicogscsv');
        $this->addMediaCollection('customcsv');
        $this->addMediaCollection('logo');
        $this->addMediaCollection('banner');
    }

    public function failed_csv_import_rows()
    {
        return $this->hasMany(FailedCsvRowImports::class);
    }

    public function marketplace_products()
    {
        return $this->hasMany(MarketplaceProduct::class, 'marketplace_seller_id');
    }

    public function customer()
    {
        return $this->belongsTo('VinylHeaven\Customer\Models\Customer');
    }

    public function policies()
    {
        return $this->hasMany(Policy::class);
    }

    
    public function countries()
    {
        return $this->belongsToMany(Country::class);
    }

    // list of all countries that the seller has policies for.
    // this makes it easier to make policy matches (when detemrining shipping price)
    public function getPolicyCountriesAttribute()
    {
        if(count($this->policies) > 0){
            $coll = collect();
            dd($coll->merge($this->policies));
        }
        return null;
    }


    /**
     * Get logo image url.
     */
    public function logo_url()
    {
        if (!$this->getFirstMedia("logo")) {
            return;
        }
        return $this->getFirstMedia("logo")->getFullUrl();
    }

    public function getLogoUrl()
    {
        if (!empty($this->logo_url)) {
            return $this->logo_url();
        }

        return asset('themes/vinylexpress/assets/images/Default-Product-Image.png');
    }

    /**
     * Get banner image url.
     */
    public function banner_url()
    {
        if (!$this->getFirstMedia("banner")) {
            return;
        }
        return $this->getFirstMedia("banner")->getFullUrl();
    }

    public function getBannerUrl()
    {
        if (!empty($this->banner_url)) {
            return $this->banner_url();
        }

        return asset('themes/vinylheaven-theme/assets/slider/3.jpg');
    }

    /**
     *  Get the Here Maps Image Api Url for the seller location 
     */
    public function getHereImageMapUrl()
    {
        $here_base_url = 'https://image.maps.ls.hereapi.com/mia/1.6/mapview?apiKey=';
        $here_url = $here_base_url . getenv('HERE_API_KEY') . '&co=' . core()->country_name($this->country) . '&ci=' . $this->city . '&zi=' . $this->postcode . '&z=16&h=320&w=1600&f=1';

        return urlencode($here_url);
    }

    // Return array with available payment methods
    public function getPaymentOptionsAttribute()
    {
        $paymentOptions = array_filter([
            "paypal" => ($this->paypal_allowed && $this->paypal_account) ? ["account" => $this->paypal_account] : null,
            "bank" => ($this->bank_allowed && $this->bank_account_holder_name && $this->bank_iban && $this->bank_bic_swift) ? ["account" => $this->bank_account_holder_name, "iban" => $this->bank_iban, "bic_swift" => $this->bank_bic_swift] : null,
            "cash" => ($this->cash_allowed) ? ["is_allowed" => true] : null,
        ]);

        if(count($paymentOptions) > 0){
            return $paymentOptions;
        }
        return null;
    }

    public function isPickupAllowed()
    {
        return $this->pickup_allowed ? true : false;
    }

    public function hasPaymentMethods()
    {
        return is_array($this->paymentOptions);
    }

    public function getPaymentMethod($method_slug)
    {
        return Payment::getMethod($method_slug);
    }

    public function getPaymentMethods()
    {
        $payment_methods = [];
        foreach ($this->paymentOptions as $method_slug => $method_options)
        {
            $payment_methods[] = array_merge(Payment::getMethod($method_slug), $method_options);
        }

        return (count($payment_methods) > 0) ? json_decode(json_encode($payment_methods)) : false;
    }

    public function getPaymentMethodOptions($method)
    {
        return isset($this->paymentOptions[$method]) ? $this->paymentOptions[$method] : false;
    }

    public function getShippingMethods()
    {
        $shipping_methods = [];

        foreach (Config::get('carriers') as $shipping_method) {
            if (!empty($shipping_method['for_vinyl_heaven'])) {
                if ($shipping_method['code'] == 'pickup' && !$this->isPickupAllowed()) {
                    continue;
                }

                $object = new $shipping_method['class'];

                if (! $object->isAvailable()) {
                    continue;
                }

                $shipping_methods[] = $object;
            }
        }

        return $shipping_methods;
    }

    public function getTotalOrderItemsAttribute()
    {
        $count = 0;
        foreach ($this->orders as $order) {
            $count += $order->items()->count();
        }
        return $count;
    }


    public function getAvgItemsPerOrderAttribute()
    {
        $items = array();
        foreach ($this->orders as $order) {
            array_push($items, $order->items()->count());
        }
        $average = array_sum($items) / count($items);
        return $average;
    }

    public function getOrdersGroupedPerMonthAttribute()
    {
        $grouped_orders = $this->orders->groupBy(function($d) {
            return Carbon::parse($d->created_at)->format('m');
        });

        $order_count_array = [
            "01" => 0,
            "02" => 0,
            "03" => 0,
            "04" => 0,
            "05" => 0,
            "06" => 0,
            "07" => 0,
            "08" => 0,
            "09" => 0,
            "10" => 0,
            "11" => 0,
            "12" => 0
        ];
        foreach ($grouped_orders as $month => $orders) {
            $order_count_array[$month] = $orders->count();
        }

        return $order_count_array;
    }

    public function getAverageRating()
    {
        $reviewRepository = app('Webkul\Marketplace\Repositories\ReviewRepository');
        return $reviewRepository->getAverageRating($this);
    }

    public function getTotalReviews()
    {
        $reviewRepository = app('Webkul\Marketplace\Repositories\ReviewRepository');
        return $reviewRepository->getTotalReviews($this);
    }
}
