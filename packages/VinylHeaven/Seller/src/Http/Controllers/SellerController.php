<?php

namespace VinylHeaven\Seller\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;

use VinylHeaven\Product\Models\Product as Product;
use Webkul\Marketplace\Http\Controllers\Shop\Account\SellerController as SellerBaseController;
use Webkul\Marketplace\Http\Controllers\Shop\Controller;
use Webkul\Marketplace\Repositories\SellerRepository;
use Webkul\Marketplace\Http\Requests\SellerForm;

use VinylHeaven\Seller\Repositories\SellerRepository as CustomSellerRepository;

/**
 * Marketplace seller page controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class SellerController extends SellerBaseController
{

    /**
     * SellerRepository object
     *
     * @var array
     */
    protected $custom_seller_repository;

    /**
     * All of the sort, order and limit attributes
     */
    public $limit;
    public $offset;
    public $sort;
    public $order;

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Marketplace\Repositories\SellerRepository $seller
     * @return void
     */
    public function __construct(SellerRepository $seller_repository, CustomSellerRepository $custom_seller_repository)
    {
        $this->custom_seller_repository = $custom_seller_repository;
        parent::__construct($seller_repository);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Webkul\Marketplace\Http\Requests\SellerForm $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $isSeller = $this->custom_seller_repository->isSeller(auth()->guard('customer')->user()->id);
        $data = request()->all();

        if(!isset($data['paypal_allowed'])){ $data['paypal_allowed'] = 0; }
        if(!isset($data['bank_allowed'])){ $data['bank_allowed'] = 0; }
        if(!isset($data['cash_allowed'])){ $data['cash_allowed'] = 0; }
        if(!isset($data['pickup_allowed'])){ $data['pickup_allowed'] = 0; }
        if(!isset($data['shop_enabled'])){ $data['shop_enabled'] = 0; }

        $data['url'] = str_slug($data['shop_title'], "-");

        if (!$isSeller) {
            return redirect()->route('marketplace.account.seller.create');
        }

        // do a file check
        try {
            $logo = $this->custom_seller_repository->checkFormFile($data, "logo");
            $banner = $this->custom_seller_repository->checkFormFile($data, "banner");
        } catch (\Throwable $th) {
            session()->flash('error', $th->getMessage());
            return redirect()->route($this->_config['redirect']);
        }

        // Update profile details
        \DB::beginTransaction();
        try {

            // try seller model update
            $this->custom_seller_repository->update($data, $id);

            // try file updates
            if ($logo) {
                $this->custom_seller_repository->uploadImage($logo, "logo", $id);
            }

            if ($banner) {
                $this->custom_seller_repository->uploadImage($banner, "banner", $id);
            }

            \DB::commit();

            session()->flash('success', 'Your profile was updated successfully.');
            return redirect()->route($this->_config['redirect']);
        } catch (\Throwable $th) {

            \DB::rollBack();
            session()->flash('error', $th->getMessage());
            return redirect()->route($this->_config['redirect']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $url
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|Response|\Illuminate\View\View
     */
    public function show($url)
    {
        // set all of the sort and order params
        $this->limit = request()->has('limit') ? request()->get('limit') : 9;
        $this->offset = request()->has('page') ? ($this->limit * request()->get('page')) : 0;
        $this->sort = request()->has('sort') ? request()->get('sort') : 'created_at';
        $this->sort = ($this->sort == 'name') ? 'product_flat.name' : 'marketplace_products.' . $this->sort;
        $this->order = request()->has('order') ? request()->get('order') : 'asc';

        // set the config for the toolbar
        $toolbar_config = [
            'has_result_counter' => false,
            'has_seller_info' => true
        ];

        // find the seller by url
        $seller = $this->seller->findByUrlOrFail($url);

        $hash = md5($this->limit . $this->offset . $this->sort . $this->order);

        if (Cache::has($hash)) {
            $seller_products = Cache::get($hash);
        } else {
            // get the seller product by sort and order params
            $seller_products = $seller->marketplace_products()
                ->offset($this->offset)
                ->limit($this->limit)
                ->orderBy($this->sort, $this->order)
                ->get();

            Cache::put($hash, $seller_products, now()->addMinutes(10));
        }

        // render the view and return it
        return view($this->_config['view'])
            ->with('seller', $seller)
            ->with('seller_products', $seller_products)
            ->with('route_name', Route::currentRouteName())
            ->with('toolbar_config', $toolbar_config);
    }
}


// $is_search = false;
//     $is_seller = true;
//     $seller_products_where = [
//         'marketplace_seller_id' => $seller->id
//     ];
//     $seller_products_skip = 0;
//     $seller_products_take = 33;
//     $seller_products = $productRepository->where($seller_products_where)->get();
//     $products = $seller_products->skip($seller_products_skip)->take($seller_products_take);

//     $toolbar_config = [
//         'has_result_counter' => false,
//         'has_seller_info' => true
//     ];
