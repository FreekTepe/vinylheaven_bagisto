<?php
    namespace VinylHeaven\Tools\Helpers;

    class GridListView
    {
        /**
         * Returns available sort orders
         *
         * @return array
         */
        public function getAvailableOrders()
        {
            return [
                'name-asc'        => 'title a-z',
                'name-desc'       => 'title z-a',
                'created_at-desc' => 'newest',
                'created_at-asc'  => 'oldest',
                'price-asc'       => 'price low-high',
                'price-desc'      => 'price high-low',
            ];
        }

        /**
         * Checks if sort order is active
         *
         * @param  string $key
         * @return bool
         */
        public function isOrderCurrent($key)
        {
            $params = request()->input();

            if (isset($params['sort']) && $key == $params['sort'] . '-' . $params['order']) {
                return true;
            } elseif (! isset($params['sort']) && $key == 'name-asc') {
                return true;
            }

            return false;
        }

        /**
         * Returns available limits
         *
         * @return array
         */
        public function getAvailableLimits()
        {
            if (core()->getConfigData('catalog.products.storefront.products_per_page')) {
                $pages = explode(',', core()->getConfigData('catalog.products.storefront.products_per_page'));

                return $pages;
            }

            return [24, 48, 72, 96];
        }

        public function getDefaultLimit()
        {
            $available_limits = $this->getAvailableLimits();

            return isset($available_limits[0]) ? $available_limits[0] : 12;
        }

        public function isGridView()
        {
            return $this->isGridModeActive();
        }

        public function isListView()
        {
            return $this->isListModeActive();
        }

        public function isGridModeActive()
        {
            return ($this->isModeActive('grid') || (!$this->isModeActive('grid') && !$this->isModeActive('list'))) ? true : false;
        }

        public function isListModeActive()
        {
            return $this->isModeActive('list');
        }

        public function isModeActive($mode)
        {
            if ($mode == 'list') {
                if (request()->has('view') && request()->get('view') == 'list') {
                    return true;
                } else {
                    return false;
                }
            }

            if ($mode == 'grid') {
                if (!request()->has('view')) {
                    return true;
                } elseif (request()->get('view') == 'grid') {
                    return true;
                } else {
                    return false;
                }
            }

            throw new Exception('Unknown mode!!!!!');
        }

        public function getLimit()
        {
            $params = request()->input();

            return isset($params['limit']) ? $params['limit'] : 9;
        }

        public function getPaginationFirstPageUrl()
        {
            return request()->fullUrlWithQuery([
                'page' => 1
            ]);
        }

        public function getPaginationLastPageUrl($products_count)
        {
            return request()->fullUrlWithQuery([
                'page' => $this->getTotalPaginationPages($products_count)
            ]);
        }

        public function hasPage()
        {
            return request()->filled('page');
        }

        public function getPage()
        {
            return request()->filled('page');
        }

        public function getCurrentPage()
        {
            $params = request()->input();

            return isset($params['page']) ? $params['page'] : 1;
        }

        public function getPageRange()
        {
            $start = 2;
            $range = 10;
            
            if ($this->getCurrentPage() > 1 && ($this->getCurrentPage() <  ($this->getCurrentPage() + 10))) {
                $start = $this->getCurrentPage();
            }

            return [
                'start' => $start,
                'max' => ($start + $range)
            ];
        }

        public function getPaginationUrl($page_number)
        {
            return request()->fullUrlWithQuery([
                'page'  => $page_number,
            ]);
        }

        public function getPaginationNextPageUrl($products_count)
        {
            if ($this->getCurrentPage() == $products_count) {
                return '#';
            } elseif ($this->getTotalPaginationPages($products_count) == 1) { 
                return '#';
            } else {
                return request()->fullUrlWithQuery([
                    'page' => ($this->getCurrentPage() + 1)
                ]);
            }
        }

        public function getPaginationPreviousPageUrl()
        {
            if ($this->getCurrentPage() == 1) {
                return '#';
            } else {
                return request()->fullUrlWithQuery([
                    'page' => ($this->getCurrentPage() - 1)
                ]);
            }
        }

        public function getTotalPaginationPages($products_count)
        {
            return ceil(($products_count / $this->getLimit()));
        }
    }