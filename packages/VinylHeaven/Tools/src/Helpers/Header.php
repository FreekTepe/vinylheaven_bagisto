<?php

    namespace VinylHeaven\Tools\Helpers;

    use Exception;
    use Illuminate\Support\Facades\Config;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Cache;
    use Illuminate\Database\Eloquent\Builder;

    use Webkul\Shipping\Facades\Shipping;
    use Webkul\Payment\Facades\Payment;
    use Webkul\Category\Models\Category;

    use VinylHeaven\Checkout\Facades\VinylHeavenCart as Cart;

    class Header
    {
        private $executionStartTimes = [];

        private $executionEndTime = [];

        private $executionTime = [];

        public function startFunctionTimer($timer_slug = null)
        {
            if (empty($timer_slug)) {
                throw new Exception('No timer slug given please provide one');
            }
            $this->executionStartTime[$timer_slug] = microtime(true);
        }

        public function stopFunctionTimer($timer_slug = null)
        {
            if (empty($timer_slug)) {
                throw new Exception('No timer slug given please provide one');
            }
            $this->executionEndTime[$timer_slug] = microtime(true);
            $this->executionTime[$timer_slug] = $this->executionEndTime[$timer_slug] - $this->executionStartTime[$timer_slug];
        }

        public function getExecutionTime($timer_slug = null)
        {
            if (empty($timer_slug)) {
                throw new Exception('No timer slug given please provide one');
            }
            return $this->executionTime[$timer_slug];
        }

        public function getGenres()
        {
            $genres = Cache::store('redis')->get('header:genres');

            if ($genres) {
                return $genres;
            } else {
                $genres = [];

                foreach ($this->getRootCategories() as $category_index => $category) {

                    if ($category->id != 1) {
                        $product_count = DB::table('categories as cat')
                            ->select(DB::raw('COUNT(DISTINCT ' . DB::getTablePrefix() . 'pc.product_id) as count'))
                            ->leftJoin('product_categories as pc', 'cat.id', '=', 'pc.category_id')
                            ->where('id', $category->id)
                            ->first();

                            $genres[$category_index] = $category;

                            $genres[$category_index]->products_count = $product_count->count;
                    }

                }
                Cache::store('redis')->put('header:genres', $genres,now()->addMinutes(10));
                return $genres;

            //Bagisto way of getting the genres but this creates a lot of queries
            //return Category::has('products')->get();
            }
        }

        public function getRootCategories()
        {
            return app('Webkul\Category\Repositories\CategoryRepository')->getRootCategories();
        }

        public function getCart()
        {
            return Cart::getCart();
        }

        public function getCurrencyUrl($currency_code)
        {
            return request()->fullUrlWithQuery([
                'currency'  => $currency_code,
            ]);
        }

        public function getShippingMethods()
        {
            $shipping_methods = [];

            foreach (Config::get('carriers') as $shippingMethod) {
                if (!empty($shippingMethod['for_vinyl_heaven'])) {
                    $object = new $shippingMethod['class'];

                    if (! $object->isAvailable()) {
                        continue;
                    }

                    $shipping_methods[] = $object;
                }
            }

            return $shipping_methods;
        }

        public function canPickup($seller)
        {
            $payment_options = $seller->paymentOptions;
            dd($payment_options);
        }

        public function getGenreImages()
        {
            return [
                'Blues' => 'themes/vinylexpress/assets/src/img/blues.png',
                'Brass & Military' => 'themes/vinylexpress/assets/src/img/brass-.png',
                'Children’s' => 'themes/vinylexpress/assets/src/img/children-.png',
                'Classical' => 'themes/vinylexpress/assets/src/img/classical-.png',
                'Electronic' => 'themes/vinylexpress/assets/src/img/electronic-.png',
                'Folk, World & Country' => 'themes/vinylexpress/assets/src/img/folk-.png',
                'Funk/Soul' => 'themes/vinylexpress/assets/src/img/funky-.png',
                'Hip Hop' => 'themes/vinylexpress/assets/src/img/hiphop-.png',
                'Jazz' => 'themes/vinylexpress/assets/src/img/jazz-.png',
                'Latin' => 'themes/vinylexpress/assets/src/img/latin-.png',
                'Non-Music' => 'themes/vinylexpress/assets/src/img/non-music-1.png',
                'Pop' => 'themes/vinylexpress/assets/src/img/pop-.png',
                'Reggae' => 'themes/vinylexpress/assets/src/img/reggae.png',
                'Rock' => 'themes/vinylexpress/assets/src/img/rock-.png',
            ];
        }

        public function getGenreImage($genre_name)
        {
            return isset($this->getGenreImages()[$genre_name]) ? $this->getGenreImages()[$genre_name] : null;
        }

        public function getGenreConfig($genre_index)
        {
            $genres_config = [
                1 => 'one genres-image',
                2 => 'two genres-image',
                3 => 'three genres-image',
                4 => 'four genres-image',
                5 => 'five genres-image',
                6 => 'six genres-image',
                7 => 'seven genres-image',
                8 => 'eight genres-image',
                9 => 'nine genres-image',
                10 => 'ten genres-image',
                11 => 'eleven genres-image',
                12 => 'twelve genres-image',
                13 => 'thirteen genres-image',
                14 => 'fourteen genres-image',
                15 => 'fifteen genres-image',
            ];

            return isset($genres_config[$genre_index]) ? $genres_config[$genre_index] : '';
        }

        public function getCleanAddressObject()
        {
            return Cart::getCleanAddressObject();
        }
    }