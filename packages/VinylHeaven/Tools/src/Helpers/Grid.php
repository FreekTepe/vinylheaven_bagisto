<?php

    namespace VinylHeaven\Tools\Helpers;

    class Grid
    {
        public function getLimit()
        {
            $params = request()->input();

            return isset($params['limit']) ? $params['limit'] : 9;
        }

        public function getPaginationFirstPageUrl()
        {
            return request()->fullUrlWithQuery([
                'page' => 1
            ]);
        }

        public function getPaginationLastPageUrl($products_count)
        {
            return request()->fullUrlWithQuery([
                'page' => $this->getTotalPaginationPages($products_count)
            ]);
        }

        public function hasPage()
        {
            return request()->filled('page');
        }

        public function getPage()
        {
            return request()->filled('page');
        }

        public function getCurrentPage()
        {
            $params = request()->input();

            return isset($params['page']) ? $params['page'] : 1;
        }

        public function getPageRange()
        {
            $start = 2;
            $range = 10;
            
            if ($this->getCurrentPage() > 1 && ($this->getCurrentPage() <  ($this->getCurrentPage() + 10))) {
                $start = $this->getCurrentPage();
            }

            return [
                'start' => $start,
                'max' => ($start + $range)
            ];
        }

        public function getPaginationUrl($page_number)
        {
            return request()->fullUrlWithQuery([
                'page'  => $page_number,
            ]);
        }

        public function getPaginationNextPageUrl($products_count)
        {
            if ($this->getCurrentPage() == $products_count) {
                return '#';
            } elseif ($this->getTotalPaginationPages($products_count) == 1) { 
                return '#';
            } else {
                return request()->fullUrlWithQuery([
                    'page' => ($this->getCurrentPage() + 1)
                ]);
            }
        }

        public function getPaginationPreviousPageUrl()
        {
            if ($this->getCurrentPage() == 1) {
                return '#';
            } else {
                return request()->fullUrlWithQuery([
                    'page' => ($this->getCurrentPage() - 1)
                ]);
            }
        }

        public function getTotalPaginationPages($products_count)
        {
            return ceil(($products_count / $this->getLimit()));
        }
    }