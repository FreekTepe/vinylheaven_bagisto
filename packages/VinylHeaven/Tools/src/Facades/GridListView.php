<?php
    namespace VinylHeaven\Tools\Facades;

    use Illuminate\Support\Facades\Facade;

    class GridListView extends Facade
    {
        protected static function getFacadeAccessor()
        {
            return 'gridlistview';
        }
    }