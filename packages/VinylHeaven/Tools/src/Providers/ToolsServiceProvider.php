<?php
    namespace VinylHeaven\Tools\Providers;

    use Illuminate\Support\ServiceProvider;

    use VinylHeaven\Tools\Helpers\GridListView;

    class ToolsServiceProvider extends ServiceProvider 
    {
        public function register()
        {
            $this->registerToolsFacades();
        }

        private function registerToolsFacades()
        {
            $this->app->singleton('gridlistview', function () {
                return new GridListView();
            });
            
            $this->app->bind('gridlistview', function() {
                return new GridListView();
            });
        }

        public function boot()
        {

        }
    }