<?php

namespace VinylHeaven\Product\Configurators;

use ScoutElastic\IndexConfigurator;
use ScoutElastic\Migratable;

class ProductIndexConfigurator extends IndexConfigurator
{
    use Migratable;

    protected $name = 'vinylexpress_product';

    /**
     * @var array
     */
    protected $settings = [];
}
