<?php

namespace VinylHeaven\Product\Repositories;

use Illuminate\Container\Container as App;
use Illuminate\Support\Str;
use Webkul\Product\Repositories\ProductRepository as BaseProductRepository;
use Webkul\Product\Repositories\ProductFlatRepository;
use Webkul\Attribute\Repositories\AttributeRepository;
use Webkul\Attribute\Models\Attribute;
use Webkul\Marketplace\Repositories\MpProductRepository;
use VinylHeaven\CustomAttributeType\Repositories\ArtistRepository;
use VinylHeaven\CustomAttributeType\Repositories\LabelRepository;
use VinylHeaven\Product\Models\ProductAttributeValue;
use VinylHeaven\Product\Models\Product;

class ProductRepository extends BaseProductRepository
{

    protected $artistRepository;

    public function __construct(
        AttributeRepository $attributeRepository,
        App $app,
        ArtistRepository $artistRepository,
        LabelRepository $labelRepository
    ) {
        $this->artistRepository = $artistRepository;
        $this->labelRepository = $labelRepository;

        parent::__construct($attributeRepository, $app);
    }

    /**
     * Returns products either from the search or just the default one
     *
     * @return \Illuminate\Support\Collection
     */
    public function getVinylExpressProducts($limit = 10)
    {
        $results = app(ProductFlatRepository::class)->scopeQuery(function ($query) {
            $channel = request()->get('channel') ?: (core()->getCurrentChannelCode() ?: core()->getDefaultChannelCode());
            $locale = request()->get('locale') ?: app()->getLocale();
            $order = request()->get('order') ?: 'desc';
            $sort = request()->get('sort') ?: 'product_id';
            $limit = request()->get('limit') ?: 12;

            $return = $query->distinct()
                ->addSelect('product_flat.*')
                ->where('product_flat.status', 1)
                ->orderBy($sort, $order)
                ->limit($limit);

            if (request()->get('term')) {
                $return->where('product_flat.name', 'like', '%' . urldecode(request()->get('term')) . '%');
            }

            return $return;
        })->get();

        return $results;
    }

    /**
     * Get the total amount of product available in database
     *
     * @return int
     */
    public function getTotalProducts()
    {
        $product_totals = app(MpProductRepository::class)->scopeQuery(function ($query) {
            return $query->from('marketplace_products')
                         ->selectRaw('count(distinct(marketplace_products.product_id)) as total_products');
        })->get();

        if (isset($product_totals[0])) {
            return $product_totals[0]->total_products;
        }

        return 0;
    }

    public function attachArtists($id, $artist_ids)
    {
        // turn array of artist_ids, into array of mysql ids to be used in pivot table.
        // + filter artist ids from array that arn't found in bagisto db.
        $ids = $this->artistRepository->prepareArtistIds($artist_ids);
        Product::find($id)->artists()->sync($ids);
    }

    public function attachLabels($id, $label_ids)
    {
        // turn array of label_ids, into array of mysql ids to be used in pivot table.
        // + filter label ids from array that arn't found in bagisto db.
        $ids = $this->labelRepository->prepareLabelIds($label_ids);
        Product::find($id)->labels()->sync($ids);
    }

    static function generateSlug($name)
    {
        $url_key_attribute_id = Attribute::where('code', '=', 'url_key')->first()->id;
        $slug = Str::slug($name, '-');

        $loop = true;
        while ($loop) {
            // check if slug isnt yet taken
            if (ProductAttributeValue::where([['attribute_id', '=', $url_key_attribute_id], ['text_value', '=', $slug]])->exists()) {
                // slug already taken, add randomize slug name
                $slug = "$slug-" . rand(1, 999);
            } else {
                return $slug;
            }
        }
    }
}
