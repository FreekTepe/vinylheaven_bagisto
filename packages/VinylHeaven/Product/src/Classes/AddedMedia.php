<?php
    namespace VinylHeaven\Product\Classes;

    class AddedMedia
    {
        protected $image_url;
        
        public function __construct($image_url)
        {
            $this->setImageUrl($image_url);
        }

        private function setImageUrl($image_url)
        {
            $this->image_url = $image_url;
        }

        private function getImageUrl()
        {
            return $this->image_url;
        }

        public function getUrl()
        {
            return $this->getImageUrl();
        }
    }