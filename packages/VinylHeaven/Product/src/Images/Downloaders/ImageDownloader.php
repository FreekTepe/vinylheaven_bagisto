<?php

    namespace VinylHeaven\Product\Images\Downloaders;

    use GuzzleHttp\Client;

    use Spatie\MediaLibrary\Downloaders\Downloader;
    use Spatie\MediaLibrary\MediaCollections\Exceptions\UnreachableUrl;

    class ImageDownloader implements Downloader 
    {
        public function getTempFile(string $url): string
        {
            $client = new Client(); //GuzzleHttp\Client

            $result = $client->get($url);
            $status = $result->getStatusCode();

            if (!$status == 200) {
                abort($status);
            }

            $ext = pathinfo($url, PATHINFO_EXTENSION);
            $temporaryFile = tempnam(sys_get_temp_dir(), 'media-library') . '.' . $ext;
            file_put_contents($temporaryFile, $result->getBody()->getContents());

            return $temporaryFile;
        }
    }