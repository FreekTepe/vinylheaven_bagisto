<?php

namespace VinylHeaven\Product\Models;

// the package Model to override
use Webkul\Product\Models\ProductAttributeValue as ProductAttributeValueBaseModel;

class ProductAttributeValue extends ProductAttributeValueBaseModel
{

    /**
     * Set the touches attribute for the ScoutElasticSearch driver
     */
    protected $touches = ['product'];

    public function product()
    {
        return $this->belongsTo('VinylHeaven\Product\Models\Product');
    }
}
