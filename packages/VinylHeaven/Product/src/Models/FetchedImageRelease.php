<?php
    namespace VinylHeaven\Product\Models;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Support\Facades\Log;

    use Spatie\MediaLibrary\HasMedia;
    use Spatie\MediaLibrary\InteractsWithMedia;
    use Spatie\MediaLibrary\MediaCollections\Models\Media;
    use Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection;
    use Spatie\MediaLibrary\MediaCollections\Filesystem;
    use Spatie\Image\Manipulations;

    use VinylHeaven\Product\Classes\AddedMedia;
    use VinylHeaven\Product\Models\Product;

    class FetchedImageRelease extends Model implements HasMedia
    {
        use InteractsWithMedia;

        protected static $conversion_size_extra_small = 'xs';
        protected static $conversion_size_small = 'sm';
        protected static $conversion_size_medium = 'md';
        protected static $conversion_size_large = 'lg';
        protected static $conversion_size_extra_large = 'xl';
        protected static $conversion_size_thumb = 'thumb';

        protected $table = 'fetched_image_releases';

        public $incrementing = true;

        protected $fillable = [
            'url','file_name','height','width','image_type','release_id'
        ];

        protected $linode_object_store_url = 'https://release-images.eu-central-1.linodeobjects.com/';

        /**
         * Register all of the needed image by our breakingpoints nfor the Spatie Media Library
         */
        public function registerMediaConversions(Media $media = null): void
        {
            $this->addMediaConversion(FetchedImageRelease::$conversion_size_extra_small)
                ->width(576)
                ->height(576)
                ->nonQueued()
                ->nonOptimized();

            $this->addMediaConversion(FetchedImageRelease::$conversion_size_small)
                ->width(384)
                ->height(384)
                ->nonQueued()
                ->nonOptimized();

            $this->addMediaConversion(FetchedImageRelease::$conversion_size_medium)
                ->width(330)
                ->height(330)
                ->nonQueued()
                ->nonOptimized();

            $this->addMediaConversion(FetchedImageRelease::$conversion_size_large)
                ->width(321)
                ->height(321)
                ->nonQueued()
                ->nonOptimized();

            $this->addMediaConversion(FetchedImageRelease::$conversion_size_extra_large)
                ->width(340)
                ->height(340)
                ->nonQueued()
                ->nonOptimized();

            $this->addMediaConversion(FetchedImageRelease::$conversion_size_thumb)
                ->width(201)
                ->height(201)
                ->nonQueued()
                ->nonOptimized();
        }

        /**
         * Get the url of the image by the breakingpoint slug if no breakingpoint slug is provided it will give back the original image.
         * If there is no image available it will give back the image placeholder.
         *
         * @param String | The image breakpoint slug
         * @return String | The url of the requested image
         */
        public function Url($conversion_slug = '')
        {
            if (!$this->libraryHasMedia()) {
                $new_images = $this->addMediaToLibrary();

                return $new_images->getUrl($conversion_slug);
            } else {
                return isset($this->getMedia($this->release_id)[0]) ? $this->getMedia($this->release_id)[0]->getUrl($conversion_slug) : $this->getImagePlaceholder();
            }
        }

        /**
         * Get the image placeholder
         *
         * @return String | the url of the image placeholder
         */
        public function getImagePlaceholder()
        {
            return asset('themes/vinylexpress/assets/images/Default-Product-Image.png');
        }

        /**
         * Get the image placeholder source set
         *
         * @return String | the url of the image placeholder source set
         */
        public static function getPlaceholderImageSourceSet()
        {
            $return_html =  '<picture>
                                <source srcset="' . asset('themes/vinylexpress/assets/images/Default-Product-Image.png') . '" media="(min-width: 1200px)" />
                                <source srcset="' . asset('themes/vinylexpress/assets/images/Default-Product-Image.png') . '" media="(min-width: 992px)" />
                                <source srcset="' . asset('themes/vinylexpress/assets/images/Default-Product-Image.png') . '" media="(min-width: 768px)" />
                                <source srcset="' . asset('themes/vinylexpress/assets/images/Default-Product-Image.png') . '" media="(min-width: 576px)" />
                                <source srcset="' . asset('themes/vinylexpress/assets/images/Default-Product-Image.png') . '" media="(max-width: 576px)" />
                                <img loading="lazy" src="' . asset('themes/vinylexpress/assets/images/Default-Product-Image.png') . '" class="img-fluid" />
                            </picture>';

            return $return_html;
        }

        /**
         * Get all of the converted images for this image
         *
         * @return Array | With all of the created converted images by there breakingpoint slug
         */
        public function getAllConvertedImages()
        {
            if (!$this->libraryHasMedia()) {
                $new_images = $this->addMediaToLibrary();

                return [
                    FetchedImageRelease::$conversion_size_extra_large => $new_images->getUrl(FetchedImageRelease::$conversion_size_extra_large),
                    FetchedImageRelease::$conversion_size_large => $new_images->getUrl(FetchedImageRelease::$conversion_size_large),
                    FetchedImageRelease::$conversion_size_medium => $new_images->getUrl(FetchedImageRelease::$conversion_size_medium),
                    FetchedImageRelease::$conversion_size_small => $new_images->getUrl(FetchedImageRelease::$conversion_size_small),
                    FetchedImageRelease::$conversion_size_extra_small => $new_images->getUrl(FetchedImageRelease::$conversion_size_extra_small),
                    'original' => $new_images->getUrl()
                ];
            } else {
                return [
                    FetchedImageRelease::$conversion_size_extra_large => $this->Url(FetchedImageRelease::$conversion_size_extra_large),
                    FetchedImageRelease::$conversion_size_large => $this->Url(FetchedImageRelease::$conversion_size_large),
                    FetchedImageRelease::$conversion_size_medium => $this->Url(FetchedImageRelease::$conversion_size_medium),
                    FetchedImageRelease::$conversion_size_small => $this->Url(FetchedImageRelease::$conversion_size_small),
                    FetchedImageRelease::$conversion_size_extra_small => $this->Url(FetchedImageRelease::$conversion_size_extra_small),
                    'original' => $this->Url()
                ];
            }
        }

        /**
         * Get the image sourceset of this image
         *
         * @return HTML
         */
        public function getImageSourceSet()
        {
            $product_primary_image_collection = $this->getAllConvertedImages();
            $return_html =  '<picture>
                                <source srcset="' . $product_primary_image_collection[FetchedImageRelease::$conversion_size_extra_large] . '" media="(min-width: 1200px)" />
                                <source srcset="' . $product_primary_image_collection[FetchedImageRelease::$conversion_size_large] . '" media="(min-width: 992px)" />
                                <source srcset="' . $product_primary_image_collection[FetchedImageRelease::$conversion_size_medium] . '" media="(min-width: 768px)" />
                                <source srcset="' . $product_primary_image_collection[FetchedImageRelease::$conversion_size_small] . '" media="(min-width: 576px)" />
                                <source srcset="' . $product_primary_image_collection[FetchedImageRelease::$conversion_size_extra_small] . '" media="(max-width: 576px)" />
                                <img loading="lazy" src="' . $product_primary_image_collection['original'] . '" class="img-fluid" />
                            </picture>';

            return $return_html;
        }

        /**
         * Add the image to the Spatie Media Library
         *
         * @return Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection
         */
        public function addMediaToLibrary()
        {
            //collection doesnot exists check if the image is already downloaded form discogs
            if ($this->downloaded == 't') {
                // the image is already downloaded to the object store so retrieve it from the the object store to create the new media collection
                $image_url = $this->linode_object_store_url . $this->file_name;
                $url_the_image_before = $this->linode_object_store_url . $this->file_name;
            } else {
                // it is not downloaded to the object store so no need for retrieving it from the object store
                $image_url = $this->url;
            }

            /**
             * Temparary fix for 403 forbidden exception. Some how we are not allowed to get the image from the linode object store.
             * If not needed any more via accetable solution, we can remove this.
             *
             * @author: Freek
             */
            $image_url = $this->url;

            // add the image to the Spatie Media Library
            try {
                $added_media = $this->addMediaFromUrl($image_url)
                                ->addCustomHeaders(['ACL' => 'public-read',])
                                ->toMediaCollection($this->release_id, 'linode');
            } catch(\Exception $e) {
                /**
                 * Left this in here because of later use only
                 * If not needed any more, we can remove this.
                 *
                 * @author: Freek
                 */
                // dd('addMediaToLibrary Exception thrown', $image_url, $url_the_image_before, $this->toArray(), $e);

                /**
                 * This was my first temparary fix for the 403 forbidden exception. Left it in here
                 * for later use only. If not needed any more via accetable solution, we can remove this.
                 *
                 * @author: Freek
                 */
                //$added_media = new AddedMedia($this->url);

                /**
                 * Logging this cautch exception for monitoring purpases
                 */
                // throw new Exception()
                dd($e);
                // Log::debug('In method FetchedImageRelase::addMediaToLibrary an Exception was catch.', json_decode(json_encode($e), true));
            }

            // report to the database that the image is downloaded and added to the linenode objectstore
            $this->downloaded = 't';
            $this->save();

            // return the added media
            return $added_media;
        }

        /**
         * Get an image by our breakpoint. Our breakingpoints we use are the Bootstrap breakingpoints.
         * For all of the Bootstrap breakingpoint see: https://getbootstrap.com/docs/4.5/layout/overview/#responsive-breakpoints
         *
         * @return String | The url of the requested image
         * @throws Exception | If the requested breakingpoint is unknown it will throw an exception
         */
        public function getImageByBreakpoint($breakpoint = null)
        {
            if (empty($breakpoint)) {
                throw new Exception('No breakpoint set!');
            }

            switch($breakpoint) {
                case 'xl':
                    return $this->getXlImage();
                    break;
                case 'lg':
                    return $this->getLgImage();
                    break;
                case 'md':
                    return $this->getMdImage();
                    break;
                case 'sm':
                    return $this->getSmImage();
                    break;
                case 'xs':
                    return $this->getXsImage();
                    break;
                default:
                    throw new Exception('Unknown breakpoint: ' . $breakpoint . '. Can only be xl, lg, md, sm, xs!');
                    break;
            }
        }

        /**
         * Get the Thumb image
         *
         * @return String | The url of the requested image
         */
        public function getThumbImage()
        {
            if (!$this->libraryHasMedia()) {
                $new_images = $this->addMediaToLibrary();

                return $new_images->getUrl(FetchedImageRelease::$conversion_size_thumb);
            } else {
                return $this->getMedia($this->release_id)[0]->getUrl(FetchedImageRelease::$conversion_size_thumb);
            }
        }

        /**
         * Get the XL breakingpoint image
         *
         * @return String | The url of the requested image
         */
        public function getXlImage()
        {
            if (!$this->libraryHasMedia()) {
                $new_images = $this->addMediaToLibrary();

                return $new_images->getUrl(FetchedImageRelease::$conversion_size_extra_large);
            } else {
                return $this->getMedia($this->release_id)[0]->getUrl(FetchedImageRelease::$conversion_size_extra_large);
            }
        }

        /**
         * Get the LG breakingpoint image
         *
         * @return String | The url of the requested image
         */
        public function getLgImage()
        {
            if (!$this->libraryHasMedia()) {
                $new_images = $this->addMediaToLibrary();

                return $new_images->getUrl(FetchedImageRelease::$conversion_size_large);
            } else {
                return $this->getMedia($this->release_id)[0]->getUrl(FetchedImageRelease::$conversion_size_large);
            }
        }

        /**
         * Get the MD breakingpoint image
         *
         * @return String | The url of the requested image
         */
        public function getMdImage()
        {
            if (!$this->libraryHasMedia()) {
                $new_images = $this->addMediaToLibrary();

                return $new_images->getUrl(FetchedImageRelease::$conversion_size_medium);
            } else {
                return $this->getMedia($this->release_id)[0]->getUrl(FetchedImageRelease::$conversion_size_medium);
            }
        }

        /**
         * Get the SM breakingpoint image
         *
         * @return String | The url of the requested image
         */
        public function getSmImage()
        {
            if (!$this->libraryHasMedia()) {
                $new_images = $this->addMediaToLibrary();

                return $new_images->getUrl(FetchedImageRelease::$conversion_size_small);
            } else {
                return $this->getMedia($this->release_id)[0]->getUrl(FetchedImageRelease::$conversion_size_small);
            }
        }

        /**
         * Get the XS breakingpoint image
         *
         * @return String | The url of the requested image
         */
        public function getXsImage()
        {
            if (!$this->libraryHasMedia()) {
                $new_images = $this->addMediaToLibrary();

                return $new_images->getUrl(FetchedImageRelease::$conversion_size_extra_small);
            } else {
                return $this->getMedia($this->release_id)[0]->getUrl(FetchedImageRelease::$conversion_size_extra_small);
            }
        }

        /**
         * Get the product for where this image belongs to
         *
         * @return Product
         */
        public function product()
        {
            return $this->belongsTo(Product::class, 'sku', 'release_id');
        }

        /**
         * Check if the Spatie Media Library has this image
         */
        public function libraryHasMedia()
        {
            return ($this->getMedia($this->release_id)->count()) ? true : false;
        }
    }