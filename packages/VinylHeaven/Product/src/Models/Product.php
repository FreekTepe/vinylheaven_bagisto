<?php

namespace VinylHeaven\Product\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use ScoutElastic\Searchable;

use Webkul\Marketplace\Repositories\SellerRepository;
use Webkul\Product\Models\Product as ProductBaseModel;
use Webkul\Product\Repositories\ProductFlatRepository;
use Webkul\Product\Models\ProductFlat;

use VinylHeaven\Marketplace\Models\MarketplaceProduct;
use VinylHeaven\CustomAttributeType\Models\Artist;
use VinylHeaven\CustomAttributeType\Models\Label;
use VinylHeaven\Product\Configurators\ProductIndexConfigurator;
use VinylHeaven\Product\Models\FetchedImageRelease;
use VinylHeaven\Shipping\Models\Rule;
use VinylHeaven\Tools\Helpers\Header;

class Product extends ProductBaseModel
{

    use Searchable;

    protected $indexConfigurator = ProductIndexConfigurator::class;

    protected $searchRules = [];

    public function toSearchableArray()
    {
        $product_array = $this->toArray();
        $marketplace_products = [];
        $root_category_id = null;
        $prices = [];
        $artists = [];
        $formats = [];

        $created_at = \DateTime::createFromFormat('Y-m-d\TH:i:s.u\Z', $product_array['created_at']);
        $updated_at = \DateTime::createFromFormat('Y-m-d\TH:i:s.u\Z', $product_array['updated_at']);

        if (!empty($this->marketplace_product)) {
            foreach ($this->marketplace_product as $mp_product) {
                $marketplace_products[] = [
                    'id' => $mp_product->id,
                    'price' => $mp_product->price,
                    'condition' => $this->getElasticSearchConditionGrading($mp_product->condition),
                    'sleeve_condition' => $this->getElasticSearchConditionGrading($mp_product->sleeve_condition),
                    'qty' => $mp_product->qty
                ];
                $prices[] = $mp_product->price;
            }
        }

        foreach ($this->categories as $category) {
            if ($category->parent_id == null) {
                $root_category_id = $category->id;
            }
        }

        foreach ($this->artists as $artist) {
            $artists[] = [
                'artist_id' => $artist->artist_id,
                'name' => $artist->name
            ];
        }

        if (!empty($this->formats)) {
            foreach ($this->formats as $format) {
                if (!empty($format['name']) || !empty($format['descriptions'])) {
                    $format_descriptions = [];
                    if (!empty($format['descriptions'])){
                        foreach ($format['descriptions'] as $description) {
                            $format_descriptions[] = [
                                'name' => $description
                            ];
                        }
                    }
                    $formats[] = [
                        'name' => $format['name'],
                        'descriptions' => $format_descriptions
                    ];
                }
            }
        }

        $product_array['root_category_id'] = $root_category_id;
        $product_array['marketplace_products'] = $marketplace_products;
        $product_array['artists'] = $artists;
        $product_array['formats'] = $formats;
        $product_array['created_at'] = $created_at->format('Y-m-d H:i:s');
        $product_array['updated_at'] = $updated_at->format('Y-m-d H:i:s');

        /**
         * Hier moet nog formats bij
         * In het zoek resultaat de filters aanpassen naar wat er mogelijk is in het resultaat
         * VIN 110 bekijken ivm memory leak
         *
         */

        $product_elastic_array = [
            'id' => $product_array['id'],
            'name' => $product_array['name'],
            'description' => $product_array['description'],
            'price' => !empty($product_array['price']) ? $product_array['price'] : 0.00,
            'min_price' => count($prices) ? min($prices) : 0.00,
            'max_price' => count($prices) ? max($prices) : 0.00,
            'special_price' => !empty($product_array['special_price']) ? $product_array['special_price'] : 0.00,
            'root_category_id' => $product_array['root_category_id'],
            'marketplace_products' => $product_array['marketplace_products'],
            'artists' => $product_array['artists'],
            'tracklist' => [],
            'formats' => $product_array['formats'],
            'created_at' => $product_array['created_at'],
            'updated_at' => $product_array['updated_at']
        ];

        if (isset($product_array['tracklist'])) {
            $product_elastic_array['tracklist'] = json_decode($product_array['tracklist']);
        }

        return $product_elastic_array;
    }

    protected $mapping = [
        'dynamic' => false,
        'properties' => [
            'name' => [
                'type' => 'text',
                'fields' => [
                    'sort' => [
                        'type' => 'keyword'
                    ],
                    'autocomplete' => [
                        'type' => 'search_as_you_type'
                    ]
                ],
            ],
            'description' => [
                'type' => 'text'
            ],
            'price' => [
                'type' => 'double'
            ],
            'special_price' => [
                'type' => 'double'
            ],
            'min_price' => [
                'type' => 'double'
            ],
            'max_price' => [
                'type' => 'double'
            ],
            'root_category_id' => [
                'type' => 'integer'
            ],
            'marketplace_products' => [
                'type' => 'nested',
                'properties' => [
                    'price' => [
                        'type' => 'double'
                    ],
                    'condition' => [
                        'type' => 'text'
                    ],
                    'sleeve_condition' => [
                        'type' => 'text'
                    ],
                    'marketplace_seller_id' => [
                        'type' => 'integer'
                    ],
                    'qty' => [
                        'type' => 'integer'
                    ],
                ],
            ],
            'tracklist' => [
                'type' => 'nested',
                'properties' => [
                    "position" => [
                        'type' => 'text'
                    ],
                    "title" => [
                        'type' => 'text'
                    ],
                    "duration" => [
                        'type' => 'text'
                    ],
                    "artists" => [
                        'type' => 'nested',
                        'properties' => [
                            "id" => [
                                'type' => 'integer'
                            ],
                            "name" => [
                                'type' => 'text'
                            ],
                            "role" => [
                                'type' => 'text'
                            ],
                        ],
                    ],
                ],
            ],
            'artists' => [
                'type' => 'nested',
                'properties' => [
                    'artist_id' => [
                        'type' => 'integer'
                    ],
                    'name' => [
                        'type' => 'text',
                        'fields' => [
                            'sort' => [
                                'type' => 'keyword'
                            ],
                            'autocomplete' => [
                                'type' => 'search_as_you_type'
                            ],
                        ],
                    ]
                ]
            ],
            'formats' => [
                'type' => 'nested',
                'properties' => [
                    'name' => [
                        'type' => 'text'
                    ],
                    'descriptions' => [
                        'type' => 'nested',
                        'properties' => [
                            'name' => [
                                'type' => 'text'
                            ]
                        ]
                    ]
                ]
            ],
            'created_at' => [
                'type' => 'date',
                'format' => 'yyyy-MM-dd HH:mm:ss'
            ],
            'updated_at' => [
                'type' => 'date',
                'format' => 'yyyy-MM-dd HH:mm:ss'
            ],
        ]
    ];

    protected $casts = [
        'tracklist' => 'array',
        'identifiers' => 'array',
        'companies' => 'array',
        'formats' => 'array'
    ];

    protected $fillable = [
        'type',
        'attribute_family_id',
        'sku',
        'parent_id',
    ];

    public function getProductsForCatalog($limit = 33)
    {
        $params = request()->input();
        $limit = isset($params['limit']) ? $params['limit'] : $limit;
        $offset = isset($params['page']) ? ($limit * $params['page']) : 0;
        $sort = isset($params['sort']) ? $params['sort'] : 'created_at';
        $order = isset($params['order']) ? $params['order'] : 'asc';

        $results = app(ProductFlatRepository::class)->scopeQuery(function ($query) use ($limit, $offset, $sort, $order) {
            $channel = request()->get('channel') ?: (core()->getCurrentChannelCode() ?: core()->getDefaultChannelCode());

            $locale = request()->get('locale') ?: app()->getLocale();

            $qb = $query->distinct()
                ->addSelect('product_flat.*')
                ->where('product_flat.channel', $channel)
                ->where('product_flat.locale', $locale)
                ->where('product_flat.status', 1)
                ->whereNotNull('product_flat.url_key');

            if ($limit && $limit > 0) {
                $qb->offset($offset)
                    ->limit($limit);
            }


            if (isset($sort)) {
                $attribute = app('Webkul\Attribute\Repositories\AttributeRepository')->findOneByField('code', $sort);

                if ($sort == 'price') {
                    if ($attribute->code == 'price') {
                        $qb->orderBy('min_price', $order);
                    } else {
                        $qb->orderBy($attribute->code, $order);
                    }
                } else {
                    $qb->orderBy($sort == 'created_at' ? 'product_flat.created_at' : $attribute->code, $order);
                }
            }

            return $qb->groupBy('product_flat.id');
        })->get();

        return $results;
    }

    public function getTotalProductsAmountForCatalog()
    {
        $params = request()->input();
        $limit = isset($params['limit']) ? $params['limit'] : 33;
        $offset = isset($params['page']) ? ($limit * $params['page']) : 0;
        $sort = isset($params['sort']) ? $params['sort'] : 'created_at';
        $order = isset($params['order']) ? $params['order'] : 'asc';

        $results = app(ProductFlatRepository::class)->scopeQuery(function ($query) use ($limit, $offset, $sort, $order) {
            $channel = request()->get('channel') ?: (core()->getCurrentChannelCode() ?: core()->getDefaultChannelCode());

            $locale = request()->get('locale') ?: app()->getLocale();

            $qb = $query->distinct()
                ->addSelect('product_flat.id')
                ->where('product_flat.channel', $channel)
                ->where('product_flat.locale', $locale)
                ->where('product_flat.status', 1)
                ->whereNotNull('product_flat.url_key');


            if (isset($sort)) {
                $attribute = app('Webkul\Attribute\Repositories\AttributeRepository')->findOneByField('code', $sort);

                if ($sort == 'price') {
                    if ($attribute->code == 'price') {
                        $qb->orderBy('min_price', $order);
                    } else {
                        $qb->orderBy($attribute->code, $order);
                    }
                } else {
                    $qb->orderBy($sort == 'created_at' ? 'product_flat.created_at' : $attribute->code, $order);
                }
            }

            return $qb->groupBy('product_flat.id');
        })->get();

        return count($results);
    }

    public function rules()
    {
        return $this->belongsToMany(Rule::class);
    }

    public function images()
    {
        return $this->hasMany(FetchedImageRelease::class, 'release_id', 'sku');
    }

    public function labels()
    {
        return $this->belongsToMany(Label::class);
    }

    public function artists()
    {
        return $this->belongsToMany(Artist::class);
    }

    public function marketplace_product()
    {
        return $this->hasMany(MarketplaceProduct::class, 'product_id', 'id')
                ->where('qty', '>', 0)
                ->orderBy('price', 'asc');
    }

    public function marketplaceProducts()
    {
            return $this->marketplace_product;
    }

    public function getMarketplaceProducts()
    {
        $marketplace_products = Cache::store('redis')->get('product::' . $this->id . '::getMarketplaceProducts');
        if ($marketplace_products) {
            return $marketplace_products;
        } else {
            $toCache = $this->marketplace_product()->get();

            Cache::store('redis')->put('product::' . $this->id . '::getMarketplaceProducts', $toCache, now()->addMinutes(4));
            return $toCache;
        }
    }

    public function marketplaceProductsWhere($where, $first = false)
    {
        if ($first) {
            return $this->hasMany(MarketplaceProduct::class, 'product_id', 'id')->where($where)->first();
        } else {
            return $this->hasMany(MarketplaceProduct::class, 'product_id', 'id')->where($where)->get();
        }
    }

    public function getMarketplaceProductsFromPrice()
    {
        $from_price = Cache::store('redis')->get('product::' . $this->id . '::getMarketplaceProductsFromPrice');

        if ($from_price) {
            return $from_price;
        } else {
            $toCache = core()->currency(0.00);
            if ($this->marketplace_product()->count()) {
                $toCache = ($this->marketplace_product()->count()) ? core()->currency(core()->convertPrice($this->marketplace_product()->first()->price, core()->getBaseCurrencyCode(), 'EUR')) : core()->currency(0.00);
            }

            Cache::store('redis')->put('product::' . $this->id . '::getMarketplaceProductsFromPrice', $toCache, now()->addMinutes(4));
            return $toCache;
        }
    }

    public function getLowestPrice()
    {
        $prices = $this->marketplace_product->filter(function ($item) {
            return !is_null($item->price);
        });

        return core()->currency(core()->convertPrice($prices->min('price'), core()->getBaseCurrencyCode(), 'EUR'));
    }

    public function getElasticSearchConditionGrading($condition)
    {
        return stristr($this->getConditionGrading($condition), '+') ? str_replace('+', 'P', $this->getConditionGrading($condition)) : $this->getConditionGrading($condition);
    }

    public function getGradingBackgroundClass($condition_grading)
    {
        // Bart this is a beter way of implementing this function it removes the need for itterorating over the gradings array.
        // $new_grading = $this->getAllConditionGradingCollection()->where('grading', $condition_grading)->first();
        // dd('yoehoe', $new_grading, $condition_grading);
        foreach ($this->getAllConditionGradings() as $grading_index => $grading) {
            if ($grading['grading'] == $condition_grading) {
                $class = '<span title=" ' . $grading['text'] .' " class="circle-text' . ' ' . $grading['class'] . '">' . $grading['short_text'] . '</span>';
                return $class;
            }
        }
    }

    public function getSleeveConditionGrading($condition)
    {

        if (stristr($condition, '(M)')) {
            return 'M';
        }
        elseif (stristr($condition, 'Generic')) {
            return 'Gen';
        } elseif (stristr($condition, 'Not Graded')) {
            return 'NG';
        } elseif (stristr($condition, 'NM') || stristr($condition, 'M-')) {
            return 'NM';
        } elseif (stristr($condition, 'VG+')) {
            return 'VG+';
        } elseif (stristr($condition, 'VG')) {
            return 'VG';
        } elseif (stristr($condition, 'G+')) {
            return 'G+';
        } elseif (stristr($condition, 'G')) {
            return 'G';
        } elseif (stristr($condition, 'P')) {
            return 'P';
        }
        return '?';
    }

    public function getConditionGrading($condition)
    {
        if (stristr($condition, '(M)')) {
            return 'M';
        } elseif (stristr($condition, 'NM') || stristr($condition, 'M-')) {
            return 'NM';
        } elseif (stristr($condition, 'VG+')) {
            return 'VG+';
        } elseif (stristr($condition, 'VG')) {
            return 'VG';
        } elseif (stristr($condition, 'G+')) {
            return 'G+';
        } elseif (stristr($condition, 'G')) {
            return 'G';
        } elseif (stristr($condition, 'P')) {
            return 'P';
        }

        return '?';
    }

    public function getSleeveConditionText($condition)
    {
        if (stristr($condition, '(M)')) {
            return 'Mint';
        } elseif (stristr($condition, 'NM') || stristr($condition, 'M-')) {
            return 'Near Mint';
        } elseif (stristr($condition, 'VG+')) {
            return 'Very Good Plus';
        } elseif (stristr($condition, 'VG')) {
            return 'Very Good';
        } elseif (stristr($condition, 'G+')) {
            return 'Good Plus';
        } elseif (stristr($condition, 'G')) {
            return 'Good';
        } elseif (stristr($condition, 'P')) {
            return 'Poor';
        }

        return 'Unknown';
    }

    public function getConditionText($condition)
    {
        if (stristr($condition, '(M)')) {
            return 'Mint';
        } elseif (stristr($condition, 'NM') || stristr($condition, 'M-')) {
            return 'Near Mint';
        } elseif (stristr($condition, 'VG+')) {
            return 'Very Good Plus';
        } elseif (stristr($condition, 'VG')) {
            return 'Very Good';
        } elseif (stristr($condition, 'G+')) {
            return 'Good Plus';
        } elseif (stristr($condition, 'G')) {
            return 'Good';
        } elseif (stristr($condition, 'P')) {
            return 'Poor';
        }

        return 'Unknown';
    }

    public function getAllConditionGradingCollection()
    {
        return collect($this->getAllConditionGradings());
    }

    public function getAllConditionGradings()
    {
        return [
            [
                'grading' => 'Mint (M)',
                'class' => 'bg-grading-m',
                'label_class' => '',
                'text' => 'Mint',
                'short_text' => 'M'
            ],
            [
                'grading' => 'Near Mint (NM or M-)',
                'class' => 'bg-grading-nm',
                'label_class' => '',
                'text' => 'Near Mint or M-',
                'short_text' => 'NM'
            ],
            [
                'grading' => 'Very Good Plus (VG+)',
                'class' => 'bg-grading-vgp',
                'label_class' => '',
                'text' => 'Very Good Plus',
                'short_text' => 'VG+'
            ],
            [
                'grading' => 'Very Good (VG)',
                'class' => 'bg-grading-vg',
                'label_class' => '',
                'text' => 'Very Good',
                'short_text' => 'VG'
            ],
            [
                'grading' => 'Good Plus (G+)',
                'class' => 'bg-grading-gp',
                'label_class' => '',
                'text' => 'Good Plus',
                'short_text' => 'G+'
            ],
            [
                'grading' => 'Good (G)',
                'class' => 'bg-grading-g',
                'label_class' => '',
                'text' => 'Good',
                'short_text' => 'G'
            ],
            [
                'grading' => 'Poor (P)',
                'class' => 'bg-grading-p',
                'label_class' => '',
                'text' => 'Poor',
                'short_text' => 'P'
            ],
            [
                'grading' => 'Fair (F)',
                'class' => 'bg-grading-f',
                'label_class' => '',
                'text' => 'Fair',
                'short_text' => 'F'
            ],
            [
                'grading' => 'Generic',
                'class' => 'bg-grading-gen',
                'label_class' => '',
                'text' => 'Generic',
                'short_text' => 'GN'
            ],
            [
                'grading' => 'Not Graded',
                'class' => 'bg-grading-gen',
                'label_class' => '',
                'text' => 'Not Graded',
                'short_text' => 'NG'
            ],
            [
                'grading' => 'No Cover',
                'class' => 'bg-grading-gen',
                'label_class' => '',
                'text' => 'No Cover',
                'short_text' => 'NC'
            ]
        ];
    }

    public function getPriceAttribute()
    {
        $product = app('Webkul\Product\Models\ProductFlat')->where('product_id', $this->id)->first();
        return isset($product->price) ? $product->price : null;
    }

    public function getPrimaryImage()
    {
        $primary_image = Cache::store('redis')->get('product::' . $this->id . '::getPrimaryImage');

        if ($primary_image) {
            return $primary_image;
        }
        elseif ($image = $this->images()->where('image_type', 'primary')->first()) {
                $toCache = $image->getImageSourceSet();
                Cache::store('redis')->put('product::' . $this->id . '::getPrimaryImage', $toCache, now()->addMinutes(4));
                return $toCache;
        }
        elseif ($image = $this->images()->where('image_type', 'secondary')->first()) {
            $toCache = $image->getImageSourceSet();
            Cache::store('redis')->put('product::' . $this->id . '::getPrimaryImage', $toCache, now()->addDay());
            return $toCache;
        } else {
            $toCache = $this->getPlaceholderImageSourceSet();
            Cache::store('redis')->put('product::' . $this->id . '::getPrimaryImage', $toCache, now()->addMinutes(4));
            return $toCache;
        }
    }

    public function getPrimaryImageThumb()
    {
        if (Cache::store('redis')->has('primaryImageThumb' . $this->id)) {
            return Cache::store('redis')->get('primaryImageThumb' . $this->id);
        }
        elseif ($image = $this->images()->where('image_type', 'primary')->first()) {
                $toCache = $image->getThumbImage();
                Cache::store('redis')->put('primaryImageThumb' . $this->id, $toCache, now()->addMinutes(4));
                return $toCache;
        }
        elseif ($image = $this->images()->where('image_type', 'secondary')->first()) {
            $toCache = $image->getThumbImage();
            Cache::store('redis')->put('primaryImageThumb' . $this->id, $toCache, now()->addDay());
            return $toCache;
        } else {
            $toCache = $this->getProductImagePlaceholder();;
            Cache::store('redis')->put('primaryImage' . $this->id, $toCache, now()->addMinutes(4));
            return $toCache;
        }
    }

    public function getProductImagePlaceholder()
    {
        return asset('themes/vinylheaven-theme/assets/images/no-cover-available.jpg');
    }

    public function getPlaceholderImageSourceSet()
    {
        $return_html =  '<picture>
                            <source srcset="' . $this->getProductImagePlaceholder() . '" media="(min-width: 1200px)" />
                            <source srcset="' . $this->getProductImagePlaceholder() . '" media="(min-width: 992px)" />
                            <source srcset="' . $this->getProductImagePlaceholder() . '" media="(min-width: 768px)" />
                            <source srcset="' . $this->getProductImagePlaceholder() . '" media="(min-width: 576px)" />
                            <source srcset="' . $this->getProductImagePlaceholder() . '" media="(max-width: 576px)" />
                            <img loading="lazy" src="' . $this->getProductImagePlaceholder() . '" class="img-fluid" />
                        </picture>';

        return $return_html;
    }

    public function getHref()
    {
        $href = Cache::store('redis')->get('product::' . $this->id . '::getHref');

        if ($href) {
            return $href;
        } else {
            $url_key = ProductFlat::where('product_id', '=', $this->id)->pluck('url_key')->first();

            if ($this->marketplace_product->count() == 1) {
                $toCache = route('marketplace.sellers.product.index', ['url' => $this->marketplace_product[0]->seller['url'], 'slug' => $url_key]);
            } else {
                $toCache = route('shop.productOrCategory.index', $url_key);
            }

            Cache::store('redis')->put('product::' . $this->id . '::getHref', $toCache, now()->addMinutes(4));
            return $toCache;
        }
    }

    public function getReleaseYear()
    {
        $release_date_split = explode('-', $this->release_date);

        if (count($release_date_split) > 1) {
            foreach ($release_date_split as $item_index => $item) {
                if (strlen($item) == 4) {
                    return $item;
                }
            }
        } elseif (isset($release_date_split[0])) {
            return $release_date_split[0];
        }

        return !empty($this->release_date) ? $this->release_date : false;
    }

    public function hasArtistName()
    {
        return isset($this->artists[0]) && !empty($this->artists[0]['name']);
    }

    public function getArtistName()
    {
        $artist_name = Cache::store('redis')->get('product::' . $this->id . '::getArtistName');

        if ($artist_name) {
            return $artist_name;
        } else {
            $toCache = isset($this->artists[0]['name']) ? $this->artists[0]['name'] : '';
            Cache::store('redis')->put('product::' . $this->id . '::getArtistName', $toCache, now()->addDay());
            return $toCache;
        }
    }

    public function hasFormatName()
    {
        return isset($this->formats[0]) && !empty($this->formats[0]['name']);
    }

    public function getFormatName()
    {
        return isset($this->formats[0]) ? $this->formats[0]['name'] : false;
    }

    public function hasCompanyName()
    {
        return isset($this->companies[0]) && !empty($this->companies[0]['name']);
    }

    public function getCompanyName()
    {
        return isset($this->companies[0]) ? $this->companies[0]['name'] : false;
    }

    public function hasGenre()
    {
        return isset($this->categories[0]) && !empty($this->categories[0]['name']);
    }

    public function getGenre()
    {
        return isset($this->categories[0]) ? $this->categories[0]['name'] : false;
    }

    public function getWidthAttribute()
    {
        return (count($this->rules) > 0 && isset($this->rules()->first()->width)) ? $this->rules()->first()->width : null;
    }

    public function getHeightAttribute()
    {
        return (count($this->rules) > 0 && isset($this->rules()->first()->height)) ? $this->rules()->first()->height : null;
    }

    public function getLengthAttribute()
    {
        return (count($this->rules) > 0 && isset($this->rules()->first()->length)) ? $this->rules()->first()->length : null;
    }

    public function getWeightAttribute()
    {
        return (count($this->rules) > 0 && isset($this->rules()->first()->weight)) ? $this->rules()->first()->weight : null;
    }

    public function formatsFormated()
    {
        if (Cache::store('redis')->has('product::' . $this->id . '::formatsFormated')) {
            return Cache::store('redis')->get('product::' . $this->id . '::formatsFormated');
        } else {
            $formats_formated = [];

            foreach ($this->formats as $format) {
                $format_string = '';

                $format_string .= trim($format['name']);

                if (!empty($format['descriptions'])) {
                    $format_string .= ' (';
                    $format_string .= implode(", ", $format['descriptions']);
                    $format_string .= ')';
                }

                if (!empty($format['qty'])) {
                    if (($format['qty']) =='1') {
                    $format_string .= ' | ' . trim($format['qty']) . ' Disk';
                    }
                    else $format_string .= ' | ' . trim($format['qty']) . ' Disks';
                }

                $formats_formated[] = $format_string;
            }

            Cache::store('redis')->put('product::' . $this->id . '::formatsFormated', $formats_formated, now()->addDay());
            return $formats_formated;
        }
    }

    public function getFormatsFormatedHtml()
    {
        $html = Cache::store('redis')->get('product::' . $this->id . '::getFormatsFormatedHtml');

        if ($html) {
            return $html;
        } else {
            $formats_formated_html = '';
            foreach($this->formatsFormated() as $format_index => $format){
                if ($format_index == 0) {
                    $formats_formated_html .= $format;
                } else {
                    $formats_formated_html .= '<span class="mx-1">|</span>' . $format;
                }
            }

            Cache::store('redis')->put('product::' . $this->id . '::getFormatsFormatedHtml', $formats_formated_html, now()->addDay());
            return $formats_formated_html;
        }
    }

    /**
     * Gets al of the model data and prepare it for sending it to our datalayer
     */
    public function toDataLayer()
    {
        // get the product array of this model
        $product = $this->toArray();

        // add the marketplace product array to the product array
        $product['marketplace_product'] = [];
        $product['marketplace_product']['count'] = $this->marketplace_product->count();

        // set product json attributes
        $product['primary_image'] = $this->getPrimaryImage();
        $product['artist_name'] = $this->getArtistName();

        foreach ($this->marketplace_product as $marketplace_product_index => $marketplace_product) {
            $product['marketplace_product'][] = $marketplace_product->toDataLayer();
        }

        // add the formats to the product array
        $product['formats'] = $this->formats;
        $product['formats']['count'] = count($this->formats);
        $product['formats']['formatted'] = $this->formatsFormated();

        // add the product images to the product array
        $product['images'] = $this->images->toArray();

        // add the labels to the product array
        $product['labels'] = $this->labels->toArray();

        // add the artists tot the product array
        $product['artists'] = $this->artists->toArray();

        // return the json object of the product
        return json_decode(json_encode($product));
        // dd('from vinylheaven product model:', $this->marketplace_product, $product, json_decode(json_encode($product)));
    }
}
