<?php

    namespace VinylHeaven\Product\Models;

    use Illuminate\Database\Eloquent\Model;
    use Webkul\Product\Models\ProductFlat as ProductFlatBaseModel;

    class ProductFlat extends ProductFlatBaseModel
    {
        /**
         * Set the touches attribute for the ScoutElasticSearch driver
         */
        protected $touches = ['product'];
    }