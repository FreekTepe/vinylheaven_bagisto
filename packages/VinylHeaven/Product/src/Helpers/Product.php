<?php

    namespace VinylHeaven\Product\Helpers;

    use Illuminate\Support\Facades\Cache;

    class Product
    {
        public function getMarketplaceProductsByProduct($product_id)
        {
            if (Cache::store('redis')->has('MPProducts' . $product_id)) {
                return Cache::store('redis')->get('MPProducts' . $product_id);
            } else {
                $toCache = app('VinylHeaven\Marketplace\Models\MarketplaceProduct')->where('product_id', $product_id)->get();
                Cache::store('redis')->put('MPProducts' . $product_id, $toCache, now()->addMinutes(10));
                return $toCache;
            }
        }

        public function getCompleteProductByProduct($product_id)
        {
            if (Cache::store('redis')->has('CProduct' . $product_id)) {
                return Cache::store('redis')->get('CProduct' . $product_id);
            } else {
                $toCache =  app('Webkul\Product\Repositories\ProductRepository')->with(['variants', 'variants.inventories'])->findOrFail($product_id);
                Cache::store('redis')->put('CProduct' . $product_id, $toCache, now()->addMinutes(10));
                return $toCache;
            }
        }

        public function getMarketplaceProductByProduct($product_id)
        {
            if (Cache::store('redis')->has('MPProduct' . $product_id)) {
                return Cache::store('redis')->get('MPProduct' . $product_id);
            } else {
                $toCache = app('Webkul\Marketplace\Repositories\MpProductRepository')->with(['variants', 'variants.inventories'])->findOrFail($product_id);
                Cache::store('redis')->put('MPProduct' . $product_id, $toCache, now()->addMinutes(10));
                return $toCache;
            }
        }
    }