<?php
    namespace VinylHeaven\Product\Jobs;

    use Illuminate\Bus\Queueable;
    use Illuminate\Contracts\Queue\ShouldQueue;
    use Illuminate\Foundation\Bus\Dispatchable;
    use Illuminate\Queue\InteractsWithQueue;
    use Illuminate\Queue\SerializesModels;
    use Webkul\Marketplace\Repositories\ProductRepository as SellerProduct;
    use Webkul\Marketplace\Repositories\MpProductRepository as Product;

    class DeleteAllProductsJob implements ShouldQueue
    {
        use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

        protected $seller;
        protected $sellerProduct;
        protected $product;

        /**
         * Create a new job instance.
         *
         * @return void
         */
        public function __construct($seller)
        {
            $this->seller = $seller;
        }

        /**
         * Execute the job.
         *
         * @return void
         */
        public function handle()
        {
            $sellerProduct = resolve(SellerProduct::class);
            $product = resolve(Product::class);

            foreach ($this->seller->marketplace_products as $marketplace_product) {
                if ($marketplace_product) {
                    if ($marketplace_product->is_owner == 1) {
                        $sellerProduct->delete($marketplace_product->id);
                        $product->delete($marketplace_product->product_id);
                    } else {
                        $sellerProduct->delete($marketplace_product->id);
                    }
                }
            }
        }
    }