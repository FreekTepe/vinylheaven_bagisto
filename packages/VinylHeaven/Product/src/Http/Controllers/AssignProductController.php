<?php

namespace VinylHeaven\Product\Http\Controllers;

use Webkul\Theme\Facades\Themes;
use Illuminate\Http\Request;
use Webkul\Marketplace\Http\Controllers\Shop\Account\AssignProductController as AssignProductControllerBaseController;
use Webkul\Attribute\Models\AttributeFamily;


class AssignProductController extends AssignProductControllerBaseController
{
    public function index()
    {
        Themes::set('vinylheaven-seller-portal-theme');
        return view($this->_config['view']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {

        dd('yay extended!');
        Themes::set('vinylheaven-seller-portal-theme');

        $this->validate(request(), [
            'condition' => 'required',
            'description' => 'required'
        ]);

        $data = request()->all();

        $this->sellerProduct->updateAssign($data, $id);

        session()->flash('success', 'Product updated successfully.');

        return redirect()->route($this->_config['redirect']);
    }
}
