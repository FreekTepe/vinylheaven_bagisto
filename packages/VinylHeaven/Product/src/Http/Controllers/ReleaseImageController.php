<?php


namespace VinylHeaven\Product\Http\Controllers;

use VinylHeaven\Seller\Repositories\SellerRepository;
use VinylHeaven\Product\Models\FetchedImageRelease;

class ReleaseImageController extends Controller
{
    public function image($release_id)
    {
        $image = FetchedImageRelease::where([['release_id', $release_id],['image_type', 'primary']])->first();

        if($image){
            return response()->json(['data' => $image->Url()], 200);
        }
        return response()->json(['data' => 'no image'], 500);

    }
}
