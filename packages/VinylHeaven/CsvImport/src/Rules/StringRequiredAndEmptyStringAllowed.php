<?php
    namespace VinylHeaven\CsvImport\Rules;

    use Illuminate\Contracts\Validation\Rule;

    class StringRequiredAndEmptyStringAllowed implements Rule
    {
        /**
         * Determine if the validation rule passes.
         *
         * @param  string  $attribute
         * @param  mixed  $value
         * @return bool
         */
        public function passes($attribute, $value)
        {
            if (is_string($value)) {
                return true;
            }
            return false;
        }

        /**
         * Get the validation error message.
         *
         * @return string
         */
        public function message()
        {
            return 'The :attribute is required and must be a string or empty string but not null.';
        }
    }