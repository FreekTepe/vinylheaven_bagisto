<?php
    namespace VinylHeaven\Payment\Methods;

    use Illuminate\Support\Facades\Config;
    use VinylHeaven\Payment\Methods\AbstractMethod;

    class Bank extends AbstractMethod
    {
        private $method_config;

        private $attributes;

        private $return_url;

        private $seller_id;

        public function __construct($attributes = [])
        {
            $this->setConfig(Config::get('paymentmethods')['bank']);
            $this->setAttributes($attributes);
        }

        public function hasConfig()
        {
            return empty($this->method_config) ? false : true;
        }

        public function setConfig($method_config)
        {
            $this->method_config = $method_config;
        }

        public function getConfig()
        {
            return $this->method_config;
        }

        public function hasAttributes()
        {
            return empty($this->attributes) ? false : true;
        }

        public function setAttributes($attributes)
        {
            $this->attributes = $attributes;
        }

        public function getAttributes()
        {
            return $this->attributes;
        }

        public function attributes()
        {
            return $this->attributes;
        }

        public function hasAttribute($attribute)
        {
            return isset($this->attributes[$attribute]) ? empty($this->attributes[$attribute]) ? false : true : false;
        }

        public function getAttribute($attribute)
        {
            return isset($this->attributes[$attribute]) ? $this->attributes[$attribute] : false;
        }

        public function setSellerId($seller_id)
        {
            $this->seller_id = $seller_id;
        }

        public function getSellerId()
        {
            return $this->seller_id;
        }

        public function returnUrl()
        {
            return route($this->getConfig()['return_route'], $this->getSellerId());
        }

        public function createUrl()
        {
            return $this->returnUrl();
        }

        public function request()
        {
            redirect()->to($this->createUrl())->send();
        }
    }