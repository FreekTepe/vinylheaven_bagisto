<?php

namespace VinylHeaven\Payment\Providers;

use Illuminate\Support\ServiceProvider;
use VinylHeaven\Payment\Classes\VinylHeavenPayment;

class PaymentServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('vinylheavenpayment', function () {
            return new VinylHeavenPayment();
        });
    }

    public function boot()
    {
        //
    }
}
