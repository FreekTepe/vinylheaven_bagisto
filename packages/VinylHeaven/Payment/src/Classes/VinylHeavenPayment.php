<?php

namespace VinylHeaven\Payment\Classes;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;

class VinylHeavenPayment
{
    public function getMethods()
    {
        return Config::get('paymentmethods');
    }

    public function hasMethod($method_slug)
    {
        return isset($this->getMethods()[$method_slug]);
    }

    public function getMethod($method_slug)
    {
        return isset($this->getMethods()[$method_slug]) ? $this->getMethods()[$method_slug] : false;
    }
}
