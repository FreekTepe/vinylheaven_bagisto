<?php

namespace VinylHeaven\Payment\Facades;

use Illuminate\Support\Facades\Facade;

class VinylHeavenPaymentFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'vinylheavenpayment';
    }
}
