<?php
    namespace VinylHeaven\DataLayer\Facades;

    use Illuminate\Support\Facades\Facade;

    class DataLayer extends Facade
    {
        protected static function getFacadeAccessor()
        {
            return 'datalayer';
        }
    }