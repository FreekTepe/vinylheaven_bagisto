<?php
    
    Route::namespace('\VinylHeaven\DataLayer\Http\Controllers\Api')->group(function(){
        Route::prefix('api/datalayer')->group(function(){
            Route::get('catalogdata', 'DataLayerController@getCatalogData')->name('api.datalayer.catalogdata.get');
            Route::post('catalogdata', 'DataLayerController@setCatalogData')->name('api.datalayer.catalogdata.set');
        });
    });

