<?php
    namespace VinylHeaven\DataLayer\Providers;

    use Illuminate\Support\ServiceProvider;

    use VinylHeaven\DataLayer\Classes\DataLayer;

    class DataLayerServiceProvider extends ServiceProvider 
    {
        public function register()
        {
            $this->registerFacades();
        }

        private function registerFacades()
        {
            $this->app->singleton('datalayer', function () {
                return new DataLayer();
            });
            
            $this->app->bind('datalayer', function() {
                return new DataLayer();
            });
        }

        public function boot()
        {
            $this->loadRoutes();
        }

        private function loadRoutes()
        {
            $this->loadRoutesFrom(__DIR__ . '/../Routes/api.php');
        }
    }