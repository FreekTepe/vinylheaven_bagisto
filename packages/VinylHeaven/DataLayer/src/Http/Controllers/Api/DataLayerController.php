<?php
    namespace VinylHeaven\DataLayer\Http\Controllers\Api;

    use Illuminate\Routing\Controller;

    use VinylHeaven\DataLayer\Facades\DataLayer;

    class DataLayerController extends Controller
    {
        public function getCatalogData()
        {
            return response()->json(DataLayer::getCatalogIndexData());
        }

        public function setCatalogData()
        {
            return response()->json(DataLayer::setCatalogIndexData());
        }
    }