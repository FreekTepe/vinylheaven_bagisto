<?php
    namespace VinylHeaven\DataLayer\Classes;

    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Cache;

    use VinylHeaven\Marketplace\Models\MarketplaceProduct;

    class DataLayer
    {
        /**
         * Flag for forcing storing in the cache
         */
        public $force = false;

        /**
         * Gets the catalog data
         */
        public function getCatalogIndexData($limit, $offset, $sort, $order, $condition, $sleeve_condition, $format, $genre, $min_price, $max_price)
        {
            // check if the cache has the catalog index data result if not set it
            $catalog_data = Cache::store('redis')->get('catalog.index.data::li:' . $limit . '::of:' . $offset . '::so:' . $sort . '::or:' . $order . '::co:' . $condition . '::sc:' . $sleeve_condition . '::fo:' . $format . '::ge:' . $genre . '::pi:' . $min_price . '::pa:' . $max_price);
            
            if ($catalog_data && !$this->force) {
                return $catalog_data;
            } else {
                return $this->setCatalogIndexData($limit, $offset, $sort, $order, $condition, $sleeve_condition, $format, $genre, $min_price, $max_price);
            }
        }

        /**
         * Sets the catalog data. This function probably needs to be called from the backend either by a job or realtime when a update or insert action is done
         * For now its being set by the get call.
         */
        public function setCatalogIndexData($limit, $offset, $sort, $order, $condition, $sleeve_condition, $format, $genre, $min_price, $max_price)
        {
            // get the catalog index data
            $catalog_data = app('VinylHeaven\Marketplace\Repositories\MarketplaceProductRepository')->getCatalogIndexData($limit, $offset, $sort, $order, $condition, $sleeve_condition, $format, $genre, $min_price, $max_price);
// dd($catalog_data);
            // store the catalog index data into the cache
            Cache::store('redis')->put('catalog.index.data::li:' . $limit . '::of:' . $offset . '::so:' . $sort . '::or:' . $order . '::co:' . $condition . '::sc:' . $sleeve_condition . '::fo:' . $format . '::ge:' . $genre . '::pi:' . $min_price . '::pa:' . $max_price, $catalog_data, now()->addMinutes(60));
            // dd('DataLayer::setCatalogIndexData', $catalog_data);
            //return the catalog index data
            return $catalog_data;
        }

        /**
         * Gets the total products count
         */
        public function getProductsTotalCount()
        {
            if (Cache::store('redis')->has('products.total.count') && !$this->force) {
                return Cache::store('redis')->get('products.total.count');
            } else {
                return $this->setProductsTotalCount();
            }
        }

        public function setProductsTotalCount()
        {
            $products_count = app('VinylHeaven\Marketplace\Repositories\MarketplaceProductRepository')->getProductsTotalCount();

            Cache::store('redis')->put('products.total.count', $products_count, now()->addMinutes(60));

            return $products_count;
        }
    }