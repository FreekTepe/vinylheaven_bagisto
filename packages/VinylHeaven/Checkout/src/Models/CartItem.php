<?php

    namespace VinylHeaven\Checkout\Models;

    use Webkul\Checkout\Models\CartItem as CartItemBase;

    use VinylHeaven\Product\Models\Product;

    class CartItem extends CartItemBase
    {
        public function product()
        {
            return $this->hasOne(Product::class, 'id', 'product_id');
        }
    }