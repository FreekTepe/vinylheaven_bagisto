<?php
    namespace VinylHeaven\Checkout\Classes;

    use Illuminate\Support\Arr;
    use Illuminate\Support\Facades\Event;

    use Webkul\Checkout\Facades\Cart;

    class VinylHeavenCart
    {
        /**
         * Gets the current bagisto cart instance
         * If it doesnot find any it wil create it
         * 
         * @return \VinylHeaven\Checkout\Models\Cart
         */
        public function getCart()
        {
            $current_cart = cart()->getCart();

            if ($current_cart == null) {
                $current_cart = cart()->create([]);
            }

            if ($current_cart->needsSellerCartsCreated()) {
                $current_cart->createSellerCarts();
            }

            return $current_cart;
        }

        /**
         * Check if the current bagisto cart instance has errors
         * 
         * @return Array
         */
        public function hasError()
        {
            return Cart::hasError();
        }

        /**
         * Deactivates current cart
         *
         * @return void
         */
        public function deActivateCart($cart = null)
        {
            if (empty($cart)) {
                return false;
            }

            $cartRepository = app('Webkul\Checkout\Repositories\CartRepository');
            $cartRepository->update(['is_active' => false], $cart->id);

            if (!$cart->isSellerCart()) {
                if (session()->has('cart')) {
                    session()->forget('cart');
                    
                    return true;
                }
            } else {
                return true;
            }
            return false;
        }

        /**
         * Remove the item from the cart
         *
         * @param  int  $itemId
         * @return boolean
         */
        public function removeItem($itemId)
        {
            Event::dispatch('checkout.cart.delete.before', $itemId);

            if (! $cart = $this->getCart()) {
                return false;
            }

            $cartItemRepository = app('Webkul\Checkout\Repositories\CartItemRepository');
            $cartItemRepository->delete($itemId);

            Event::dispatch('checkout.cart.delete.after', $itemId);

            return true;
        }

        /**
         * Save the address for the SellerCart the customer address will be copied from the BagistoCart 
         * and store it into the database with the correct SellerCart id
         * 
         * @param integer $seller_id
         * 
         * @return void is the cart valid
         */
        public function saveSellerCartCustomerAddress($data, $seller_id): bool
        {
            if (! $cart = $this->getCart()) {
                return false;
            }

            $cartAddressRepository = app('Webkul\Checkout\Repositories\CartAddressRepository');
            $cartAddresses = $cartAddressRepository->where([['cart_id', '=', $cart->id]])->get();
            $billingAddress = $this->sanatizeDataArray($cartAddresses->where('address_type', 'billing')->first()->toArray());
            $shippingAddress = $this->sanatizeDataArray($cartAddresses->where('address_type', 'shipping')->first()->toArray());
            $sellerCart = $cart->getSellerCart($seller_id);
            $billingAddress['cart_id'] = $sellerCart->id;
            $shippingAddress['cart_id'] = $sellerCart->id;
            
            $cartAddressRepository->create($billingAddress);
            $cartAddressRepository->create($shippingAddress);

            $sellerCart->customer_email = $cart->billing_address->email;
            $sellerCart->customer_first_name = $cart->billing_address->first_name;
            $sellerCart->customer_last_name = $cart->billing_address->last_name;

            $sellerCart->save();
            $sellerCart->collectTotals();

            return true;
        }

        /**
         * Sanatize data array for storing into database
         * 
         * @return Array $data_array
         */
        public function sanatizeDataArray($data_array)
        {
            if (isset($data_array['id'])) {
                unset($data_array['id']);
            }

            if (isset($data_array['created_at'])) {
                unset($data_array['created_at']);
            }

            if (isset($data_array['updated_at'])) {
                unset($data_array['updated_at']);
            }

            return $data_array;
        }

        /**
         * Validate order before creation
         *
         * @return array
         */
        public function prepareSellerCartDataForOrder($seller_id)
        {
            $sellerCart = $this->getCart()->getSellerCart($seller_id);
            $data = $this->sellerCartToArray($seller_id);

            $finalData = [
                'cart_id'               => $sellerCart->id,
                'customer_id'           => $data['customer_id'],
                'is_guest'              => $data['is_guest'],
                'customer_email'        => $data['customer_email'],
                'customer_first_name'   => $data['customer_first_name'],
                'customer_last_name'    => $data['customer_last_name'],
                'customer'              => Cart::getCurrentCustomer()->check() ? Cart::getCurrentCustomer()->user() : null,
                'total_item_count'      => $data['items_count'],
                'total_qty_ordered'     => $data['items_qty'],
                'base_currency_code'    => $data['base_currency_code'],
                'channel_currency_code' => $data['channel_currency_code'],
                'order_currency_code'   => $data['cart_currency_code'],
                'grand_total'           => $data['grand_total'],
                'base_grand_total'      => $data['base_grand_total'],
                'sub_total'             => $data['sub_total'],
                'base_sub_total'        => $data['base_sub_total'],
                'tax_amount'            => $data['tax_total'],
                'base_tax_amount'       => $data['base_tax_total'],
                'coupon_code'           => $data['coupon_code'],
                'applied_cart_rule_ids' => $data['applied_cart_rule_ids'],
                'discount_amount'       => $data['discount_amount'],
                'base_discount_amount'  => $data['base_discount_amount'],
                'billing_address'       => Arr::except($data['billing_address'], ['id', 'cart_id']),
                'payment'               => Arr::except($data['payment'], ['id', 'cart_id']),
                'channel'               => core()->getCurrentChannel(),
            ];

            if ($sellerCart->haveStockableItems()) {
                $finalData = array_merge($finalData, [
                    'shipping_method'               => $data['selected_shipping_rate']['method'],
                    'shipping_title'                => $data['selected_shipping_rate']['carrier_title'] . ' - ' . $data['selected_shipping_rate']['method_title'],
                    'shipping_description'          => $data['selected_shipping_rate']['method_description'],
                    'shipping_amount'               => $data['selected_shipping_rate']['price'],
                    'base_shipping_amount'          => $data['selected_shipping_rate']['base_price'],
                    'shipping_address'              => Arr::except($data['shipping_address'], ['id', 'cart_id']),
                    'shipping_discount_amount'      => $data['selected_shipping_rate']['discount_amount'],
                    'base_shipping_discount_amount' => $data['selected_shipping_rate']['base_discount_amount'],
                ]);

            }

            foreach ($data['items'] as $item) {
                $finalData['items'][] = Cart::prepareDataForOrderItem($item);
            }

            return $finalData;
        }

        /**
         * Returns cart details in array
         *
         * @return array
         */
        public function sellerCartToArray($seller_id)
        {
            $cart = $this->getCart()->getSellerCart($seller_id);

            $data = $cart->toArray();

            $data['billing_address'] = $cart->billing_address->toArray();

            if ($cart->haveStockableItems()) {
                $data['shipping_address'] = $cart->shipping_address->toArray();

                $data['selected_shipping_rate'] = $cart->selected_shipping_rate ? $cart->selected_shipping_rate->toArray() : 0.0;
            }

            $data['payment'] = $cart->payment->toArray();

            $data['items'] = $cart->items()->get()->toArray();

            return $data;
        }

        /**
         * Gets an clean address object for the checkout
         * 
         * @return stdClass
         */
        public function getCleanAddressObject()
        {
            return json_decode(json_encode([
                'customer_first_name' => '',
                'customer_last_name' => '',
                'first_name' => '',
                'last_name' => '',
                'customer_email' => '',
                'company_name' => '',
                'state' => '',
                'country' => '',
                'phone' => '',
                'postcode' => '',
                'address1' => '',
                'city' => '',
            ]));
        }
    }