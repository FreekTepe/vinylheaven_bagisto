<?php

namespace VinylHeaven\Checkout\Http\Controllers;

use Illuminate\Support\Facades\Event;

use Webkul\Shop\Http\Controllers\CartController as CartControllerBaseController;
use Webkul\Checkout\Contracts\Cart as CartModel;
use Webkul\Product\Repositories\ProductRepository;

use Webkul\Checkout\Facades\Cart;

class CartController extends CartControllerBaseController
{
    /**
     * Function for guests user to add the product in the cart.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function add($id)
    {
        try {
            $result = $this->addProductToCart($id, request()->all());

            if ($this->onWarningAddingToCart($result)) {
                session()->flash('warning', $result['warning']);

                return redirect()->back();
            }

            if ($result instanceof CartModel) {
                session()->flash('success', trans('shop::app.checkout.cart.item.success'));

                if ($customer = auth()->guard('customer')->user()) {
                    app('Webkul\Customer\Repositories\WishlistRepository')->deleteWhere(['product_id' => $id, 'customer_id' => $customer->id]);
                }

                if (request()->get('is_buy_now')) {
                    Event::dispatch('shop.item.buy-now', $id);

                    return redirect()->route('shop.checkout.onepage.index');
                }
            }
        } catch(\Exception $e) {
            session()->flash('error', trans($e->getMessage()));

            $product = $this->productRepository->find($id);

            return redirect()->route('shop.productOrCategory.index', $product->url_key);
        }

        return redirect()->back();
    }

    /**
     * Add Items in a cart with some cart and item details.
     *
     * @param  int  $productId
     * @param  array  $data
     * @return \Webkul\Checkout\Contracts\Cart|\Exception|array
     */
    private function addProductToCart($productId, $data)
    {
        $this->beforeAddItemToCart($productId);

        $cart = Cart::getCart();

        if (! $cart && ! $cart = Cart::create($data)) {
            return ['warning' => __('shop::app.checkout.cart.item.error-add')];
        }

        $productRepository = app('Webkul\Product\Repositories\ProductRepository');
        $product = $productRepository->findOneByField('id', $productId);
        
        $product_simple_type_instance = $product->getTypeInstance();
        $cartProducts = $this->prepareForCart($data, $product, $product_simple_type_instance);
        
        if (is_string($cartProducts)) {
            $cart->collectTotals();

            if (count($cart->all_items) <= 0) {
                session()->forget('cart');
            }

            throw new \Exception($cartProducts);
        } else {
            $parentCartItem = null;

            foreach ($cartProducts as $cartProduct) {
                if (empty($cartProduct['price'])) {
                    $cartProduct['price'] = 0.00;
                }

                if (empty($cartProduct['base_price'])) {
                    $cartProduct['base_price'] = 0.00;
                }

                $cartItem = $this->getItemByProduct($cartProduct, true);

                if (isset($cartProduct['parent_id'])) {
                    $cartProduct['parent_id'] = $parentCartItem->id;
                }

                $cartItemRepository = app('Webkul\Checkout\Repositories\CartItemRepository');
                if (! $cartItem) {
                    $cartItem = $cartItemRepository->create(array_merge($cartProduct, ['cart_id' => $cart->id]));
                } else {
                    if (isset($cartProduct['parent_id']) && $cartItem->parent_id != $parentCartItem->id) {
                        $cartItem = $cartItemRepository->create(array_merge($cartProduct, [
                            'cart_id' => $cart->id
                        ]));
                    } else {
                        $cartItem = $cartItemRepository->update($cartProduct, $cartItem->id);
                    }
                }

                if (! $parentCartItem) {
                    $parentCartItem = $cartItem;
                }
            }
        }

        $this->afterAddItemToCart();

        $cart->collectTotals();

        return $cart;
    }

    public function beforeAddItemToCart($productId)
    {
        $data = request()->all();

        if (!isset($data['seller_info'])) {
            throw new \Exception("There was an exception. No seller data provided");
            
        }

        if ($data['seller_info']['is_owner']) {
            throw new \Exception("There was an exception. As an owner of the product you cannot by your own products!!");
        }

        $sellerProduct = $this->productRepository->find($data['seller_info']['product_id']);

        if (!$sellerProduct) {
            return;
        }

        if (!isset($data['quantity'])) {
            $data['quantity'] = 1;
        }

        $product = app('Webkul\Product\Repositories\ProductRepository')->findOneByField('id', $productId);

        if ($cart = Cart::getCart()) {
            $cartItem = $cart->items()->where('product_id', $sellerProduct->product_id)->first();

            if ($cartItem) {
                if (!$sellerProduct->haveSufficientQuantity($data['quantity']))
                    throw new \Exception('Requested quantity not available.');

                $quantity = $cartItem->quantity + $data['quantity'];
            } else {
                $quantity = $data['quantity'];
            }
        } else {
            $quantity = $data['quantity'];
        }

        if (!$sellerProduct->haveSufficientQuantity($quantity)) {
            throw new \Exception('Requested quantity not available.');
        }
    }

    public function afterAddItemToCart()
    {
        $items_before = Cart::getCart()->items;
        $items_after = [];
        
        foreach(Cart::getCart()->items as $item)
        {
            if (isset($item->additional['seller_info']) && !$item->additional['seller_info']['is_owner']) {
                $product = app('VinylHeaven\Marketplace\Models\MarketplaceProduct')->where(['id' => $item->additional['seller_info']['product_id']])->first();
                if ($product) {
                    $item_before = $item;
                    $item->price = core()->convertPrice($product->price);
                    $item->base_price = $product->price;
                    $item->custom_price = $product->price;
                    $item->total = core()->convertPrice($product->price * $item->quantity);
                    $item->base_total = $product->price * $item->quantity;

                    $item->save();
                }

                $item->save();
            } else {
                $item->save();
            }

            $items_after[] = $item;
        }
    }

    /**
     * Add product. Returns error message if can't prepare product.
     *
     * @param  array  $data
     * @return array
     */
    public function prepareForCart($data, $product, $type_instance)
    {
        $data['quantity'] = $data['quantity'] ?? 1;

        $data = $this->getQtyRequest($data);
        
        if (!$type_instance->haveSufficientQuantity($data['quantity'])) {
            return trans('shop::app.checkout.cart.quantity.inventory_warning');
        }

        $price = $type_instance->getFinalPrice();
        
        $products = [
            [
                'product_id'        => $product->id,
                'sku'               => $product->sku,
                'quantity'          => $data['quantity'],
                'name'              => $product->name,
                'price'             => $convertedPrice = core()->convertPrice($price),
                'base_price'        => $price,
                'total'             => $convertedPrice * $data['quantity'],
                'base_total'        => $price * $data['quantity'],
                'weight'            => $product->weight ?? 0,
                'total_weight'      => ($product->weight ?? 0) * $data['quantity'],
                'base_total_weight' => ($product->weight ?? 0) * $data['quantity'],
                'type'              => $product->type,
                'additional'        => $type_instance->getAdditionalOptions($data),
            ]
        ];

        return $products;
    }

    /**
     * Get request quantity
     *
     * @param  array  $data
     * @return array
     */
    public function getQtyRequest($data)
    {
        if ($item = $this->getItemByProduct(['additional' => $data], true)) {
            $data['quantity'] += $item->quantity;
        }

        return $data;
    }

    /**
     * Get cart item by product
     *
     * @param  array  $data
     * @return \Webkul\Checkout\Contracts\CartItem|void
     */
    public function getItemByProduct($data, $check = false)
    {
        $items = Cart::getCart()->all_items;

        foreach ($items as $item) {
            if ($item->product->getTypeInstance()->compareOptions($item->additional, $data['additional'])) {
                if (isset($data['additional']['parent_id'])) {
                    if ($item->parent->product->getTypeInstance()->compareOptions($item->parent->additional, request()->all())) {
                        return $item;
                    }
                } else {
                    if (!$check) {
                        return $item;
                    } else {
                        if ($item->additional['seller_info']['seller_id'] == $data['additional']['seller_info']['seller_id']) {
                            return $item;
                        }
                    }
                }
            }
        }
    }

    /**
     * Returns true, if result of adding product to cart is an array and contains a key "warning"
     *
     * @param  array  $result
     * @return boolean
     */
    private function onWarningAddingToCart($result): bool
    {
        return is_array($result) && isset($result['warning']);
    }
}