<?php

    namespace VinylHeaven\Checkout\Helpers;

    use VinylHeaven\Checkout\Facades\Checkout as CO;

    class Checkout
    {
        public function getMainCart()
        {
            return CO::getMainCart();
        }

        public function getSellerCarts()
        {
            return CO::getSellerCarts();
        }

        public function getBagistoCart()
        {
            return CO::getBagistoCart();
        }

        public function sellers()
        {
            return $this->getBagistoCart()->sellers();
        }
    }