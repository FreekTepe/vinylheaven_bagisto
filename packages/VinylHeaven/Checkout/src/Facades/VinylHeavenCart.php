<?php

namespace VinylHeaven\Checkout\Facades;

use Illuminate\Support\Facades\Facade;

class VinylHeavenCart extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'vinylheavencart';
    }
}