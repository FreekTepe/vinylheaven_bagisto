<?php
    namespace VinylHeaven\Marketplace\Http\Controllers;

    use Illuminate\Foundation\Bus\DispatchesJobs;
    use Illuminate\Routing\Controller;
    use Illuminate\Foundation\Validation\ValidatesRequests;

    use Auth;

    class MarketplaceRedirectController extends Controller
    {
        /**
         * Redirect requests for /marketplace this is a part of the marketplace extension that we wil not use
         * So for good user experience we redirect the request based upon if a customer is logged in or not
         */
        public function redirectMarketplaceUrl()
        {
            if (Auth::guard('customer')->check()) {
                //customer is logged in check if the customer is a seller
                $customer = Auth::guard('customer')->user();
                if ($customer->isSeller()) {
                    //customer is a seller redirect to seller dashboard
                    return redirect()->route('marketplace.account.dashboard.index');
                }

                //customer is not a seller redirect to customer dashboard
                return redirect()->route('customer.profile.index');
            }
            
            //no customer is logged in redirect to login page
            return redirect()->route('customer.session.index');
        }
    }