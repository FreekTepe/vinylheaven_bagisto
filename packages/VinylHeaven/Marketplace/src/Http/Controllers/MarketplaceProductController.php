<?php

namespace VinylHeaven\Marketplace\Http\Controllers;

use App\Jobs\DeleteAllProductsJob;
use VinylHeaven\Seller\Models\Seller;
use VinylHeaven\Seller\Repositories\SellerRepository;
use App\Exports\MarketplaceProductsExport;
use Webkul\Marketplace\Http\Controllers\Shop\Account\ProductController as BaseProductController;


class MarketplaceProductController extends BaseProductController
{
    // Destroy all marketplace seller products
    public function destroy_all()
    {
        $sellerRepository = resolve(SellerRepository::class);
        $seller = $sellerRepository->getSellerByUserId(auth()->guard('customer')->user()->id);

        // dispatch job
        DeleteAllProductsJob::dispatch($seller);

        session()->flash('success', 'Your products are being deleted, this may take a while.');
        return back();
    }

    public function download_csv()
    {
        $sellerRepository = resolve(SellerRepository::class);
        $seller = $sellerRepository->getSellerByUserId(auth()->guard('customer')->user()->id);

        return \Excel::download(new MarketplaceProductsExport($seller->marketplace_products), 'products.csv');
    }
}
