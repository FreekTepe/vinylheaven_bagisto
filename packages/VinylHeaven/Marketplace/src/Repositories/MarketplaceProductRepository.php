<?php
    namespace VinylHeaven\Marketplace\Repositories;

    use Illuminate\Support\Facades\DB;

    use Webkul\Marketplace\Repositories\MpProductRepository as MarketplaceProductBaseRepository;

    class MarketplaceProductRepository extends MarketplaceProductBaseRepository
    {
        public function model()
        {
            return 'VinylHeaven\Marketplace\Models\MarketplaceProduct';
        }

        public function getCatalogIndexData($limit, $offset, $sort, $order, $condition, $sleeve_condition, $format, $genre, $min_price, $max_price)
        {
            $formatAttributeID = DB::table('attributes')->where('code', 'formats')->value('id');

            $result = $this->scopeQuery(function ($query) use($formatAttributeID, $limit, $offset, $sort, $order, $condition, $sleeve_condition, $format, $genre, $min_price, $max_price) {
                $queryBuilder = $query->join('product_flat', 'marketplace_products.product_id', '=', 'product_flat.product_id')
                ->join('product_attribute_values', 'marketplace_products.product_id', '=', 'product_attribute_values.product_id')
                ->join('product_categories', 'marketplace_products.product_id', '=', 'product_categories.product_id')
                ->join('category_translations', 'product_categories.category_id', '=', 'category_translations.category_id')
                ->where('product_attribute_values.attribute_id', $formatAttributeID)
                ->select('product_attribute_values.json_value as formats', 'marketplace_products.*', 'product_flat.*', 'product_categories.category_id', 'category_translations.name as category_name');

                if ($condition) {
                    $conditions_splitted = explode(',', $condition);

                    foreach ($conditions_splitted as $condition_split_index => $condition_split) {
                        if ($condition_split_index == 0) {
                            $queryBuilder = $queryBuilder->where('marketplace_products.condition', '=', data_get($this->getAllConditionGradings()->firstWhere('short_text', $condition_split), 'grading'));
                        } else {
                            $queryBuilder = $queryBuilder->orWhere('marketplace_products.condition', '=', data_get($this->getAllConditionGradings()->firstWhere('short_text', $condition_split), 'grading'));
                        }
                    }
                }

                if ($sleeve_condition) {
                    $conditions_splitted = explode(',', $sleeve_condition);

                    foreach ($conditions_splitted as $condition_split_index => $condition_split) {
                        if ($condition_split_index == 0) {
                            $queryBuilder = $queryBuilder->where('marketplace_products.sleeve_condition', '=', data_get($this->getAllConditionGradings()->firstWhere('short_text', $condition_split), 'grading'));
                        } else {
                            $queryBuilder = $queryBuilder->orWhere('marketplace_products.sleeve_condition', '=', data_get($this->getAllConditionGradings()->firstWhere('short_text', $condition_split), 'grading'));
                        }
                    }
                }

                if ($format) {
                    $queryBuilder = $queryBuilder->whereJsonContains('json_value', ['name' => $format]);
                }

                if ($min_price) {
                    $queryBuilder = $queryBuilder->where('marketplace_products.price', '>=', $min_price);
                }

                if ($max_price) {
                    $queryBuilder = $queryBuilder->where('marketplace_products.price', '<=', $max_price);
                }

                if ($genre) {
                    $genres_splitted = explode(',', $genre);

                    foreach ($genres_splitted as $genre_split_index => $genre_split) {
                        if ($genre_split_index == 0) {
                            $queryBuilder = $queryBuilder->where('product_categories.category_id', '=', $genre_split);
                        } else {
                            $queryBuilder = $queryBuilder->orWhere('product_categories.category_id', '=', $genre_split);
                        }
                    }
                }

                return $queryBuilder->offset($offset)
                    ->limit($limit)
                    ->orderBy($sort, $order)
                    ->groupBy('marketplace_products.product_id');
            });

            return $result->get();
        }

        public function getProductsTotalCount()
        {
            return $this->scopeQuery(function($query){
                return $query->select('marketplace_products.*')
                    ->groupBy('marketplace_products.product_id');
            })->get()->count();
        }

        /**
         * Gets all of the condition gradings collection
         *
         * @return Illuminate\Database\Eloquent\Collection returns the condition gradings collection
         */
        public function getAllConditionGradings()
        {
            return collect(
                [
                    [
                        'grading' => 'Mint (M)',
                        'class' => 'bg-grading-m',
                        'label_class' => '',
                        'text' => 'Mint',
                        'short_text' => 'M'
                    ],
                    [
                        'grading' => 'Near Mint (NM or M-)',
                        'class' => 'bg-grading-nm',
                        'label_class' => '',
                        'text' => 'Near Mint or M-',
                        'short_text' => 'NM'
                    ],
                    [
                        'grading' => 'Very Good Plus (VG+)',
                        'class' => 'bg-grading-vgp',
                        'label_class' => '',
                        'text' => 'Very Good Plus',
                        'short_text' => 'VG+'
                    ],
                    [
                        'grading' => 'Very Good (VG)',
                        'class' => 'bg-grading-vg',
                        'label_class' => '',
                        'text' => 'Very Good',
                        'short_text' => 'VG'
                    ],
                    [
                        'grading' => 'Good Plus (G+)',
                        'class' => 'bg-grading-gp',
                        'label_class' => '',
                        'text' => 'Good Plus',
                        'short_text' => 'G+'
                    ],
                    [
                        'grading' => 'Good (G)',
                        'class' => 'bg-grading-g',
                        'label_class' => '',
                        'text' => 'Good',
                        'short_text' => 'G'
                    ],
                    [
                        'grading' => 'Poor (P)',
                        'class' => 'bg-grading-p',
                        'label_class' => '',
                        'text' => 'Poor',
                        'short_text' => 'P'
                    ],
                    [
                        'grading' => 'Fair (F)',
                        'class' => 'bg-grading-f',
                        'label_class' => '',
                        'text' => 'Fair',
                        'short_text' => 'F'
                    ],
                    [
                        'grading' => 'Generic',
                        'class' => 'bg-grading-gen',
                        'label_class' => '',
                        'text' => 'Generic',
                        'short_text' => 'GN'
                    ],
                    [
                        'grading' => 'Not Graded',
                        'class' => 'bg-grading-gen',
                        'label_class' => '',
                        'text' => 'Not Graded',
                        'short_text' => 'NG'
                    ],
                    [
                        'grading' => 'No Cover',
                        'class' => 'bg-grading-gen',
                        'label_class' => '',
                        'text' => 'No Cover',
                        'short_text' => 'NC'
                    ]
                ]
            );
        }
    }