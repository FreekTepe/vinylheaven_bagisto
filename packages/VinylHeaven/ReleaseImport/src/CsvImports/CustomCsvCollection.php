<?php

namespace VinylHeaven\ReleaseImport\CsvImports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;

class CustomCsvCollection implements ToCollection, WithHeadingRow, WithCustomCsvSettings
{
    use Importable;

    protected $settings;

    public function __construct($settings)
    {
        $this->settings = $settings;
    }

    /**
     * @param  Illuminate\Support\Collection  $row
     * @return void
     */
    public function collection(Collection $rows)
    {
    }

    public function getCsvSettings(): array
    {
        return $this->settings;
    }
}
