<?php
    namespace VinylHeaven\ReleaseImport\CsvImports\Resolvers;

    use GuzzleHttp\Client;

    class ElasticReleaseSuggestion
    {
        protected $suggestions;

        public function __construct($data)
        {
            $client = new Client();

            $result = $client->request(
                'POST', 
                "https://discogsapi.rockhouse.nl/releases/autocomplete", 
                [
                    'json' => array_filter($data)
                ]
            );

            $status = $result->getStatusCode();

            if (!$status == 200) {
                abort($status);
            }

            $contents = json_decode($result->getBody()->getContents(), true);

            $this->suggestions = $contents;
        }

        public function getSuggestions()
        {
            return  $this->suggestions;
            // [
            //     "release_title" => $this->mapSuggestions('release_title'),
            //     "artist_name" => $this->mapSuggestions('artist_name'),
            //     "released" => $this->mapSuggestions('released'),
            //     "barcode" => $this->mapSuggestions('barcode'),
            //     "label" => $this->mapSuggestions('label'),
            //     "cat_no" => $this->mapSuggestions('cat_no')
            // ];

        }

        private function mapSuggestions($key)
        {
            if(isset($this->suggestions[$key]) && count($this->suggestions[$key]) > 0){
                $suggestion_options = array();
                foreach($this->suggestions[$key] as $suggestion){
                    foreach($suggestion['options'] as $option){
                        array_push($suggestion_options, $option['text']);
                    }
                }
                return $suggestion_options;
            }
            return null;
        }
    }