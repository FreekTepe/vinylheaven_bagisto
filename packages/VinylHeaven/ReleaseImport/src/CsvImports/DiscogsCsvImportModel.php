<?php

namespace VinylHeaven\ReleaseImport\CsvImports;

use Maatwebsite\Excel\Concerns\WithStartRow;
use VinylHeaven\ReleaseImport\Models\DiscogsCsv;
use Maatwebsite\Excel\Concerns\ToModel;


class DiscogsCsvImportModel implements ToModel, WithStartRow /*, WithChunkReading*/
{

    protected $seller_id;
    protected $media_id;

    public function __construct($seller_id, $media_id)
    {
        $this->seller_id = $seller_id;
        $this->media_id = $media_id;
    }

    /**
     * Skip the first row which is the header (start on row 2)
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new DiscogsCsv([
            'import_status' => 100,
            'seller_id' => $this->seller_id,
            'media_id' => $this->media_id,
            'listing_id' => $row[0],
            'artist' => $row[1],
            'title' => $row[2],
            'label' => $row[3],
            'catno' => $row[4],
            'format' => $row[5],
            'release_id' => $row[6],
            'status' => $row[7],
            'price' => $row[8],
            'listed' => $row[9],
            'comments' => $row[10],
            'media_condition' => $row[11],
            'sleeve_condition' => $row[12],
            'accept_offer' => $row[13],
            'external_id' => $row[14],
            'weight' => $row[15],
            'format_quantity' => $row[16],
            'flat_shipping' => $row[17],
            'location' => $row[18]
        ]);
    }
}
