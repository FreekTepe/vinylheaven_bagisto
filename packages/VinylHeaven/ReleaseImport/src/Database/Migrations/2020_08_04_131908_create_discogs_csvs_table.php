<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscogsCsvsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discogs_csvs', function (Blueprint $table) {
            $table->bigIncrements('id');

            // crucial data for running discogs csv import
            $table->bigInteger('seller_id'); // marketplace_seller table ID
            $table->bigInteger('media_id'); // media table ID
            $table->bigInteger("release_id"); // discogs release ID
            $table->integer('import_status')->default(100);
            $table->longText('exception')->nullable();

            // data
            $table->bigInteger('listing_id')->nullable();
            $table->string('artist')->nullable();
            $table->string('title')->nullable();
            $table->string('label')->nullable();
            $table->string('catno')->nullable();
            $table->string('format')->nullable();
            $table->string('status')->nullable();
            $table->string('price')->nullable();
            $table->string('listed')->nullable();
            $table->text('comments')->nullable();
            $table->string('media_condition')->nullable();
            $table->string('sleeve_condition')->nullable();
            $table->string('accept_offer')->nullable();
            $table->string('external_id')->nullable();
            $table->integer('weight')->nullable();
            $table->integer('format_quantity')->nullable();
            $table->string('flat_shipping')->nullable();
            $table->text('location')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discogs_csvs');
    }
}
