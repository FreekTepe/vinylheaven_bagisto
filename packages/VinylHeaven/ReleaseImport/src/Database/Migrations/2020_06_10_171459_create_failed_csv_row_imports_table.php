<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFailedCsvRowImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('failed_csv_row_imports', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('seller_id');
            $table->integer('csv_id');
            $table->string('collection_type');
            $table->longText('csv_row');
            $table->longText('exception');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('failed_csv_row_imports');
    }
}
