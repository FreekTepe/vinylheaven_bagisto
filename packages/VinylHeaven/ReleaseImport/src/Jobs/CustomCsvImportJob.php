<?php

namespace VinylHeaven\ReleaseImport\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use VinylHeaven\ReleaseImport\Models\FailedCsvRowImports;
use VinylHeaven\ReleaseImport\Models\PendingCustomCsvImportMatches;
use VinylHeaven\ReleaseImport\CsvImports\CustomCsvCollection;
use VinylHeaven\ReleaseImport\Mail\CustomCsvImportCompletedMail;
use VinylHeaven\ReleaseImport\CsvImports\Resolvers\CustomCSVRow;
use VinylHeaven\ReleaseImport\CsvImports\Resolvers\ElasticReleaseMatches;

class CustomCsvImportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $file;
    protected $seller;
    protected $user;
    protected $csv_map;
    protected $delimiter;

    protected $successes = 0;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($file, $seller, $user, $csv_map, $delimiter = ",")
    {
        $this->file = $file;
        $this->seller = $seller;
        $this->user = $user;
        // normalize csv_map headers (fix for DatagridHeader mismatch)
        $this->csv_map = collect($csv_map)->map(function ($item) {
            return [
                "csv_header" => strtolower(str_replace(' ', '_', trim($item->csv_header))),
                "bagisto_field" => $item->bagisto_field
            ];
        });
        $this->delimiter = $delimiter;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        // Normally jobs should run seperately of any auth sessions, but bagisto repository methods rely on auth() guard. So we need to authenticate the user object.
        //
        \Auth::guard('customer')->login($this->user);

        // Todo: make grid settings (like delimiter) dynamic based on file structure analysis.
        $customCsvCollection = new CustomCsvCollection([
            'delimiter'              => $this->delimiter,
            'enclosure'              => '"',
            'escape_character'       => '\\',
            'contiguous'             => false,
            'input_encoding'         => 'UTF-8'
        ]);

        // parse csv to array
        $csvData = $customCsvCollection->toArray($this->file->getPath())[0];

        foreach ($csvData as $file_row) {
            try {
                // Dynamische validatie uitdenken aan de hand van ervaringen met elasticsearch
                $validated_mapped_row = new CustomCSVRow($file_row, $this->csv_map); // validate row and make processable
                $matches = new ElasticReleaseMatches($validated_mapped_row->getElasticMatchData(), null);
                $this->succeed($file_row, $matches, $validated_mapped_row->getSellerData());
                continue;
            } catch (\Throwable $th) {
                $this->fail($file_row, $th);
                continue;
            }
        }

        \Mail::to($this->seller->customer->email)->send(new CustomCsvImportCompletedMail($this->successes, $this->seller, $this->file));
    }

    private function succeed($file_row, $matches, $seller_data)
    {
        // persist the csv row matches to database (for seller evaluation)
        PendingCustomCsvImportMatches::create([
            "seller_id" => $this->seller->id,
            "csv_id" => $this->file->id,
            "collection_type" => $this->file->collection_name,
            "csv_row" => $file_row,
            "matches" => serialize($matches),
            "seller_data" => $seller_data
        ]);
        $this->successes = $this->successes + 1;
    }

    private function fail($file_row, $th)
    {
        // persist failure to database
        FailedCsvRowImports::create([
            "seller_id" => $this->seller->id,
            "csv_id" => $this->file->id,
            "collection_type" => $this->file->collection_name,
            "csv_row" => $file_row,
            "exception" => $th
        ]);
    }
}
