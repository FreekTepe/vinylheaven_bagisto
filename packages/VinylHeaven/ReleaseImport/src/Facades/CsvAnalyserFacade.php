<?php

namespace VinylHeaven\ReleaseImport\Facades;

use Illuminate\Support\Facades\Facade;

class CsvAnalyserFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'csvanalyser';
    }
}
