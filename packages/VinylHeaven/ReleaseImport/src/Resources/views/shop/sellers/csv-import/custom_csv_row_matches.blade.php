@extends('marketplace::shop.layouts.account')

@section('page_title')
    Custom CSV row matches
@endsection
@section('content')

    @php
        $match_count = count(unserialize($pending_product->matches)->getReleases()['data']);
    @endphp

    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <a href="{{ url()->previous() }}" class="page-title-icon" style="text-decoration: none;">
                    <i class="lnr lnr-chevron-left  icon-gradient bg-tempting-azure"></i>
                </a>
                <div>Custom CSV row matches
                    <div class="page-title-subheading">Matches found on custom CSV row.</div>
                </div>
            </div>
        </div>
    </div>

    <div class="main-card  card">
        <div class="card-header">
            <span class="badge badge-pill badge-info">{{ $match_count }} matches</span>
        </div>
        <div class="card-body">
            <table class="table table-bordered table-responsive">
            <thead>
                <tr>
                    @foreach ($pending_product->csv_row as $header => $value)
                        <th>{{ $header }}</th> 
                    @endforeach
                </tr>
            </thead>
            <tbody>
                <tr>
                    @foreach ($pending_product->csv_row as $header => $value)
                        <td style="white-space: nowrap; width: 1%;">{{ $value }}</td> 
                    @endforeach
                </tr>
            </tbody>
            </table>
        </div>
    </div>
    <pending-custom-csv-product 
        :pending_product_id="{{ $pending_product->id }}"
        :csv_id="{{ $pending_product->csv_id }}"
        :csv_row="{{ json_encode($pending_product->csv_row) }}"
        :matches="{{ json_encode(unserialize($pending_product->matches)->getReleases()['data']) }}"
        :seller_data="{{ json_encode($pending_product->seller_data) }}"
    ></pending-custom-csv-product>

@endsection