@extends('marketplace::shop.layouts.account')

@section('page_title')
    Custom CSV matches
@endsection
@section('content')


    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-box2 icon-gradient bg-tempting-azure"></i>
                </div>
                <div>Custom CSV matches
                    <div class="page-title-subheading">Matches found on custom CSV rows.</div>
                </div>
            </div>
        </div>
    </div>





    
        <div class="grid-container">
            <div class="main-card mb-3 card" style="width:100%; overflow:hidden;">
                <div class="card-body">
                    <table class="table table-hover table-striped table-bordered table-responsive">
                        <colgroup>
                            <col class="bg-white">
                        </colgroup>
                        <thead>
                            <tr>
                                {{-- <th>Actions</th>  --}}
                                <th class="success" style="position:sticky;
                                left:0px; background-color: #FFFFFF; border:0px;">
                                </th> 
                                @foreach ($pending[0]->csv_row as $header => $value)
                                    <th>{{ $header }}</th> 
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pending as $index => $pending_product)
                                @php
                                    $match_count = count(unserialize($pending_product->matches)->getReleases()['data']);
                                @endphp
                                <tr>
                                    {{-- <td style="white-space: nowrap; width: 100px;">
                                        <div class="action">
                                            <a href="http://localhost:8000/marketplace/custom-csv-import/custom-csv-matches/186" class="btn-icon btn-icon-only btn btn-info btn-sm btn-shadow">
                                                <i class="lnr-eye btn-icon-wrapper"></i>
                                            </a>
                                        </div>
                                    </td> --}}
                                    <td style="position:sticky; left:0px; background-color: @if($index % 2 == 0) #F2F2F2 @else white @endif; border:0px; box-shadow: 0 20px 10px 0px rgba(0,0,0,0.20);">
                                        <div class="action">
                                            <a href="{{ route('custom-csv-import.custom_csv_matches.single_row', $pending_product->id) }}" class="btn-icon btn-icon-only btn btn-info btn-sm btn-shadow">
                                                <i class="lnr-eye btn-icon-wrapper"></i> 
                                            </a>
                                        </div>
                                    </td> 
                                    @foreach ($pending_product->csv_row as $header => $value)
                                        <td style="white-space: nowrap; width: 1%;">{{ $value }}</td> 
                                    @endforeach
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    {{ $pending->links('vendor.pagination.architect') }}

@endsection