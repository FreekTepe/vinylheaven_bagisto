@extends('marketplace::shop.layouts.account')

@section('page_title')
    Custom CSV matches
@endsection
@section('content')

<div class="container">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-box2 icon-gradient bg-tempting-azure"></i>
                </div>
                <div>Imported custom CSV files
                    <div class="page-title-subheading">Custom CSV files you have imported.</div>
                </div>
            </div>
        </div>
    </div>   

    {{-- $custom_csv_files --}}



    {{-- "id" => 45
    "model_type" => "VinylHeaven\Seller\Models\Seller"
    "model_id" => 1
    "uuid" => "2c8d463c-ada5-4605-aaa1-5b1627d63752"
    "collection_name" => "customcsv"
    "name" => "customcsv-test"
    "file_name" => "customcsv-test.csv"
    "mime_type" => "text/plain"
    "disk" => "public"
    "conversions_disk" => "public"
    "size" => 4181
    "manipulations" => "[]"
    "custom_properties" => "[]"
    "responsive_images" => "[]"
    "order_column" => 45
    "created_at" => "2020-06-11 12:24:48"
    "updated_at" => "2020-06-11 12:24:48" --}}

    <ul class="list-group list-group-flush">

        @foreach ($custom_csv_files as $custom_csv_file)

        <li class="list-group-item">
            <div class="widget-content
                p-0">
                <div class="widget-content-wrapper">
                    <div class="widget-content-left
                        mr-3">
                        <div class="icon-wrapper
                            border-light
                            rounded
                            m-0">
                            <div class="icon-wrapper-bg
                                bg-light"></div>
                            <i class="lnr lnr-file-empty
                                icon-gradient
                                bg-happy-itmeo"></i></div>
                    </div>
                    <div class="widget-content-left">
                        <div class="widget-heading"><a href="{{ route('custom-csv-import.custom_csv_matches.single_file',  $custom_csv_file->id) }}">{{ $custom_csv_file->file_name }}</a></div>
                    </div>
                    <div class="widget-content-right">
                        {{-- <button class="btn-pill
                            btn-hover-shine
                            btn
                            btn-focus
                            btn-sm">324</button> --}}
                    </div>
                </div>
            </div>
        </li>

        @endforeach
    </ul>

    {{ $custom_csv_files->links('vendor.pagination.architect') }}

    
</div>

@endsection