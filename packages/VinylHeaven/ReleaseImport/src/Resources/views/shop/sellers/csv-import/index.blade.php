@extends('shop::layouts.master')

@section('page_title')
CSV import
@endsection

@section('content-wrapper')


<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-cloud-upload icon-gradient bg-tempting-azure"></i>
            </div>
            <div>CSV import
                <div class="page-title-subheading">Here you can import products to your store using CSV files.</div>
            </div>
        </div>
    </div>
</div>   

<ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">
    <li class="nav-item">
        <a role="tab" class="nav-link active show" id="tab-0" data-toggle="tab" href="#tab-content-0" aria-selected="true">
            <span>Discogs CSV</span>
        </a>
    </li>
    <li class="nav-item">
        <a role="tab" class="nav-link show" href="{{ route('custom-csv-import.index') }}" aria-selected="false">
            <span>Custom CSV</span>
        </a>
    </li>
</ul>


{{-- Upload --}}
<div class="card mb-3 mt-3">
    <div class="card-body">
        <discogs-csv-import></discogs-csv-import>
    </div>
</div>

{{-- DiscogsCsvFilesDataGrid renders table --}}
{!! app('VinylHeaven\CvsImport\DataGrids\DiscogsCsvFilesDataGrid')->render() !!}

@endsection