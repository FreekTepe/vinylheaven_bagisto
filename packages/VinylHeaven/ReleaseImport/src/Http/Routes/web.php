<?php

Route::group(['middleware' => ['web', 'theme', 'locale', 'currency', 'customer', 'set.theme.sellerdashboard']], function () {
    Route::namespace('\VinylHeaven\ReleaseImport\Http\Controllers')->group(function () {

        Route::prefix('marketplace/custom-csv-import')->group(function () {
            Route::get('', 'DiscogsCsvImportController@index')->defaults('_config', [
                'view' => 'release-import::shop.sellers.csv-import.custom_csv'
            ])->name('custom-csv-import.index');

            Route::get('custom-csv-matches', 'DiscogsCsvImportController@customCsvFiles')->defaults('_config', [
                'view' => 'release-import::shop.sellers.csv-import.custom_csv_files'
            ])->name('custom-csv-import.custom_csv_matches');

            Route::get('custom-csv-matches/{file_id}', 'DiscogsCsvImportController@customCsvMatches')->defaults('_config', [
                'view' => 'release-import::shop.sellers.csv-import.custom_csv_matches'
            ])->name('custom-csv-import.custom_csv_matches.single_file');

            Route::get('custom-csv-matches/row/{pending_id}', 'DiscogsCsvImportController@customCsvRowMatches')->defaults('_config', [
                'view' => 'release-import::shop.sellers.csv-import.custom_csv_row_matches'
            ])->name('custom-csv-import.custom_csv_matches.single_row');
        });

        Route::prefix('marketplace/discogs-csv-import')->group(function () {
            Route::get('', 'DiscogsCsvImportController@index')->defaults('_config', [
                'view' => 'release-import::shop.sellers.csv-import.index'
            ])->name('discogs-csv-import.index');

            Route::get('delete/{id}', 'DiscogsCsvImportController@delete')->defaults('_config', [
                'view' => 'release-import::shop.sellers.csv-import.index'
            ])->name('discogs-csv-import.delete');

            Route::post('', 'DiscogsCsvImportController@import')->defaults('_config', [
                'view' => 'release-import::shop.sellers.csv-import.index'
            ])->name('discogs-csv-import.import');
        });
    });
});
