<?php

Route::namespace('\VinylHeaven\ReleaseImport\Http\Controllers\Api')->group(function () {
    Route::prefix('api/release')->group(function () {
        Route::post('import', 'ReleaseImportController@import')->name('release.import');
        Route::post('import/customCsv', 'ReleaseImportController@custom_csv_import')->name('release.import.customcsv');
        Route::post('import/discogsCsv', 'ReleaseImportController@discogs_csv_import')->name('release.import.discogscsv');

        Route::get('delete/pending_custom_csv_product/{pending_id}', 'ReleaseImportController@delete_pending_custom_csv_product')->name('release.import.delete_pending_custom_csv_product');

        Route::get('find/{id}', 'ReleaseMatchController@find')->name('release.find');
        Route::get('countries', 'ReleaseMatchController@countries')->name('release.find.countries');
        Route::post('search', 'ReleaseMatchController@search')->name('release.search');
        Route::post('autocomplete', 'ReleaseMatchController@autocomplete')->name('release.search.autocomplete');
    });
});
