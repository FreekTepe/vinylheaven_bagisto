<?php

namespace VinylHeaven\ReleaseImport\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MatchReleases extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'release_title' => 'string',
            'artist_name' => 'string',
            'label' => 'string',
            'cat_no' => 'string',
            'barcode' => 'string',
            'released' => 'string'
        ];
    }


    public function withValidator($validator)
    {
        if ($validator->fails()) {
            dd($validator->errors());
        }
    }
}
