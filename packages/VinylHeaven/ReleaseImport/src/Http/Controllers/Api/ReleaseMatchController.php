<?php
    namespace VinylHeaven\ReleaseImport\Http\Controllers\Api;

    use GuzzleHttp\Client;
    use Illuminate\Routing\Controller;
    use Illuminate\Validation\ValidationException;

    use Webkul\Theme\Facades\Themes;

    use VinylHeaven\ReleaseImport\Http\Requests\MatchReleases;
    use VinylHeaven\Category\Repositories\CategoryRepository;
    use VinylHeaven\Seller\Repositories\SellerRepository;
    use VinylHeaven\Checkout\Mail\ValidationExceptionOccured;
    use VinylHeaven\Response\Http\Resources\ResponseResource;
    use VinylHeaven\ReleaseImport\CsvImports\Resolvers\ElasticReleaseMatches;
    use VinylHeaven\ReleaseImport\CsvImports\Resolvers\ElasticReleaseSuggestion;
    use VinylHeaven\ReleaseImport\CsvImports\Resolvers\MongoReleaseMatch;
    use VinylHeaven\Product\Models\FetchedImageRelease;


    class ReleaseMatchController extends Controller
    {

        protected $categoryRepository;

        public function __construct(
            CategoryRepository $categoryRepository,
            SellerRepository $sellerRepository
        ) {
            Themes::set('default'); // NTS!: temp fix for VIN-52

            // JWT AUTH
            auth()->setDefaultDriver('customer');
            $this->middleware('auth.jwt');

            $this->categoryRepository = $categoryRepository;
            $this->sellerRepository = $sellerRepository;
        }

        public function find($id)
        {
            $seller = $this->sellerRepository->getSellerByUserId(auth()->user()->id);

            try {
                // Match a release in mongo db by id
                $match = new MongoReleaseMatch($id, $seller);

                return response()->json(['data' => $match->getReleaseData()], 200);
            } catch (\Throwable $th) {
                return (new ResponseResource('error', 'Oops...', $th->getMessage(), 3000, true))->response()->setStatusCode($this->status_code($th));
            }
        }

        // Try to make a match with the discogs mongodb data based on sellers search input.
        public function search(MatchReleases $request)
        {
            Themes::set('vinylheaven-seller-portal-theme');

            $data = $request->input();
            $seller = $this->sellerRepository->getSellerByUserId(auth()->user()->id);

            try {
                $releases_additional = [];
                $matches = new ElasticReleaseMatches($data, $seller);
                $releases = $matches->getReleases();
                $channel = core()->getCurrentChannel();

                foreach ($releases as $release_index => $release) {
                    $release_images = FetchedImageRelease::where('release_id', '=', $release['sku'])->get();
                    
                    if ($release_images->count() > 0) {
                        $release_primary_image = $release_images->where('image_type', '=', 'primarys')->first();

                        if (!$release_primary_image) {
                            $release_primary_image = $release_images->first();
                        }

                        $primary_image = $release_primary_image->getImageSourceSet();
                    } else {
                        $primary_image = FetchedImageRelease::getPlaceholderImageSourceSet();
                    }

                    $releases_additional[$release_index]['primary_image'] = $primary_image;
                    $releases_additional[$release_index]['release_formats'] = $this->releaseFormats($release['formats']);
                    $releases_additional[$release_index]['artist_name'] = isset($release['artists'][0]['name']) ? $release['artists'][0]['name'] : '';
                    $releases_additional[$release_index]['release_labels'] = $this->releaseLabels($release['labels']);
                    $releases_additional[$release_index]['seller_id'] = $seller->id;
                    $releases_additional[$release_index]['url_key'] = "";
                    $releases_additional[$release_index]['channel'] = $channel->code;
                    $releases_additional[$release_index]['channels'] = [0 => $channel->id];
                    $releases_additional[$release_index]['quest_checkout'] = true;
                    $releases_additional[$release_index]['locale'] = core()->getCurrentLocale()->code;
                    $releases_additional[$release_index]['price'] = 0.00;
                    $releases_additional[$release_index]['status'] = 1;
                    $releases_additional[$release_index]['type'] = "simple";
                    $releases_additional[$release_index]['visible_individually'] = 1;
                    $releases_additional[$release_index]['weight'] = 0.000;
                }

                $result_list_view = view('marketplace::shop.sellers.account.catalog.products.search.result.list', ['releases' => $releases, 'releases_additional' => $releases_additional])->render();

                return response()->json(['view' => $result_list_view], 200);
            } catch (\Throwable $th) {
                dd($th);
                return (new ResponseResource('error', 'Oops...', $th->getMessage(), 3000, true))->response()->setStatusCode($this->status_code($th));
            }
        }

        private function releaseLabels($labels)
        {
            $label_html_string = '';

            foreach ($labels as $label_index => $label) {
                if ($label_index == 0) {
                    $label_html_string = $label['name'];
                } else {
                    $label_html_string .= '<span class="my-1">|</span>' . $label['name'];
                }
            }

            return $label_html_string;
        }

        private function releaseFormats($formats)
        {
            $formats_formated_html = '';

            foreach ($formats as $format_index => $format) {
                $format_string = '';

                $format_string .= trim($format['name']);

                if (!empty($format['descriptions'])) {
                    $format_string .= ' (';
                    $format_string .= implode(", ", $format['descriptions']);
                    $format_string .= ')';
                }

                if (!empty($format['qty'])) {
                    if (($format['qty']) =='1') {
                    $format_string .= ' | ' . trim($format['qty']) . ' Disk';
                    }
                    else $format_string .= ' | ' . trim($format['qty']) . ' Disks';
                }

                if ($format_index == 0) {
                    $formats_formated_html .= $format_string;
                } else {
                    $formats_formated_html .= '<span class="mx-1">|</span>' . $format_string;
                }
            }

            return $formats_formated_html;
        }

        public function autocomplete(MatchReleases $request)
        {

            $data = request()->input();

            try {
                $suggestions = new ElasticReleaseSuggestion($data);

                return response()->json(['suggestions' => $suggestions->getSuggestions()], 200);
            } catch (\Throwable $th) {
                dd($th);
                return (new ResponseResource('error', 'Oops...', $th->getMessage(), 3000, true))->response()->setStatusCode($this->status_code($th));
            }
        }

        public function countries()
        {
            try {
                $client = new Client();

                $result = $client->request(
                    'POST', 
                    env('LUMEN_API') . "/releases/countries", 
                    [
                        'json' => array_filter($data)
                    ]
                );

                $status = $result->getStatusCode();

                if (!$status == 200) {
                    abort($status);
                }

                $contents = json_decode($result->getBody()->getContents(), true);
                return response()->json(['countries' => $contents['countries']], 200);
            } catch (\Throwable $th) {
                dd($th);
                return (new ResponseResource('error', 'Oops...', $th->getMessage(), 3000, true))->response()->setStatusCode($this->status_code($th));
            }
        }

        private function status_code($th)
        {
            if (method_exists($th, 'getStatusCode')) {
                return $th->getStatusCode();
            } else if (isset($th->status)) {
                return $th->status;
            }
            return 500;
        }
    }