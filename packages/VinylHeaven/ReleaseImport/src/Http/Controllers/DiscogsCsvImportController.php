<?php

namespace VinylHeaven\ReleaseImport\Http\Controllers;

use Webkul\Theme\Facades\Themes;
use Illuminate\Http\Request;
use VinylHeaven\Seller\Repositories\SellerRepository;
use VinylHeaven\ReleaseImport\Jobs\DiscogsCsvImportJob;
use VinylHeaven\ReleaseImport\Models\PendingCustomCsvImportMatches;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class DiscogsCsvImportController extends Controller
{

    protected $_config;

    public function __construct(
        SellerRepository $sellerRepository
    ) {
        Themes::set('vinylheaven-seller-portal-theme'); // NTS!: temp fix for VIN-52
        $this->_config = request('_config');
        $this->sellerRepository = $sellerRepository;
    }

    public function index()
    {
        $user = auth()->guard('customer')->user();
        $seller = $this->sellerRepository->getSellerByUserId($user->id);

        $custom_csv_files = $seller->media()->where('collection_name', 'customcsv')->paginate(10);

        return view($this->_config['view'], compact('custom_csv_files'));
    }

    public function import(Request $request)
    {

        $user = auth()->guard('customer')->user();
        $seller = $this->sellerRepository->getSellerByUserId($user->id);
        $file = $request->file('file');
        $valid_extension = ['csv', 'xls'];
        $fileDir = 'imported-products/files';

        $request->validate([
            'file' => 'required',
        ]);

        try {
            $delimiter = \CsvAnalyser::detectDelimiter($file);
        } catch (\Throwable $th) {
            session()->flash('error', 'CSV file is corrupted');
            return redirect()->back();
        }


        $f = fopen($file, 'r');
        $firstLine = fgets($f); //get first line of csv file
        fclose($f); // close file

        $headers = str_getcsv(trim($firstLine), ',', '"'); //parse to array

        // prepare array for laravel validator
        foreach ($headers as $key => $value) {
            $headers[$value] = $value;
            unset($headers[$key]);
        }

        $validator = \Validator::make($headers, [
            'release_id' => 'required',
            'price' => 'required',
            'comments' => 'required',
            'media_condition' => 'required',
            'sleeve_condition' => 'required'
        ]);

        if ($validator->fails()) {
            session()->flash('error', 'Required columns are missing');
            return redirect()->back();
        }

        if ((in_array($file->getClientOriginalExtension(), $valid_extension))) {

            $uploaded_file = $seller->addMedia($file)->toMediaCollection('dicogscsv');
            DiscogsCsvImportJob::dispatch($uploaded_file, $seller, $user, $delimiter);

            session()->flash('success', 'CSV is processing');
            return view($this->_config['view']);
        } else {
            session()->flash('error', 'Wrong file extension');
            return redirect()->back();
        }
    }

    public function customCsvFiles()
    {
        $user = auth()->guard('customer')->user();
        $seller = $this->sellerRepository->getSellerByUserId($user->id);

        $custom_csv_files = $seller->media()->where('collection_name', 'customcsv')->paginate(10);

        return view($this->_config['view'], compact('custom_csv_files'));
    }


    public function customCsvMatches($file_id)
    {
        $user = auth()->guard('customer')->user();
        $seller = $this->sellerRepository->getSellerByUserId($user->id);

        $pending = PendingCustomCsvImportMatches::where([
            ['seller_id', $seller->id],
            ['csv_id', $file_id]
        ])->paginate(10);

        return view($this->_config['view'], compact('pending'));
    }

    public function customCsvRowMatches($pending_id)
    {
        $pending_product = PendingCustomCsvImportMatches::findOrFail($pending_id);
        return view($this->_config['view'], compact('pending_product'));
    }


    public function delete($id)
    {
        $media = Media::findOrFail($id);
        $media->delete();
        return back();
    }
}
