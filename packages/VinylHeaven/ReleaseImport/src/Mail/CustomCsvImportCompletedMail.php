<?php

namespace VinylHeaven\ReleaseImport\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CustomCsvImportCompletedMail extends Mailable
{
    use Queueable, SerializesModels;

    public $successes;
    public $seller;
    public $errors;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($successes, $seller, $file)
    {
        $this->successes = $successes;
        $this->seller = $seller;
        $this->errors = count($seller->failed_csv_import_rows()->where('csv_id', $file->id)->get());
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('release-import::mail.discogs-csv-import-completed')
            ->from('info@vinylheaven.com', 'Vinyl Heaven')
            ->subject('Import report')
            ->replyTo('info@vinylheaven.com', 'Vinyl Heaven');
    }
}
