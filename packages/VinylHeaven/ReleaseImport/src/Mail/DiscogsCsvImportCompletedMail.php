<?php

namespace VinylHeaven\ReleaseImport\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use VinylHeaven\ReleaseImport\Models\DiscogsCsv;

class DiscogsCsvImportCompletedMail extends Mailable
{
    use Queueable, SerializesModels;

    public $seller;
    public $file;
    public $successes;
    public $errors;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($seller, $file)
    {
        $this->seller = $seller;
        $this->file = $file;

        $this->successes = DiscogsCsv::where([
            ['import_status', 200],
            ['seller_id', $this->seller->id],
            ['media_id', $this->file->id]
        ])->get()->count();

        $this->errors = DiscogsCsv::where([
            ['import_status', 300],
            ['seller_id', $this->seller->id],
            ['media_id', $this->file->id]
        ])->get()->count();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('release-import::mail.discogs-csv-import-completed')
            ->from('info@vinylheaven.com', 'Vinyl Heaven')
            ->subject('Import report')
            ->replyTo('info@vinylheaven.com', 'Vinyl Heaven');
    }
}
