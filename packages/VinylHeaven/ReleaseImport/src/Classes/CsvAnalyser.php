<?php

namespace VinylHeaven\ReleaseImport\Classes;

class CsvAnalyser
{
    /**
     * @param string $csvFile Path to the CSV file
     * @return string Delimiter
     */
    public function extractDelimiter($csvFile)
    {
        $delimiters = [";" => 0, "," => 0, "\t" => 0, "|" => 0];

        $handle = fopen($csvFile, "r");
        $firstLine = fgets($handle);
        fclose($handle);
        foreach ($delimiters as $delimiter => &$count) {
            $count = count(str_getcsv($firstLine, $delimiter));
        }

        return array_search(max($delimiters), $delimiters);
    }


    public function extractDiscogsCsvHeader($csvFile, $delimiter)
    {
        $f = fopen($csvFile, 'r');
        $firstLine = fgets($f); //get first line of csv file
        fclose($f); // close file

        $headers = str_getcsv(trim($firstLine), $delimiter, '"'); //parse to array

        // prepare array for laravel validator
        foreach ($headers as $key => $value) {
            $headers[$value] = $value;
            unset($headers[$key]);
        }

        return $headers;
    }

    public function hasDuplicate($file, $seller, $collection_name)
    {
        $seller_files = $seller->getMedia($collection_name);

        //get an array of filee hashes
        $hashed_seller_files = [];
        foreach ($seller_files as $seller_file) {
            array_push($hashed_seller_files, md5_file($seller_file->getPath()));
        }

        $hashed_new_file = md5_file($file->path());

        if (in_array($hashed_new_file, $hashed_seller_files)) {
            // duplicate found
            return true;
        } else {
            // no duplicate found
            return false;
        }
    }
}
