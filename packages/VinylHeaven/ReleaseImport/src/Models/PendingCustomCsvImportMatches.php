<?php

namespace VinylHeaven\ReleaseImport\Models;

use Illuminate\Database\Eloquent\Model;

class PendingCustomCsvImportMatches extends Model
{
    //
    protected $fillable = ["seller_id", "csv_id", "collection_type", "csv_row", "matches", "seller_data"];

    protected $casts = [
        'csv_row' => 'array',
        'seller_data' => 'array'
    ];
}
