<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropPolymorphicArtistTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('artist_solos');
        Schema::drop('artist_groups');
        Schema::drop('artist_group_artist_solo');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('artist_solos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('artist_id')->unique();
            $table->string('name');
            $table->string('realname')->nullable();
            $table->text('profile')->nullable();
            $table->text('data_quality')->nullable();
            $table->timestamps();
        });

        Schema::create('artist_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('artist_id')->unique();
            $table->string('name');
            $table->text('profile')->nullable();
            $table->text('data_quality')->nullable();
            $table->timestamps();
        });

        Schema::create('artist_group_artist_solo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('artist_group_id');
            $table->integer('artist_solo_id');
            $table->timestamps();
        });
    }
}
