<?php

namespace VinylHeaven\CustomAttributeType\Repositories;

use Webkul\Core\Eloquent\Repository;
use Illuminate\Support\Facades\DB;
use VinylHeaven\CustomAttributeType\Models\Artist;

class ArtistRepository extends Repository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return 'VinylHeaven\CustomAttributeType\Models\Artist';
    }


    public function createArtists($artists)
    {
        DB::beginTransaction();
        try {
            $collection = collect();
            foreach ($artists as $artist) {
                // check if artist already exists in db, in that case: skip create
                if (Artist::where('artist_id', $artist['artist_id'])->exists()) {
                    $collection->push(Artist::where('artist_id', $artist['artist_id'])->first());
                } else {
                    // artist with artist_id does not yet exist, create new artist
                    $collection->push($this->create($artist));
                }
            }
            DB::commit();

            // array of created artists
            return $collection;
        } catch (\Exception $e) {
            DB::rollBack();
            return false;
        }
    }

    // turn array of discogs artist ids into bagisto artist->ids
    // directly filters the artists it couldn't match.
    public function prepareArtistIds($artist_ids)
    {
        // build array of artist ids (mysql ids)
        $ids = array();

        // Check if artists 
        foreach ($artist_ids as $artist_id) {
            if ($artist = Artist::where('artist_id', $artist_id)->first()) {
                array_push($ids, $artist->id);
            }
        }
        return $ids;
    }
}
