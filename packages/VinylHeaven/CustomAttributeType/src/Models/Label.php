<?php

namespace VinylHeaven\CustomAttributeType\Models;

use Illuminate\Database\Eloquent\Model;
use VinylHeaven\Product\Models\Product;

class Label extends Model
{

    protected $fillable = [
        "label_id", 
        "name", 
        "cat_no"
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
