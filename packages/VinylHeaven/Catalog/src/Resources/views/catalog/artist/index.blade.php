@extends('shop::layouts.master-new')

@inject ('searchHelper', 'VinylHeaven\Search\Helpers\Search')

@section('page_title')
    {{ $artist->name }}
@stop

@section('content-wrapper')
    <section class="container-fluid container-lg container-xl container-artist">
        <div class="row mb-5">
            <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                <!-- ARTIST IMAGE -->
                <img class="img-fluid" src="{{ asset('themes/vinylexpress/assets/images/Default-Product-Image.png') }}" alt="artist: {{ $artist->name }}" />
            </div>

            <div class="col-12 col-sm-6 col-md-8 col-lg-9">
                <!-- ARTIST INFO -->
                <div class="artist-info">
                    <div class="row">
                        <div class="col-12">
                            <div class="info-name">
                                {{ $artist->name }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="heading">
            <div class="heading-text medium">
                {{ $artist->name }} records for sale
            </div>
        </div>

        <div class="artist-products">
            @foreach($artist_products_for_sale as $product_index => $product)
                <div class="artist-product" id="artist-product-{{ $product_index }}" data-toggle="collapse" data-target="#artist-products-for-sale-{{ $product_index }}" role="button" aria-expanded="false" aria-controls="artist-products-for-sale-{{ $product_index }}">
                    <div class="row justify-content-start align-items-center">
                        <div class="col-auto">
                            {{ ($product_index + 1) }}.
                        </div>

                        <div class="col-auto product-image">
                            <img src="{{ $product->getPrimaryImageThumb() }}" alt="{{ $product->name }}" />
                        </div>

                        <div class="col-auto mr-auto">
                            <div class="row flex-column">
                                <div class="col-auto product-name mb-1">
                                    {{ $product->name }}
                                </div>

                                <div class="col-auto mt-1">
                                    <div class="row flex-row">
                                        <div class="col-auto">
                                            <strong>Formats:</strong>
                                        </div>

                                        @foreach($product->formatsFormated() as $format_index => $format)
                                            @if ($format_index == 0)
                                                <div class="col-auto product-format first">
                                                    {{ $format }}
                                                </div>
                                            @else
                                                <div class="col-auto product-format">
                                                    {{ $format }}
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-auto">
                            {{ $product->marketplace_product->count() }} items for sale.
                        </div>

                        <div class="col-auto">
                            <i class="fa fa-chevron-down"></i>
                        </div>
                    </div>
                </div>

                <div class="collapse products-for-sale has-transition my-3" id="artist-products-for-sale-{{ $product_index }}" data-trigger="#artist-product-{{ $product_index }}">
                    @foreach($product->marketplace_product as $marketplace_product_index => $marketplace_product)
                        <div class="row justify-content-center align-items-center mb-2">
                            <div class="col-auto">
                                <div class="row flex-column justify-content-start">
                                    <div class="col-auto mb-1">
                                        <strong>{{ $marketplace_product->seller->shop_title }}</strong>
                                    </div>

                                    <div class="col-auto mt-1">
                                        <div class="row flex-row justify-content-start align-items-center">
                                            <div class="col-auto">
                                                reviews:
                                            </div>

                                            <div class="col-auto">
                                                {{ $marketplace_product->seller->getTotalReviews() }}
                                            </div>

                                            <div class="col-auto">
                                                rating:
                                            </div>

                                            <div class="col-auto">
                                                @for ($i = 1; $i <= $marketplace_product->seller->getAverageRating(); $i++)
                                                    <i class="fa fa-star"></i>
                                                @endfor

                                                @for ($i = 1; $i <= ($marketplace_product->seller->max_rating - $marketplace_product->seller->getAverageRating()); $i++)
                                                    <i class="fa fa-star-o"></i>
                                                @endfor
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-auto">
                                <div class="row flex-column justify-content-center align-items-center">
                                    <div class="col-auto mb-1">
                                        <strong>Seller notes:</strong>
                                    </div>

                                    <div class="col-auto mt-1">{{ $marketplace_product->description }}</div>
                                </div>
                            </div>

                            <div class="col-auto">
                                <div class="row flex-column justify-content-center align-items-center">
                                    <div class="col-auto mb-1">
                                        <strong>Media condition</strong>
                                    </div>

                                    <div class="col-auto mt-1">
                                        <div class="condition-grading-circle product-grading d-inline-flex justify-content-center align-items-center">
                                            <span class="circle-text {{ $searchHelper->getConditionGradingBackgroundClass($product->getConditionGrading($marketplace_product->condition)) }}">{{ $product->getConditionGrading($marketplace_product->condition) }}</span>
                                            <span class="product-grading-text">{{ $product->getConditionText($marketplace_product->condition) }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-auto mr-auto">
                                <div class="row flex-column justify-content-center align-items-center">
                                    <div class="col-auto mb-1">
                                        <strong>Sleeve condition</strong>
                                    </div>

                                    <div class="col-auto mt-1">
                                        <div class="condition-grading-circle product-grading d-inline-flex justify-content-center align-items-center">
                                            <span class="circle-text {{ $searchHelper->getConditionGradingBackgroundClass($product->getSleeveConditionGrading($marketplace_product->sleeve_condition)) }}">{{ $product->getSleeveConditionGrading($marketplace_product->sleeve_condition) }}</span>
                                            <span class="product-grading-text">{{ $product->getSleeveConditionText($marketplace_product->sleeve_condition) }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-auto">
                                {{ core()->currency($marketplace_product->price) }}
                            </div>

                            <div class="col-auto">
                                <form action="{{ route('cart.add', $marketplace_product->product_id) }}" method="POST" class="form-inline">
                                    @csrf
                                    <input type="hidden" name="product_id" value="{{ $marketplace_product->product_id }}">
                                    <input type="hidden" name="quantity" value="1">
                                    <input type="hidden" name="seller_info[product_id]" value="{{ $marketplace_product->id }}">
                                    <input type="hidden" name="seller_info[seller_id]" value="{{ $marketplace_product->marketplace_seller_id }}">
                                    <input type="hidden" name="seller_info[is_owner]" value="0">

                                    <button type="submit" class="button button-bag" style="min-width:auto;"></button>
                                </form>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
    </section>
@endsection

@push('scripts')
    <script type="text/javascript" src="{{ asset('themes/vinylheaven-theme/assets/dist/js/components/artist.js') }}"></script>
@endpush