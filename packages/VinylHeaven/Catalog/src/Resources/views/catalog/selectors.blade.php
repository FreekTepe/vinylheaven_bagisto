<div class="col-12">
    <h4>Artists</h4>
</div>

<div class="form-row">
    <div class="form-group col-12">
        <input type="text" class="form-control" id="artists-search-input" placeholder="Search artists" />
    </div>
</div>

<div class="isotope-artists">
    @foreach($artists as $artist)
        <div class="col-12 isotope-item">{{ $artist->name }}</div>
    @endforeach
</div>

