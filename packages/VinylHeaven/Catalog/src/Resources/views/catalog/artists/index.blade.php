@extends('shop::layouts.master-new')

@section('page_title')
    Artists
@stop

@section('content-wrapper')
    <section class="container-fluid container-lg container-xl container-artists">
        <div class="artists-tab-header">
            <div class="heading heading-artists">
                <div class="heading-text">
                    Artists
                </div>
            </div>

            <div class="row artists-tabs-row">
                <div class="col-12">
                    <ul class="nav nav-tabs" id="artists-tabs" role="tablist">
                        @foreach($artists as $artist_group_index => $artist_group)
                            @php
                                $aria_selected = false;
                                $tab_active = '';
                                if ($artist_group_index == 'a') {
                                    $aria_selected = true;
                                    $tab_active = 'active';
                                }
                            @endphp

                            <li class="nav-item" role="presentation">
                                <a class="nav-link {{ $tab_active }}" id="artists-{{ $artist_group_index }}-tab" data-toggle="tab" href="#artists-{{ $artist_group_index }}-pane" role="tab" aria-controls="artists-{{ $artist_group_index }}-pane" aria-selected="{{ $aria_selected }}">{{ strtoupper($artist_group_index) }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>

        <div class="row artists-tab-panes-row">
            <div class="col-12">
                <div class="tab-content" id="artists-tabs-content">
                    @foreach($artists as $artist_group_index => $artist_group)
                        @php
                            $tab_pane_active = '';
                            if ($artist_group_index == 'a') {
                                $tab_pane_active = 'show active';
                            }
                        @endphp

                        <div class="tab-pane fade {{ $tab_pane_active }}" id="artists-{{ $artist_group_index }}-pane" role="tabpanel" aria-labelledby="artists-{{ $artist_group_index }}-tab">
                            @foreach($artist_group as $artist)
                                <div class="row">
                                    <div class="col-12">
                                        <a href="{{ route('catalog.catalog.artist.index', $artist->id) }}">{{ $artist->name }}</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection