@extends('shop::layouts.master-new')

@section('page_title')
    Catalog
@endsection

@inject ('toolbarHelper', 'VinylHeaven\Tools\Helpers\Toolbar')
@inject ('GridListView', 'VinylHeaven\Tools\Helpers\GridListView')
@inject ('searchHelper', 'VinylHeaven\Search\Helpers\Search')

@php
    $view=request()->input('view');
@endphp

@section('content-wrapper')
    <section class="container-fluid container-lg container-xl container-catalog">
        @include('shop::tools.list-grid-view-toolbar', ['toolbar_config' => $toolbar_config])

        @if (!$products_count)
            <div class="row">
                <div class="col-12 py-4">
                    <h1>No products found.</h1>
                </div>
            </div>
        @else
            <div class="row">
                <div class="col-12 col-lg-3">
                    @include('shop::search.filters')
                </div>

                <div class="col-12 col-lg-9">
                    @if ($GridListView->isGridView())
                        <div class="result-grid {{ $toolbarHelper->getGridViewDisplay() }} py-4">
                            <div class="row justify-content-start align-items-center">
                                @foreach ($products as $marketplace_product)
                                    @php
                                        $product = $marketplace_product->product;
                                        $product_marketplace_products = $product->getMarketplaceProducts();
                                        $product_artist_name = $product->getArtistName();
                                    @endphp

                                    @if (isset($product->url_key) && !empty($product->url_key))
                                        <div class="col-12 col-sm-6 col-md-4 col-lg-4 mb-3">
                                            @include('shop::products.card')
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    @endif

                    @if ($GridListView->isListView())
                        <div class="result-list py-4">
                            @foreach ($products as $marketplace_product_index => $marketplace_product)
                                @php
                                    $product = $marketplace_product->product;
                                    $product_marketplace_products = $product->getMarketplaceProducts();
                                    $product_artist_name = $product->getArtistName();
                                @endphp

                                @if (isset($product->url_key) && !empty($product->url_key))
                                    @include('shop::products.list')
                                @endif
                            @endforeach
                        </div>
                    @endif

                    <div class="col-12">
                        @include('shop::search.pagination')
                    </div>
                </div>
            </div>
        @endif
    </section>
@endsection