<?php

namespace VinylHeaven\Catalog\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

use VinylHeaven\Catalog\Http\Middleware\Currency;

class CatalogServiceProvider extends ServiceProvider
{
    public function register()
    {
        
    }

    public function boot(Router $router)
    {
        $this->loadRoutesFrom(__DIR__ . '/../Http/routes.php');

        $this->loadViewsFrom(__DIR__ . '/../Resources/views', 'catalog');

        // $router->aliasMiddleware('currency', Currency::class);
    }
}
