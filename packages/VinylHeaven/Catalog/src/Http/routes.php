<?php

// Route::group(['middleware' => ['web', 'locale', 'theme', 'currency']], function () {
//     Route::namespace('\VinylHeaven\Catalog\Http\Controllers')->group(function () {
//         Route::get('/catalog', 'CatalogController@index')->defaults('_config', [
//             'view' => 'catalog::catalog.index'
//         ])->name('catalog.catalog.index');

//         Route::get('/catalog/artists', 'CatalogController@artists')->defaults('_config', [
//             'view' => 'catalog::catalog.artists.index'
//         ])->name('catalog.catalog.artists.index');

//         Route::get('/catalog/artist/{id}', 'CatalogController@artist')->defaults('_config', [
//             'view' => 'catalog::catalog.artist.index'
//         ])->name('catalog.catalog.artist.index');

//         // Route::get('')->defaults()->name();
//     });
// });

Route::group(['middleware' => ['theme']], function () {
    Route::namespace('\VinylHeaven\Catalog\Http\Controllers')->group(function () {
        Route::get('/catalog', 'CatalogController@index')->defaults('_config', [
            'view' => 'catalog::catalog.index'
        ])->name('catalog.catalog.index');
        // Route::get('/catalog', function(){
        //     dump('---');
        // });

        Route::get('/catalog/artists', 'CatalogController@artists')->defaults('_config', [
            'view' => 'catalog::catalog.artists.index'
        ])->name('catalog.catalog.artists.index');

        Route::get('/catalog/artist/{id}', 'CatalogController@artist')->defaults('_config', [
            'view' => 'catalog::catalog.artist.index'
        ])->name('catalog.catalog.artist.index');

        // Route::get('')->defaults()->name();
    });
});