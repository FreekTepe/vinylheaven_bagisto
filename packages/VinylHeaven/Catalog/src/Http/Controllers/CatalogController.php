<?php

namespace Vinylheaven\Catalog\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;

use Webkul\Marketplace\Http\Controllers\Shop\Controller;
use Webkul\Category\Repositories\CategoryRepository;
use Webkul\Product\Repositories\ProductRepository;

use VinylHeaven\DataLayer\Facades\DataLayer;

use VinylHeaven\Tools\Facades\GridListView;

class CatalogController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * All of the sort, order and limit attributes
     */
    public $limit;
    public $offset;
    public $sort;
    public $order;
    public $condition;
    public $sleeve_condition;
    public $format;
    public $genre;
    public $min_price;
    public $max_price;

    public function __construct()
    {
        $this->_config = request('_config');
    }

    /**
     * Show all of our products that we currently have in our catalog
     */
    public function index()
    {
        // set all of the sort and order params
        $this->limit = request()->has('limit') ? request()->get('limit') : 12;
        $this->offset = request()->has('page') ? ($this->limit * request()->get('page')) : 0;
        $this->sort = request()->has('sort') ? 'marketplace_products.' . request()->get('sort') : 'marketplace_products.name';
        $this->sort = ($this->sort == 'marketplace_products.name') ? 'product_flat.name' : $this->sort; 
        $this->order = request()->has('order') ? request()->get('order') : 'asc';
        $this->condition = request()->has('condition') ? request()->get('condition') : null;
        $this->sleeve_condition = request()->has('sleeve_condition') ? request()->get('sleeve_condition') : null;
        $this->format = request()->has('format') ? request()->get('format') : null;
        $this->genre = request()->has('genre') ? request()->get('genre') : null;
        $this->min_price = request()->has('price_min') ? floatval(str_replace(',', '.', request()->get('price_min'))) : null;
        $this->max_price = request()->has('price_max') ? floatval(str_replace(',', '.', request()->get('price_max'))) : null;

        // get the needed catalog data as marketplace products
        $products = DataLayer::getCatalogIndexData($this->limit, $this->offset, $this->sort, $this->order, $this->condition, $this->sleeve_condition, $this->format, $this->genre, $this->min_price, $this->max_price);
// dd($products);
        // set the current products counts
        $products_count = DataLayer::getProductsTotalCount();
        $toolbar_config = [
            'has_result_counter' => false,
            'has_seller_info' => false,
            'toolbar_limits' => [24, 48, 72, 96]
        ];
// dump($products);
        // render the view with the view params
        return view($this->_config['view'])
            ->with('products', $products)
            ->with('products_count', $products_count)
            ->with('toolbar_config', $toolbar_config)
            ->with('route_name', Route::currentRouteName());


        
        
        
        
        dd('CatalogController::index', $catalog_data);


        // foreach ($catalog_data as $product) {
        //     if ($product->marketplace_products->count) {
        //         dd($product);
        //     }
        // }

        // set all of the sort and order params
        $this->limit = request()->has('limit') ? request()->get('limit') : 24;
        $this->offset = request()->has('page') ? ($this->limit * request()->get('page')) : 0;
        $this->sort = request()->has('sort') ? request()->get('sort') : 'created_at';
        $this->sort = ($this->sort == 'name') ? 'products.name' : 'products.' . $this->sort; 
        $this->order = request()->has('order') ? request()->get('order') : 'asc';

        // get the product model for quering the products
        // $products = app('VinylHeaven\Product\Models\Product');

        // set the current products counts
        $products_count = 3127;
        $toolbar_config = [
            'has_result_counter' => false,
            'has_seller_info' => false
        ];

        // query the product with the sort and order params
        // $products = $products->offset($this->offset)
        //                 ->limit($this->limit)
        //                 ->orderBy($this->sort, $this->order)
        //                 ->get();
        $products = $catalog_data;

        // render the view with the view params
        return view($this->_config['view'])
            ->with('products', $products)
            ->with('products_count', $products_count)
            ->with('toolbar_config', $toolbar_config)
            ->with('route_name', Route::currentRouteName());
    }

    /**
     * Show all of the artists where we have products off in our catalog
     */
    public function artists()
    {
        // get the artist model for quering later
        $artists = app('VinylHeaven\CustomAttributeType\Models\Artist');

        // get the artists alphabatical array
        $artists_alphabatic = $this->getArtistsAlphabaticArray();

        // get all of the artists sorted by name
        $all_artists = $artists->orderBy('name', 'asc')->get();

        // loop thru all of the artists and set them to the correct alphabatic array entry
        foreach($all_artists as $artist) {
            if (isset($artists_alphabatic[strtolower($artist->name[0])])) {
                $artists_alphabatic[strtolower($artist->name[0])][] = $artist;
            } else {
                $artists_alphabatic['other'][] = $artist;
            }
        }

        // render the view with view params
        return view($this->_config['view'])
            ->with('artists', $artists_alphabatic);
    }

    /**
     * Show the artist information and all of the product of this artist that we have in our catalog
     * 
     * @param Integer $id | The artist unique id
     */
    public function artist(int $id)
    {
        //get the artist
        $artist = app('VinylHeaven\CustomAttributeType\Models\Artist')->find($id);
        
        // set the array where all of the records from this artist we have for sale(has marketplace products)
        $artist_products_for_sale = [];

        // loop thru the artist product for detemine if we have products for sale
        foreach ($artist->products as $product) {
            if ($product->marketplace_product->count()) {
                $artist_products_for_sale[] = $product;
            }
        }

        // dd('requested artist id:', $id, $artist, $artist->products, $artist_products_for_sale[0]->marketplace_product[0]->seller->getAverageRating());

        // render the view
        return view($this->_config['view'])
            ->with('artist', $artist)
            ->with('artist_products_for_sale', $artist_products_for_sale);
    }

    /**
     * Show product or category view. If neither category nor product matches, abort with code 404.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\View\View|\Exception
     */
    public function productOrCategory(Request $request)
    {
        $slugOrPath = trim($request->getPathInfo(), '/');

        if (preg_match('/^([a-z0-9-]+\/?)+$/', $slugOrPath)) {
            $categoryRepository = app('Webkul\Category\Repositories\CategoryRepository');
            if ($category = $categoryRepository->findByPath($slugOrPath)) {
                return view($this->_config['category_view'], compact('category'));
            }

            $productRepository = app('Webkul\Product\Repositories\ProductRepository');
            if ($product = $productRepository->findBySlug($slugOrPath)) {
                $product->product = $productRepository->where('id', $product->product_id)->first();
                $customer = auth()->guard('customer')->user();
                $product_view_config = json_decode(json_encode([
                    'is_seller_product' => false,
                    'aria_expanded' => true,
                    'has_from_price' => true,
                    'classes' => [
                        'product_other_sellers' => 'collapse-open',
                        'product_other_sellers_collapse' => 'show'
                    ],
                    'other_products_count_text' => $product->product->marketplace_product()->count()
                ]));

                return view('marketplace::shop.sellers.product.index', compact('product', 'customer', 'product_view_config'));
            }
        }

        abort(404);
    }

    public function sellerProduct($url, $slug)
    {
        if (Cache::store('redis')->has($url.$slug)) {
            return view($this->_config['view'], Cache::store('redis')->get($url.$slug));
        } else {
            $seller = app('Webkul\Marketplace\Repositories\SellerRepository')->findByUrlOrFail($url);
            $baseProduct = app('Webkul\Product\Repositories\ProductRepository')->findBySlugOrFail($slug);
            $product = app('Webkul\Marketplace\Repositories\ProductRepository')->where([['product_id', "=", $baseProduct->product_id], ['marketplace_seller_id', "=", $seller->id]])->first();
            $product_view_config = json_decode(json_encode([
                'is_seller_product' => true,
                'aria_expanded' => false,
                'has_from_price' => false,
                'classes' => [
                    'product_other_sellers' => '',
                    'product_other_sellers_collapse' => ''
                ],
                'other_products_count_text' => ($product->product->marketplace_product()->count() - 1) . ' more'
            ]));
            
            $toCache = compact('seller', 'product', 'product_view_config');

            Cache::store('redis')->put($url.$slug, $toCache, now()->addMinutes(10));

            return view($this->_config['view'], $toCache);
        }
    }

    private function getArtistsAlphabaticArray()
    {
        return [
            'a' => [],
            'b' => [],
            'c' => [],
            'd' => [],
            'e' => [],
            'f' => [],
            'g' => [],
            'h' => [],
            'i' => [],
            'j' => [],
            'k' => [],
            'l' => [],
            'm' => [],
            'n' => [],
            'o' => [],
            'p' => [],
            'q' => [],
            'r' => [],
            's' => [],
            't' => [],
            'u' => [],
            'v' => [],
            'w' => [],
            'x' => [],
            'y' => [],
            'z' => [],
            'other' => []
        ];
    }
}
