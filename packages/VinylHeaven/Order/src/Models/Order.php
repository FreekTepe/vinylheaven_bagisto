<?php

namespace VinylHeaven\Order\Models;

use VinylHeaven\Message\Models\Message;
use Webkul\Sales\Models\Order as OrderBaseModel;

class Order extends OrderBaseModel
{
    public function messages()
    {
        return $this->hasMany(Message::class);
    }
}
