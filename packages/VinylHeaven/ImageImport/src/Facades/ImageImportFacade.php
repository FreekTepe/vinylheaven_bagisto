<?php

namespace VinylHeaven\ImageImport\Facades;

use Illuminate\Support\Facades\Facade;

class ImageImportFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'imageimport';
    }
}
