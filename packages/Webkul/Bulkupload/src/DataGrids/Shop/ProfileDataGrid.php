<?php

namespace Webkul\Bulkupload\DataGrids\Shop;

use DB;
use Webkul\Ui\DataGrid\DataGrid;

/**
 * Order Data Grid class
 *
 * @author Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class ProfileDataGrid extends DataGrid
{
    /**
     * @var integer
     */
    protected $index = 'id';

    public function prepareQueryBuilder()
    {
        $queryBuilder = DB::table('marketplace_bulkupload_dataflowprofile')
                ->leftJoin('attribute_families', 'marketplace_bulkupload_dataflowprofile.attribute_family_id', '=', 'attribute_families.id')
                ->select('marketplace_bulkupload_dataflowprofile.id', 'marketplace_bulkupload_dataflowprofile.profile_name','attribute_families.name', 'marketplace_bulkupload_dataflowprofile.created_at')
                ->where('seller_id', auth()->guard('customer')->user()->id);

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
        $this->addColumn([
            'index' => 'profile_name',
            'label' => trans('bulkupload::app.shop.bulk-upload.profile-name'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'name',
            'label' => trans('bulkupload::app.shop.bulk-upload.attribute-set-name'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'created_at',
            'label' => trans('bulkupload::app.shop.sellers.account.profile.date'),
            'type' => 'datetime',
            'sortable' => true,
            'searchable' => false,
            'filterable' => true
        ]);
    }

    public function prepareActions() {

        $this->addAction([
            'type' => 'Edit',
            'method' => 'GET', // use GET request only for redirect purposes
            'route' => 'marketplace.bulkupload.profile.edit',
            'icon' => 'icon pencil-lg-icon'
        ]);

        $this->addAction([
            'type' => 'Delete',
            'method' => 'GET', // use GET request only for redirect purposes
            'route' => 'marketplace.bulkupload.profile.delete',
            'icon' => 'icon trash-icon'
        ]);

        $this->enableAction = true;
    }


    public function prepareMassActions()
    {
        $this->addMassAction([
            'type' => 'delete',
            'label' => 'Delete',
            'action' => route('marketplace.bulkupload.profile.massDelete'),
            'method' => 'DELETE'
        ]);
    }
}