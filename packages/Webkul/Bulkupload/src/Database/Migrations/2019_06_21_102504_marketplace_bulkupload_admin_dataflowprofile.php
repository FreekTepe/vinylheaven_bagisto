<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MarketplaceBulkuploadAdminDataflowprofile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketplace_bulkupload_admin_dataflowprofile', function (Blueprint $table) {
            $table->increments('id');
            $table->string('profile_name');
            $table->integer('attribute_family_id')->unsigned();
            $table->foreign('attribute_family_id', 'mp_bulkupload_admin_foreign_attribute_family_id')->references('id')->on('attribute_families')->onDelete('cascade');
            $table->string('seller_id')->references('id')->on('customers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketplace_bulkupload_admin_dataflowprofile');
    }
}
