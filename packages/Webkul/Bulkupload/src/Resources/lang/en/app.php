<?php

return [
    'shop' => [
        'bulk-upload' => [
            'set-seller' => 'Set Seller',
            'bulk-upload' => 'Bulk Upload Product',
            'bulk-upload-dataflow-profile' => 'Bulk Upload Dataflow Profile',
            'upload-files' => 'Upload Files',
            'run-profile' => 'Run Profile',
            'download-sample' => 'Download Sample',
            'attribute-set' => 'Attribute Set',
            'data-flow-profile' => 'Data Flow Profile',
            'file' => 'CSV/XLS file',
            'image' => 'Image Zip file',
            'is_downloadable' => 'Is Downloadable',
            'run-profile-in-popup-btn' => 'Run Profile in Popup',
            'profile-name' => 'Profile Name',
            'save-profile' => 'Save Profile',
            'update-profile' => 'Update Profile',
            'delete-profiles' => 'Delete Profile',
            'attribute-set-name' => 'Attribute Family Name',
            'date' => 'Date',
            'add-mapping' => 'Add Mapping',
            'import-products' => 'Import Products',
            'seller-name' => 'Seller Name'
        ],

        'message' => [
            'record-not-found' => 'Record Not Found',
            'image-not-found' => 'Image Not Found',
            'finished' => 'Finished Importing Data',
            'data-profile-not-selected' => 'Data Flow Profile not selected',
            'file-format-error' => 'Invalid File Extension',
            'duplicate-error' => 'Url Key must be unique, duplicate url key :identifier at row :position.',
        ],

        'profile' => [
            'success' => 'Successfully Saved',
            'error' => 'Failed',
            'edit-success' => 'Profile successfully updated',
            'edit-fail' => 'Error! Profile Cannot Be Updated, Please Try Again Later',
            'delete' => 'Deleted',
            'profile-deleted'=>'Dataflow profile is deleted successfully',
            'admin-grid' => 'Admin Profile Grid',
            'seller-grid' => 'Seller Profile Grid'
        ],

        'layouts' => [
            'become-seller' => 'Become Seller',
            'marketplace' => 'Marketplace',
            'profile' => 'Profile',
            'dashboard' => 'Dashboard',
            'products' => 'Products',
            'orders' => 'Orders',
            'reviews' => 'Reviews',
            'transactions' => 'Transactions',
            'sell' => 'Sell'
        ],

        'marketplace' => [
            'title' => 'Turn Your Passion Into a Business',
            'open-shop-label' => 'Open Shop Now',
            'features' => 'Attracting Features',
            'features-info' => 'Want to start an online business? Before any decision, please check our unbeatable features.',
            'popular-sellers' => 'Popular Sellers',
            'setup-title' => 'Really Easy to Setup',
            'setup-info' => 'Setting an ecommerce store and customization is really very easy.',
            'setup-1' => 'Create An Account',
            'setup-2' => 'Add Your Shop Details',
            'setup-3' => 'Customize Your Profile',
            'setup-4' => 'Add Products',
            'setup-5' => 'Start Selling Your Products',
            'open-shop-info' => 'Open your online shop with us and get explore the new world with more then millions of shoppers.',
        ],

        'sellers' => [
            'account' => [
                'signup' => [
                    'want-to-be-seller' => 'Do You Want To Become A Seller / Vendor?',
                    'shop_url' => 'Shop Url',
                    'yes' => 'Yes',
                    'no' => 'No',
                    'shop_url_available' => 'Shop url is available.',
                    'shop_url_not_available' => 'Shop url is unavailable.'
                ],

                'profile' => [
                    'create-title' => 'Become Seller',
                    'edit-title' => 'Edit Seller Profile',
                    'date' => 'Created At',
                    'url' => 'Shop Url',
                    'save-btn-title' => 'Save',
                    'view-collection-page' => 'View Collection page',
                    'view-seller-page' => 'View Seller Page',
                    'waiting-for-approval' => 'Waiting for approval from admin',
                    'general' => 'General',
                    'shop_title' => 'Shop Title',
                    'tax_vat' => 'Tax/VAT Number',
                    'phone' => 'Contact Number',
                    'address1' => 'Address 1',
                    'address2' => 'Address 2',
                    'city' => 'City',
                    'state' => 'State',
                    'country' => 'Country',
                    'postcode' => 'Post Code',
                    'media' => 'Media',
                    'logo' => 'Logo',
                    'banner' => 'Banner',
                    'add-image-btn-title' => 'Add Image',
                    'about' => 'About Shop',
                    'social_links' => 'Social Links',
                    'twitter' => 'Twitter Id',
                    'facebook' => 'Facebook Id',
                    'youtube' => 'Youtube Id',
                    'instagram' => 'Instagram Id',
                    'skype' => 'Skype Id',
                    'linked_in' => 'Linked In',
                    'pinterest' => 'Pinterest Id',
                    'policies' => 'Policies',
                    'return_policy' => 'Return Policy',
                    'shipping_policy' => 'Shipping Policy',
                    'privacy_policy' => 'privacy Policy',
                    'seo' => 'SEO',
                    'meta_title' => 'Meta Title',
                    'meta_description' => 'Meta Description',
                    'meta_keywords' => 'meta Keywords',
                ],

                'dashboard' => [
                    'title' => 'Dashboard'
                ],

                'catalog' => [
                    'products' => [
                        'title' => 'Products',
                        'create' => 'Create',
                        'create-new' => 'Create New',
                        'search-title' => 'Search Products',
                        'create-title' => 'Add Product',
                        'assing-title' => 'Sell Yours',
                        'assing-edit-title' => 'Edit Product',
                        'edit-title' => 'Edit Product',
                        'save-btn-title' => 'Save',
                        'assign-info' => 'Search for products, if product exist then sell your with different price or :create_link',
                        'search' => 'Search',
                        'search-term' => 'Product Name ...',
                        'no-result-found' => 'Products not found with this name.',
                        'enter-search-term' => 'Type atleast 3 characters',
                        'searching' => 'Searching ...',
                        'general' => 'General',
                        'product-condition' => 'Product Condition',
                        'new' => 'New',
                        'old' => 'Old',
                        'price' => 'Price',
                        'description' => 'Description',
                        'images' => 'Images',
                        'inventory' => 'Inventory',
                        'variations' => 'Variations',
                        'id' => 'Id',
                        'sku' => 'SKU',
                        'name' => 'Name',
                        'price' => 'Price',
                        'quantity' => 'Quantity',
                        'is-approved' => 'Is Approved',
                        'yes' => 'Yes',
                        'no' => 'No',
                        'delete' => 'Delete'
                    ]
                ],

                'sales' => [
                    'orders' => [
                        'title' => 'Orders',
                        'view-title' => 'Order #:order_id',
                        'info' => 'Information',
                        'invoices' => 'Invoices',
                        'shipments' => 'Shipments',
                        'placed-on' => 'Placed On',
                        'status' => 'Status',
                        'customer-name' => 'Customer Name',
                        'email' => 'Email',
                        'inventory-source' => 'Inventory Source',
                        'carrier-title' => 'Carrier Title',
                        'tracking-number' => 'Tracking Number',
                        'id' => 'Id',
                        'base-total' => 'Base Total',
                        'grand-total' => 'Grand Total',
                        'order-date' => 'Order Date',
                        'channel-name' => 'Channel Name',
                        'status' => 'Status',
                        'processing' => 'Processing',
                        'completed' => 'Completed',
                        'canceled' => 'Canceled',
                        'closed' => 'Closed',
                        'pending' => 'Pending',
                        'pending-payment' => 'Pending Payment',
                        'fraud' => 'Fraud',
                        'billed-to' => 'Billed To'
                    ],

                    'invoices' => [
                        'title' => 'Invoices',
                        'create-title' => 'Create Invoice',
                        'create' => 'Create',
                        'order-id' => 'Order Id',
                        'qty-ordered' => 'Qty Ordered',
                        'qty-to-invoice' => 'Qty to Invoice',
                        'product-name' => 'Product Name'
                    ],

                    'shipments' => [
                        'title' => 'Shipments',
                        'create-title' => 'Create Shipment',
                        'create' => 'Create',
                        'order-id' => 'Order Id',
                        'carrier-title' => 'Carrier Title',
                        'tracking-number' => 'Tracking Number',
                        'source' => 'Source',
                        'select-source' => 'Please Select Source',
                        'product-name' => 'Product Name',
                        'qty-ordered' => 'Qty Ordered',
                        'qty-to-ship' => 'Qty to Ship',
                        'available-sources' => 'Available Sources',
                        'qty-available' => 'Qty Available'
                    ],

                    'transactions' => [
                        'title' => 'Transactions',
                        'view-title' => 'Transaction #:transaction_id',
                        'id' => 'Id',
                        'total' => 'Total',
                        'transaction-id' => 'Transaction Id',
                        'comment' => 'Comment',
                        'order-id' => 'Order #:order_id',
                        'commission' => 'Commission',
                        'seller-total' => 'Seller Total',
                        'created-at' => 'Created At',
                        'payment-method' => 'Payment Method',
                        'total-sale' => 'Total Sale',
                        'total-payout' => 'Total Payout',
                        'remaining-payout' => 'Remaining Payout',
                    ]
                ],

                'reviews' => [
                    'title' => 'Reviews',
                    'id' => 'Id',
                    'customer-name' => 'Customer name',
                    'rating' => 'Rating',
                    'comment' => 'Comment'
                ]
            ],

            'profile' => [
                'count-products' => ':count products',
                'contact-seller' => 'Contact Seller',
                'total-rating' => ':total_rating Ratings & :total_reviews Reviews',
                'visit-store' => 'Visit Store',
                'about-seller' => 'About Seller',
                'member-since' => 'Member Since :date',
                'all-reviews' => 'All Reviews',
                'return-policy' => 'Return Policy',
                'shipping-policy' => 'Shipping Policy',
                'by-user-date' => '- By :name :date',
                'name' => 'Name',
                'email' => 'Email',
                'subject' => 'Subject',
                'query' => 'Query',
                'submit' => 'Submit'
            ],

            'reviews' => [
                'title' => 'Reviews - :shop_title',
                'create-title' => 'Write Review - :shop_title',
                'write-review' => 'Write Review',
                'total-rating' => ':total_rating Ratings & :total_reviews Reviews',
                'view-more' => 'View More',
                'write-review' => 'Write Review',
                'by-user-date' => '- By :name :date',
                'rating' => 'Rating',
                'comment' => 'Comment'
            ],

            'products' => [
                'title' => 'Products - :shop_title'
            ],

            'mails' => [
                'contact-seller' => [
                    'subject' => 'Customer Query - :subject',
                    'dear' => 'Dear :name',
                    'info' => 'I would like to ask a question, please revert back to me as soon as possible.',
                    'query' => 'Query',
                    'thanks' => 'Thanks'
                ]
            ]
        ],

        'products' => [
            'popular-products' => 'Popular Products',
            'all-products' => 'All Products',
            'sold-by' => 'Sold By : :url',
            'seller-count' => ':count More Seller(s)',
            'more-sellers' => 'More Sellers',
            'seller-total-rating' => ':avg_rating (:total_rating ratings)',
            'add-to-cart' => 'Add To Cart',
            'new' => 'New',
            'used' => 'Used',
            'out-of-stock' => 'Out of Stock'
        ],
    ],

    'admin' => [
        'bulk-upload' => [
            'set-seller' => 'Set Seller',
            'bulk-upload' => 'Bulk Upload Product',
            'bulk-upload-dataflow-profile' => 'Bulk Upload Data flow Profile',
            'upload-files' => 'Upload Files',
            'run-profile' => 'Run Profile',
            'download-sample' => 'Download Sample',
            'attribute-set' => 'Attribute Set',
            'data-flow-profile' => 'Data Flow Profile',
            'file' => 'CSV/XLS file',
            'image' => 'Image Zip file',
            'is_downloadable' => 'Is Downloadable',
            'run-profile-in-popup-btn' => 'Run Profile in Popup',
            'mass-delete-success' => 'All the selected index of data flow profile have been deleted successfully'
        ],
        'layouts' => [
            'marketplace' => 'Marketplace',
            'sellers' => 'Sellers',
            'products' => 'Products',
            'seller-reviews' => 'Seller Reviews',
            'orders' => 'Orders',
            'transactions' => 'Transactions',
        ],

        'acl' => [
            'marketplace' => 'Marketplace',
            'sellers' => 'Sellers',
            'products' => 'Products',
            'reviews' => 'Seller Reviews'
        ],

        'system' => [
            'marketplace' => 'Marketplace',
            'settings' => 'Settings',
            'general' => 'General',
            'commission-per-unit' => 'Commission Per Unit (In Percentage)',
            'seller-approval-required' => 'Seller Approval Required',
            'product-approval-required' => 'Product Approval Required',
            'can-create-invoice' => 'Seller Can Create Invoice',
            'can-create-shipment' => 'Seller Can Create Shipment',
            'yes' => 'Yes',
            'no' => 'No',
            'landing-page' => 'Landing Page',
            'page-title' => 'Page Title',
            'show-banner' => 'Show Banner',
            'banner' => 'Banner',
            'banner-content' => 'Banner Content',
            'show-features' => 'Show Features',
            'feature-heading' => 'Feature Heading',
            'feature-info' => 'Feature Information',
            'feature-icon-1' => 'Feature Icon 1',
            'feature-icon-label-1' => 'Feature Icon Label 1',
            'feature-icon-2' => 'Feature Icon 2',
            'feature-icon-label-2' => 'Feature Icon Label 2',
            'feature-icon-3' => 'Feature Icon 3',
            'feature-icon-label-3' => 'Feature Icon Label 3',
            'feature-icon-4' => 'Feature Icon 4',
            'feature-icon-label-4' => 'Feature Icon Label 4',
            'feature-icon-5' => 'Feature Icon 5',
            'feature-icon-label-5' => 'Feature Icon Label 5',
            'feature-icon-6' => 'Feature Icon 6',
            'feature-icon-label-6' => 'Feature Icon Label 6',
            'feature-icon-7' => 'Feature Icon 7',
            'feature-icon-label-7' => 'Feature Icon Label 7',
            'feature-icon-8' => 'Feature Icon 8',
            'feature-icon-label-8' => 'Feature Icon Label 8',
            'show-popular-sellers' => 'Show Popular Sellers',
            'open-shop-button-label' => 'Open Shop Button Label',
            'about-marketplace' => 'About Marketplace',
            'show-open-shop-block' => 'Show Open Shop Block',
            'open-shop-info' => 'Open Shop Information',
        ],

        'sellers' => [
            'title' => 'Sellers',
            'id' => 'Id',
            'customer-name' => 'Customer Name',
            'customer-email' => 'Customer Email',
            'created-at' => 'Created At',
            'is-approved' => 'Is Approved',
            'approved' => 'Approved',
            'un-approved' => 'Unapproved',
            'approve' => 'Approve',
            'unapprove' => 'Unapprove',
            'delete' => 'Delete',
            'update' => 'Update',
            'delete-success-msg' => 'Seller deleted successfully.',
            'mass-delete-success' => 'Selected sellers deleted successfully.',
            'mass-update-success' => 'Selected sellers updated successfully.'
        ],

        'orders' => [
            'title' => 'Orders',
            'manage-title' => 'Manage Seller\'s Order',
            'order-id' => 'Order Id',
            'seller-name' => 'Seller Name',
            'sub-total' => 'Sub Total',
            'grand-total' => 'Grand Total',
            'commission' => 'Commission',
            'seller-total' => 'Seller Total',
            'total-paid' => 'Paid',
            'remaining-total' => 'Remaining',
            'invoice-pending' => 'Invoice Pending',
            'seller-total-invoiced' => 'Invoiced',
            'order-date' => 'Order Date',
            'channel-name' => 'Channel Name',
            'status' => 'Status',
            'processing' => 'Processing',
            'completed' => 'Completed',
            'canceled' => 'Canceled',
            'closed' => 'Closed',
            'pending' => 'Pending',
            'pending-payment' => 'Pending Payment',
            'fraud' => 'Fraud',
            'billed-to' => 'Billed To',
            'withdrawal-requested' => 'Withdrawal Requested',
            'pay' => 'Pay',
            'already-paid' => 'Already Paid',
            'yes' => 'Yes',
            'no' => 'No',
            'pay-seller' => 'Pay Seller',
            'comment' => 'Comment',
            'payment-success-msg' => 'Payment has been successfully done for this seller',
            'order-not-exist' => 'Order not exist',
            'no-amount-to-paid' => 'No amount left to paid to this seller.'
        ],

        'transactions' => [
            'title' => 'Transactions',
            'id' => 'Id',
            'seller-name' => 'Seller Name',
            'total' => 'Total',
            'transaction-id' => 'Transaction Id',
            'comment' => 'Comment',
            'order-id' => 'Order #:order_id',
            'commission' => 'Commission',
            'seller-total' => 'Seller Total',
            'created-at' => 'Created At',
            'payment-method' => 'Payment Method',
            'total-sale' => 'Total Sale',
            'total-payout' => 'Total Payout',
            'remaining-payout' => 'Remaining Payout',
        ],

        'products' => [
            'title' => 'Products',
            'product-id' => 'Product Id',
            'seller-name' => 'Seller Name',
            'sku' => 'SKU',
            'name' => 'Name',
            'price' => 'Price',
            'quantity' => 'Quantity',
            'status' => 'Status',
            'is-approved' => 'Is Approved',
            'approved' => 'Approved',
            'un-approved' => 'Unapproved',
            'approve' => 'Approve',
            'unapprove' => 'Unapprove',
            'delete' => 'Delete',
            'update' => 'Update',
            'delete-success-msg' => 'Product deleted successfully.',
            'mass-delete-success' => 'Selected products deleted successfully.',
            'mass-update-success' => 'Selected products updated successfully.'
        ],

        'reviews' => [
            'title' => 'Reviews',
            'id' => 'Id',
            'comment' => 'Comment',
            'rating' => 'Rating',
            'customer-name' => 'Customer Name',
            'seller-name' => 'Seller Name',
            'status' => 'Status',
            'approved' => 'Approved',
            'un-approved' => 'Unapproved',
            'approve' => 'Approve',
            'unapprove' => 'Unapprove',
            'update' => 'Update',
        ],

        'response' => [
            'create-success' => ':name created successfully.',
            'update-success' => ':name updated successfully.',
            'delete-success' => ':name deleted successfully.',
        ]
    ],

    'mail' => [
        'seller' => [
            'welcome' => [
                'subject' => 'Seller Request Notification',
                'dear' => 'Dear :name',
                'info' => 'Thanks for registering as seller, your account is under review. We will inform you about your account approval via mail.'
            ],

            'approval' => [
                'subject' => 'Seller Approval Notification',
                'dear' => 'Dear :name',
                'info' => 'This is to inform you that you are approved as seller. Click below button to login.',
                'login' => 'Login'
            ],
        ],

        'sales' => [
            'order' => [
                'subject' => 'New Order Notification',
                'greeting' => 'You have a new Order :order_id placed on :created_at',
            ],

            'invoice' => [
                'subject' => 'New Invoice Notification',
                'greeting' => 'You have a new Invoice for order :order_id placed on :created_at',
            ],

            'shipment' => [
                'heading' => 'Shipment Confirmation!',
                'subject' => 'New Shipment Notification',
                'greeting' => 'You have a new Shipment for order :order_id placed on :created_at',
            ]
        ],

        'product' => [
                'subject' => 'Product Approval Notification',
                'dear' => 'Dear :name',
                'info' => 'This is to inform you that your product <b>:name</b> has been approved.'
        ]
    ]
];