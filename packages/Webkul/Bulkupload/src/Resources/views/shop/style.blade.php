<style>
    .icon {
        background-size: cover;
        display: inline-block;
    }

    progress {
        margin: 15px 0px 15px 34px;
        width: 70%;
        height: 15px;
    }

    .check-accent {
        width: 22px;
        height: 18px;
        background-image: url("../../packages/Webkul/Bulkupload/src/Resources/assets/images/check-accent.svg");
    }

    .icon-crossed {
        width: 18px;
        height: 15px;
        background-image: url("../../packages/Webkul/Bulkupload/src/Resources/assets/images/Icon-Crossed.svg");
    }

    .cross-accent {
        width: 22px;
        height: 18px;
        background-image: url("../../packages/Webkul/Bulkupload/src/Resources/assets/images/cross-accent.svg");
    }

    .finish-icon {
        width: 22px;
        height: 18px;
        background-image: url("../../packages/Webkul/Bulkupload/src/Resources/assets/images/finish.svg");
    }
</style>
