<?php

namespace Webkul\Bulkupload\Repositories;

use Webkul\Core\Eloquent\Repository;
use Illuminate\Support\Facades\Event;


/**
 * Seller Invoice Reposotory
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class MarketplaceProductRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'Webkul\Bulkupload\Contracts\MarketplaceProduct';
    }

    public function create(array $data)
    {
        Event::fire('marketplace.catalog.product.create.before');

        $sellerProduct = parent::create(array_merge($data, [
                'marketplace_seller_id' => $data['marketplace_seller_id'],
                'is_approved' => core()->getConfigData('marketplace.settings.general.product_approval_required') ? 0 : 1
            ]));

        foreach ($sellerProduct->product->variants as $baseVariant) {
            parent::create([
                    'parent_id' => $sellerProduct->id,
                    'product_id' => $baseVariant->id,
                    'is_owner' => 1,
                    'marketplace_seller_id' => $data['marketplace_seller_id'],
                    'is_approved' => core()->getConfigData('marketplace.settings.general.product_approval_required') ? 0 : 1
                ]);
        }

        Event::fire('marketplace.catalog.product.create.after', $sellerProduct);

        return $sellerProduct;
    }
}