<?php

Route::group(['middleware' => ['web']], function () {

    Route::prefix('admin/marketplace/tablerate/')->group(function () {

        Route::group(['middleware' => ['admin']], function () {

            //Table Super Sets Shipping
            Route::get('/super-sets', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Admin\SupersetController@index')->defaults('_config', [
                'view' => 'marketplace_tablerate_shipping::admin.super_sets.index'
            ])->name('admin.marketplace.tablerate.super_sets.index');

            //Add Superset
            Route::get('/super-sets/create', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Admin\SupersetController@create')->defaults('_config', [
                'view' => 'marketplace_tablerate_shipping::admin.super_sets.create'
            ])->name('admin.marketplace.tablerate.super_sets.create');

            //Store superset
            Route::post('/super-sets/create', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Admin\SupersetController@store')->defaults('_config', [
                'redirect' => 'admin.marketplace.tablerate.super_sets.index'
            ])->name('admin.marketplace.tablerate.super_sets.store');

            //Edit Superset
            Route::get('/super-sets/edit/{id}', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Admin\SupersetController@edit')->defaults('_config', [
                'view' => 'marketplace_tablerate_shipping::admin.super_sets.edit'
            ])->name('admin.marketplace.tablerate.super_sets.edit');

            //Update Superset
            Route::put('/super-sets/edit/{id}', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Admin\SupersetController@update')->defaults('_config', [
                'redirect' => 'admin.marketplace.tablerate.super_sets.index'
            ])->name('admin.marketplace.tablerate.super_sets.update');

            //Mass Update Superset
            Route::post('sellers/massupdate', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Admin\SupersetController@massUpdate')->defaults('_config', [
                'redirect' => 'admin.marketplace.tablerate.super_sets.index'
            ])->name('admin.marketplace.tablerate.super_sets.massupdate');

            //delete Superset
            Route::post('/super-sets/delete/{id}', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Admin\SupersetController@destroy')->name('admin.marketplace.tablerate.super_sets.delete');

             //mass delete Superset
             Route::post('/super-sets/mass-delete/', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Admin\SupersetController@massDestroy')->defaults('_config', [
                'redirect' => 'admin.marketplace.tablerate.super_sets.index'
            ])->name('admin.marketplace.tablerate.super_sets.mass-delete');


            //Superset rates
            Route::get('/super-set-rates', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Admin\SupersetRateController@index')->defaults('_config', [
                'view' => 'marketplace_tablerate_shipping::admin.super_set_rates.index'
            ])->name('admin.marketplace.tablerate.super_set_rates.index');

            //Create New Superset Rate
            Route::get('/super-set-rates/create', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Admin\SupersetRateController@create')->defaults('_config', [
                'view' => 'marketplace_tablerate_shipping::admin.super_set_rates.create'
            ])->name('admin.marketplace.tablerate.super_set_rates.create');

            //Store New Superset Rate
            Route::post('/super-set-rates/create', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Admin\SupersetRateController@store')->defaults('_config', [
                'redirect' => 'admin.marketplace.tablerate.super_set_rates.index'
            ])->name('admin.marketplace.tablerate.super_set_rates.store');

            //Edit Superset Rates
            Route::get('/super-set-rates/edit/{id}', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Admin\SupersetRateController@edit')->defaults('_config', [
                'view' => 'marketplace_tablerate_shipping::admin.super_set_rates.edit'
            ])->name('admin.marketplace.tablerate.super_set_rates.edit');

            //Update Superset Rate
            Route::post('/super-set-rates/edit/{id}', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Admin\SupersetRateController@update')->defaults('_config', [
                'redirect' => 'admin.marketplace.tablerate.super_set_rates.index'
            ])->name('admin.marketplace.tablerate.super_set_rates.update');

            //Delete one Superset Rate
            Route::post('/super-set-rates/destroy/{id}', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Admin\SupersetRateController@destroy')
            ->name('admin.marketplace.tablerate.super_set_rates.delete');

            //Mass Delete Superset Rate
            Route::post('/super-set-rates/mass-delete', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Admin\SupersetRateController@massDestroy')->defaults('_config', [
                'redirect' => 'admin.marketplace.tablerate.super_set_rates.index'
            ])->name('admin.marketplace.tablerate.super_set_rates.mass_delete');


            //Shipping rate
            Route::get('/rates', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Admin\ShippingRateController@index')->defaults('_config', [
                'view' => 'marketplace_tablerate_shipping::admin.rates.index'
            ])->name('admin.marketplace.tablerate.rates.index');

            //Add Shipping rate
            Route::get('/rates/create', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Admin\ShippingRateController@create')->defaults('_config', [
                'view' => 'marketplace_tablerate_shipping::admin.rates.create'
            ])->name('admin.marketplace.tablerate.rates.create');

            //Edit Shipping Rates
            Route::get('/rates/edit/{id}', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Admin\ShippingRateController@edit')->defaults('_config', [
                'view' => 'marketplace_tablerate_shipping::admin.rates.edit'
            ])->name('admin.marketplace.tablerate.rates.edit');

            //Update Shipping Rate
            Route::post('/rates/edit/{id}', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Admin\ShippingRateController@update')->defaults('_config', [
                'redirect' => 'admin.marketplace.tablerate.rates.index'
            ])->name('admin.marketplace.tablerate.rates.update');

            //Delete one Shipping Rate
            Route::post('/shipping-rates/destroy/{id}', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Admin\ShippingRateController@destroy')
            ->name('admin.marketplace.tablerate.rates.delete');

            //Mass Delete Shipping rate
            Route::post('/rates/mass-delete', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Admin\ShippingRateController@massDestroy')->defaults('_config', [
                'redirect' => 'admin.marketplace.tablerate.rates.index'
            ])->name('admin.marketplace.tablerate.rates.mass-delete');

            //File Shipping rate
            Route::post('/rates/import', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Admin\ShippingRateController@import')->defaults('_config', [
                'redirect' => 'admin.marketplace.tablerate.rates.index'
            ])->name('admin.marketplace.tablerate.rates.import');

            //DataGrid Export
            Route::post('/rates/export', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Admin\ExportController@export')->name('marketplace.admin.datagrid.export');

            //Download Sample File CSV
            Route::get('/rates/sample-download', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Admin\ShippingRateController@sampleDownload')->defaults('_config', [
                'view' => 'marketplace_tablerate_shipping::admin.rates.index'
            ])->name('admin.marketplace.tablerate.rates.sample_download');
        });

    });

});