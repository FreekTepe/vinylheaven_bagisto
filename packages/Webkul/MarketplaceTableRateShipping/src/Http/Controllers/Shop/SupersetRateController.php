<?php

namespace Webkul\MarketplaceTableRateShipping\Http\Controllers\Shop;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Webkul\Marketplace\Repositories\SellerRepository;
use Webkul\MarketplaceTableRateShipping\Repositories\SupersetRateRepository;
use Webkul\MarketplaceTableRateShipping\Repositories\SupersetRepository;

/**
 * Marketplace Superset Rate page controller
 *
 * @author    Naresh Verma <naresh.verma327@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */


class SupersetRateController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * supersetRateRepository object
     *
     * @var array
    */
    protected $supersetRateRepository;

    /**
     * sellerRepository object
     *
     * @var array
    */
    protected $sellerRepository;

    /**
     * supersetRepository object
     *
     * @var array
    */
    protected $supersetRepository;

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Marketplace\Repositories\SellerRepository $seller
     * @return void
     */
    public function __construct(
        SupersetRateRepository $supersetRateRepository,
        SellerRepository $sellerRepository,
        SupersetRepository $supersetRepository
    )

    {
        $this->_config = request('_config');

        $this->supersetRateRepository = $supersetRateRepository;

        $this->sellerRepository = $sellerRepository;

        $this->supersetRepository = $supersetRepository;
    }

    /**
     * Display the specified resource.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $isSeller = $this->sellerRepository->isSeller(auth()->guard('customer')->user()->id);

        if (! $isSeller)
            return redirect()->route('marketplace.account.seller.create');

        $shippingMethods = $this->supersetRepository->with('sellerSuperset')->all();

        if (core()->getConfigData('sales.carriers.mptablerate.active')) {

            return view($this->_config['view'])->with('shippingMethods',$shippingMethods);
        } else {
            return redirect()->route('customer.profile.index');
        }
    }

    /**
     * create the specified resource.
     *
     */
    public function create()
    {
        $shippingMethods = $this->supersetRepository->with('sellerSuperset')->all();

        return view($this->_config['view'])->with('shippingMethods',$shippingMethods);
    }

    /**
     * store the specified resource.
     *
     */
    public function store(Request $request)
    {
        $shippingMethods = $this->supersetRepository->with('sellerSuperset')->all();

        $sellerId = $this->sellerRepository->findOneByField('customer_id', auth()->guard('customer')->user()->id);

        $data = request()->all();

        $this->validate(request(),[
            'price_from' => 'numeric|required',
            'price_to' => 'numeric|required|min:'.$data['price_from'],
            'shipping_type' => 'string|required',
            'shipping_method' => 'string|required',
            'price' => 'numeric|required',
        ]);

        $data['marketplace_seller_id'] = $sellerId->id;

        foreach ($shippingMethods as $method) {
            if ($method->id == $data['shipping_method']) {
                $data['marketplace_superset_id'] = $method->id;
            }
        }

        $superSets = $this->supersetRateRepository->all();

        foreach ($superSets as $superSet) {
            if ($sellerId->id == $superSet->marketplace_seller_id
                && $data['marketplace_superset_id'] == $superSet->marketplace_superset_id) {

                if ($data['price_from'] >= $superSet->price_from
                    && $data['price_from'] <= $superSet->price_to) {
                    session()->flash('error',trans('marketplace_tablerate_shipping::app.admin.shipping-rates.range-exist',
                    ['name' => 'Superset Rate']));

                    return back();
                }

                if ($data['price_from'] <= $superSet->price_from
                    && $data['price_to'] >= $superSet->price_from
                    && $data['price_to'] <= $superSet->price_to) {
                    session()->flash('error',trans('marketplace_tablerate_shipping::app.admin.shipping-rates.range-exist',
                    ['name' => 'Superset Rate']));

                    return back();
                }

                if ($data['price_from'] <= $superSet->price_from
                    && $data['price_to'] >= $superSet->price_to) {

                    session()->flash('error',trans('marketplace_tablerate_shipping::app.admin.shipping-rates.range-exist',
                    ['name' => 'Superset Rate']));

                    return back();
                }

                if ($data['price_from'] == $superSet->price_from
                    &&  $data['price_to'] == $superSet->price_to) {

                    session()->flash('error',trans('marketplace_tablerate_shipping::app.admin.shipping-rates.range-exist',
                    ['name' => 'Superset Rate']));

                    return back();
                }
            }

        }

        $superSet = $this->supersetRateRepository->create($data);

        session()->flash('success',trans('marketplace_tablerate_shipping::app.admin.shipping-rates.create-success',
        ['name' => 'Superset Rate']));

        return redirect()->route($this->_config['redirect']);
    }

    public function edit($id)
    {
        $shippingMethods = $this->supersetRepository->with('sellerSuperset')->all();

        $superset = $this->supersetRateRepository->findOrFail($id);

        return view($this->_config['view'])->with('superSetData', $superset)->with('shippingMethods',$shippingMethods);
    }

    /**
     * Update supersetRate.
     *
     * @param  int  $id
     *
     */
    public function update($id)
    {
        $shippingMethods = $this->supersetRepository->with('sellerSuperset')->all();

        $sellerId = $this->sellerRepository->findOneByField('customer_id', auth()->guard('customer')->user()->id);

        $superSetData = $this->supersetRateRepository->findOrFail($id);

        $data = request()->all();

        if ($data['shipping_type'] == 'Free') {
            $data['price'] = 0;
        }

        if ($data['price'] == 0) {
            $data['shipping_type'] ='Free';
        }

        $this->validate(request(), [
            'price_from' => 'numeric|required',
            'price_to' => 'numeric|required|min:'.$data['price_from'],
            'shipping_type' => 'string|required',
            'shipping_method' => 'string|required',
            'price' => 'numeric|required'
        ]);

        foreach ($shippingMethods as $method) {
            if ($method->id == $data['shipping_method']) {
                $data['marketplace_superset_id'] = $method->id;
            }
        }

        $superSets = $this->supersetRateRepository->all();

        foreach ($superSets as $superSet) {
            if ($sellerId->id == $superSet->marketplace_seller_id
                && (string)$id != (string)$superSet->id
                && $data['marketplace_superset_id'] == $superSet->marketplace_superset_id) {

                if ($data['price_from'] >= $superSet->price_from
                    && $data['price_from'] <= $superSet->price_to) {

                    session()->flash('error',trans('marketplace_tablerate_shipping::app.admin.shipping-rates.range-exist',
                    ['name' => 'Superset Rate']));

                    return back();
                }

                if ($data['price_from'] <= $superSet->price_from
                    && !($data['price_to'] <= $superSet->price_from)) {

                    session()->flash('error',trans('marketplace_tablerate_shipping::app.admin.shipping-rates.range-exist',
                    ['name' => 'Superset Rate']));

                    return back();
                }

                if ($data['price_from'] >= $superSet->price_from
                    && $data['price_from'] <= $superSet->price_to
                    && !($data['price_from'] <= $superSet->price_from)
                    && !($data['price_to'] <= $superSet->price_to)) {

                    session()->flash('error',trans('marketplace_tablerate_shipping::app.admin.shipping-rates.range-exist',
                    ['name' => 'Superset Rate']));

                    return back();
                }

                if ($data['price_from'] == $superSet->price_from
                    &&  $data['price_to'] == $superSet->price_to) {

                    session()->flash('error',trans('marketplace_tablerate_shipping::app.admin.shipping-rates.range-exist',
                    ['name' => 'Superset Rate']));

                    return back();
                }
            }

        }

        $this->supersetRateRepository->update($data, $id);

        session()->flash('success', trans('marketplace_tablerate_shipping::app.admin.shipping-rates.update-success',
        ['name' => 'Superset Rate']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Delete SupersetRate.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->supersetRateRepository->delete($id);

        session()->flash('success', trans('marketplace_tablerate_shipping::app.admin.shipping-rates.delete-success',
        ['name' => 'Superset Rate']));

        return response()->json(['message' => true], 200);
    }

    /**
     * Mass Delete Superset Rate
     *
     * @return response
     */
    public function massDestroy()
    {
        $supersetIds = explode(',', request()->input('indexes'));

        foreach ($supersetIds as $supersetId) {
            $this->supersetRateRepository->delete($supersetId);
        }

        session()->flash('success', trans('marketplace_tablerate_shipping::app.admin.superset-rates.mass-delete-success'));

        return redirect()->route($this->_config['redirect']);
    }

}
