<?php

namespace Webkul\MarketplaceTableRateShipping\Http\Controllers\Admin;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Webkul\Marketplace\Repositories\SellerRepository;
use Webkul\MarketplaceTableRateShipping\Repositories\SupersetRateRepository;
use Webkul\MarketplaceTableRateShipping\Repositories\ShippingRateRepository;
use Webkul\MarketplaceTableRateShipping\Repositories\SupersetRepository;
use Webkul\Marketplace\Repositories\ProductRepository;

class SupersetRateController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * supersetRateRepository object
     *
     * @var array
    */
    protected $supersetRateRepository;

    /**
     * shippingRateRepository object
     *
     * @var array
    */
    protected $shippingRateRepository;

    /**
     * sellerRepository object
     *
     * @var array
    */
    protected $sellerRepository;

    /**
     * supersetRepository object
     *
     * @var array
    */
    protected $supersetRepository;

    /**
     * cartItemRepository object
     *
     * @var array
    */
    protected $cartItemRepository;

    /**
     * productRepository object
     *
     * @var array
    */
    protected $productRepository;



    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Marketplace\Repositories\SuperShippingSetRepository $superShippingSet
     * @return void
     */
    public function __construct(
        SupersetRateRepository $supersetRateRepository,
        SellerRepository $sellerRepository ,
        SupersetRepository $supersetRepository,
        ShippingRateRepository $shippingRateRepository,
        ProductRepository $productRepository
    )

    {
        $this->_config = request('_config');

        $this->supersetRateRepository = $supersetRateRepository;

        $this->sellerRepository = $sellerRepository;

        $this->supersetRepository = $supersetRepository;

        $this->shippingRateRepository = $shippingRateRepository;

        $this->productRepository = $productRepository;
    }

    /**
     * Method to populate the supersetRate page.
     *
     * @return Mixed
     */
    public function index()
    {
        return view($this->_config['view']);
    }

    /**
     * Add New  Shipping Set
     *
     * @return response
     */
    public function create()
    {
        $sellerData = [];

        $shippingMethods = $this->supersetRepository->with('sellerSuperset')->all();

        $customerData = $this->sellerRepository->get();
        if (count($customerData) > 0) {
            $customerData = $this->sellerRepository->all()[0]->with('customer')->get();
        }

        foreach ($customerData as $sellers) {
            $sellerData[] = $sellers->customer;
        }

        return view($this->_config['view'])->with('sellerData', $sellerData)->with('shippingMethods', $shippingMethods);
    }

    /**
     * Store The supersetRate
     *
     * @return response
     */
    public function store()
    {
        $data = request()->all();

        if ($data['shipping_type'] == 'Free') {
            $data['price'] = 0;
        }

        if ($data['price'] == 0) {
            $data['shipping_type'] = 'Free';
        }

        $this->validate(request(),[
            'price_from' => 'numeric|required',
            'price_to' => 'numeric|required|min:'.$data['price_from'],
            'shipping_type' => 'string|required',
            'shipping_method' => 'string|required',
            'seller_name' => 'string|required',
            'price' => 'numeric|required',
        ]);

        if ($data['seller_name'] == '0') {
            $data['marketplace_seller_id'] = null;
        }

        $shippingMethods = $this->supersetRepository->with('sellerSuperset')->all();

        $customerData = $this->sellerRepository->get();

        if (count($customerData) > 0) {
            $customerData = $this->sellerRepository->all()[0]->with('customer')->get();

            foreach ($customerData as $sellers) {
                $sellerData[] = $sellers->customer;
            }

            foreach ($sellerData as $sellerId) {
                if ($sellerId['id'] == $data['seller_name'] ) {
                    $data['marketplace_seller_id'] = $sellerId['id'];
                }
            }
        }

        foreach ($shippingMethods as $method) {
            if ($method->id == $data['shipping_method']) {
                $data['marketplace_superset_id'] = $method->id;
            }
        }

        $superSets = $this->supersetRateRepository->all();

        foreach ($superSets as $superSet) {
            if ($data['marketplace_seller_id'] == null) {
                if ($data['price_from'] < $superSet->price_from
                    && $data['price_to'] > $superSet->price_to
                    && $data['marketplace_superset_id'] == $superSet->marketplace_superset_id
                    && $superSet->marketplace_seller_id == null) {
                    session()->flash('erro',trans('marketplace_tablerate_shipping::app.admin.superset-rates.range-exist',
                    ['name' => 'Superset Rate']));

                    return back();
                }

                if ($data['price_from'] > $superSet->price_from
                     && $data['price_to'] < $superSet->price_to
                     && $data['marketplace_superset_id'] == $superSet->marketplace_superset_id
                     && $superSet->marketplace_seller_id == null) {
                    session()->flash('success',trans('marketplace_tablerate_shipping::app.admin.superset-rates.range-exist',
                    ['name' => 'Superset Rate']));

                    return back();
                }

                if (isset($superSet)) {
                    if ($data['price_from'] < $superSet->price_from
                        && $data['price_to'] > $superSet->price_to
                        && $data['marketplace_superset_id'] == $superSet->marketplace_superset_id
                        && $superSet->marketplace_seller_id == null) {
                        session()->flash('success',trans('marketplace_tablerate_shipping::app.admin.superset-rates.range-exist',
                        ['name' => 'Superset Rate']));

                        return back();
                    }
                }

                if ($data['price_from'] <= $superSet->price_from
                    && $data['price_to'] >= $superSet->price_to
                    && $data['marketplace_superset_id'] == $superSet->marketplace_superset_id
                    && $superSet->marketplace_seller_id == null) {
                    session()->flash('error',trans('marketplace_tablerate_shipping::app.admin.shipping-rates.range-exist',
                    ['name' => 'Superset Rate']));

                    return back();
                }

                if ($data['price_from'] >= $superSet->price_from
                    && $data['price_from'] <= $superSet->price_to
                    && $data['marketplace_superset_id'] == $superSet->marketplace_superset_id
                    && $superSet->marketplace_seller_id == null) {
                        session()->flash('error',trans('marketplace_tablerate_shipping::app.admin.shipping-rates.range-exist',
                        ['name' => 'Superset Rate']));

                        return back();
                }

            } else {
                if ($data['marketplace_seller_id'] == $superSet->marketplace_seller_id
                    && $superSet->marketplace_superset_id == $data['marketplace_superset_id']) {
                    if ($data['price_from'] == $superSet->price_from
                        && $data['price_to'] == $superSet->price_to) {
                        session()->flash('error',trans('marketplace_tablerate_shipping::app.admin.superset-rates.range-exist',
                        ['name' => 'Superset Rate']));

                        return back();
                    }

                    if ($data['price_from'] >= $superSet->price_from
                        && $data['price_from'] <= $superSet->price_to
                        && $data['marketplace_superset_id'] == $superSet->marketplace_superset_id) {

                        session()->flash('error',trans('marketplace_tablerate_shipping::app.admin.shipping-rates.range-exist',
                        ['name' => 'Superset Rate']));

                        return back();
                    }

                    if ($data['price_from'] < $superSet->price_from
                        && $data['price_to'] > $superSet->price_to) {

                        session()->flash('error',trans('marketplace_tablerate_shipping::app.admin.superset-rates.range-exist',
                        ['name' => 'Superset Rate']));

                        return back();
                    }

                    if ($data['price_from'] > $superSet->price_from
                        && $data['price_to'] < $superSet->price_to) {

                        session()->flash('error',trans('marketplace_tablerate_shipping::app.admin.superset-rates.range-exist',
                        ['name' => 'Superset Rate']));

                        return back();
                    }

                    if ($data['price_from'] <= $superSet->price_from
                    && $data['price_to'] >= $superSet->price_to
                    && $data['marketplace_superset_id'] == $superSet->marketplace_superset_id
                    && $superSet->marketplace_seller_id == $data['marketplace_seller_id']) {
                    session()->flash('error',trans('marketplace_tablerate_shipping::app.admin.shipping-rates.range-exist',
                    ['name' => 'Superset Rate']));

                    return back();
                    }

                    if (isset ($superSet)) {
                        if ($data['price_from'] < $superSet->price_from
                        && $data['price_to'] > $superSet->price_to) {
                            session()->flash('error',trans('marketplace_tablerate_shipping::app.admin.superset-rates.range-exist',
                            ['name' => 'Superset Rate']));

                            return back();
                        }
                    }
                }
            }
        }

        $superSet = $this->supersetRateRepository->create($data);

        session()->flash('success',trans('marketplace_tablerate_shipping::app.admin.superset-rates.create-success', ['name' => 'Superset Rate']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Edit superseRate .
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $shippingMethods = $this->supersetRepository->with('sellerSuperset')->all();

        $customerData = $this->sellerRepository->all()[0]->with('customer')->get();

        foreach ($customerData as $sellers) {
            $sellerData[] = $sellers->customer;
        }

        $superSet = $this->supersetRateRepository->findOrFail($id);

        return view($this->_config['view'])->with('superSet', $superSet)->with('sellerData', $sellerData)->with('shippingMethods', $shippingMethods);
    }

     /**
     * Update supersetRate.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $shippingMethods = $this->supersetRepository->with('sellerSuperset')->all();

        $customerData = $this->sellerRepository->all()[0]->with('customer')->get();

        foreach ($customerData as $sellers) {
            $sellerData[] = $sellers->customer;
        }

        $data = request()->all();

        if ($data['shipping_type'] == 'Free') {
            $data['price'] = 0;
        }

        if ($data['price'] == 0) {
            $data['shipping_type'] ='Free';
        }

        $this->validate(request(), [
            'price_from' => 'numeric|required',
            'price_to' => 'numeric|required|min:'.$data['price_from'],
            'shipping_type' => 'string|required',
            'shipping_method' => 'string|required',
            'seller_name' => 'string|required',
            'price' => 'numeric|required',
        ]);

        foreach ($sellerData as $sellerId) {

            if ($sellerId['id'] == $data['seller_name'] ) {
                $data['marketplace_seller_id'] = $sellerId['id'];
            }
        }

        foreach ($shippingMethods as $method) {

            if ($method->id == $data['shipping_method']) {
                $data['marketplace_superset_id'] = $method->id;
            }
        }

        if ($data['seller_name'] == 0) {
           $data['marketplace_seller_id'] = null;
        }

        $superSets = $this->supersetRateRepository->all();

        foreach ($superSets as $superSet) {
            if ($data['marketplace_seller_id'] == null) {
                if ((string)$id != (string)$superSet->id) {
                    if ($data['price_from'] >= $superSet->price_from
                        && $data['price_from'] <= $superSet->price_to
                        && $data['marketplace_superset_id'] == $superSet->marketplace_superset_id
                        && $data['marketplace_seller_id'] == $superSet->marketplace_seller_id) {
                        session()->flash('error',trans('marketplace_tablerate_shipping::app.admin.shipping-rates.range-exist',
                        ['name' => 'Superset Rate']));

                        return back();
                    }

                    if ($data['price_from'] == $superSet->price_from
                        &&  $data['price_to'] == $superSet->price_to
                        && $data['marketplace_superset_id'] == $superSet->marketplace_superset_id
                        && $superSet->marketplace_seller_id == null) {
                        session()->flash('error',trans('marketplace_tablerate_shipping::app.admin.shipping-rates.range-exist',
                        ['name' => 'Superset Rate']));

                        return back();
                    }
                }
            } else {
                if ((string)$id != (string)$superSet->id) {

                    if($data['marketplace_seller_id'] == $superSet->marketplace_seller_id
                        && $data['marketplace_superset_id'] == $superSet->marketplace_superset_id
                        && $data['price_from'] <= $superSet->price_to
                        && $data['price_to'] >= $superSet->price_from) {
                        session()->flash('error',trans('marketplace_tablerate_shipping::app.admin.shipping-rates.range-exist',
                            ['name' => 'Superset Rate']));

                            return back();
                    }
                    if($data['marketplace_seller_id'] == $superSet->marketplace_seller_id
                        && $data['marketplace_superset_id'] == $superSet->marketplace_superset_id
                        && $data['price_from'] <= $superSet->price_to
                        && !($data['price_from'] <= $superSet->price_from)
                        && !($data['price_to'] <= $superSet->price_to)) {

                            session()->flash('error',trans('marketplace_tablerate_shipping::app.admin.shipping-rates.range-exist',
                            ['name' => 'Superset Rate']));

                            return back();
                    }

                    if ($data['price_from'] == $superSet->price_from
                        &&  $data['price_to'] == $superSet->price_to
                        && $data['marketplace_superset_id'] == $superSet->marketplace_superset_id
                        && $superSet->marketplace_seller_id == $data['marketplace_seller_id']) {
                        session()->flash('error',trans('marketplace_tablerate_shipping::app.admin.shipping-rates.range-exist',
                        ['name' => 'Superset Rate']));

                        return back();
                    }
                }
            }


        }

        $this->supersetRateRepository->update($data, $id);

        session()->flash('success', trans('marketplace_tablerate_shipping::app.admin.superset-rates.update-success',
        ['name' => 'Superset Rate']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Mass Delete supersetRate
     *
     * @return response
     */
    public function massDestroy()
    {
        $supersetIds = explode(',', request()->input('indexes'));

        foreach ($supersetIds as $supersetId) {

            $this->supersetRateRepository->delete($supersetId);
        }

        session()->flash('success', trans('marketplace_tablerate_shipping::app.admin.superset-rates.mass-delete-success'));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Delete supersetRate
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->supersetRateRepository->delete($id);

        session()->flash('success', trans('marketplace_tablerate_shipping::app.admin.superset-rates.delete-success',
        ['name' => 'Superset Rate']));

        return response()->json(['message' => true], 200);
    }
}