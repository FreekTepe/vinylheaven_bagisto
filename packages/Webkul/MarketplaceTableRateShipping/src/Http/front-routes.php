<?php

Route::group(['middleware' => ['web', 'theme', 'locale', 'currency']], function () {

    //Marketplace seller routes starts here
    Route::prefix('marketplace/shop')->group(function () {

        Route::group(['middleware' => ['customer']], function () {

            //Seller Table Rate Routes
            Route::get('/rates', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Shop\ShippingRateController@index')->defaults('_config', [
                'view' => 'marketplace_tablerate_shipping::shop.rates.index'
            ])->name('shop.marketplace.tablerate.rates.index');

            //create Shipping rates
            Route::get('/rates/create', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Shop\ShippingRateController@create')->defaults('_config', [
                'view' => 'marketplace_tablerate_shipping::shop.rates.create'
            ])->name('shop.marketplace.tablerate.rates.create');

            //Store Shipping rates
            Route::post('/rates/create', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Shop\ShippingRateController@store')->defaults('_config', [
                'redirect' => 'shop.marketplace.tablerate.rates.index'
            ])->name('shop.marketplace.tablerate.rates.store');

            //Edit Specific Shipping rates
            Route::get('/rates/edit/{id}', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Shop\ShippingRateController@edit')->defaults('_config', [
                'view' => 'marketplace_tablerate_shipping::shop.rates.edit'
            ])->name('shop.marketplace.tablerate.rates.edit');

            //Update Specific Shipping rates
            Route::post('/rates/edit/{id}', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Shop\ShippingRateController@update')->defaults('_config', [
                'redirect' => 'shop.marketplace.tablerate.rates.index'
            ])->name('shop.marketplace.tablerate.rates.update');

            //Delete Specific Shipping rates
            Route::get('/rates/delete/{id}', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Shop\ShippingRateController@delete')->defaults('_config', [
                'redirect' => 'shop.marketplace.tablerate.rates.index'
            ])->name('shop.marketplace.tablerate.rates.delete');

            //Mass Delete Shipping rates
            Route::get('/rates/mass-delete/{key}', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Shop\ShippingRateController@massDelete')->defaults('_config', [
                'redirect' => 'shop.marketplace.tablerate.rates.index'
            ])->name('shop.marketplace.tablerate.rates.mass-delete');


            //Create Superset Rate
            Route::get('/super-set-rates/create', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Shop\SupersetRateController@create')->defaults('_config', [
                'view' => 'marketplace_tablerate_shipping::shop.super_set_rates.create'
            ])->name('shop.marketplace.tablerate.super_set_rates.create');

            //Manage Superset Rate
            Route::get('/super-set-rates', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Shop\SupersetRateController@index')->defaults('_config', [
                'view' => 'marketplace_tablerate_shipping::shop.super_set_rates.index'
            ])->name('shop.marketplace.tablerate.super_set_rates.index');

            //Store Superset Rate
            Route::post('/super-set-rates/store', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Shop\SupersetRateController@store')->defaults('_config', [
                'redirect' => 'shop.marketplace.tablerate.super_set_rates.index'
            ])->name('shop.marketplace.tablerate.super_set_rates.store');

            //Edit SuperSet Rate
            Route::get('/super-set-rates/edit/{id}', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Shop\SupersetRateController@edit')->defaults('_config', [
                'view' => 'marketplace_tablerate_shipping::shop.super_set_rates.edit'
            ])->name('shop.marketplace.tablerate.super_set_rates.edit');

            //Update SuperSet Rates
            Route::Post('/super-set-rates/edit/{id}', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Shop\SupersetRateController@update')->defaults('_config', [
                'redirect' => 'shop.marketplace.tablerate.super_set_rates.index'
            ])->name('shop.marketplace.tablerate.super_set_rates.update');

            //Mass Delete Superset Rates
            Route::post('/super-set-rates/mass-delete', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Shop\SupersetRateController@massDestroy')->defaults('_config', [
                'redirect' => 'shop.marketplace.tablerate.super_set_rates.index'
            ])->name('shop.marketplace.tablerate.super_set_rates.mass-delete');

            //Delete Specific Super Shipping Set
            Route::post('/super-set-rates/delete{id}', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Shop\SupersetRateController@destroy')->name('shop.marketplace.tablerate.super_set_rates.delete');


            //Download Sample CSV file
            Route::get('seller/download', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Shop\ShippingRateController@sampleDownload')->defaults('_config', [
                'view' => 'marketplace_tablerate_shipping::shop.rates.index'
            ])->name('shop.marketplace.tablerate.sampledownload');

            //Import File CSV
            Route::post('seller/manage-shipping/upload', 'Webkul\MarketplaceTableRateShipping\Http\Controllers\Shop\ShippingRateController@import')->defaults('_config', [
                'redirect' => 'shop.marketplace.tablerate.rates.index'
            ])->name('shop.marketplace.tablerate.import');
        });
    });
});



