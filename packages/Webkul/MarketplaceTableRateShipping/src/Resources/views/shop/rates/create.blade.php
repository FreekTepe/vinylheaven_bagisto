@extends('marketplace_tablerate_shipping::shop.layouts.master')

@section('page_title')
    {{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.add-title') }}
@endsection

@section('content')
    <div class="account-layout dashboard">
        <form method="POST" action="{{route('shop.marketplace.tablerate.rates.store')}}" @submit.prevent="onSubmit" enctype="multipart/form-data">
            @csrf
            <div class="page-header" style="display: inline-flex; width: 100%; border-bottom:0px !important">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        {{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.add-title') }}
                    </h1>
                </div>

                <div class="page-action" style="margin-left: 48%;">
                    <button type="submit" class="btn btn-lg btn-primary">
                        {{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.save') }}
                    </button>
                </div>
            </div>

            <div class="control-group">
                <div class="page-content">
                    <div class="form-container">
                        <accordian :title="'{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.shipping-information') }}'" :active="true" style="width:130%;">
                            <div slot="body">
                                <div class="control-group" :class="[errors.has('shipping_type') ? 'has-error' : '']">
                                    <label for="text" class="required">{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.shipping-method') }}<i class="export-icon"></i> </label>
                                    <select class="control" v-validate="'required'" id="" name="shipping_type" data-vv-as="&quot;{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.shipping-method') }}&quot;">
                                        @foreach($shippingMethods as $method)
                                            @if($method->status == 1)
                                                <option value="{{$method->id}}" >{{$method->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    <span class="control-error" v-if="errors.has('shipping_type')">@{{ errors.first('shipping_type') }}</span>
                                </div>

                                <div class="control-group" :class="[errors.has('country') ? 'has-error' : '']">
                                    <label for="text" class="required">{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.country') }}<i class="export-icon"></i> </label>
                                    <span class="control-error" v-if="errors.has('country')">@{{ errors.first('country') }}</span>
                                    <select class="control" v-validate="'required'" id="" name="country" data-vv-as="&quot;{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.country') }}&quot;">
                                        @foreach($countries as $keyCountry => $country)
                                        <option value="{{$country->code}}">{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="control-group" :class="[errors.has('region') ? 'has-error' : '']">
                                    <label for="text" class="required">{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.region') }}<i class="export-icon"></i> </label>
                                    <input type="text" v-validate="'required'" class="control" name="region" value="{{old('region')}}" data-vv-as="&quot;{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.region') }}&quot;">
                                    <span class="control-error" v-if="errors.has('region')">@{{ errors.first('region') }}</span>
                                </div>

                                <div class="control-group" :class="[errors.has('weight_from') ? 'has-error' : '']">
                                    <label for="text" class="required">{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.weight-from') }}<i class="export-icon"></i> </label>
                                    <input type="text" v-validate="'required'" class="control" name="weight_from" value="{{old('weight_from')}}" data-vv-as="&quot;{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.weight-from') }}&quot;">
                                    <span class="control-error" v-if="errors.has('weight_from')">@{{ errors.first('weight_from') }}</span>
                                </div>

                                <div class="control-group" :class="[errors.has('weight_to') ? 'has-error' : '']">
                                    <label for="text" class="required">{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.weight-to') }}<i class="export-icon"></i> </label>
                                    <input type="text" v-validate="'required'" class="control" name="weight_to" value="{{old('weight_to')}}" data-vv-as="&quot;{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.weight-to') }}&quot;">
                                    <span class="control-error" v-if="errors.has('weight_to')">@{{ errors.first('weight_to') }}</span>
                                </div>

                                <div class="control-group" :class="[errors.has('zip_from') ? 'has-error' : '']">
                                    <label for="text" class="required">{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.zip-from') }}<i class="export-icon"></i> </label>
                                    <input type="text" v-validate="'required'" class="control" name="zip_from" value="{{old('zip_from')}}" data-vv-as="&quot;{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.zip-from') }}&quot;">
                                    <span class="control-error" v-if="errors.has('zip_from')">@{{ errors.first('zip_from') }}</span>
                                </div>

                                <div class="control-group" :class="[errors.has('zip_to') ? 'has-error' : '']">
                                    <label for="text" class="required">{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.zip-to') }}<i class="export-icon"></i> </label>
                                    <input type="text" v-validate="'required'" class="control" name="zip_to" value="{{old('zip_to')}}" data-vv-as="&quot;{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.zip-to') }}&quot;">
                                    <span class="control-error" v-if="errors.has('zip_to')">@{{ errors.first('zip_to') }}</span>
                                </div>

                                <div class="control-group" :class="[errors.has('price') ? 'has-error' : '']">
                                    <label for="text" class="required">{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.price') }}<i class="export-icon"></i> </label>
                                    <input type="text" v-validate="'required'" class="control" name="price" value="{{old('price')}}" data-vv-as="&quot;{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.price') }}&quot;">
                                    <span class="control-error" v-if="errors.has('price')">@{{ errors.first('price') }}</span>
                                </div>

                                <div class="control-group" :class="[errors.has('is_zip_range') ? 'has-error' : '']">
                                    <label for="status" class="required">{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.is-range') }}<i class="export-icon"></i></label>
                                    <span class="control-error" v-if="errors.has('is_zip_range')">@{{ errors.first('is_zip_range') }}</span>
                                    <select class="control" v-validate="'required'" id="is_zip_range" name="is_zip_range" data-vv-as="&quot;{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.is_zip_range') }}&quot;">
                                        <option value="0">
                                            {{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.isrange-yes') }}
                                        </option>

                                        <option value="1">
                                            {{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.isrange-no') }}
                                        </option>
                                    </select>
                                </div>

                                <div class="control-group" :class="[errors.has('zip_code') ? 'has-error' : '']">
                                    <label for="text" class="required">{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.zip-code') }}<i class="export-icon"></i> </label>
                                    <input type="text" v-validate="'required'" class="control" name="zip_code" value="{{old('zip_code')}}" data-vv-as="&quot;{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.zip-code') }}&quot;">
                                    <span class="control-error" v-if="errors.has('zip_code')">@{{ errors.first('zip_code') }}</span>
                                </div>
                            </div>
                        </accordian>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
