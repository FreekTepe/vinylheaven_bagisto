@extends('marketplace_tablerate_shipping::shop.layouts.master')

@section('page_title')
    {{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.title') }}
@endsection

@section('content')
    <div class="account-layout dashboard">
        <div class="account-head mb-10">
            <span class="account-heading">
                {{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.seller-title') }}
            </span>
        </div>

        <div class="control-group">
            <div class="page-content">
                <div class="form-container">
                    <accordian :title="'{{ __('marketplace_tablerate_shipping::app.shop.sellers.tablerate.shipping-manager') }}'" :active="true" style="width:130%;">
                        <div slot="body">
                            <form method="POST" action="{{ route('shop.marketplace.tablerate.import') }}" enctype="multipart/form-data" @submit.prevent="onSubmit">
                                @csrf()

                            <div class="control-group" :class="[errors.has('file') ? 'has-error' : '']">
                                <label for="file" class="required">{{ __('marketplace_tablerate_shipping::app.shop.sellers.tablerate.upload-csv') }}</label>
                                <input v-validate="'required'" type="file" class="control" id="file" name="file" data-vv-as="&quot;{{ __('admin::app.export.file') }}&quot;" value="{{ old('file') }}"/ style="padding-top: 5px">
                                <span class="control-error" v-if="errors.has('file')">@{{ errors.first('file') }}</span>
                                <label for="fileupload" style="width: 100%;">
                                    <a href="{{route('shop.marketplace.tablerate.sampledownload')}}" class="locale" style="color: red; text-decoration: underline; padding-right: 68%; padding-bottom: 10px;">
                                        {{ __('marketplace_tablerate_shipping::app.shop.sellers.tablerate.download-file') }}</a>
                                </label>
                            </div>

                            <div>
                                <button type="submit" class="btn btn-lg btn-primary">
                                    {{ __('marketplace_tablerate_shipping::app.shop.sellers.tablerate.upload') }}
                                </button>
                            </div>
                        </div>
                    </accordian>

                    <accordian :title="'{{ __('marketplace_tablerate_shipping::app.shop.sellers.tablerate.shipping-rate-list') }}'" :active="true" style="width:130%;">
                        <div slot="body">
                            <div class="page-action" style="float:right;">
                                <a href="{{ route('shop.marketplace.tablerate.rates.create') }}" class="btn btn-lg btn-primary">
                                    {{ __('marketplace_tablerate_shipping::app.shop.sellers.tablerate.save-btn-shipping-rates') }}
                                </a>
                            </div>
                        </div>
                    </accordian>

                    <div class="custom-grid-container" style="width: auto;margin-bottom: 50px;">
                        @foreach($shippingMethods as $key => $methods)

                        <div class="icon-grid">
                            <div class="table-container" style="width: 130%; margin-top: 20px; font-weight: 700;padding: 12px 10px;background: #f8f9fa;color: #3a3a3a; display:inline-flex;">
                                @foreach($supersetMethods as $superset)
                                    @if( $superset->id == $key )
                                        {{$superset->name}}
                                    @endif
                                @endforeach
                                <div class="trash-icon" style="display: inline-flex; margin-left: auto;">
                                    <a href="{{ route('shop.marketplace.tablerate.rates.mass-delete', $key) }}">
                                        <span class="icon trash-icon"></span>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="table-data" style="width: 130%;overflow-x: auto; border:1px solid rgb(199, 199, 199);">
                            <table style="width: 100%;border-collapse: collapse;text-align: left;">
                                <thead>
                                    <tr>
                                        <th style="font-weight: 700;padding: 12px 10px;background: #f8f9fa;color: #3a3a3a;">{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.country') }}</th>
                                        <th style="font-weight: 700;padding: 12px 10px;background: #f8f9fa;color: #3a3a3a;">{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.region') }}</th>
                                        <th style="font-weight: 700;padding: 12px 10px;background: #f8f9fa;color: #3a3a3a;">{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.zip-from') }}</th>
                                        <th style="font-weight: 700;padding: 12px 10px;background: #f8f9fa;color: #3a3a3a;">{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.zip-to') }}</th>
                                        <th style="font-weight: 700;padding: 12px 10px;background: #f8f9fa;color: #3a3a3a;">{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.weight-from') }}</th>
                                        <th style="font-weight: 700;padding: 12px 10px;background: #f8f9fa;color: #3a3a3a;">{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.weight-to') }}</th>
                                        <th style="font-weight: 700;padding: 12px 10px;background: #f8f9fa;color: #3a3a3a;">{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.price') }}</th>
                                        <th style="font-weight: 700;padding: 12px 10px;background: #f8f9fa;color: #3a3a3a;">{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.is-range') }}</th>
                                        <th style="font-weight: 700;padding: 12px 10px;background: #f8f9fa;color: #3a3a3a;">{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.zip-code') }}</th>
                                        <th style="font-weight: 700;padding: 12px 10px;background: #f8f9fa;color: #3a3a3a;">{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.action') }}</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach($methods as $shippingType)
                                    <tr style="border-bottom: 1px solid #c7c7c7; text-align: center;">
                                        <td>{{$shippingType->country}}</td>
                                        <td>{{$shippingType->region}}</td>
                                        <td>{{$shippingType->zip_from}}</td>
                                        <td>{{$shippingType->zip_to}}</td>
                                        <td>{{$shippingType->weight_from}}</td>
                                        <td>{{$shippingType->weight_to}}</td>
                                        <td>{{$shippingType->price}}</td>
                                        <td>
                                            @if($shippingType->is_zip_range == 0)
                                                {{ __('marketplace_tablerate_shipping::app.shop.sellers.tablerate.yes') }}
                                            @elseif($shippingType->is_zip_range == 1)
                                                {{ __('marketplace_tablerate_shipping::app.shop.sellers.tablerate.no') }}
                                            @endif
                                        </td>

                                        <td>{{$shippingType->zip_code}}</td>

                                        <td style="padding: 10px;border-bottom: solid 1px #d3d3d3;color: #3a3a3a;vertical-align: top;">
                                            <div class="action" style="display:inline-flex;">
                                                <a href="{{ route('shop.marketplace.tablerate.rates.edit', ['id' => $shippingType->id]) }}">
                                                    <span class="icon pencil-lg-icon"></span>
                                                </a>

                                                <a href="">
                                                    <a href="{{ route('shop.marketplace.tablerate.rates.delete', $shippingType->id) }}">
                                                        <span class="icon trash-icon"></span>
                                                    </a>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


