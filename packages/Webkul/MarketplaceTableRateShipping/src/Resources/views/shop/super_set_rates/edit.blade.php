@extends('marketplace_tablerate_shipping::shop.layouts.master')

@section('page_title')
    {{ __('marketplace_tablerate_shipping::app.admin.superset-rates.edit-title') }}
@endsection

@section('content')

    @php
        $shipping = array('Fixed','Free');
    @endphp

    <div class="account-layout dashboard">
        <form method="POST" action="{{route('shop.marketplace.tablerate.super_set_rates.update',$superSetData->id)}}" @submit.prevent="onSubmit" enctype="multipart/form-data">
            @csrf
            <div class="page-header" style="display: inline-flex; width: 100%; border-bottom:0px !important">
                <div class="title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        {{ __('marketplace_tablerate_shipping::app.admin.superset-rates.edit-title') }}
                    </h1>
                </div>

                <div class="page-action" style="margin-left: 47%;">
                    <button type="submit" class="btn btn-lg btn-primary">
                        {{ __('marketplace_tablerate_shipping::app.admin.superset-rates.save-btn') }}
                    </button>
                </div>
            </div>

            <div class="control-group">
                <div class="page-content">
                    <div class="form-container">
                        <accordian :title="'{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.edit-information') }}'" :active="true" style="width:130%;">

                            <div slot="body">
                                <div class="control-group" :class="[errors.has('price_from') ? 'has-error' : '']">
                                    <label for="text" class="required">{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.price-from') }}<i class="export-icon"></i> </label>
                                    <input type="text" v-validate="'required'" class="control" name="price_from" value="{{$superSetData->price_from}}" data-vv-as="&quot;{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.price-from') }}&quot;">
                                    <span class="control-error" v-if="errors.has('price_from')">@{{ errors.first('price_from') }}</span>
                                </div>

                                <div class="control-group" :class="[errors.has('price_to') ? 'has-error' : '']">
                                    <label for="text" class="required">{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.price-to') }}<i class="export-icon"></i> </label>
                                    <input type="text" v-validate="'required'" class="control" name="price_to" value="{{$superSetData->price_to}}" data-vv-as="&quot;{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.price-to') }}&quot;">
                                    <span class="control-error" v-if="errors.has('price_to')">@{{ errors.first('price_to') }}</span>
                                </div>

                                <div class="control-group" :class="[errors.has('shipping_method') ? 'has-error' : '']">
                                    <label for="text" class="required">{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.shipping-method') }}<i class="export-icon"></i> </label>
                                    <select class="control" v-validate="'required'" id="" name="shipping_method" data-vv-as="&quot;{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.shipping-method') }}&quot;">
                                        @foreach($shippingMethods as $method)
                                            @if($method->status == 1)
                                                <option value="{{$method->id}}" @if($superSetData->marketplace_superset_id == $method->id) selected @endif>{{$method->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    <span class="control-error" v-if="errors.has('shipping_method')">@{{ errors.first('shipping_method') }}</span>
                                </div>

                                <div class="control-group" :class="[errors.has('shipping_type') ? 'has-error' : '']">
                                    <label for="text" class="required">{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.shipping-type') }}<i class="export-icon"></i> </label>
                                    <span class="control-error" v-if="errors.has('shipping_type')">@{{ errors.first('shipping_type') }}</span>
                                    <select class="control" v-validate="'required'" id="shipping_type" name="shipping_type" data-vv-as="&quot;{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.shipping-type') }}&quot;">
                                        @foreach($shipping as $type)
                                            <option value="{{$type}}" @if ($superSetData->shipping_type == $type) selected @endif>{{$type}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="control-group" :class="[errors.has('price') ? 'has-error' : '']" id="get_pricediv">
                                    <label for="text" class="required">{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.price') }}<i class="export-icon"></i> </label>
                                    <input type="text" v-validate="'required'" id="get_price" class="control" name="price" value="{{$superSetData->price}}" data-vv-as="&quot;{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.price') }}&quot;">
                                    <span class="control-error" v-if="errors.has('price')">@{{ errors.first('price') }}</span>
                                </div>
                            </div>
                        </accordian>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@push('scripts')

    <script>
        $(document).ready(function () {
            getprice = $('#get_price').val();

            if (getprice == 0) {
                $('#get_pricediv').hide();
            } else {
                $('#get_pricediv').show();
            }

            $('#shipping_type').on('change',function(){

                if ($(this).val() == 'Fixed') {
                    $('#get_pricediv').show();
                } else  {
                    $('#get_pricediv').hide();
                }
            });
        });
    </script>
@endpush