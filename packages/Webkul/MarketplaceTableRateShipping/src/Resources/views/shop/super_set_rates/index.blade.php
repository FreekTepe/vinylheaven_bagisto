@extends('marketplace::shop.layouts.account')

@section('page_title')
    {{ __('marketplace_tablerate_shipping::app.admin.superset-rates.title') }}
@endsection

@section('content')
    <div class="account-layout">
        <div class="page-header" style="display: inline-flex; width: 100%; border-bottom:0px !important">
            <div class="title">
                <h1>
                    {{ __('marketplace_tablerate_shipping::app.admin.superset-rates.title') }}
                </h1>
            </div>

            <div class="page-action" style="margin-left: 55%; margin-bottom: 10px;">
                <a href="{{ route('shop.marketplace.tablerate.super_set_rates.create') }}" class="btn btn-lg btn-primary">
                    {{ __('marketplace_tablerate_shipping::app.admin.superset-rates.add-rates') }}
                </a>
            </div>
        </div>

        <div class="account-items-list">
            <accordian :title="'{{ __('marketplace_tablerate_shipping::app.shop.sellers.tablerate.superset-list') }}'" :active="true">
                <div slot="body">
                    <div class="page-content">
                        {!! app('Webkul\MarketplaceTableRateShipping\DataGrids\Shop\SupersetRateDataGrid')->render() !!}
                    </div>
                </div>
            </accordian>
        </div>
    </div>
@endsection