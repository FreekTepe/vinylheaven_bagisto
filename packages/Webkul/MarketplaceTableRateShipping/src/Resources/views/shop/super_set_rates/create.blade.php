@extends('marketplace::shop.layouts.account')

@section('page_title')
    {{ __('marketplace_tablerate_shipping::app.admin.superset-rates.add-btn-title') }}
@endsection

@section('content')
    <div class="account-layout">
        <form method="POST" action="{{route('shop.marketplace.tablerate.super_set_rates.store')}}" @submit.prevent="onSubmit" enctype="multipart/form-data">
            @csrf
        <div class="page-header" style="display: inline-flex; width: 100%; margin-bottom: 10px;">
            <div class="title" style ="width: 50%;">
                <h1>
                    <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                    {{ __('marketplace_tablerate_shipping::app.admin.superset-rates.add-btn-title') }}
                </h1>
            </div>

            <div class="page-action" style="margin-left: 28%;">
                <button type="submit" class="btn btn-lg btn-primary">
                    {{ __('marketplace_tablerate_shipping::app.admin.superset-rates.save-btn') }}
                </button>
            </div>
        </div>

        <div class="control-group">
            <div class="page-content">
                <div class="form-container">
                    <accordian :title="'{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.add-rates') }}'" :active="true" style="width:130%;">
                        <div slot="body">
                            <div class="control-group" :class="[errors.has('price_from') ? 'has-error' : '']">
                                <label for="text" class="required">{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.price-from') }}<i class="export-icon"></i> </label>
                                <input type="text" v-validate="'required'" class="control" name="price_from" value="{{old('price_from')}}" data-vv-as="&quot;{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.price-from') }}&quot;">
                                <span class="control-error" v-if="errors.has('price_from')">@{{ errors.first('price_from') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('price_to') ? 'has-error' : '']">
                                <label for="text" class="required">{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.price-to') }}<i class="export-icon"></i> </label>
                                <input type="text" v-validate="'required'" class="control" name="price_to" value="{{old('price_to')}}" data-vv-as="&quot;{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.price-to') }}&quot;">
                                <span class="control-error" v-if="errors.has('price_to')">@{{ errors.first('price_to') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('shipping_method') ? 'has-error' : '']">
                                <label for="text" class="required">{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.shipping-method') }}<i class="export-icon"></i> </label>
                                <select class="control" v-validate="'required'" id="" name="shipping_method" data-vv-as="&quot;{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.shipping-method') }}&quot;">
                                    @foreach($shippingMethods as $method)
                                        @if($method->status == 1)
                                            <option value="{{$method->id}}" >{{$method->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <span class="control-error" v-if="errors.has('shipping_method')">@{{ errors.first('shipping_method') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('shipping_type') ? 'has-error' : '']">
                                <label for="shipping_type" class="required">{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.shipping-type') }}<i class="export-icon"></i> </label>
                                <span class="control-error" v-if="errors.has('shipping_type')">@{{ errors.first('shipping_type') }}</span>
                                <select class="control shipping_type" v-validate="'required'" name="shipping_type" id="shipping_type" data-vv-as="&quot;{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.seller-name') }}&quot;">
                                    <option value="Fixed" >Fixed</option>
                                    <option value="Free" >Free</option>
                            </select>
                            </div>

                            <div class="control-group price" :class="[errors.has('price') ? 'has-error' : '']">
                                <label for="text" class="required">{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.price') }}<i class="export-icon"></i> </label>
                                <input type="text" v-validate="'required'" class="control" id="control" name="price" value="{{old('price')}}" data-vv-as="&quot;{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.price') }}&quot;">
                                <span class="control-error" id="spanprice" v-if="errors.has('price')">@{{ errors.first('price') }}</span>
                            </div>
                        </div>
                    </accordian>
                </div>
            </div>
        </div>
        </form>
    </div>
@endsection