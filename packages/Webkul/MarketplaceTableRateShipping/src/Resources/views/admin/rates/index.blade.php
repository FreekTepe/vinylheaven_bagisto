@extends('marketplace_tablerate_shipping::admin.layouts.content')

@section('page_title')
    {{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.title') }}
@stop

@section('content')
    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.title') }}</h1>
            </div>

            <div class="page-action">
                <div class="export-import" @click="showModal('uploadDataGrid')" style="margin-right: 20px;">
                    <i class="import-icon"></i>
                    <span>
                        {{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.import') }}
                    </span>
                </div>

                <div class="export-import" @click="showModal('marketplacedownloadDataGrid')">
                    <i class="export-icon"></i>
                    <span>
                        {{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.export') }}
                    </span>
                </div>
            </div>
        </div>

        <modal id="marketplacedownloadDataGrid" :is-open="modalIds.marketplacedownloadDataGrid">
            <h3 slot="header">{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.download') }}</h3>

            <div slot="body">
                <export-form></export-form>
            </div>
        </modal>

        <modal id="uploadDataGrid" :is-open="modalIds.uploadDataGrid">

            <h3 slot="header">{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.upload') }}</h3>

            <div slot="body">
                <form method="POST" action="{{ route('admin.marketplace.tablerate.rates.import') }}" enctype="multipart/form-data" @submit.prevent="onSubmit">
                    @csrf()

                    <div class="control-group" :class="[errors.has('file') ? 'has-error' : '']">
                        <label for="file" class="required">{{ __('admin::app.export.file') }}</label>
                        <input v-validate="'required'" type="file" class="control" id="file" name="file" data-vv-as="&quot;
                        {{ __('admin::app.export.file') }}&quot;" value="{{ old('file') }}" style="padding-top: 5px">
                        <span>{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.allowed-type') }}</span>
                        <span><b>{{ __('marketplace_tablerate_shipping::app.admin.shipping-rates.file-type') }}</b></span>
                        <span class="control-error" v-if="errors.has('file')">@{{ errors.first('file') }}</span>
                    </div>

                    <button type="submit" class="btn btn-lg btn-primary">
                        {{__('marketplace_tablerate_shipping::app.admin.shipping-rates.import') }}
                    </button>
                </form>

                <label for="fileupload">
                    <a href="{{ route('admin.marketplace.tablerate.rates.sample_download') }}" class="locale" style="color: red; text-decoration: underline; padding-right: 68%; padding-bottom: 10px;">{{__('marketplace_tablerate_shipping::app.admin.shipping-rates.download-file') }}</a>
                </label>
            </div>
        </modal>

        <div slot="body">
            <div class="page-content">

                {!! app('Webkul\MarketplaceTableRateShipping\DataGrids\Admin\ShippingRateDataGrid')->render() !!}

                @inject('ShippingRateDataGrid', 'Webkul\MarketplaceTableRateShipping\DataGrids\Admin\ShippingRateDataGrid')
            </div>
        </div>
    </div>
@stop

@push('scripts')

    @include('marketplace_tablerate_shipping::export.export', ['gridName' => $ShippingRateDataGrid])

@endpush