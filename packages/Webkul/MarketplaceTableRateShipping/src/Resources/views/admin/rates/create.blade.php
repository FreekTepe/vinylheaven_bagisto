@extends('marketplace_tablerate_shipping::admin.layouts.content')

@section('page_title')
    {{ __('marketplace_tablerate_shipping::app.admin.super-shipping-set.add-supershipping-title') }}
@stop

@section('content')
    <div class="content">
        <form method="POST" action="{{ route('admin.marketplace.tablerate.rates.store') }}" @submit.prevent="onSubmit" enctype="multipart/form-data">
            @csrf
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        {{ __('marketplace_tablerate_shipping::app.admin.super-shipping-set.add-method-title') }}
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">

                        {{ __('marketplace_tablerate_shipping::app.admin.super-shipping-set.save') }}

                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    <accordian :title="'{{ __('marketplace_tablerate_shipping::app.admin.super-shipping-set.add-detail') }}'" :active="true">
                        <div slot="body">
                            @csrf()

                            <div class="control-group" :class="[errors.has('code') ? 'has-error' : '']">
                                <label for="text" class="required">{{ __('marketplace_tablerate_shipping::app.admin.add-superset.code') }}<i class="export-icon"></i> </label>
                                <input type="text" v-validate="'required'" class="control" name="code" value="{{old('code')}}" data-vv-as="&quot;{{ __('marketplace_tablerate_shipping::app.admin.add-superset.code') }}&quot;">
                                <span class="control-error" v-if="errors.has('code')">@{{ errors.first('code') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('name') ? 'has-error' : '']">
                                <label for="text" class="required">{{ __('marketplace_tablerate_shipping::app.admin.add-superset.name') }}<i class="export-icon"></i> </label>
                                <input type="text" v-validate="'required'" class="control" name="name" value="{{old('name')}}" data-vv-as="&quot;{{ __('marketplace_tablerate_shipping::app.admin.add-superset.name') }}&quot;">
                                <span class="control-error" v-if="errors.has('name')">@{{ errors.first('name') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('status') ? 'has-error' : '']">
                                <label for="status" class="required">{{ __('marketplace_tablerate_shipping::app.admin.add-superset.status-active') }}<i class="export-icon"></i></label>
                                <span class="control-error" v-if="errors.has('status')">@{{ errors.first('status') }}</span>
                                <select class="control" v-validate="'required'" id="status" name="status" data-vv-as="&quot;{{ __('marketplace_tablerate_shipping::app.admin.add-superset.status') }}&quot;">

                                    <option value="0">

                                        {{ __('marketplace_tablerate_shipping::app.admin.table-rate.isrange-yes') }}

                                    </option>
                                    <option value="1">

                                        {{ __('marketplace_tablerate_shipping::app.admin.table-rate.isrange-no') }}

                                    </option>
                                </select>
                            </div>
                        </div>
                    </accordian>

                    <accordian :title="'{{ __('marketplace_tablerate_shipping::app.admin.super-shipping-set.add-detail') }}'" :active="true">
                        <div slot="body">
                            <div class="page-content">
                                {!! app('Webkul\MarketplaceTableRateShipping\DataGrids\Admin\ShippingMethodDataGrid')->render() !!}
                            </div>
                        </div>

                    </accordian>
                </div>
            </div>
        </form>
    </div>
@stop
