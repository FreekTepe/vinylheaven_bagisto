@extends('marketplace_tablerate_shipping::admin.layouts.content')

@section('page_title')
    {{ __('marketplace_tablerate_shipping::app.admin.superset-rates.edit-title') }}
@stop

@section('content')

@php
$shipping = array('Fixed', 'Free');
@endphp

    <div class="content">
        <form method="POST" action="{{ route('admin.marketplace.tablerate.super_set_rates.update',$superSet->id) }}" @submit.prevent="onSubmit" enctype="multipart/form-data">
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        {{ __('marketplace_tablerate_shipping::app.admin.superset-rates.edit-title') }}
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        {{ __('marketplace_tablerate_shipping::app.admin.superset-rates.save-btn-title') }}
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    <accordian :title="'{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.details') }}'" :active="true">
                        <div slot="body">
                            @csrf()

                            <div class="control-group" :class="[errors.has('price_from') ? 'has-error' : '']">
                                <label for="text" class="required">{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.price-from') }}<i class="export-icon"></i> </label>
                                <input type="text" v-validate="'required'" class="control" name="price_from" value="{{$superSet->price_from}}" data-vv-as="&quot;{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.price-from') }}&quot;">
                                <span class="control-error" v-if="errors.has('price_from')">@{{ errors.first('price_from') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('price_to') ? 'has-error' : '']">
                                <label for="text" class="required">{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.price-to') }}<i class="export-icon"></i> </label>
                                <input type="text" v-validate="'required'" class="control" name="price_to" value="{{$superSet->price_to}}" data-vv-as="&quot;{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.price-to') }}&quot;">
                                <span class="control-error" v-if="errors.has('price_to')">@{{ errors.first('price_to') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('shipping_type') ? 'has-error' : '']">
                                <label for="text" class="required">{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.shipping-type') }}<i class="export-icon"></i> </label>
                                <span class="control-error" v-if="errors.has('shipping_type')">@{{ errors.first('shipping_type') }}</span>
                                <select class="control" v-validate="'required'" id="shipping_type" name="shipping_type" data-vv-as="&quot;{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.shipping-type') }}&quot;">
                                    @foreach($shipping as $type)
                                        <option value="{{$type}}" @if ($superSet->shipping_type == $type) selected @endif>{{$type}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="control-group" :class="[errors.has('shipping_method') ? 'has-error' : '']">
                                <label for="text" class="required">{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.shipping-method') }}<i class="export-icon"></i> </label>
                                <select class="control" v-validate="'required'" id="" name="shipping_method" data-vv-as="&quot;{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.shipping-method') }}&quot;">
                                    @foreach($shippingMethods as $method)
                                        @if($method->status == 1)
                                            <option value="{{$method->id}}" @if($superSet->marketplace_superset_id == $method->id) selected @endif>{{$method->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <span class="control-error" v-if="errors.has('shipping_method')">@{{ errors.first('shipping_method') }}</span>
                            </div>

                            <div class="control-group" :class="[errors.has('seller_name') ? 'has-error' : '']">
                                <label for="status" class="required">{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.seller-name') }}<i class="export-icon"></i></label>
                                <span class="control-error" v-if="errors.has('seller_name')">@{{ errors.first('seller_name') }}</span>
                                <select class="control" v-validate="'required'" id="" name="seller_name" data-vv-as="&quot;{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.seller-name') }}&quot;">
                                    <option value="0">Admin</option>
                                    @foreach($sellerData as $sellerName)
                                        <option value="{{$sellerName['id']}}"
                                            @if ($sellerName['id'] == $superSet->marketplace_seller_id)
                                                selected
                                            @endif

                                            @if ($sellerName['id'] == null)
                                                selected
                                            @endif>
                                            {{ $sellerName->first_name . ' '. $sellerName->last_name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="control-group" :class="[errors.has('price') ? 'has-error' : '']" id="get_pricediv">
                                <label for="text" class="required">{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.price') }}<i class="export-icon"></i> </label>
                                <input type="text" v-validate="'required'" id="get_price" class="control" name="price" value="{{$superSet->price}}" data-vv-as="&quot;{{ __('marketplace_tablerate_shipping::app.admin.superset-rates.price') }}&quot;">
                                <span class="control-error" v-if="errors.has('price')">@{{ errors.first('price') }}</span>
                            </div>
                        </div>
                    </accordian>
                </div>
            </div>
        </form>
    </div>
@stop

@push('scripts')

    <script>
        $(document).ready(function () {
            getprice = $('#get_price').val();

            if (getprice == 0) {
                $('#get_pricediv').hide();
            } else {
                $('#get_pricediv').show();
            }

            $('#shipping_type').on('change',function(){

                if ($(this).val() == 'Fixed') {
                    $('#get_pricediv').show();
                } else  {
                    $('#get_pricediv').hide();
                }
            });
        });
    </script>
@endpush

