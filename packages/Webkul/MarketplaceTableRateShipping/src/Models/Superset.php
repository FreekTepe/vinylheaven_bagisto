<?php

namespace Webkul\MarketplaceTableRateShipping\Models;

use Illuminate\Database\Eloquent\Model;
use Webkul\MarketplaceTableRateShipping\Contracts\Superset as SupersetContract;

class Superset extends Model implements SupersetContract
{
    protected $table = 'marketplace_tablerate_supersets';

    protected $fillable = ['code', 'name', 'status'];

    public function sellerSuperset()
    {
        return $this->hasMany(SupersetRateProxy::modelClass() ,'marketplace_superset_id');
    }

    public function shippingRate()
    {
        return $this->hasMany(ShippingRateProxy::modelClass() ,'marketplace_tablerate_superset_id');
    }
}