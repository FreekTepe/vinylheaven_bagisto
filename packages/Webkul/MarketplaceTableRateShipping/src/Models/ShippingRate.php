<?php

namespace Webkul\MarketplaceTableRateShipping\Models;

use Illuminate\Database\Eloquent\Model;
use Webkul\MarketplaceTableRateShipping\Contracts\ShippingRate as ShippingRateContract;
use Webkul\Marketplace\Models\SellerProxy;

class ShippingRate Extends Model implements ShippingRateContract
{
    protected $table = 'marketplace_tablerate_shipping_rates';

    protected $fillable = [
        'marketplace_seller_id', 'marketplace_tablerate_superset_id', 'zip_from', 'zip_to', 'region', 'country', 'is_zip_range', 'weight_from', 'weight_to', 'price', 'zip_code'
    ];

    public function seller()
    {
        return $this->belongsTo(SellerProxy::modelClass(), 'marketplace_seller_id');
    }

    public function superset()
    {
        return $this->belongsTo(SupersetProxy::modelClass(), 'marketplace_superset_id');
    }
}