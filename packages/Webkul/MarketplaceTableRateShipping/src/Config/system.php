<?php

return [
    [
        'key' => 'sales.carriers.mptablerate',
        'name' => 'marketplace_tablerate_shipping::app.admin.system.table-rate-shipping',
        'sort' => 3,
        'fields' => [
            [
                'name' => 'title',
                'title' => 'marketplace_tablerate_shipping::app.admin.system.title',
                'type' => 'text',
                'validation' => 'required',
                'channel_based' => false,
                'locale_based' => true
            ], [
                'name' => 'description',
                'title' => 'marketplace_tablerate_shipping::app.admin.system.description',
                'type' => 'textarea',
                'channel_based' => false,
                'locale_based' => true
            ], [
                'name' => 'active',
                'title' => 'marketplace_tablerate_shipping::app.admin.system.status',
                'type' => 'select',
                'options' => [
                    [
                        'title' => 'Active',
                        'value' => true
                    ], [
                        'title' => 'Inactive',
                        'value' => false
                    ]
                ],
                'validation' => 'required'
            ]
        ]
    ],
];