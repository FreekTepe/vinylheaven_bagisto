<?php

return [
    [
        'key' => 'marketplace.mptablerate',
        'name' => 'marketplace_tablerate_shipping::app.admin.layouts.manage-shipping-rates',
        'route' => 'shop.marketplace.tablerate.rates.index',
        'sort' => 8
    ], [
        'key' => 'marketplace.superset',
        'name' => 'marketplace_tablerate_shipping::app.admin.layouts.manage-superset-rates',
        'route' => 'shop.marketplace.tablerate.super_set_rates.index',
        'sort' => 9
    ],
    
];