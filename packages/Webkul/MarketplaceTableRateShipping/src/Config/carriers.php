<?php

return [
    'mptablerate' => [
        'code' => 'mptablerate',
        'title' => 'Marketplace Table Rate Shipping',
        'description' => 'Marketplace Table Rate Shipping',
        'active' => true,
        'default_rate' => 20,
        'class' => 'Webkul\MarketplaceTableRateShipping\Carriers\MarketplaceTablerate',
    ]
];