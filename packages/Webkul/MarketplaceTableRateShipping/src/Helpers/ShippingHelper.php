<?php

namespace Webkul\MarketplaceTableRateShipping\Helpers;

use Webkul\Marketplace\Repositories\SellerRepository;
use Webkul\MarketplaceTableRateShipping\Repositories\SupersetRateRepository;
use Webkul\MarketplaceTableRateShipping\Repositories\ShippingRateRepository;
use Webkul\MarketplaceTableRateShipping\Repositories\SupersetRepository;
use Webkul\Marketplace\Repositories\ProductRepository;
use Webkul\Product\Repositories\ProductFlatRepository;
use Webkul\Checkout\Facades\Cart;
use Webkul\Checkout\Repositories\CartAddressRepository;
use Webkul\MarketplaceTableRateShipping\Helpers\AdminRate;
use Webkul\MarketplaceTableRateShipping\Helpers\SellerRate;

class ShippingHelper
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * Admin Rate
     *
     * @var array
     */
    protected $adminRate;

    /**
     * Seller Rate
     *
     * @var array
     */
    protected $sellerRate;

    /**
     * supersetRateRepository Object
     *
     * @var array
    */
    protected $supersetRate;

    /**
     * shippingRateRepository Object
     *
     * @var array
    */
    protected $shippingRate;

    /**
     * sellerRepository Object
     *
     * @var array
    */
    protected $sellerRepository;

    /**
     * supersetRepository Object
     *
     * @var array
    */
    protected $supersetRepository;

    /**
     * productRepository Object
     *
     * @var array
    */
    protected $productRepository;

    /**
     * cart Address Object
     *
     * @var array
    */
    protected $cartAddressRepository;

    /**
     * product flat Object
     *
     * @var array
    */
    protected $productFlatRepository;

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Marketplace\Repositories\SupersetRateRepository $supersetRate
     * @return void
     */
    public function __construct(
        SupersetRateRepository $supersetRate,
        SellerRepository $sellerRepository,
        SupersetRepository $supersetRepository,
        ShippingRateRepository $shippingRate,
        ProductRepository $productRepository,
        CartAddressRepository $cartAddressRepository,
        ProductFlatRepository $productFlatRepository,
        AdminRate $adminRate,
        SellerRate $sellerRate
    )

    {
        $this->_config = request('_config');

        $this->supersetRate = $supersetRate;

        $this->sellerRepository = $sellerRepository;

        $this->supersetRepository = $supersetRepository;

        $this->shippingRate = $shippingRate;

        $this->productRepository = $productRepository;

        $this->cartAddressRepository = $cartAddressRepository;

        $this->productFlatRepository = $productFlatRepository;

        $this->adminRate = $adminRate;

        $this->sellerRate = $sellerRate;
    }

    /**
     * Find Appropriate TableRate Methods
     *
     * @return $shippingData
     */
    public function findAppropriateTableRateMethods()
    {
        $cart = Cart::getCart();
        $cartItems = $cart->items;
        $itemCount = count($cartItems);

        //Get The Sellers Admin and Product
        foreach ($cart->items()->get() as $item) {

            $cartItem = $this->getSellerAdminItem($item);

            if ($cartItem['marketplace_seller_id'] == null) {

                $shippingRates = $this->adminRate->getAvailableAdminRate($cartItem);
            } else {
                $shippingRates = $this->sellerRate->getAvailableSellerRate($cartItem);
            }

            if (isset($shippingRates) && $shippingRates != null) {
                if ($cartItem['marketplace_seller_id'] == null)
                    $cartItem['marketplace_seller_id'] = 0;

                $shippingMethods[$cartItem['marketplace_seller_id']][$item->product_id] = $shippingRates;
            } else {
                $shippingMethods = null;
            }
        }

        return $shippingMethods;
    }

    /**
     * Get The Seller or Admin Item
     *
     * @param $item
     * @return $cartItem
     */
    public function getSellerAdminItem($item)
    {
        if (isset($item->additional['seller_info']) && ! $item->additional['seller_info']['is_owner']) {
            $seller = $this->sellerRepository->find($item->additional['seller_info']['seller_id']);
        } else {
            $seller = $this->productRepository->getSellerByProductId($item->product_id);
        }

        $sellerId = 0;
        $cartItem = null;

        if ($seller && $seller->is_approved) {

            $sellerId = $seller->id;
            $sellerProduct = $this->productRepository->findOneWhere([
                'product_id' => $item->product->id,
                'marketplace_seller_id' => $seller->id,
            ]);

            if ($sellerProduct['is_owner'] == 1) {

                $additionalInfo = [
                    'product' => $item->additional['product'],
                    'quantity' => $item->additional['quantity'],
                    'is_configurable' => $item->additional['is_configurable'],
                    'seller_info' => [
                        'seller_id' => $sellerId
                    ],
                ];

                $cartItem = collect([
                    'product_id' => $item->product_id,
                    'weight' => $item->weight,
                    'price' => $item->base_price,
                    'base_price' => $item->base_price,
                    'total' => $item->total,
                    'quantity' => $item->quantity,
                    'additional' => $additionalInfo,
                    'parent_id' => $item->parent_id,
                    'cart_id' => $item->cart_id,
                    'sku' => $item->sku,
                    'marketplace_seller_id' => $sellerId,
                ]);
            } else {
                $data = $this->productRepository->findOneWhere([
                    'product_id' => $item->product->id,
                    'marketplace_seller_id' => $seller->id,
                ]);

                $cartItem = collect([
                    'product_id' => $item->product_id,
                    'weight' => $item->weight,
                    'price' => $item->price,
                    'base_price' => $item->base_price,
                    'total' => $item->total,
                    'quantity' => $item->quantity,
                    'parent_id' => $item->parent_id,
                    'cart_id' => $item->cart_id,
                    'sku' => $item->sku,
                    'marketplace_seller_id' => $data->marketplace_seller_id,
                    'additional' => $item->additional,
                ]);
            }
        } else {
            $cartItem = collect([
                'product_id' => $item->product_id,
                'weight' => $item->weight,
                'price' => $item->price,
                'base_price' => $item->base_price,
                'total' => $item->total,
                'quantity' => $item->quantity,
                'additional' => $item->additional,
                'parent_id' => $item->parent_id,
                'cart_id' => $item->cart_id,
                'sku' => $item->sku,
                'marketplace_seller_id' => null,
            ]);
        }

        return $cartItem;
    }

    /**
     * Validate Shipping Method
     *
     * @param $shippingMethods, $itemCount
     * @return $shippingMethods
     */
    public function validateShippingRate($shippingMethods, $itemCount)
    {
        $shippingMethodRates = null;
        if ($shippingMethods != null && count($shippingMethods) == 1 && key($shippingMethods) == 0) {

            //only for admins product
            foreach ($shippingMethods as $sellerId => $shippingMethod) {

                if ($shippingMethod != null) {
                    foreach($shippingMethod as $productId => $methods) {
                        if ($methods != null) {
                            foreach($methods as $methodCode => $method) {
                                $codeWiseMethods[$methodCode][] = $method;
                            }
                        }
                    }
                } else {
                    return $shippingMethods = null;
                }
            }

            foreach ($codeWiseMethods as $methodCode => $commonMethods) {
                if (count($commonMethods) == $itemCount) {
                    $shippingMethodRates[$methodCode] = $commonMethods;
                }
            }

            return $shippingMethodRates;
        } elseif ($shippingMethods != null) {
            //if cart have other then admins product
            foreach ($shippingMethods as $sellerId => $shippingMethod) {

                if ($shippingMethod != null) {
                    foreach($shippingMethod as $productId => $methods) {
                        if ($methods != null) {
                            foreach($methods as $methodCode => $method) {
                                $codeWiseMethods[$methodCode][] = $method;
                            }
                        }
                    }
                } else {
                    return $shippingMethods = null;
                }
            }

            foreach ($codeWiseMethods as $methodCode => $commonMethods) {
                if (count($commonMethods) == $itemCount) {
                    $shippingMethodRates[$methodCode] = $commonMethods;
                }
            }

            if ($shippingMethodRates == null ) {
                //when there is no shipping method found for seller product find the method from the admin rates
                $shippingMethodRates = $this->getSellerRateFromAdminRate($itemCount);
            } else {
                return $shippingMethodRates;
            }

        } else {
            $shippingMethodRates = $this->getSellerRateFromAdminRate($itemCount);
        }

        return $shippingMethodRates;
    }

    /**
     * Get the Seller Rate From Admin Rate
     *
     * @param $itemCount
     * @return $cartItem
     */
    public function getSellerRateFromAdminRate($itemCount)
    {
        $cart = Cart::getCart();

        foreach ($cart->items()->get() as $item) {

            $cartItem = $this->getSellerAdminItem($item);

            $shippingRates = $this->adminRate->getAvailableAdminRate($cartItem);

            if (isset($shippingRates) && $shippingRates != null) {
                if ($cartItem['marketplace_seller_id'] == null)
                    $cartItem['marketplace_seller_id'] = 0;

                $shippingMethods[$cartItem['marketplace_seller_id']][$item->product_id] = $shippingRates;
            } else {
                $shippingMethods = null;
            }
        }

        if ($shippingMethods != null) {

            foreach ($shippingMethods as $shippingMethod) {
                foreach($shippingMethod as $methods) {

                    if ($methods != null) {
                        foreach($methods as $methodCode => $method) {

                            $codeWiseMethods[$methodCode][] = $method;
                        }
                    }
                }
            }

            foreach ($codeWiseMethods as $methodCode => $commonMethods) {
                if (count($commonMethods) == $itemCount) {
                    $shippingMethodRates[$methodCode] = $commonMethods;
                }
            }
        } else {
            $shippingMethodRates = null;
        }

        return $shippingMethodRates;
    }

    /**
     * Get the Method Wise Shipping Data
     *
     * @param $shippingMethods
     * @return $data
     */
    public function getMethodWiseShippingData($shippingMethods)
    {
        if ($shippingMethods != null) {
            foreach ($shippingMethods as $sellerId => $shippingMethod) {

                foreach($shippingMethod as $productId => $methods) {
                    if ($methods != null) {
                        foreach($methods as $methodCode => $method) {
                            $codeWiseMethods[$methodCode][] = $method;
                        }
                    }
                }
            }

            foreach ($codeWiseMethods as $methodCode => $commonMethods) {

                $data[$methodCode] = $commonMethods;
            }

            return $data;
        }
    }
}