<?php

namespace Webkul\MarketplaceTableRateShipping\Helpers;

use Webkul\Marketplace\Repositories\SellerRepository;
use Webkul\MarketplaceTableRateShipping\Repositories\SupersetRateRepository;
use Webkul\MarketplaceTableRateShipping\Repositories\ShippingRateRepository;
use Webkul\MarketplaceTableRateShipping\Repositories\SupersetRepository;
use Webkul\Marketplace\Repositories\ProductRepository;
use Webkul\Product\Repositories\ProductFlatRepository;
use Webkul\Checkout\Facades\Cart;
use Webkul\Checkout\Repositories\CartAddressRepository;

class AdminRate
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * supersetRateRepository Object
     *
     * @var array
    */
    protected $supersetRate;

    /**
     * shippingRateRepository Object
     *
     * @var array
    */
    protected $shippingRate;

    /**
     * sellerRepository Object
     *
     * @var array
    */
    protected $sellerRepository;

    /**
     * supersetRepository Object
     *
     * @var array
    */
    protected $supersetRepository;

    /**
     * productRepository Object
     *
     * @var array
    */
    protected $productRepository;

    /**
     * cart Address Object
     *
     * @var array
    */
    protected $cartAddressRepository;

    /**
     * product flat Object
     *
     * @var array
    */
    protected $productFlatRepository;

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Marketplace\Repositories\SupersetRateRepository $supersetRate
     * @return void
     */
    public function __construct(
        SupersetRateRepository $supersetRate,
        SellerRepository $sellerRepository,
        SupersetRepository $supersetRepository,
        ShippingRateRepository $shippingRate,
        ProductRepository $productRepository,
        CartAddressRepository $cartAddressRepository,
        ProductFlatRepository $productFlatRepository
    )

    {
        $this->_config = request('_config');

        $this->supersetRate = $supersetRate;

        $this->sellerRepository = $sellerRepository;

        $this->supersetRepository = $supersetRepository;

        $this->shippingRate = $shippingRate;

        $this->productRepository = $productRepository;

        $this->cartAddressRepository = $cartAddressRepository;

        $this->productFlatRepository = $productFlatRepository;
    }

    /**
     * Get All The Available Admin Shipping Rate
     *
     * @param $cartItem
     * @return $rates
     */
    public function getAvailableAdminRate($cartItem)
    {

        $adminSupesetRate = $this->supersetRate->findWhere([
            'marketplace_seller_id' => null
        ]);

        if (isset($adminSupesetRate) && $adminSupesetRate != null && count($adminSupesetRate) > 0) {

            foreach ($adminSupesetRate as $supersetRate) {
                if ($supersetRate->price_from <= $cartItem['price'] && $supersetRate->price_to >= $cartItem['price']) {
                    $superSet = $this->supersetRepository->findOneWhere(['id' => $supersetRate->marketplace_superset_id, 'status' => 1]);

                    if ($superSet != null) {
                        $adminRate[$superSet->code] = $this->getDataInArrayFormate($cartItem, $supersetRate, $superSet);
                    }
                }
            }

            if (! isset($adminRate)) {
                $adminRate = $this->getAdminShippingRate($cartItem);
            }
        } else {
            $adminRate = $this->getAdminShippingRate($cartItem);
        }

        return $adminRate;
    }

    /**
     * Get Admin ShippingRate
     *
     * @param $cartItem
     * @return $adminRate
     */
    public function getAdminShippingRate($cartItem)
    {
        $destinationAddress = $this->cartAddressRepository->findOneWhere(['cart_id' => $cartItem['cart_id']])->postcode;
        $adminRate = null;

        $adminShippingRate = $this->shippingRate->findWhere([
            'marketplace_seller_id' => null
        ]);

        if (isset($adminShippingRate) && $adminShippingRate != null && count($adminShippingRate) >0) {

            foreach ($adminShippingRate as $shippingRate) {

                $superSet = $this->supersetRepository->findOneWhere(['id' => $shippingRate->marketplace_tablerate_superset_id, 'status' => 1]);

                if ($superSet != null) {
                    if ($shippingRate->weight_from <= $cartItem['weight']
                       && $shippingRate->weight_to >= $cartItem['weight']) {

                        if ($shippingRate->zip_from <= $destinationAddress
                            && $shippingRate->zip_to >= $destinationAddress) {

                            if ($shippingRate->is_zip_range == 0) {

                                $adminRate[$superSet->code] = $this->getDataInArrayFormate($cartItem, $shippingRate, $superSet);
                            } elseif ($shippingRate->is_zip_range == 1
                                && $shippingRate->zip_code == '*'
                                || $shippingRate->zip_code == $destinationAddress) {

                                $adminRate[$superSet->code] = $this->getDataInArrayFormate($cartItem, $shippingRate, $superSet);
                            }
                        }
                    }
                }
            }
        }

        return $adminRate;
    }

    /**
     * Get Data in Array
     *
     * @param $methods, $cartItem, $superSet
     * @return $adminRate
     */
    public function getDataInArrayFormate($cartItem, $methods, $superSet)
    {
        $adminRate = [
            'price' => $cartItem['price'],
            'base_price' => $cartItem['base_price'],
            'weight' => $cartItem['weight'],
            'shipping_cost' => $methods->price,
            'marketplace_seller_id' => null,
            'supersetName' => $superSet->name,
            'methodCode' => $superSet->code,
            'quantity' => $cartItem['quantity']
        ];

        return $adminRate;
    }
}