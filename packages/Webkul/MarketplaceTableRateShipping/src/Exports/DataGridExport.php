<?php

namespace Webkul\MarketplaceTableRateShipping\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

/**
 * DataGridExport class
 *
 * @author    Naresh Verma <naresh.verma327@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class DataGridExport implements FromView, ShouldAutoSize
{

    /**
     * DataGrid instance
     *
     * @var mixed
     */
    protected $gridData = [];

    /**
     * Create a new instance.
     *
     * @param mixed DataGrid
     * @return void
     */
    public function __construct($gridData)
    {
        $this->gridData = $gridData;
    }

    /**
     * function to create a blade view for export.
     *
     */
    public function view(): View
    {
        $columns = [];

        foreach($this->gridData as $key => $gridData) {
            $columns = array_keys((array) $gridData);
            break;
        }

        return view('marketplace_tablerate_shipping::export.temp', [
            'columns' => $columns,
            'records' => $this->gridData,
        ]);
    }
}