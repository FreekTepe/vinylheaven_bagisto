<?php

namespace Webkul\MarketplaceTableRateShipping\DataGrids\Admin;

use DB;
use Webkul\Ui\DataGrid\DataGrid;

/**
 * Shippingset Data Grid Class
 *
 *
 * @author Naresh Verma <naresh.verma327@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
Class SupersetDataGrid extends DataGrid
{
    /**
     *
     * @var integer
    */
    public $index = 'id';

    public function prepareQueryBuilder()
    {
        $queryBuilder  = DB::table('marketplace_tablerate_supersets')
                ->select('id', 'code', 'name', 'status');

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
        $this->addColumn([
            'index' => 'id',
            'label' => trans('marketplace_tablerate_shipping::app.admin.supersets.id'),
            'type' => 'number',
            'searchable' => false,
            'sortable' => true,
            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'code',
            'label' => trans('marketplace_tablerate_shipping::app.admin.supersets.code'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index' => 'name',
            'label' => trans('marketplace_tablerate_shipping::app.admin.supersets.name'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index' => 'status',
            'label' => trans('marketplace_tablerate_shipping::app.admin.supersets.status'),
            'type' => 'boolean',
            'searchable' => false,
            'sortable' => false,
            'filterable' => true,
            'wrapper' => function($data) {
                if($data->status == 1) {
                    return 'Yes';
                }
                 else {
                    return 'No';
                }
            }
        ]);

    }

    public function prepareActions()
    {
        $this->addAction([
            'type' => 'Edit',
            'method' => 'GET',
            'route' => 'admin.marketplace.tablerate.super_sets.edit',
            'icon' => 'icon pencil-lg-icon'
        ]);

        $this->addAction([
            'type' => 'Delete',
            'method' => 'POST',
            'route' => 'admin.marketplace.tablerate.super_sets.delete',
            'confirm_text' => trans('ui::app.datagrid.massaction.delete', ['resource' => 'Shipping Method']),
            'icon' => 'icon trash-icon'
        ]);

        $this->enableAction = true;
    }

    public function prepareMassActions()
    {
        $this->addMassAction([
            'type' => 'delete',
            'label' => trans('marketplace_tablerate_shipping::app.admin.supersets.delete'),
            'action' => route('admin.marketplace.tablerate.super_sets.mass-delete'),
            'method' => 'DELETE'
        ]);

        $this->addMassAction([
            'type' => 'update',
            'label' => trans('marketplace::app.admin.products.update'),
            'action' => route('admin.marketplace.tablerate.super_sets.massupdate'),
            'method' => 'PUT',
            'options' => [
                trans('marketplace_tablerate_shipping::app.admin.supersets.in-active') => 0,
                trans('marketplace_tablerate_shipping::app.admin.supersets.active') => 1
            ]
        ]);

        $this->enableMassAction = true;
    }
}