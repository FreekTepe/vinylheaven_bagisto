<?php

namespace Webkul\MarketplaceTableRateShipping\DataGrids\Admin;

use DB;
use Webkul\Ui\DataGrid\DataGrid;

/**
 * Shipping Rate Data Grid Class
 *
 *
 * @author Naresh Verma <naresh.verma327@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
Class ShippingRateDataGrid extends DataGrid
{
    /**
     * @var integer
     *
     */
    public $index = 'id';

    public function prepareQueryBuilder()
    {
        $queryBuilder  = DB::table('marketplace_tablerate_shipping_rates')
                ->leftJoin('marketplace_sellers', 'marketplace_tablerate_shipping_rates.marketplace_seller_id', '=', 'marketplace_sellers.id')
                ->leftJoin('customers', 'marketplace_sellers.customer_id', '=', 'customers.id')
                ->leftJoin('marketplace_tablerate_supersets', 'marketplace_tablerate_shipping_rates.marketplace_tablerate_superset_id', '=', 'marketplace_tablerate_supersets.id')
                ->select(
                    'marketplace_tablerate_shipping_rates.id',
                    'marketplace_tablerate_supersets.name as shipping_method',
                    'marketplace_tablerate_shipping_rates.marketplace_seller_id',
                    'marketplace_tablerate_shipping_rates.country',
                    'marketplace_tablerate_shipping_rates.zip_from',
                    'marketplace_tablerate_shipping_rates.zip_to',
                    'marketplace_tablerate_shipping_rates.region',
                    'marketplace_tablerate_shipping_rates.weight_from',
                    'marketplace_tablerate_shipping_rates.weight_to',
                    'marketplace_tablerate_shipping_rates.price',
                    'marketplace_tablerate_shipping_rates.is_zip_range',
                    'marketplace_tablerate_shipping_rates.zip_code'
                )
                ->addSelect(DB::raw('CONCAT(customers.first_name, " ", customers.last_name) as seller_name'));

        $this->addFilter('id', 'marketplace_tablerate_shipping_rates.id');
        $this->addFilter('created_at', 'marketplace_tablerate_shipping_rates.created_at');
        $this->addFilter('country','marketplace_tablerate_shipping_rates.country');
        $this->addFilter('seller_name', DB::raw('CONCAT(customers.first_name, " ", customers.last_name)'));
        $this->addFilter('shipping_method', 'marketplace_tablerate_supersets.name');
        $this->addFilter('is_zip_range', 'marketplace_tablerate_shipping_rates.is_zip_range');


        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
        $this->addColumn([
            'index' => 'id',
            'label' => trans('marketplace_tablerate_shipping::app.admin.shipping-rates.id'),
            'type' => 'number',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true
        ]);

        // $this->addColumn([
        //     'index' => 'marketplace_seller_id',
        //     'label' => trans('marketplace_tablerate_shipping::app.admin.shipping-rates.seller-id'),
        //     'type' => 'number',
        //     'searchable' => true,
        //     'sortable' => true,
        //     'filterable' => true,

        // ]);

        $this->addColumn([
            'index' => 'seller_name',
            'label' => trans('marketplace_tablerate_shipping::app.admin.shipping-rates.seller-name'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true,
            'wrapper' => function($data) {
                if ($data->marketplace_seller_id == 0) {
                    return 'Admin';
                } else {
                    return $data->seller_name;
                }
            }
        ]);

        $this->addColumn([
            'index' => 'shipping_method',
            'label' => trans('marketplace_tablerate_shipping::app.admin.shipping-rates.shipping-method'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'country',
            'label' => trans('marketplace_tablerate_shipping::app.admin.shipping-rates.country'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'region',
            'label' => trans('marketplace_tablerate_shipping::app.admin.shipping-rates.region'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'zip_from',
            'label' => trans('marketplace_tablerate_shipping::app.admin.shipping-rates.zip-from'),
            'type' => 'number',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'zip_to',
            'label' => trans('marketplace_tablerate_shipping::app.admin.shipping-rates.zip-to'),
            'type' => 'number',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'weight_from',
            'label' => trans('marketplace_tablerate_shipping::app.admin.shipping-rates.weight-from'),
            'type' => 'number',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'weight_to',
            'label' => trans('marketplace_tablerate_shipping::app.admin.shipping-rates.weight-to'),
            'type' => 'number',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'price',
            'label' => trans('marketplace_tablerate_shipping::app.admin.shipping-rates.price'),
            'type' => 'price',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true
        ]);

        $this->addColumn([
            'index' => 'is_zip_range',
            'label' => trans('marketplace_tablerate_shipping::app.admin.shipping-rates.is-range'),
            'type' => 'boolean',
            'searchable' => true,
            'sortable' => true,
            'wrapper' => function($row) {
                if ($row->is_zip_range == 0)
                    return trans('marketplace_tablerate_shipping::app.admin.shipping-rates.isrange-yes');
                else
                    return trans('marketplace_tablerate_shipping::app.admin.shipping-rates.isrange-no');
            }
        ]);

        $this->addColumn([
            'index' => 'zip_code',
            'label' => trans('marketplace_tablerate_shipping::app.admin.shipping-rates.zip-code'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true
        ]);
    }

    public function prepareActions()
    {
        $this->addAction([
            'type' => 'Edit',
            'method' => 'GET',
            'route' => 'admin.marketplace.tablerate.rates.edit',
            'icon' => 'icon pencil-lg-icon'
        ]);

        $this->addAction([
            'type' => 'Delete',
            'method' => 'POST',
            'route' => 'admin.marketplace.tablerate.rates.delete',
            'confirm_text' => trans('ui::app.datagrid.massaction.delete', ['resource' => 'Shipping Rate']),
            'icon' => 'icon trash-icon'
        ]);

        $this->enableAction = true;
    }

    public function prepareMassActions()
    {
        $this->addMassAction([
            'type' => 'delete',
            'label' => trans('marketplace_tablerate_shipping::app.admin.shipping-rates.delete'),
            'action' => route('admin.marketplace.tablerate.rates.mass-delete'),
            'method' => 'DELETE'
        ]);

        $this->enableMassAction = true;
    }
}