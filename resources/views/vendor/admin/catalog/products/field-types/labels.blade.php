<table class="table">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Label id</th>
        <th scope="col">Name</th>
    </tr>
    </thead>
    <tbody>
        @if (isset($product->labels))
            @forelse ($product->labels as $label)
                <tr>
                    <th scope="row">{{ $label->id }}</th>
                    <td scope="row">{{ $label->label_id }}</td>
                    <td>{{ $label->name }}</td>
                </tr>
            @empty
                <tr><td colspan="6">No labels.</td></tr>
            @endforelse
        @else
            <tr><td colspan="6">No labels.</td></tr>
        @endif
    </tbody>
</table>
