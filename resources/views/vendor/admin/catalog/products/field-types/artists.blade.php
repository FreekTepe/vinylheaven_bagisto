<table class="table">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Artist id</th>
        <th scope="col">Name</th>
        <th scope="col">Realname</th>
        <th scope="col">Profile</th>
        <th scope="col">Data quality</th>
        <th scope="col">Type</th>
    </tr>
    </thead>
    <tbody>
        @if (isset($product->artists))
            @forelse ($product->artists as $artist)
                <tr>
                    <th scope="row">{{ $artist->id }}</th>
                    <th scope="row">{{ $artist->artist_id }}</th>
                    <td>{{ $artist->name }}</td>
                    <td>{{ $artist->realname? $artist->realname : "-" }}</td>
                    <td>{{ Illuminate\Support\Str::limit($artist->profile, 50) }}</td>
                    <td>{{ Illuminate\Support\Str::limit($artist->data_quality, 50) }}</td>
                    <td>{{ $artist->type }}</td>
                </tr>
            @empty
                <tr><td colspan="6">No artists.</td></tr>
            @endforelse
        @else
            <tr><td colspan="6">No artists.</td></tr>
        @endif
    </tbody>
</table>

{{-- vuejs component for adding/removing artists? --}}
{{-- <artist-picker></artist-picker> --}}