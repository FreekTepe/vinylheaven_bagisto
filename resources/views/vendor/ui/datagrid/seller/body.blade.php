<tbody>
    @if (count($records))
        @foreach ($records as $key => $record)
            <tr>
                @foreach ($columns as $column)
                    @php
                        $columnIndex = explode('.', $column['index']);

                        $columnIndex = end($columnIndex);
                    @endphp

                    @if (isset($column['wrapper']))
                        @if (isset($column['closure']) && $column['closure'] == true)
                            <td data-value="{{ $column['label'] }}">{!! $column['wrapper']($record) !!}</td>
                        @else
                            <td data-value="{{ $column['label'] }}">{{ $column['wrapper']($record) }}</td>
                        @endif
                    @else
                        @if ($column['type'] == 'price')
                            @if (isset($column['currencyCode']))
                                <td data-value="{{ $column['label'] }}">{{ core()->formatPrice($record->{$columnIndex}, $column['currencyCode']) }}</td>
                            @else
                                <td data-value="{{ $column['label'] }}">{{ core()->formatBasePrice($record->{$columnIndex}) }}</td>
                            @endif
                        @else
                            <td data-value="{{ $column['label'] }}">{{ $record->{$columnIndex} }}</td>
                        @endif
                    @endif
                @endforeach

                @if ($enableActions)
                    <td style="white-space: nowrap; width: 100px;" data-value="{{ __('ui::app.datagrid.actions') }}">
                        {{-- <button class="btn-icon btn-icon-only btn btn-primary btn-sm  btn-shadow">
                            <i class="far fa-edit btn-icon-wrapper"></i>
                        </button>
                        <button class="btn-icon btn-icon-only btn btn-danger btn-sm  btn-shadow">
                            <i class="far fa-trash-alt btn-icon-wrapper"></i>
                        </button> --}}

                        {{-- 
                            ^ array:4 [
                            "type" => "Edit"
                            "method" => "GET"
                            "route" => "marketplace.account.products.edit"
                            "icon" => "icon pencil-lg-icon"
                            ] 
                        --}}



                        <div class="action">
                            @foreach ($actions as $action)

                                @php
                                    $toDisplay = (isset($action['condition']) && gettype($action['condition']) == 'object') ? $action['condition']() : true;
                                    // dd($action);
                                @endphp

                                @if ($toDisplay)

                                    @if ($action['method'] == 'GET' && $action['type'] == 'Edit')

                                        @if ($action['route'] == 'marketplace.account.products.edit-assign')

                                            <a
                                                class="btn-icon btn-icon-only btn btn-info btn-sm btn-shadow"
                                                href="{{ route($action['route'], $record->marketplace_product_id) }}"


                                                data-method="{{ $action['method'] }}"
                                                data-action="{{ route($action['route'], $record->marketplace_product_id) }}"
                                                data-token="{{ csrf_token() }}"

                                                @if (isset($action['target']))
                                                    target="{{ $action['target'] }}"
                                                @endif

                                                @if (isset($action['title']))
                                                    title="{{ $action['title'] }}"
                                                @endif
                                            >
                                                <i class="far fa-edit btn-icon-wrapper"></i>
                                            </a>
                                        @else
                                            <a
                                                class="btn-icon btn-icon-only btn btn-info btn-sm btn-shadow"
                                                href="{{ route($action['route'], $record->{$action['index'] ?? $index}) }}"


                                                data-method="{{ $action['method'] }}"
                                                data-action="{{ route($action['route'], $record->{$index}) }}"
                                                data-token="{{ csrf_token() }}"

                                                @if (isset($action['target']))
                                                    target="{{ $action['target'] }}"
                                                @endif

                                                @if (isset($action['title']))
                                                    title="{{ $action['title'] }}"
                                                @endif
                                            >
                                                <i class="far fa-edit btn-icon-wrapper"></i>
                                            </a>

                                        @endif
                                    @endif

                                    @if ($action['method'] == 'GET' && $action['type'] == 'Show')

                                            <a
                                            class="btn-icon btn-icon-only btn btn-info btn-sm btn-shadow"
                                            href="{{ route($action['route'], $record->{$action['index'] ?? $index}) }}"


                                            data-method="{{ $action['method'] }}"
                                            data-action="{{ route($action['route'], $record->{$index}) }}"
                                            data-token="{{ csrf_token() }}"

                                            @if (isset($action['target']))
                                                target="{{ $action['target'] }}"
                                            @endif

                                            @if (isset($action['title']))
                                                title="{{ $action['title'] }}"
                                            @endif
                                        >
                                            <i class="far fa-eye btn-icon-wrapper"></i>
                                        </a>
                                        
                                    @endif

                                    @if ($action['method'] == 'GET' && $action['type'] == 'View')
                                    <a
                                        class="btn-icon btn-icon-only btn btn-info btn-sm btn-shadow"
                                        href="{{ route($action['route'], $record->{$action['index'] ?? $index}) }}"


                                        data-method="{{ $action['method'] }}"
                                        data-action="{{ route($action['route'], $record->{$index}) }}"
                                        data-token="{{ csrf_token() }}"

                                        @if (isset($action['target']))
                                            target="{{ $action['target'] }}"
                                        @endif

                                        @if (isset($action['title']))
                                            title="{{ $action['title'] }}"
                                        @endif
                                    >
                                        <i class="far fa-eye btn-icon-wrapper"></i>
                                    </a>
                                @endif

                                    @if ($action['method'] == 'GET' && $action['type'] == 'Delete')
                                    <div class="dropdown d-inline-block show">
                                        <a aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="btn-icon btn-icon-only btn btn-danger btn-sm btn-shadow" style="color:white;">
                                            <i class="far fa-trash-alt btn-icon-wrapper"></i>
                                        </a>
                                        
                                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu-right dropdown-menu-rounded dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 33px, 0px);">
                                            <h6 tabindex="-1" class="dropdown-header">Are you sure?</h6>
                                            <div tabindex="-1" class="dropdown-divider"></div>
                                            <a 
                                                tabindex="0" 
                                                class="dropdown-item"
                                                href="{{ route($action['route'], $record->{$action['index'] ?? $index}) }}"
                                                data-method="{{ $action['method'] }}"
                                                data-action="{{ route($action['route'], $record->{$index}) }}"
                                                data-token="{{ csrf_token() }}"
                                                @if (isset($action['target']))
                                                    target="{{ $action['target'] }}"
                                                @endif
        
                                                @if (isset($action['title']))
                                                    title="{{ $action['title'] }}"
                                                @endif
                                                >
                                                <span>Yes, delete item</span>
                                            </a>
                                        </div>
                                    </div>
                                    {{--                                     
                                    <a
                                        class="btn-icon btn-icon-only btn btn-danger btn-sm btn-shadow"
                                        href="{{ route($action['route'], $record->{$action['index'] ?? $index}) }}"


                                        data-method="{{ $action['method'] }}"
                                        data-action="{{ route($action['route'], $record->{$index}) }}"
                                        data-token="{{ csrf_token() }}"

                                        @if (isset($action['target']))
                                            target="{{ $action['target'] }}"
                                        @endif

                                        @if (isset($action['title']))
                                            title="{{ $action['title'] }}"
                                        @endif
                                    >
                                        <i class="far fa-trash-alt btn-icon-wrapper"></i>
                                    </a> --}}
                                @endif

                                @if ($action['method'] != 'GET')
                                    <a
                                        class="btn-icon btn-icon-only btn btn-danger btn-sm btn-shadow"
                                        v-on:click="doAction($event)"

                                        data-method="{{ $action['method'] }}"
                                        data-action="{{ route($action['route'], $record->{$index}) }}"
                                        data-token="{{ csrf_token() }}"

                                        @if (isset($action['target']))
                                            target="{{ $action['target'] }}"
                                        @endif

                                        @if (isset($action['title']))
                                            title="{{ $action['title'] }}"
                                        @endif
                                    >
                                        <i class="far fa-hand-point-up btn-icon-wrapper"></i>
                                    </a>
                                @endif
                            @endif
                            @endforeach
                        </div>
                    </td>
            @endif
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="10" style="text-align: center;">{{ $norecords }}</td>
        </tr>
    @endif
</tbody>
