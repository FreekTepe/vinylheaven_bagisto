@if ($paginator->hasPages())
<div class="row justify-content-center">
    <div class="col-4 p-4 d-flex justify-content-center">
        <div>
            <nav class="" aria-label="Page navigation">
                <ul class="pagination">

                    {{-- Previous page link --}}
                    @if ($paginator->onFirstPage())
                        <li class="page-item"><a class="page-link" aria-label="Previous"><span aria-hidden="true">«</span><span class="sr-only">Previous</span></a></li>
                    @else
                        <li class="page-item"><a class="page-link" href="{{ urldecode($paginator->previousPageUrl()) }}" aria-label="Previous"><span aria-hidden="true">«</span><span class="sr-only">Previous</span></a></li>
                    @endif

                    {{-- Pagination Elements --}}
                    @foreach ($elements as $element)

                        {{-- "Three Dots" Separator --}}
                        @if (is_string($element))
                            <li class="page-item disabled"><a class="page-link">{{ $element }}</a></li>
                        @endif

                        {{-- Array Of Links --}}
                        @if (is_array($element))
                            @foreach ($element as $page => $url)
                                @if ($page == $paginator->currentPage())
                                    <li class="page-item active"><a class="page-link">{{ $page }}</a></li>
                                @else
                                    <li class="page-item"><a href="{{ urldecode($url) }}" class="page-link">{{ $page }}</a></li>
                                @endif
                            @endforeach
                        @endif
                    @endforeach

                    {{-- Next Page Link --}}
                    @if ($paginator->hasMorePages())
                        <li class="page-item"><a href="{{ urldecode($paginator->nextPageUrl()) }}" class="page-link" aria-label="Next"><span aria-hidden="true">»</span><span class="sr-only">Next</span></a></li>
                    @else
                        <li class="page-item"><a class="page-link" aria-label="Next"><span aria-hidden="true">»</span><span class="sr-only">Next</span></a></li>
                    @endif
                </ul>
            </nav>
        </div>

    </div>
</div>
@endif