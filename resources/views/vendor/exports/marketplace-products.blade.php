<table>
    <thead>
    <tr>
        <th>sku</th>
        <th>Artist</th>
        <th>title</th>

        <th>condition</th>
        <th>price</th>
        <th>description</th>

        <th>comments</th>
        <th>storage location</th>
    </tr>
    </thead>
    <tbody>
    @foreach($marketplace_products as $marketplace_product)
        <tr>
            <td>{{ $marketplace_product->product->sku }}</td>

            @if (count($marketplace_product->product->artists))
                <td>
                    @foreach ($marketplace_product->product->artists as $artist)
                        {{ $artist->name }},
                    @endforeach
                </td>
            @else
                <td>-</td>
            @endif
            
            <td>{{ $marketplace_product->product->name }}</td>

            <td>{{ $marketplace_product->condition }}</td>
            <td>{{ $marketplace_product->price }}</td>
            <td>{{ $marketplace_product->description }}</td>

            <td>{{ $marketplace_product->private_comments }}</td>
            <td>{{ $marketplace_product->storage_location }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
