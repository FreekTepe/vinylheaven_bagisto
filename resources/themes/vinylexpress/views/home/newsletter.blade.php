<section class="newsletter container" style="background-image: url({{ asset('themes/vinylexpress/assets/src/img/newsletter.jpg') }});">
    <div class="heading newsletter__mb-hide">
        <h2>Our Newsletter</h2>
    </div>

    <div class="newsletter__mb-image newsletter__mb-hide"
        style="background-image: url({{ asset('themes/vinylexpress/assets/src/img/newsletter.jpg') }});">

    </div>

    <div class="row">
        <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1 newsletter__mb-order">
            <div class="heading newsletter__heading">
                <h2>Our Newsletter</h2>
            </div>
            <p>Join the <strong>10.234 people</strong> who already read our newsletter for latest releases
                and
                industry trends.</p>

            <form action="#">
                <input type="email" placeholder="Your email address">

                <button type="submit">Subscribe</button>
            </form>
        </div>
    </div>
</section>