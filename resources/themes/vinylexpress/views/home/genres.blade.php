<section class="genres container">
    <div class="heading">
        <h2>Genres</h2>
    </div>

    <div class="genres__image-holder">
        <div class="one genres__image">
            <img src="{{ asset('themes/vinylexpress/assets/src/img/blues.png') }}" alt="">
        </div>
        <div class="two genres__image">
            <img src="{{ asset('themes/vinylexpress/assets/src/img/classical-.png') }}" alt="">
        </div>
        <div class="three genres__image">
            <img src="{{ asset('themes/vinylexpress/assets/src/img/folk-.png') }}" alt="">
        </div>
        <div class="four genres__image">
            <img src="{{ asset('themes/vinylexpress/assets/src/img/electronic-.png') }}" alt="">
        </div>
        <div class="five genres__image">
            <img src="{{ asset('themes/vinylexpress/assets/src/img/funky-.png') }}" alt="">
        </div>
        <div class="six genres__image">
            <img src="{{ asset('themes/vinylexpress/assets/src/img/hiphop-.png') }}" alt="">
        </div>
        <div class="seven genres__image">
            <img src="{{ asset('themes/vinylexpress/assets/src/img/jazz-.png') }}" alt="">
        </div>
        <div class="eight genres__image">
            <img src="{{ asset('themes/vinylexpress/assets/src/img/latin-.png') }}" alt="">
        </div>
        <div class="nine genres__image">
            <img src="{{ asset('themes/vinylexpress/assets/src/img/pop-.png') }}" alt="">
        </div>
        <div class="ten genres__image">
            <img src="{{ asset('themes/vinylexpress/assets/src/img/reggae.png') }}" alt="">
        </div>
        <div class="eleven genres__image">
            <img src="{{ asset('themes/vinylexpress/assets/src/img/rock-.png') }}" alt="">
        </div>
        <div class="twelve genres__image">
            <img src="{{ asset('themes/vinylexpress/assets/src/img/stage-copy.png') }}" alt="">
        </div>
        <div class="thirteen genres__image">
            <img src="{{ asset('themes/vinylexpress/assets/src/img/brass-.png') }}" alt="">
        </div>
        <div class="fourteen genres__image">
            <img src="{{ asset('themes/vinylexpress/assets/src/img/non-music-1.png') }}" alt="">
        </div>
        <div class="fifteen genres__image">
            <img src="{{ asset('themes/vinylexpress/assets/src/img/children-.png') }}" alt="">
        </div>
    </div>

    <ul class="genres__list">
        <li id="one"><a href="#">Blues<sup>148.674</sup></a></li>
        <li id="two"><a href="#">Classical<sup>234.102</sup></a></li>
        <li id="three"><a href="#">Electronic<sup>34.102</sup></a></li>
        <li id="four"><a href="#">Folk, World & Country<sup>374.102</sup></a></li>
        <li id="five"><a href="#">Funk/Soul<sup>374.102</sup></a></li>
        <li id="six"><a href="#">HipHop<sup>148.674</sup></a></li>
        <li id="seven"><a href="#">Jazz<sup>148.674</sup></a></li>
        <li id="eight"><a href="#">Latin<sup>148.674</sup></a></li>
        <li id="nine"><a href="#">Pop<sup>148.674</sup></a></li>
        <li id="ten"><a href="#">Reggae<sup>148.674</sup></a></li>
        <li id="eleven"><a href="#">Rock<sup>148.674</sup></a></li>
        <li id="twelve"><a href="#">Stage & Screen<sup>148.674</sup></a></li>
        <li id="thirteen"><a href="#">Non-Music<sup>148.674</sup></a></li>
        <li id="fourteen"><a href="#">Brass & Military<sup>148.674</sup></a></li>
        <li id="fifteen"><a href="#">Children's<sup>148.674</sup></a></li>
    </ul>

    <div class="genres__load">
        <a href="#" class="button button--arrow">See All Genres</a>
    </div>
</section>