@extends('shop::layouts.master-new')

@section('page_title')
    {{ __('shop::app.checkout.success.title') }}
@stop

@section('content-wrapper')
<section class="container my-5">
    <div class="row">
        <div class="col-12">
            <h2>{{ __('shop::app.checkout.success.thanks') }}</h2>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <p>{{ __('shop::app.checkout.success.order-id-info', ['order_id' => $order->increment_id]) }}</p>

            <p>{{ __('shop::app.checkout.success.info') }}</p>       
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            {{ view_render_event('bagisto.shop.checkout.continue-shopping.before', ['order' => $order]) }}

            <a href="{{ route('shop.home.index') }}" class="button button-arrow">
                {{ __('shop::app.checkout.cart.continue-shopping') }}
            </a>

            {{ view_render_event('bagisto.shop.checkout.continue-shopping.after', ['order' => $order]) }}
        </div>
    </div>
</section>
@endsection