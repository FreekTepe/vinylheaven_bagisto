@extends('shop::layouts.master-new')

@inject('productRepository', 'Webkul\Product\Repositories\ProductRepository')
@inject('productHelper', 'VinylHeaven\Product\Helpers\Product')
@inject('marketplace_product', 'VinylHeaven\Marketplace\Models\MarketplaceProduct')
@inject('marketplace_repository', 'Webkul\Marketplace\Repositories\MpProductRepository')

@php
    //dd($product);
    //$product_full = $product_helper->getCompleteProductByProduct($product->product_id);
@endphp

@section('page_title')
    {{ trim($product->meta_title) != "" ? $product->meta_title : $product->name }}
@stop

@section('seo')
    <meta name="description" content="{{ trim($product->meta_description) != "" ? $product->meta_description : str_limit(strip_tags($product->description), 120, '') }}"/>
    <meta name="keywords" content="{{ $product->meta_keywords }}"/>
@stop

@php
$has_from_price = true;
@endphp

@section('content-wrapper')
    @include('shop::products.view.social')
    @include('shop::products.view.details')
    @include('shop::products.view.sellers')
    @include('shop::products.view.info')
@endsection