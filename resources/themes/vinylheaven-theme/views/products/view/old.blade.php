

@inject ('reviewHelper', 'Webkul\Product\Helpers\Review')
@inject ('customHelper', 'Webkul\Velocity\Helpers\Helper')
@inject ('productImageHelper', 'Webkul\Product\Helpers\ProductImage')
@inject ('productRepository', 'Webkul\Product\Repositories\ProductRepository')
@inject ('marketplace_product', 'VinylHeaven\Marketplace\Models\MarketplaceProduct')
@inject ('marketplace_repository', 'Webkul\Marketplace\Repositories\MpProductRepository')

@php
    $total = $reviewHelper->getTotalReviews($product);

    $avgRatings = $reviewHelper->getAverageRating($product);
    $avgStarRating = ceil($avgRatings);

    $productImages = [];
    $images = $productImageHelper->getGalleryImages($product);

    foreach ($images as $key => $image) {
        array_push($productImages, $image['medium_image_url']);
    }
    $product_full = $productRepository->with(['variants', 'variants.inventories'])->findOrFail($product->product_id);
    $marketplace_products = $marketplace_product = app('VinylHeaven\Marketplace\Models\MarketplaceProduct')->where([['product_id', "=",$product->product_id]])->get();
    //dd($product_full->marketplace_product->condition);
@endphp

@section('page_title')
    {{ trim($product->meta_title) != "" ? $product->meta_title : $product->name }}
@stop

@section('seo')
    <meta name="description" content="{{ trim($product->meta_description) != "" ? $product->meta_description : str_limit(strip_tags($product->description), 120, '') }}"/>
    <meta name="keywords" content="{{ $product->meta_keywords }}"/>
@stop

@push('css')
    <style type="text/css">
        .related-products {
            width: 100%;
        }

        .recently-viewed {
            margin-top: 20px;
        }

        .store-meta-images > .recently-viewed:first-child {
            margin-top: 0px;
        }

        .main-content-wrapper {
            margin-bottom: 0px;
        }
    </style>
@endpush

@section('content-wrapper')

    {!! view_render_event('bagisto.shop.products.view.before', ['product' => $product]) !!}

    <section class="quality-result container">
        <div class="quality-result__share">
            <div class="quality-result__share-title">&nbsp;</div>

            <ul class="social-section__holder">
                <li><a class="twitter" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a class="facebook" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a class="gplus" href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                <li><a class="linkedin" href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                <li><a class="telegram" href="#"><i class="fa fa-telegram" aria-hidden="true"></i></a></li>
            </ul>
        </div>

        <div class="quality-result__tablet-show">
            <div class="quality-result__title">{{ $product->name }}</div>

            <div class="quality-result__witches-title"><span>{{ $product_full->artists[0]->name }}</span></div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-12">
                <div class="quality-result__left" data-component="quality-slider">
                    <div class="quality-result__number">12”</div>
                    
                    <div class="quality-result__slider-for js-quality-for">
                        @if (count($product_full->images))
                            @foreach ($product_full->images as $image_index => $image)
                                @if ($image_index < 4)
                                    <figure>
                                        <img src="{{ $image->url }}" alt="">

                                        <figcaption>Images are from an external source and may differ from reality</figcaption>
                                    </figure>
                                @endif
                            @endforeach
                        @else
                            <figure>
                                <img src="{{ asset('themes/vinylexpress/assets/images/Default-Product-Image.png') }}" alt="">

                                <figcaption>Images are from an external source and may differ from reality</figcaption>
                            </figure>
                        @endif
                    </div>

                    <div class="quality-result__slider-nav js-quality-nav">
                        @if (count($product_full->images))
                            @foreach ($product_full->images as $image_index => $image)
                                @if ($image_index < 4)
                                    <figure>
                                        <img src="{{ $image->url }}" alt="">
                                    </figure>
                                @endif
                            @endforeach
                        @else
                            <figure>
                                <img src="{{ asset('themes/vinylexpress/assets/images/Default-Product-Image.png') }}" alt="">
                            </figure>
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-12 pt-3">
                <div class="quality-result__tablet-hide">
                    <div class="quality-result__title">{{ $product->name }}</div>

                    <div class="quality-result__witches-title"><span>{{ $product_full->artists[0]->name }}</span></div>
                </div>

                <div class="quality-result__tablet-heading">
                    <div class="quality-result__number quality-result__tablet-show">12”</div>

                    <div class="quality-result__records-title">
                        @if (isset($product_full->formats[0]))
                            {{ $product_full->formats[0]['name'] }} <br>
                        @endif

                        @if (isset($product_full->companies[0]))
                            {{ $product_full->companies[0]['name'] }} <br>
                        @endif
                        
                        {{ $product_full->release_date }}. {{ $product_full->country }}
                    </div>

                    <div class="quality-result__song-title">
                        @if (isset($product_full->categories[0]))
                            {{ $product_full->categories[0]['name'] }}
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 other-release pt-1">
                        <div class="quality-result__witches-title"><span>Sellers</span></div>

                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr class="other-release__hide-mobile">
                                <th style="padding:16px;">Seller</th>
                                <th style="padding:16px;">Condition</th>
                                <th style="padding:16px;">Price</th>
                                <th style="padding:16px;"></th>
                            </tr>
                            
                            @foreach ($marketplace_products as $mp_product_index => $mp_product)
                                @php
                                    $seller = app('Webkul\Marketplace\Models\Seller')->where([['id', "=",$mp_product->marketplace_seller_id]])->first();
                                @endphp
                                
                                <tr class="other-release__number">
                                    <td>{{ ($mp_product_index + 1)}}</td>
                                </tr>
                                
                                <tr onclick="location.href='/marketplace/seller/{{ $seller->url }}/product/{{ $product->url_key }}';">
                                    <td style="padding:16px;"><small>Seller</small>{{ $seller->shop_title }}</td>
                                    <td style="padding:16px;"><small>Condition</small><span class="light-green">{{ $product_full->getConditionGrading($mp_product->condition) }}</span></td>
                                    <td style="padding:16px;"><small>Price</small><strong>€ {{ number_format($mp_product->price, 2, ',', '.') }}</strong> <cite>Excluding Shipping Costs </cite></td>
                                    <td style="padding:16px;"><a href="#" class="button button--bag" style="min-width:auto;"></a></td>
                                </tr>

                                <tr class="other-release__hide-desktop">
                                    <td>
                                        <a href="/marketplace/seller/{{ $seller->url }}/product/{{ $product->url_key }}" class="button">view this release</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="product-info container">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-12 js-info-block-1">
                <div class="product-info__title js-info-toggle-1 product-info__title--line">Product Information</div>

                <ul class="product-info__list">
                    <li>
                        <div class="product-info__left">Relase date</div>

                        <div class="product-info__right"><p>{{ $product_full->release_date }}</p></div>
                    </li>
                    
                    @if (isset($product_full->companies))
                        <li>
                            <div class="product-info__left">Companies, etc</div>

                            <div class="product-info__right">
                                @foreach ($product_full->companies as $company_index => $company)
                                    <p>{{ $company['entity_type_name'] }} — {{ $company['name'] }}</p>
                                @endforeach
                            </div>
                        </li>
                    @endif

                    <li>
                        <div class="product-info__left"><p>Notes</p></div>

                        <div class="product-info__right">
                            <p>Limited to 500.</p>
                            <p>Packaged in a PVC sleeve.</p>
                            <p>Recorded at Expanding Studios, Toronto.</p>
                        </div>
                    </li>

                    @if (isset($product_full->identifiers))
                        <li>
                            <div class="product-info__left">Barcode and other identifiers</div>

                            <div class="product-info__right">
                                @foreach($product_full->identifiers as $identifier_index => $identifier)
                                    <p>
                                        {{ $identifier['type'] }}
                                        @if ($identifier['description'] != null)
                                            ({{ $identifier['description']}})
                                        @endif
                                        : {{ $identifier['value'] }}
                                    </p>
                                @endforeach
                            </div>
                        </li>
                    @endif
                    
                    <li>
                        <div class="product-info__left">Credits</div>

                        <div class="product-info__right">
                            @if (isset($product_full->artists))
                                @foreach($product_full->artists as $artist_index => $artist)
                                    @if(isset($artist->name))
                                        <p>
                                            <strong>{{ $artist->name }}</strong>
                                        </p>
                                    @endif
                                @endforeach
                            @endif
                        </div>
                    </li>
                </ul>
            </div>

            <div class="col-md-6 col-sm-12 col-12 js-info-block-2">
                <div class="product-info__title js-info-toggle-2">Tracklist</div>

                <ul class="product-info__list">
                    @foreach ($product_full->tracklist as $item_index => $item)
                        <li>
                            <div class="product-info__col-1">{{ $item['position'] }}</div>
                            <div class="product-info__col-2">{{ $item['title'] }}</div>
                            <div class="product-info__col-3">{{ $item['duration'] }}</div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </section>

    <section class="other-release container">
        <div class="heading">
            <h2>Other Releases of "All Them Witches"</h2>
        </div>

        <table width="100%" cellpadding="0" cellspacing="0">
            <tr class="other-release__hide-mobile">
                <th>Catalogue No.</th>
                <th>Format</th>
                <th>Label</th>
                <th>Country</th>
                <th>Condition</th>
                <th>Price</th>
            </tr>
            <tr class="other-release__number">
                <td>01.</td>
            </tr>
            <tr onclick="location.href='#';">
                <td><small>CATALOGUE NO.</small> 8WM 2562</td>
                <td><small>Format</small><strong>LP</strong></td>
                <td><small>LABEL</small>Warner Bros Records</td>
                <td><small>COUNTRY</small>Canada</td>
                <td><small>CONDITION</small><span class="light-green">VG</span> Very Good</td>
                <td><small>PRICE</small><strong>€100,00</strong> <cite>Excluding Shipping Costs </cite></td>
            </tr>
            <tr class="other-release__hide-desktop">
                <td><a href="#" class="button">view this release</a></td>
            </tr>
            <tr class="other-release__number">
                <td>02.</td>
            </tr>
            <tr onclick="location.href='#';">
                <td><small>CATALOGUE NO.</small>8WM 2562</td>
                <td><small>Format</small><strong>LP</strong></td>
                <td><small>LABEL</small>Vertigo</td>
                <td><small>COUNTRY</small>US</td>
                <td><small>CONDITION</small><span class="blue">SS</span> Still Sealed</td>
                <td><small>PRICE</small><strong>€100,00</strong></td>
            </tr>
            <tr class="other-release__hide-desktop">
                <td><a href="#" class="button">view this release</a></td>
            </tr>
            <tr class="other-release__number">
                <td>03.</td>
            </tr>
            <tr onclick="location.href='#';">
                <td><small>CATALOGUE NO.</small>8WM 2562</td>
                <td><small>Format</small><strong>LP</strong></td>
                <td><small>LABEL</small>Vertigo</td>
                <td><small>COUNTRY</small>US</td>
                <td><small>CONDITION</small><span class="green">M</span> Mint</td>
                <td><small>PRICE</small><strong>€100,00</strong></td>
            </tr>
            <tr class="other-release__hide-desktop">
                <td><a href="#" class="button">view this release</a></td>
            </tr>
            <tr class="other-release__number">
                <td>04.</td>
            </tr>
            <tr onclick="location.href='#';">
                <td><small>CATALOGUE NO.</small>8WM 2562</td>
                <td><small>Format</small><strong>LP</strong></td>
                <td><small>LABEL</small>Vertigo</td>
                <td><small>COUNTRY</small>US</td>
                <td><small>CONDITION</small><span class="red">F</span> Fair</td>
                <td><small>PRICE</small><strong>€100,00</strong></td>
            </tr>
            <tr class="other-release__hide-desktop">
                <td><a href="#" class="button">view this release</a></td>
            </tr>
        </table>

        <div class="other-release__more">
            <a href="#" class="button other-release__button button--more">load more options</a>
        </div>
    </section>

    <section class="related-products container">
        <div class="heading">
            <h2>Related Product "All Them Witches"</h2>
        </div>

        <div class="row related-products__slider" data-component="product-slider">
            <div class="col-lg-3 col-md-4 col-sm-6 col-6 related-products__repeat">
                <div class="card-products">
                    <a href="#" class="card-products__list">
                        <div class="card-products__area">
                            <div class="card-products__year">2001-10"</div>
                            <div class="card-products__records">5</div>
                        </div>
                        <div class="card-products__image">
                            <img src="src/img/album-cover.jpg" alt=""/>
                        </div>
                        <div class="card-products__content">
                            <div class="card-products__title">Back in Black</div>
                            <div class="card-products__category">- ACDC</div>
                            <div class="card-products__price">From €27,00</div>
                        </div>
                    </a>
                    <a class="card-products__bag" href="#">Add to my bag</a>
                </div>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-6 col-6 related-products__repeat">
                <div class="card-products">
                    <a href="#" class="card-products__list">
                        <div class="card-products__area">
                            <div class="card-products__year">2001-10"</div>
                            <div class="card-products__records">5</div>
                        </div>
                        <div class="card-products__image">
                            <img src="src/img/second-img.jpg" alt=""/>
                        </div>
                        <div class="card-products__content">
                            <div class="card-products__title">Back in Black</div>
                            <div class="card-products__category">- ACDC</div>
                            <div class="card-products__price">From €27,00</div>
                        </div>
                    </a>
                    <a class="card-products__bag" href="#">Add to my bag</a>
                </div>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-6 col-6 related-products__repeat">
                <div class="card-products">
                    <a href="#" class="card-products__list">
                        <div class="card-products__area">
                            <div class="card-products__year">2001-10"</div>
                            <div class="card-products__records">5</div>
                        </div>
                        <div class="card-products__image">
                            <img src="src/img/sour-soul.jpg" alt=""/>
                        </div>
                        <div class="card-products__content">
                            <div class="card-products__title">Back in Black</div>
                            <div class="card-products__category">- ACDC</div>
                            <div class="card-products__price">From €27,00</div>
                        </div>
                    </a>
                    <a class="card-products__bag" href="#">Add to my bag</a>
                </div>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-6 col-6 related-products__repeat">
                <div class="card-products">
                    <a href="#" class="card-products__list">
                        <div class="card-products__area">
                            <div class="card-products__year">2001-10"</div>
                            <div class="card-products__records">5</div>
                        </div>
                        <div class="card-products__image">
                            <img src="src/img/bbc.jpg" alt=""/>
                        </div>
                        <div class="card-products__content">
                            <div class="card-products__title">Back in Black</div>
                            <div class="card-products__category">- ACDC</div>
                            <div class="card-products__price">From €27,00</div>
                        </div>
                    </a>
                    <a class="card-products__bag" href="#">Add to my bag</a>
                </div>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-6 col-6 related-products__repeat">
                <div class="card-products">
                    <a href="#" class="card-products__list">
                        <div class="card-products__area">
                            <div class="card-products__year">2001-10"</div>
                            <div class="card-products__records">5</div>
                        </div>
                        <div class="card-products__image">
                            <img src="src/img/black-star.jpg" alt=""/>
                        </div>
                        <div class="card-products__content">
                            <div class="card-products__title">Back in Black</div>
                            <div class="card-products__category">- ACDC</div>
                            <div class="card-products__price">From €27,00</div>
                        </div>
                    </a>
                    <a class="card-products__bag" href="#">Add to my bag</a>
                </div>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-6 col-6 related-products__repeat">
                <div class="card-products">
                    <a href="#" class="card-products__list">
                        <div class="card-products__area">
                            <div class="card-products__year">2001-10"</div>
                            <div class="card-products__records">5</div>
                        </div>
                        <div class="card-products__image">
                            <img src="src/img/instant-image.jpg" alt=""/>
                        </div>
                        <div class="card-products__content">
                            <div class="card-products__title">Back in Black</div>
                            <div class="card-products__category">- ACDC</div>
                            <div class="card-products__price">From €27,00</div>
                        </div>
                    </a>
                    <a class="card-products__bag" href="#">Add to my bag</a>
                </div>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-6 col-6 related-products__repeat">
                <div class="card-products">
                    <a href="#" class="card-products__list">
                        <div class="card-products__area">
                            <div class="card-products__year">2001-10"</div>
                            <div class="card-products__records">5</div>
                        </div>
                        <div class="card-products__image">
                            <img src="src/img/seeds.jpg" alt=""/>
                        </div>
                        <div class="card-products__content">
                            <div class="card-products__title">Back in Black</div>
                            <div class="card-products__category">- ACDC</div>
                            <div class="card-products__price">From €27,00</div>
                        </div>
                    </a>
                    <a class="card-products__bag" href="#">Add to my bag</a>
                </div>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-6 col-6 related-products__repeat">
                <div class="card-products">
                    <a href="#" class="card-products__list">
                        <div class="card-products__area">
                            <div class="card-products__year">2001-10"</div>
                            <div class="card-products__records">5</div>
                        </div>
                        <div class="card-products__image">
                            <img src="src/img/whole-love.jpg" alt=""/>
                        </div>
                        <div class="card-products__content">
                            <div class="card-products__title">Back in Black</div>
                            <div class="card-products__category">- ACDC</div>
                            <div class="card-products__price">From €27,00</div>
                        </div>
                    </a>
                    <a class="card-products__bag" href="#">Add to my bag</a>
                </div>
            </div>
        </div>
    </section>

    <section class="related-products container">
        <div class="heading">
            <h2>Other Customer Also Viewed</h2>
        </div>

        <div class="row related-products__slider" data-component="product-slider">
            <div class="col-lg-3 col-md-4 col-sm-6 col-6 related-products__repeat">
                <div class="card-products">
                    <a href="#" class="card-products__list">
                        <div class="card-products__area">
                            <div class="card-products__year">2001-10"</div>
                            <div class="card-products__records">5</div>
                        </div>
                        <div class="card-products__image">
                            <img src="src/img/album-cover.jpg" alt=""/>
                        </div>
                        <div class="card-products__content">
                            <div class="card-products__title">Back in Black</div>
                            <div class="card-products__category">- ACDC</div>
                            <div class="card-products__price">From €27,00</div>
                        </div>
                    </a>
                    <a class="card-products__bag" href="#">Add to my bag</a>
                </div>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-6 col-6 related-products__repeat">
                <div class="card-products">
                    <a href="#" class="card-products__list">
                        <div class="card-products__area">
                            <div class="card-products__year">2001-10"</div>
                            <div class="card-products__records">5</div>
                        </div>
                        <div class="card-products__image">
                            <img src="src/img/second-img.jpg" alt=""/>
                        </div>
                        <div class="card-products__content">
                            <div class="card-products__title">Back in Black</div>
                            <div class="card-products__category">- ACDC</div>
                            <div class="card-products__price">From €27,00</div>
                        </div>
                    </a>
                    <a class="card-products__bag" href="#">Add to my bag</a>
                </div>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-6 col-6 related-products__repeat">
                <div class="card-products">
                    <a href="#" class="card-products__list">
                        <div class="card-products__area">
                            <div class="card-products__year">2001-10"</div>
                            <div class="card-products__records">5</div>
                        </div>
                        <div class="card-products__image">
                            <img src="src/img/sour-soul.jpg" alt=""/>
                        </div>
                        <div class="card-products__content">
                            <div class="card-products__title">Back in Black</div>
                            <div class="card-products__category">- ACDC</div>
                            <div class="card-products__price">From €27,00</div>
                        </div>
                    </a>
                    <a class="card-products__bag" href="#">Add to my bag</a>
                </div>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-6 col-6 related-products__repeat">
                <div class="card-products">
                    <a href="#" class="card-products__list">
                        <div class="card-products__area">
                            <div class="card-products__year">2001-10"</div>
                            <div class="card-products__records">5</div>
                        </div>
                        <div class="card-products__image">
                            <img src="src/img/bbc.jpg" alt=""/>
                        </div>
                        <div class="card-products__content">
                            <div class="card-products__title">Back in Black</div>
                            <div class="card-products__category">- ACDC</div>
                            <div class="card-products__price">From €27,00</div>
                        </div>
                    </a>
                    <a class="card-products__bag" href="#">Add to my bag</a>
                </div>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-6 col-6 related-products__repeat">
                <div class="card-products">
                    <a href="#" class="card-products__list">
                        <div class="card-products__area">
                            <div class="card-products__year">2001-10"</div>
                            <div class="card-products__records">5</div>
                        </div>
                        <div class="card-products__image">
                            <img src="src/img/black-star.jpg" alt=""/>
                        </div>
                        <div class="card-products__content">
                            <div class="card-products__title">Back in Black</div>
                            <div class="card-products__category">- ACDC</div>
                            <div class="card-products__price">From €27,00</div>
                        </div>
                    </a>
                    <a class="card-products__bag" href="#">Add to my bag</a>
                </div>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-6 col-6 related-products__repeat">
                <div class="card-products">
                    <a href="#" class="card-products__list">
                        <div class="card-products__area">
                            <div class="card-products__year">2001-10"</div>
                            <div class="card-products__records">5</div>
                        </div>
                        <div class="card-products__image">
                            <img src="src/img/instant-image.jpg" alt=""/>
                        </div>
                        <div class="card-products__content">
                            <div class="card-products__title">Back in Black</div>
                            <div class="card-products__category">- ACDC</div>
                            <div class="card-products__price">From €27,00</div>
                        </div>
                    </a>
                    <a class="card-products__bag" href="#">Add to my bag</a>
                </div>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-6 col-6 related-products__repeat">
                <div class="card-products">
                    <a href="#" class="card-products__list">
                        <div class="card-products__area">
                            <div class="card-products__year">2001-10"</div>
                            <div class="card-products__records">5</div>
                        </div>
                        <div class="card-products__image">
                            <img src="src/img/seeds.jpg" alt=""/>
                        </div>
                        <div class="card-products__content">
                            <div class="card-products__title">Back in Black</div>
                            <div class="card-products__category">- ACDC</div>
                            <div class="card-products__price">From €27,00</div>
                        </div>
                    </a>
                    <a class="card-products__bag" href="#">Add to my bag</a>
                </div>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-6 col-6 related-products__repeat">
                <div class="card-products">
                    <a href="#" class="card-products__list">
                        <div class="card-products__area">
                            <div class="card-products__year">2001-10"</div>
                            <div class="card-products__records">5</div>
                        </div>
                        <div class="card-products__image">
                            <img src="src/img/whole-love.jpg" alt=""/>
                        </div>
                        <div class="card-products__content">
                            <div class="card-products__title">Back in Black</div>
                            <div class="card-products__category">- ACDC</div>
                            <div class="card-products__price">From €27,00</div>
                        </div>
                    </a>
                    <a class="card-products__bag" href="#">Add to my bag</a>
                </div>
            </div>
        </div>
    </section>

    {!! view_render_event('bagisto.shop.products.view.after', ['product' => $product]) !!}

@endsection

@section('full-content-wrapper')

    {!! view_render_event('bagisto.shop.products.view.before', ['product' => $product]) !!}
        <div class="row no-margin">
            <section class="col-12 product-detail">
                <div class="layouter">
                    <product-view>
                        <div class="form-container">
                            @csrf()

                            <input type="hidden" name="product_id" value="{{ $product->product_id }}">

                            {{-- product-gallery --}}
                            <div class="left col-lg-5">
                                @include ('shop::products.view.gallery')
                            </div>

                            {{-- right-section --}}
                            <div class="right col-lg-7">
                                {{-- product-info-section --}}
                                <div class="row info">
                                    <h2 class="col-lg-12">{{ $product->name }}</h2>

                                    @if ($total)
                                        <div class="reviews col-lg-12">
                                            <star-ratings
                                                push-class="mr5"
                                                :ratings="{{ $avgStarRating }}"
                                            ></star-ratings>

                                            <div class="reviews">
                                                <span>
                                                    {{ __('shop::app.reviews.ratingreviews', [
                                                        'rating' => $avgRatings,
                                                        'review' => $total])
                                                    }}
                                                </span>
                                            </div>
                                        </div>
                                    @endif

                                    @include ('shop::products.view.stock', ['product' => $product])

                                    <div class="col-12 price">
                                        @include ('shop::products.price', ['product' => $product])
                                    </div>

                                    <div class="product-actions">
                                        @include ('shop::products.add-to-cart', [
                                            'form' => false,
                                            'product' => $product,
                                            'showCompare' => true,
                                            'showCartIcon' => false,
                                        ])
                                    </div>
                                </div>

                                {!! view_render_event('bagisto.shop.products.view.short_description.before', ['product' => $product]) !!}

                                @if ($product->short_description)
                                    <div class="description">
                                        <h3 class="col-lg-12">{{ __('velocity::app.products.short-description') }}</h3>

                                        {!! $product->short_description !!}
                                    </div>
                                @endif

                                {!! view_render_event('bagisto.shop.products.view.short_description.after', ['product' => $product]) !!}


                                {!! view_render_event('bagisto.shop.products.view.quantity.before', ['product' => $product]) !!}

                                @if ($product->getTypeInstance()->showQuantityBox())
                                    <div>
                                        <quantity-changer></quantity-changer>
                                    </div>
                                @else
                                    <input type="hidden" name="quantity" value="1">
                                @endif

                                {!! view_render_event('bagisto.shop.products.view.quantity.after', ['product' => $product]) !!}

                                @include ('shop::products.view.configurable-options')

                                @include ('shop::products.view.downloadable')

                                @include ('shop::products.view.grouped-products')

                                @include ('shop::products.view.bundle-options')

                                @include ('shop::products.view.attributes', [
                                    'active' => true
                                ])

                                {{-- product long description --}}
                                @include ('shop::products.view.description')

                                {{-- reviews count --}}
                                @include ('shop::products.view.reviews', ['accordian' => true])
                            </div>
                        </div>
                    </product-view>
                </div>
            </section>

            <div class="related-products">
                @include('shop::products.view.related-products')
                @include('shop::products.view.up-sells')
            </div>

            <div class="store-meta-images col-3">
                @if(
                    isset($velocityMetaData['product_view_images'])
                    && $velocityMetaData['product_view_images']
                )
                    @foreach (json_decode($velocityMetaData['product_view_images'], true) as $image)
                        @if ($image && $image !== '')
                            <img src="{{ url()->to('/') }}/storage/{{ $image }}" />
                        @endif
                    @endforeach
                @endif
            </div>
        </div>
    {!! view_render_event('bagisto.shop.products.view.after', ['product' => $product]) !!}
@endsection

@push('scripts')
    <script type='text/javascript' src='https://unpkg.com/spritespin@4.1.0/release/spritespin.js'></script>

    <script type="text/x-template" id="product-view-template">
        <form
            method="POST"
            id="product-form"
            @click="onSubmit($event)"
            action="{{ route('cart.add', $product->product_id) }}">

            <input type="hidden" name="is_buy_now" v-model="is_buy_now">

            <slot v-if="slot"></slot>

            <div v-else>
                <div class="spritespin"></div>
            </div>

        </form>
    </script>

    <script>
        Vue.component('product-view', {
            inject: ['$validator'],
            template: '#product-view-template',
            data: function () {
                return {
                    slot: true,
                    is_buy_now: 0,
                }
            },

            mounted: function () {
                // this.open360View();

                let currentProductId = '{{ $product->url_key }}';
                let existingViewed = window.localStorage.getItem('recentlyViewed');

                if (! existingViewed) {
                    existingViewed = [];
                } else {
                    existingViewed = JSON.parse(existingViewed);
                }

                if (existingViewed.indexOf(currentProductId) == -1) {
                    existingViewed.push(currentProductId);

                    if (existingViewed.length > 3)
                        existingViewed = existingViewed.slice(Math.max(existingViewed.length - 4, 1));

                    window.localStorage.setItem('recentlyViewed', JSON.stringify(existingViewed));
                } else {
                    var uniqueNames = [];

                    $.each(existingViewed, function(i, el){
                        if ($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
                    });

                    uniqueNames.push(currentProductId);

                    uniqueNames.splice(uniqueNames.indexOf(currentProductId), 1);

                    window.localStorage.setItem('recentlyViewed', JSON.stringify(uniqueNames));
                }
            },

            methods: {
                onSubmit: function(event) {
                    if (event.target.getAttribute('type') != 'submit')
                        return;

                    event.preventDefault();

                    this.$validator.validateAll().then(result => {
                        if (result) {
                            this.is_buy_now = event.target.classList.contains('buynow') ? 1 : 0;

                            setTimeout(function() {
                                document.getElementById('product-form').submit();
                            }, 0);
                        }
                    });
                },

                open360View: function () {
                    this.slot = false;

                    setTimeout(() => {
                        $('.spritespin').spritespin({
                            source: SpriteSpin.sourceArray('http://shubham.webkul.com/3d-image/sample-{lane}-{frame}.jpg', {
                                lane: [0,5],
                                frame: [0,5],
                                digits: 2
                            }),
                            // width and height of the display
                            width: 400,
                            height: 225,
                            // the number of lanes (vertical angles)
                            lanes: 12,
                            // the number of frames per lane (per vertical angle)
                            frames: 24,
                            // interaction sensitivity (and direction) modifier for horizontal movement
                            sense: 1,
                            // interaction sensitivity (and direction) modifier for vertical movement
                            senseLane: -2,

                            // the initial lane number
                            lane: 6,
                            // the initial frame number (within the lane)
                            frame: 0,
                            // disable autostart of the animation
                            animate: false,

                            plugins: [
                                'progress',
                                '360',
                                'drag'
                            ]
                        });
                    }, 0);
                }
            }
        });

        window.onload = function() {
            var thumbList = document.getElementsByClassName('thumb-list')[0];
            var thumbFrame = document.getElementsByClassName('thumb-frame');
            var productHeroImage = document.getElementsByClassName('product-hero-image')[0];

            if (thumbList && productHeroImage) {
                for (let i=0; i < thumbFrame.length ; i++) {
                    thumbFrame[i].style.height = (productHeroImage.offsetHeight/4) + "px";
                    thumbFrame[i].style.width = (productHeroImage.offsetHeight/4)+ "px";
                }

                if (screen.width > 720) {
                    thumbList.style.width = (productHeroImage.offsetHeight/4) + "px";
                    thumbList.style.minWidth = (productHeroImage.offsetHeight/4) + "px";
                    thumbList.style.height = productHeroImage.offsetHeight + "px";
                }
            }

            window.onresize = function() {
                if (thumbList && productHeroImage) {

                    for(let i=0; i < thumbFrame.length; i++) {
                        thumbFrame[i].style.height = (productHeroImage.offsetHeight/4) + "px";
                        thumbFrame[i].style.width = (productHeroImage.offsetHeight/4)+ "px";
                    }

                    if (screen.width > 720) {
                        thumbList.style.width = (productHeroImage.offsetHeight/4) + "px";
                        thumbList.style.minWidth = (productHeroImage.offsetHeight/4) + "px";
                        thumbList.style.height = productHeroImage.offsetHeight + "px";
                    }
                }
            }
        };
    </script>
@endpush