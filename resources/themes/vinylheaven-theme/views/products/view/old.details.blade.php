@inject ('searchHelper', 'VinylHeaven\Search\Helpers\Search')

@php
if(!isset($has_from_price)) {
    $marketplace_product = $mp_product;
}
//dd($product_full->formats);
@endphp

<section class="container product-details-container">
    <div class="row">
        <div class="col-12 col-md-6">
            <!-- Product Format, Product Gallery -->
            <div class="product-gallery" data-component="quality-slider">
                <div class="product-format">12"</div>

                <div class="product-slider js-quality-for">
                    @if (count($product_full->images))
                        @foreach ($product_full->images as $image_index => $image)
                            @if ($image_index < 4)
                                <figure>
                                    <img src="{{ $image->url }}" alt="">

                                    <figcaption>Images are from an external source and may differ from reality</figcaption>
                                </figure>
                            @endif
                        @endforeach
                    @else
                        <figure>
                            <img src="{{ asset('themes/vinylexpress/assets/images/Default-Product-Image.png') }}" alt="">

                            <figcaption>Images are from an external source and may differ from reality</figcaption>
                        </figure>
                    @endif
                </div>

                <div class="product-slider-nav js-quality-nav">
                    @if (count($product_full->images))
                        @foreach ($product_full->images as $image_index => $image)
                            @if ($image_index < 4)
                                <figure>
                                    <img src="{{ $image->url }}" alt="">
                                </figure>
                            @endif
                        @endforeach
                    @else
                        <figure>
                            <img src="{{ asset('themes/vinylexpress/assets/images/Default-Product-Image.png') }}" alt="">
                        </figure>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-12 col-md-6">
            <!-- Product Info -->
            <div class="row">
                <div class="col-12">
                    <div class="product-details">
                        <div class="details-title">{{ $product->name }}</div>

                        <div class="details-artist-name">
                            <span>{{ $product_full->getArtistName() }}</span>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col">
                    <div class="product-details">
                        @if ($product_full->hasGenre())
                            <h4 class="details-heading">Genre</h4>

                            <div class="details-info">
                                {{ $product_full->getGenre() }}
                            </div>
                        @endif

                        <h4 class="details-heading">Release date</h4>

                        <div class="details-info">
                            {{ $product_full->release_date }}
                        </div>

                        <h4 class="details-heading">Country</h4>

                        <div class="details-info">
                            {{ $product_full->country }}
                        </div>

                        @if ($product_full->hasCompanyName())
                            <h4 class="details-heading">Company</h4>

                            <div class="details-info">
                                {{ $product_full->getCompanyName() }}
                            </div>
                        @endif

                        <h4 class="details-heading">Notes</h4>

                        <div class="details-info">
                            {{ $product_full->description }}
                        </div>
                    </div>
                </div>

                <div class="col-6">
                    @if (!isset($has_from_price))
                        <div class="product-details">
                            <h4 class="details-heading">Condition</h4>

                            <div class="details-info">
                                <div class="condition-grading-circle list-view">
                                    <span class="circle-text {{ $searchHelper->getConditionGradingBackgroundClass($product_full->getConditionGrading($marketplace_product->condition)) }}">{{ $product_full->getConditionGrading($marketplace_product->condition) }}</span> 
                                    {{ $product_full->getConditionText($marketplace_product->condition) }}
                                </div>
                            </div>

                            <h4 class="details-heading">Sleeve Condition</h4>

                            <div class="details-info">
                                <div class="condition-grading-circle list-view">
                                    <span class="circle-text  {{ $searchHelper->getConditionGradingBackgroundClass($product_full->getSleeveConditionGrading($marketplace_product->sleeve_condition)) }}">{{ $product_full->getSleeveConditionGrading($marketplace_product->sleeve_condition) }}</span> 
                                    {{ $product_full->getSleeveConditionText($marketplace_product->sleeve_condition) }}
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class="product-details">
                        <h4 class="details-heading">Formats</h4>

                        <table class="table table-sm table-borderless">
                            <tbody>
                                @foreach ($product_full->formats as $format)
                                    <tr>
                                        <td><strong>Type:</strong></td>
                                        <td>{{ $format['name'] }}</td>
                                    </tr>

                                    <tr>
                                        <td><strong>Quantity:</strong></td>
                                        <td>{{ $format['qty'] }}</td>
                                    </tr>

                                    @if (!empty($format['text']))
                                        <tr>
                                            <td><strong>Text:</strong></td>
                                            <td>{{ $format['text'] }}</td>
                                        </tr>
                                    @endif

                                    @if (isset($format['descriptions']))
                                        @if (count($format['descriptions']))
                                            @foreach ($format['descriptions'] as $index => $description)
                                                @if ($index == 0)
                                                    <tr>
                                                        <td><strong>Notes:</strong></td>
                                                        <td>{{ $description }}</td>
                                                    </tr>
                                                @else
                                                    <tr>
                                                        <td></td>
                                                        <td>{{ $description }}</td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@push('scripts')
<script type="text/javascript" src="{{ asset('themes/vinylheaven-theme/assets/dist/js/vendor/slick.js') }}"></script>
<script type="text/javascript" src="{{ asset('themes/vinylheaven-theme/assets/dist/js/components/quality-slider.js') }}"></script>
@endpush