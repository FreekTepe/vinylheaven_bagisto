<div class="container-fluid container-lg container-xl">
    <div class="row">
    <div class="col-12 col-md-6 mt-2">
            <div class="info-heading">Tracklist</div>
            <div class="table-responsive">
            <table class="table table-borderless table-sm">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Duration</th>
                    </tr>
                </thead>
                @foreach ($product->product->tracklist as $item_index => $item)
                    <tr>
                        <td>{{ $item['position'] }}</td>
                        <td>{{ $item['title'] }}</td>
                        <td>{{ $item['duration'] }}</td>
                    </tr>
                @endforeach
            </table>
            </div>
        </div>



        <div class="col-12 col-md-6 mt-2">
            <div class="info-heading">Product Details</div>



            @if (isset($product->product->description))
            <div class="text-justify font-weight-light mb-3">
                {{ $product->product->description }}
            </div>


            <ul class="info-list mb-3">
                @foreach($product->product->artists as $artist_index => $artist)
                <li class="list-item">
                    <div class="item-left">
                    Artist
                    </div>
                    <div class="item-right">
                        @if(isset($artist->name))
                            {{ $artist->name }}
                        @endif
                    </div>
                </li>
                @endforeach
            </ul>
            @endif

            @if (isset($product->product->companies))

            <ul class="info-list mb-3">
                @foreach ($product->product->companies as $company_index => $company)
                    <li class="list-item">
                        <div class="item-left">{{ $company['entity_type_name'] }}</div>
                        <div class="item-right">{{ $company['name'] }}</div>
                    </li>
                @endforeach
            </ul>

            @endif

            @if (isset($product->product->identifiers))

            <ul class="info-list mb-3">
                @foreach($product->product->identifiers as $identifier_index => $identifier)
                <li class="list-item">
                    <div class="item-left">{{ $identifier['type'] }}
                    @if ($identifier['description'] != null)
                        ({{ $identifier['description']}})
                    @endif
                    </div>
                    <div class="item-right">{{ $identifier['value'] }}</div>
                </li>
                @endforeach
            </ul>
            @endif

        </div>
        

    </div>
</div>

<div class="info-show-more" id="info-show-more">
    <div class="d-flex justify-content-center align-items-end show-more" id="info-show-more-toggler">
        <span class="more-icon"><i class="fas fa-angle-double-down"></i></span>
    </div>
</div>

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#info-show-more-toggler').click(function(){
                if(!$('.product-info').hasClass('open')) {
                    $('.product-info').addClass('open');
                } else {
                    $('.product-info').removeClass('open');
                }
            });
        });
    </script>
@endpush