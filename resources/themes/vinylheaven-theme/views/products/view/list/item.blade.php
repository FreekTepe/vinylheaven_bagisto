@inject ('wishListHelper', 'Webkul\Customer\Helpers\Wishlist')
@php
    $product_link = '';
    $marketProductsCount = $product->product->marketplaceProducts()->count();
    if (isset($has_from_price) || isset($is_catalog)){
        $product_link = '/'. $product->url_key;
    } else {
        $product_link = '/marketplace/seller/' . $seller->url . '/product/'. $product->url_key;
        $product_link = '/marketplace/seller/' . $product->seller->url . '/product/'. $product->product->url_key;
    }
    $reviewRepository = app('Webkul\Marketplace\Repositories\ReviewRepository');
@endphp

<div class="row">
    <div class="col-12">
        <a href="{{ $product_link }}" class="list-item" data-toggle="tooltip" title="{{ $product->product->name }} - {{ $product->product->getArtistName() }}">
            <div class="row">
                <div class="col-12 col-md-4 col-lg-3">
                    <div class="row">
                        <div class="col-12">
                            <div class="item-image">
                                {!! $product->product->getPrimaryImage() !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-8 col-lg-9">
                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-3 col-xl-3">
                            <div class="content-title">
                                <p>
                                    {{ $product->product->name }}
                                </p>
                            </div>
                            <div class="content-category">- {{ $product->product->getArtistName() }}</div>

                            <div class="product-details">
                                @foreach($product->product->formats as $format)
                                    @if ($format !== 0)
                                        {{$format['name']}}
                                        @if ($format['descriptions'])
                                            ({{ $format['descriptions'][0] }})
                                        @endif
                                        | {{ $format['qty'] }} disk
                                    @endif
                                @endforeach

                                @if ($product->product->hasGenre())
                                    <div class="details-info">
                                        {{ $product->product->getGenre() }}
                                    </div>
                                @endif

                                <div class="details-info">
                                    {{ $product->product->release_date }}
                                </div>

                                <div class="details-info">
                                    {{ $product->product->country }}
                                </div>

                                @foreach ($product->product->labels()->get() as $label_index => $label)
                                    @if($label_index == 0)
                                        <div class="details-info">
                                                {{$label->name}} - {{$label->cat_no}} {{$label->label_id}}
                                        </div>
                                    @else
                                        <div class="details-info">
                                                {{$label->name}} - {{$label->cat_no}} {{$label->label_id}}
                                        </div>
                                    @endif
                                @endforeach

                                @if ($product->product->hasCompanyName())
                                    <div class="details-info">
                                        {{ $product->product->getCompanyName() }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-12 col-md-6 col-lg-4 col-xl-5">
                            @if ($is_seller)
                                <div class="font-weight-bold text-center">
                                    <p class="text-con">
                                        @include('shop::products.card.condition')
                                    </p>
                                </div>
                                <div class="card">
                                    <div class="card-body">
                                        <p class="font-weight-bold">Notes:</p>
                                        <p class="card-text">CD and booklet show minimal wear. CD has light marks.
                                            Booklet shows some wear from taking it in and out of the jewel case.</p>
                                    </div>
                                </div>
                            @else
                                <div class="text-center">
                                    <div class="font-weight-bold">
                                        <p class="text-con">
                                            @include('shop::products.card.condition')
                                        </p>
                                    </div>
                                    <p class="text font-weight-bold">
                                        @if($product->seller)
                                            {{$product->seller->shop_title }}<br>
                                            Star Rating
                                            @for ($i = 1; $i <= $reviewRepository->getAverageRating($seller); $i++)
                                                <span class="icon star-icon"></span>
                                            @endfor
                                            ({{ $reviewRepository->getTotalReviews($product->seller) }})
                                    </p>
                                    <p class="text">{{ core()->country_name($product->seller->country) }}</p>
                                        @endif
                                </div>
                                <div class="card">
                                    <div class="card-body">
                                        <p class="font-weight-bold">Notes:</p>
                                        <p class="card-text">CD and booklet show minimal wear. CD has light marks.
                                            Booklet shows some wear from taking it in and out of the jewel case.</p>
                                    </div>
                                </div>
                            @endif
                        </div>

                        <div class="col-12 col-md-12 col-lg-4 col-xl-4">
                            <div class="row">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="item-price-list col-12 col-md-7 col-lg-11 col-xl-10 text-center">
                                            @if ($is_search)
                                                @include('shop::products.card.from-price-search')
                                            @else
                                                @include('shop::products.card.price')
                                            @endif
                                        </div>
                                        <div class="info-price-add-to-cart-list d-flex justify-content-start align-items-center">
                                            <form action="{{ route('cart.add', $product->product->product_id) }}" method="POST" class="info-add-to-cart-form">
                                                @csrf
                                                <input type="hidden" name="product_id" value="{{ $product->product->product_id }}">
                                                <input type="hidden" name="quantity" value="1">
                                                <input type="hidden" name="seller_info[product_id]" value="{{ $product->id }}">
                                                <input type="hidden" name="seller_info[seller_id]" value="{{ $product->product->marketplace_product()->first()->seller->id }}">
                                                <input type="hidden" name="seller_info[is_owner]" value="0">

                                                <button class="add-to-cart-button col-12 text-white text-uppercase" {{ $product->product->isSaleable() ? '' : '' }} >
                                                    {{ __('shop::app.products.add-to-cart') }}  <i class="fas fa-shopping-basket"></i>
                                                </button>
                                            </form>
                                        </div>
                                        <div class="w-100"></div>
                                        <div class="info-price-add-to-cart-list d-flex justify-content-start align-items-center">
                                            @auth('customer')
                                                {!! view_render_event('bagisto.shop.products.wishlist.before') !!}
                                                <button
                                                    @if ($wishListHelper->getWishlistProduct($product))
                                                    class="already add-to-wishlist-button col-md-9 col-lg-8 col-xl-4"
                                                    title="{{ __('shop::app.customer.account.wishlist.remove-wishlist-text') }}"
                                                    id="wishlist-changer"
                                                    href="{{ route('customer.wishlist.add', $product->product_id) }}"
                                                    data-toggle="marketplace">
                                                    <i class="far fa-heart"></i>
                                                    @else
                                                        class="add-to-wishlist-button col-md-9 col-lg-8 col-xl-4"
                                                        title="{{ __('shop::app.customer.account.wishlist.add-wishlist-text') }}"
                                                        id="wishlist-changer"
                                                        href="{{ route('customer.wishlist.add', $product->product_id) }}"
                                                        data-toggle="marketplace">
                                                        <i class="fa fa-heart" aria-hidden="true"></i>
                                                    @endif
                                                </button>
                                                {!! view_render_event('bagisto.shop.products.wishlist.after') !!}
                                            @endauth
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>