@if($product_view_config->is_seller_product)
    @guest('customer')
        @php
            $shipping_price = $product->getShippingPrice();
        @endphp
    @endguest

    @auth('customer')
        @php
            $shipping_price = $product->getShippingPrice(auth()->guard('customer')->user());
        @endphp
    @endauth
@endif

<div class="row">
    <div class="col-12">
        <div class="details-title">{{ $product->product->name }}</div>
    </div>

    <div class="col-12">
        <div class="details-artist">
            <span class="artist">{{ $product->product->getArtistName() }}</span>
        </div>
    </div>
</div>

<div class="row mb-4">
    @if($product_view_config->is_seller_product)
        <div class="col-auto details-statistic first">
            {{ $seller->shop_title }}
        </div>

        <div class="col-auto details-statistic">

            @if ($seller->getAverageRating() > 0)
                <span class="mr-1">
                    @for ($i = 1; $i <= $seller->getAverageRating(); $i++)
                        <i class="fa fa-star"></i>
                    @endfor

                    @for ($i = 1; $i <= ($seller->max_rating - $seller->getAverageRating()); $i++)
                        <i class="fa fa-star-o"></i>
                    @endfor
                </span>
                <span class="mr-1">
                    {{ round($seller->getAverageRating()) }}/{{ $seller->max_rating }}
                </span>
            @else
                <span class="mr-1">New seller</span>
            @endif
        </div>
        <div class="col-auto details-statistic last">
            Ships from: {{ core()->country_name($seller->country) }}
        </div>
    @endif
</div>

<div class="row align-items-center mb-4">
    <div class="col-auto details-price-shipping">
        <div class="row aling-items-center">
            <div class="col details-price">
                @if($product_view_config->is_seller_product)
                    <span class="text-danger">{{ core()->currency(core()->convertPrice($product->price, core()->getBaseCurrencyCode(), 'EUR')) }}</span>
                @else
                    From: <span class="text-danger">{{ $product->product->getMarketplaceProductsFromPrice() }}</span>
                    <div class="col details-shipping-price">+ S&H <i class="fa fa-truck"></i></div>
                @endif
            </div>
        </div>

        @if($product_view_config->is_seller_product)
            <div class="row align-items-center">
                <div class="col details-shipping-price">
                    @if ($shipping_price)
                    + {{ core()->currency(core()->convertPrice($shipping_price, core()->getBaseCurrencyCode(), 'EUR')) }}
                    @else
                    + S&H
                    @endif
                    <i class="fa fa-truck"></i></span>
                </div>
            </div>
        @endif
    </div>

    @if($product_view_config->is_seller_product)
        <div class="col-auto ml-auto details-add-to-cart">
            <form action="{{ route('cart.add', $product->product_id) }}" method="POST" class="form-inline">
                @csrf
                <input type="hidden" name="product_id" value="{{ $product->product_id }}">
                <input type="hidden" name="quantity" value="1">
                <input type="hidden" name="seller_info[product_id]" value="{{ $product->id }}">
                <input type="hidden" name="seller_info[seller_id]" value="{{ $seller->id }}">
                <input type="hidden" name="seller_info[is_owner]" value="0">
                
                <button class="button button-bag" {{ $product->product->isSaleable() ? '' : '' }}>{{ __('shop::app.products.add-to-cart') }}</button>
            </form>
        </div>
    @endif

    @auth('customer')
        <div class="col-auto details-add-to-wishlist">
            <a href="#" class="button button-heart">
                <img src="{{ asset('themes/vinylheaven-theme/assets/img/heart-icon.png') }}" alt="">
            </a>
        </div>
    @endauth
</div>

@if($product_view_config->is_seller_product)
    <div class="row mb-2 details-conditions">
        <div class="col-6">
            <div class="text-header">Media condition</div>
            
            <div class="details-condition condition-grading-circle">
                {!! $product->getMediaConditionGradingCircle() !!}
                {{ $product->condition }}
            </div>
        </div>
        <div class="col-6">
            <div class="text-header">Sleeve condition</div>
            
            <div class="details-condition condition-grading-circle list-view">
                {!! $product->getSleeveConditionGradingCircle() !!}
                {{ $product->sleeve_condition }}
            </div>
        </div>
    </div>
    <div class="row mb-4">
        <div class="col-12">
            @if($product->description)
                <div class="text-header">Seller notes</div>
                    <div class="details-seller-notes">{{ $product->description }}</div>
            @endif
        </div>
    </div>
@endif

    <div class="row">
        <div class="col-12 details-formats table-responsive">
            <table class="table table-borderless table-sm table-striped">
                <tr>
                    <td>Artist</td>
                    <td>
                    @foreach ($product->product->artists as $artist_index => $artist)
                        @if ($artist_index == 0)
                            {{ $artist->name }}
                        @else
                            <br>{{ $artist->name }}
                        @endif
                    @endforeach
                    </td>
                </tr>
                <tr>
                    <td>Format</td>
                    <td>
                    @foreach($product->product->formatsFormated() as $format_index => $format)
                            @if ($format_index == 0)
                                {{$format}}
                            @else
                                <br>{{$format}}
                            @endif
                        @endforeach
                    </td>
                </tr>
                @if (!empty($product->product->country))
                <tr>
                    <td>Country</td>
                    <td>{{ $product->product->country }}</td>
                </tr>
                @endif
                @if (!empty($product->product->release_date))
                <tr>
                    <td>Release date</td>
                    <td>{{ $product->product->release_date }}</td>
                </tr>
                @endif
                <tr>
                    <td>Label</td>
                    <td>
                        @foreach ($product->product->labels()->get() as $label_index => $label)
                            @if ($label_index == 0)
                                {{$label->name}} - {{$label->cat_no}}
                            @else
                                <br>{{$label->name}} - {{$label->cat_no}}
                            @endif
                        @endforeach
                    </td>
                </tr>
                @if (!empty($product->product->identifiers))
                <tr>
                    <td>Barcode</td>
                    <td>
                        @foreach ($product->product->identifiers as $identifier)
                            @if(strtoupper($identifier['type']) == 'BARCODE')
                                {{-- $identifier['type'] --}} {{ $identifier['value'] }} 
                                @php 
                                    break; 
                                @endphp
                            @endif
                        @endforeach        
                    </td>
                </tr>
                @endif
            </table>
    </div>
</div>