@php
    $count = $product->product->images()->count();
@endphp

@if ($count > 1)

    <div class="product-gallery gallery-images">
        <div class="d-inline-flex flex-column flex-lg-row justify-content-center align-items-start align-content-center">
            <div class="col-12 col-lg-3 gallery-film-roll">
                <div class="d-inline-flex justify-content-center align-items-center align-content-center w-100 film-roll-move-up">
                    <i class="fa fa-chevron-up"></i>
                </div>

                <div id="product-gallery-film-roll" class="list-group film-roll flex-wrap">
                    @foreach ($product->product->images as $image_index => $image)
                        <div class="list-group-item border-0" data-slide="{{ $image_index }}">
                            <img src="{{ $image->getThumbImage() }}" class="gallery-film-roll-image" title="{{ $product->product->name }}" alt="" />
                        </div>
                    @endforeach
                </div>

                <div class="d-inline-flex justify-content-center align-items-center align-content-center w-100 film-roll-move-down">
                    <i class="fa fa-chevron-down"></i>
                </div>
            </div>

            <div class="col-12 col-lg-9 gallery">
                <div id="product-gallery" class="carousel slide">
                    <div class="carousel-inner">
                        @foreach ($product->product->images as $image_index => $image)
                            @if ($image_index == 0)
                                <div class="carousel-item active">
                                    <img src="{{ $image->Url() }}" class="gallery-film-rol-image img-fluid" title="{{ $product->product->name }}" alt="" />
                                </div>
                            @else
                                <div class="carousel-item">
                                    <img src="{{ $image->Url() }}" class="gallery-film-rol-image img-fluid" title="{{ $product->product->name }}" alt="" />
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div> 
    </div>

    @push('scripts')
    <script type="text/javascript" src="{{ asset('themes/vinylheaven-theme/assets/dist/js/components/gallery.js') }}"></script>
    @endpush

@elseif ($count == 1)

    <div class="product-gallery gallery-images">
        <div class="d-inline-flex flex-column flex-lg-row justify-content-center align-items-start align-content-center">
            <div class="col-12 col-lg-3"></div>
                
            <div class="col-12 col-lg-9 gallery">
            @foreach ($product->product->images as $image_index => $image)
                <img src="{{ $image->Url() }}" title="{{ $product->product->name }}" alt="" />
            @endforeach
            </div>
        </div>
    </div>


@else ($count == 0)

    <div class="product-gallery gallery-images">
        <div class="d-inline-flex flex-column flex-lg-row justify-content-center align-items-start align-content-center">
            <div class="col-12 col-lg-3"></div>
                
            <div class="col-12 col-lg-9 gallery">
                <img src="{{ $product->product->getProductImagePlaceholder() }}" title="{{ $product->product->name }}" alt="" />
            </div>
        </div>
    </div>

@endif