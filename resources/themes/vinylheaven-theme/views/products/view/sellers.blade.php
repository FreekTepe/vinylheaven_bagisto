@inject ('searchHelper', 'VinylHeaven\Search\Helpers\Search')

<section class="container product-sellers">
    <div class="row">
        <div class="col-12">
            <table class="table table-responsive-sm product-sellers-table">
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th>Seller</th>
                        <th class="text-center">Condition</th>
                        <th class="text-center">Sleeve</th>
                        <th class="text-center">Price</th>
                        <th></th>
                    </tr>
                </thead>    
            
                <tbody>
                    @foreach ($product_helper->getMarketplaceProductsByProduct($product->product_id) as $mp_product_index => $mp_product)
                        @php
                            $seller = app('Webkul\Marketplace\Models\Seller')->where([['id', "=",$mp_product->marketplace_seller_id]])->first();
                        @endphp
                        
                        <tr onclick="location.href='/marketplace/seller/{{ $seller->url }}/product/{{ $product->url_key }}';">
                            <td class="align-middle text-center" scope="row">{{ ($mp_product_index + 1) }}</td>
                            
                            <td class="align-middle">{{ $seller->shop_title }}</td>
                            
                            <td class="align-middle text-center">
                                <div class="condition-grading-circle">
                                    <span class="circle-text {{ $searchHelper->getConditionGradingBackgroundClass($product_full->getConditionGrading($mp_product->condition)) }}">{{ $product_full->getConditionGrading($mp_product->condition) }}</span>
                                </div>
                            </td>

                            <td class="align-middle text-center">
                                <div class="condition-grading-circle">
                                    <span class="circle-text {{ $searchHelper->getConditionGradingBackgroundClass($product_full->getSleeveConditionGrading($mp_product->sleeve_condition)) }}">{{ $product_full->getSleeveConditionGrading($mp_product->sleeve_condition) }} pipo</span>
                                </div>
                            </td>
                            
                            <td class="align-middle text-center">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-12">
                                            <strong>{{ core()->currency($mp_product->price) }}</strong>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">
                                            <cite>Excluding Shipping Costs </cite></td>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            
                            <td class="align-middle text-center">
                                <form action="{{ route('cart.add', $product->product_id) }}" method="POST" class="form-inline">
                                    @csrf
                                    <input type="hidden" name="product_id" value="{{ $product->product_id }}">
                                    <input type="hidden" name="quantity" value="1">
                                    <input type="hidden" name="seller_info[product_id]" value="{{ $mp_product->id }}">
                                    <input type="hidden" name="seller_info[seller_id]" value="{{ $mp_product->marketplace_seller_id }}">
                                    <input type="hidden" name="seller_info[is_owner]" value="0">

                                    <button type="submit" class="button button-bag" style="min-width:auto;"></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>