
<div class="col-6">
    <div class="text-header mb-2">Media condition</div>
    <div class='condition-grading-circle circle-text'>
        {!! $marketplace_product->product->getGradingBackgroundClass($marketplace_product->condition) !!}
        {{ $marketplace_product->condition }}
    </div>
</div>
<div class="col-6">
    <div class="text-header mb-2">Sleeve condition</div>
    <div class='condition-grading-circle circle-text'>
        {!! $marketplace_product->product->getGradingBackgroundClass($marketplace_product->sleeve_condition) !!}
        {{ $marketplace_product->sleeve_condition }}
    </div>
</div>