<div class="mt-1 mb-1" title="{{ $marketplace_product->seller->shop_title }}">

    {{ $marketplace_product->seller->shop_title }}

        @if ($marketplace_product->seller->getAverageRating() > 0)
            | <span class="mr-1">
                @for ($i = 1; $i <= $marketplace_product->seller->getAverageRating(); $i++)
                    <i class="fa fa-star"></i>
                @endfor

                @for ($i = 1; $i <= ($marketplace_product->seller->max_rating - $marketplace_product->seller->getAverageRating()); $i++)
                    <i class="fa fa-star-o"></i>
                @endfor
            </span>

            <span class="mr-1">
                {{ round($marketplace_product->seller->getAverageRating()) }}/{{ $marketplace_product->seller->max_rating }}
            </span>
            <span>
            ({{ $marketplace_product->seller->getTotalReviews() }})
            </span>
        @else
            | <span class="mr-1">New seller</span>
        @endif

        | Ships from: {{ core()->country_name($marketplace_product->seller->country) }}
</div>