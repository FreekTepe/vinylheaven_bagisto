@inject('wishListHelper', 'Webkul\Customer\Helpers\Wishlist')

<div class="row">
    <div class="col-12">
        <a href="{{ route('marketplace.sellers.product.index', ['url' => $marketplace_product->seller->url, 'slug' => $marketplace_product->product->url_key]) }}" class="list-item" data-toggle="tooltip" title="{{ $marketplace_product->product->name }} - {{ $marketplace_product->product->getArtistName() }}">
            <div class="row">
                <div class="col-12 col-md-3 col-lg-2">
                    <div class="row">
                        <div class="col-12">
                            <div class="item-image">
                                {!! $marketplace_product->product->getPrimaryImage() !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-9 col-lg-10">
                    <div class="row">
                        <div class="col-12 col-md-5">
                            <div class="content-title">
                                {{ $marketplace_product->product->name }}
                            </div>

                            <div class="content-category">- {{ $marketplace_product->product->getArtistName() }}</div>

                            <div class="product-details">
                                @foreach($marketplace_product->product->formatsFormated() as $format_index => $format)
                                    @if ($format_index == 0)
                                        {{$format}}
                                    @else
                                        <span class="mx-1">|</span>{{$format}}
                                    @endif
                                @endforeach

                                @if ($marketplace_product->product->hasGenre())
                                    <div class="details-info">
                                        {{ $marketplace_product->product->getGenre() }}
                                    </div>
                                @endif

                                <div class="details-info">
                                    {{ $marketplace_product->product->release_date }}
                                </div>

                                <div class="details-info">
                                    {{ $marketplace_product->product->country }}
                                </div>

                                @foreach ($marketplace_product->product->labels()->get() as $label_index => $label)
                                    @if($label_index == 0)
                                        <div class="details-info">
                                                {{$label->name}} - {{$label->cat_no}} {{$label->label_id}}
                                        </div>
                                    @else
                                        <div class="details-info">
                                                {{$label->name}} - {{$label->cat_no}} {{$label->label_id}}
                                        </div>
                                    @endif
                                @endforeach

                                @if ($marketplace_product->product->hasCompanyName())
                                    <div class="details-info">
                                        {{ $marketplace_product->product->getCompanyName() }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-12 col-md-4">
                            <div class="d-flex flex-column justify-content-center align-items-center w-100">
                                <div class="font-weight-bold text-center">
                                    <p class="text-con mb-0">
                                        @include('shop::products.marketplace.card.condition')
                                    </p>
                                </div>

                                @if ($route_name != 'marketplace.seller.show')
                                    <div class="info-seller">
                                        @include('shop::products.marketplace.list.seller')
                                    </div>
                                @endif

                                <div class="card seller-notes-card w-75 mt-2">
                                    <div class="card-body px-3 py-2">
                                        <p class="font-weight-bold mb-1">Nootes:</p>
                                        
                                        <p class="card-text">
                                            {{ $marketplace_product->description }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-md-3">
                            <div class="row flex-column justify-content-center align-items-center">
                                <div class="item-price-list text-center">
                                    @include('shop::products.marketplace.list.price')
                                </div>

                                <div class="info-price-add-to-cart-list w-100 d-inline-flex justify-content-end align-items-center mt-2">
                                    <form action="{{ route('cart.add', $marketplace_product->product_id) }}" method="POST" class="info-add-to-cart-form w-100 d-inline-flex justify-content-end align-items-center">
                                        @csrf
                                        <input type="hidden" name="product_id" value="{{ $marketplace_product->product_id }}">
                                        <input type="hidden" name="quantity" value="1">
                                        <input type="hidden" name="seller_info[product_id]" value="{{ $marketplace_product->id }}">
                                        <input type="hidden" name="seller_info[seller_id]" value="{{ $marketplace_product->seller->id }}">
                                        <input type="hidden" name="seller_info[is_owner]" value="0">

                                        <button class="add-to-cart-button text-white text-uppercase mb-2" {{ $marketplace_product->isSaleable() ? '' : '' }} >
                                            {{ __('shop::app.products.add-to-cart') }}  <i class="fa fa-shopping-basket"></i>
                                        </button>
                                    </form>
                                </div>

                                <div class="info-price-add-to-cart-list w-100 d-inline-flex justify-content-center align-items-center">
                                    @auth('customer')
                                        {!! view_render_event('bagisto.shop.products.wishlist.before') !!}
                                        <button
                                            @if ($wishListHelper->getWishlistProduct($marketplace_product->product))
                                                class="already add-to-wishlist-button ml-2"
                                                title="{{ __('shop::app.customer.account.wishlist.remove-wishlist-text') }}"
                                                id="wishlist-changer"
                                                href="{{ route('customer.wishlist.add', $marketplace_product->product_id) }}"
                                                data-toggle="marketplace">

                                                <i class="fa fa-heart"></i>
                                            @else
                                                class="add-to-wishlist-button ml-2"
                                                title="{{ __('shop::app.customer.account.wishlist.add-wishlist-text') }}"
                                                id="wishlist-changer"
                                                href="{{ route('customer.wishlist.add', $marketplace_product->product_id) }}"
                                                data-toggle="marketplace">
                                                
                                                <i class="fa fa-heart" aria-hidden="true"></i>
                                            @endif
                                        </button>
                                        {!! view_render_event('bagisto.shop.products.wishlist.after') !!}
                                    @endauth
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>