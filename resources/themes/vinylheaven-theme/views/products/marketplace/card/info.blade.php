<div class="info-product-list">
    @foreach($marketplace_product->product->formatsFormated() as $format_index => $format)
        @if ($format_index == 0)
            {{$format}}
        @else
            <span class="mx-1">|</span>{{$format}}
        @endif
    @endforeach

    @if (!empty($marketplace_product->product->country))
        <span class="mx-1">|</span>{{ $marketplace_product->product->country }}
    @endif

    @if (!empty($marketplace_product->product->getReleaseYear()))
        <span class="mx-1">|</span>{{ $marketplace_product->product->getReleaseYear() }}
    @endif
</div>