@guest('customer')
    @php
        /* $shipping_price = $product->marketplace_product()->first()->getShippingPrice(); */
        $shipping_price ='';
    @endphp
@endguest

@auth('customer')
    @php
        $shipping_price = $marketplace_product->getShippingPrice(auth()->guard('customer')->user());
    @endphp
@endauth

<div class="info-price">
    <span data-toggle="modal" data-target="#terms-conditions-modal-{{ $marketplace_product->id }}" title="See terms & conditions from this seller">
        <span class="text-danger">{{ core()->currency($marketplace_product->price) }}</span>
        <span class="font-weight-light">
            @if ($shipping_price)
                + {{ core()->currency($shipping_price) }}
            @else
                + S&H 
            @endif
            <i class="fa fa-truck"></i>
        </span>
    </span>
</div>

<!-- Modal -->
<div class="modal fade" id="terms-conditions-modal-{{ $marketplace_product->id }}" tabindex="-1" role="dialog" aria-labelledby="terms-conditions-modal-{{ $marketplace_product->id }}" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">{{ $marketplace_product->seller->shop_title }} - Shipping policy</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <p>
                    @if(!empty ($marketplace_product->seller->shipping_policy))
                        {!! $marketplace_product->seller->shipping_policy !!}
                    @else
                        This seller has not yet set a shipping policy.
                    @endif
                </p>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>