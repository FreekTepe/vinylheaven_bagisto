@inject('wishListHelper', 'Webkul\Customer\Helpers\Wishlist')

<div class="marketplace product-card d-flex flex-row justify-content-center mx-auto">
    <a href="{{ route('marketplace.sellers.product.index', ['url' => $marketplace_product->seller->url, 'slug' => $marketplace_product->product->url_key]) }}" class="card-info" title="{{ $marketplace_product->product->name }} - {{ $marketplace_product->product->getArtistName() }}">
        <div class="info-image">
            <div class="image-holder info-image-{{ $marketplace_product->product->url_key }}">

                {!! $marketplace_product->product->getPrimaryImage() !!}

            </div>

        </div>

        <div class="info-content" title="{{ $marketplace_product->product->name }} - {{ $marketplace_product->product->getArtistName() }}">

            <div class="content-title">{{ $marketplace_product->product->name }}</div>

            <div class="content-category">- {{ $marketplace_product->product->getArtistName() }}</div>

            <div class="info-product font-weight-light">
                @include('shop::products.marketplace.card.info')
            </div>
        </div>
        
        @if ($route_name != 'marketplace.seller.show')
            <div class="info-seller">
                @include('shop::products.marketplace.card.seller')
            </div>
        @endif

            <div class="row">
                <div class="col-12 col-sm-4 col-md-4 col-lg-4 mb-2 item-condition">
                    @include('shop::products.marketplace.card.condition')
                </div>
                <div class="col-12 col-sm-8 col-md-8 col-lg-8 item-price">
                    @include('shop::products.marketplace.card.price')
                </div>
            </div>
        
        <div class="add-to-cart d-flex justify-content-start align-items-center">
            <form action="{{ route('cart.add', $marketplace_product->product_id) }}" method="POST" class="info-add-to-cart-form">
                @csrf
                <input type="hidden" name="product_id" value="{{ $marketplace_product->product_id }}">
                <input type="hidden" name="quantity" value="1">
                <input type="hidden" name="seller_info[product_id]" value="{{ $marketplace_product->id }}">
                <input type="hidden" name="seller_info[seller_id]" value="{{ $marketplace_product->seller->id }}">
                <input type="hidden" name="seller_info[is_owner]" value="0">

                <button class="button button-small button-bag button-bag-small" {{ $marketplace_product->isSaleable() ? '' : '' }}>
                    {{ __('shop::app.products.add-to-cart') }}
                </button>
                @auth('customer')
                    {!! view_render_event('bagisto.shop.products.wishlist.before') !!}
                        <button
                            @if ($wishListHelper->getWishlistProduct($marketplace_product))
                                class="add-to-wishlist already button button-small button-heart"
                                title="{{ __('shop::app.customer.account.wishlist.remove-wishlist-text') }}"
                            @else
                                class="add-to-wishlist button button-small button-heart"
                                title="{{ __('shop::app.customer.account.wishlist.add-wishlist-text') }}"
                            @endif
                            
                            id="wishlist-changer"
                            href="{{ route('customer.wishlist.add', $marketplace_product->product_id) }}"
                            data-toggle=marketplace>

                            <i class="fa fa-heart" aria-hidden="true"></i>
                        </button>
                    {!! view_render_event('bagisto.shop.products.wishlist.after') !!}
                @endauth
            </form>
        </div>
    </a>
</div>