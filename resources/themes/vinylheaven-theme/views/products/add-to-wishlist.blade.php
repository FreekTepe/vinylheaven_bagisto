{!! view_render_event('bagisto.shop.products.add_to_wishlist.before', ['product' => $product]) !!}

    <div class="mx-0 no-padding">
        @if (isset($showCompare) && $showCompare)
            <compare-component
                @auth('customer')
                    customer="true"
                @endif

                @guest('customer')
                    customer="false"
                @endif

                slug="{{ $product->url_key }}"
                product-id="{{ $product->id }}"
            ></compare-component>
        @endif

        @if (! (isset($showWishlist) && !$showWishlist))
            @include('shop::products.wishlist', [
                'addClass' => $addWishlistClass ?? ''
            ])
        @endif

        <div class="add-to-wishlist-btn pl0">
            @if (isset($form) && !$form)
                <button
                    type="submit"
                    {{ ! $product->isSaleable() ? 'disabled' : '' }}
                    class="theme-btn {{ $addToCartBtnClass ?? '' }}">

                    @if (! (isset($showCartIcon) && !$showCartIcon))
                        <i class="material-icons text-down-3">shopping_cart</i>
                    @endif

                        {{ __('shop::app.products.add-to-wishlist') }}
                </button>
            @elseif(isset($addToCartForm) && !$addToCartForm)
                <form
                    method="POST"
                    action="{{ route('wishlist.add', $product->product_id) }}">

                    @csrf

                    <input type="hidden" name="product_id" value="{{ $product->product_id }}">
                    <input type="hidden" name="quantity" value="1">
                    <button
                        type="submit"
                        {{ ! $product->isSaleable() ? 'disabled' : '' }}
                        class="btn btn-add-to-wishlist {{ $addToCartBtnClass ?? '' }}">

                        @if (! (isset($showCartIcon) && !$showCartIcon))
                            <i class="material-icons text-down-3">wishlist_cart</i>
                        @endif

                        <span class="fs14 fw6 text-uppercase text-up-4">
                            {{ $btnText ?? __('shop::app.products.add-to-wishlist') }}
                        </span>
                    </button>
                </form>
            @else
                <add-to-wishlist
                    form="true"
                    csrf-token='{{ csrf_token() }}'
                    product-flat-id="{{ $product->id }}"
                    product-id="{{ $product->product_id }}"
                    reload-page="{{ $reloadPage ?? false }}"
                    move-to-wishlist="{{ $moveToCart ?? false }}"
                    add-class-to-btn="{{ $addToCartBtnClass ?? '' }}"
                    is-enable={{ ! $product->isSaleable() ? 'false' : 'true' }}
                    show-wishlist-icon={{ !(isset($showCartIcon) && !$showCartIcon) }}
                    btn-text="{{ $btnText ?? __('shop::app.products.add-to-wishlist') }}">
                </add-to-wishlist>
            @endif
        </div>
    </div>

{!! view_render_event('bagisto.shop.products.add_to_wishlist.after', ['product' => $product]) !!}