@inject('wishListHelper', 'Webkul\Customer\Helpers\Wishlist')

<div class="row">
    <div class="col-12 border-bottom">
        <a href="{{ $product->getHref() }}" class="list-item" data-toggle="tooltip" title="{{ $product->name }} - {{ $product_artist_name }}">
            <div class="row">
                <div class="col-12 col-md-3">
                    <div class="item-image">
                        {!! $product->getPrimaryImage() !!}
                    </div>
                </div>

                <div class="col-12 col-md-9">
                    <div class="row">
                        <div class="col-12">
                            <div class="content-title">
                                {{ $product->name }}
                            </div>
                            <div class="content-category">
                                - {{ $product_artist_name }}
                            </div>
                            @if (($product_marketplace_products->count() == 1) && ($route_name != 'marketplace.seller.show'))
                                <div class="info-seller text-right font-weight-light">
                                    @include('shop::products.marketplace.list.seller', ['marketplace_product' => $product_marketplace_products->first()])
                                </div>
                            @else
                                <div class="items-for-sale font-weight-bold text-right">
                                    {{ $product_marketplace_products->count() }} items for sale on the marketplace
                                </div>
                            @endif
                        </div>

                        <div class="col-12 col-md-4 product-details text">
                            <div class="details-format mt-2 ml-3">
                                @foreach($product->formatsFormated() as $format_index => $format)
                                    @if ($format_index == 0)
                                        {{$format}}
                                    @else
                                        <br>{{$format}}
                                    @endif
                                @endforeach
                            </div>

                            @if ((!empty($product->country)) || (!empty($product->release_date)))
                                @if (empty($product->country))
                                    <div class="details-country-year mt-2 ml-3">
                                        {{ $product->release_date }}
                                    </div>
                                @elseif (empty($product->release_date))
                                    <div class="details-country-year mt-2 ml-3">
                                        {{ $product->country }}
                                    </div>
                                @else
                                    <div class="details-country-year mt-2 ml-3">
                                        {{ $product->country }} | {{ $product->release_date }}
                                    </div>
                                @endif
                            @endif

                            <div class="details-label mt-2 ml-3">
                                @foreach ($product->labels()->get() as $label_index => $label)
                                    @if ($label_index == 0)
                                        {{$label->name}} - {{$label->cat_no}}
                                    @else
                                        <br>{{$label->name}} - {{$label->cat_no}}
                                    @endif
                                @endforeach
                            </div>

                            @if (!empty($product->product->identifiers))
                                <div class="details-barcode mt-2 ml-3">
                                    @foreach ($product->product->identifiers as $identifier)
                                        @if(strtoupper($identifier['type']) == 'BARCODE')
                                            {{ $identifier['type'] }} {{ $identifier['value'] }} 
                                            @php 
                                                break; 
                                            @endphp
                                        @endif
                                    @endforeach
                                </div>
                            @endif       

                            @if(!empty ($product_marketplace_products->first()->description) && ($product_marketplace_products->count() == 1))
                                <div class="card seller-notes-card mt-2">
                                    <div class="card-body px-3 py-2">
                                        <p class="mb-1">Seller notes:</p>
                                        
                                        <p class="card-text">
                                            {{ $product_marketplace_products->first()->description }}
                                        </p>
                                    </div>
                                </div>
                            @endif
                        </div>

                        <div class="col-12 col-md-8">
                            
                            @if ($product_marketplace_products->count() == 1)

                            <div class="row">

                                <div class="col-12 col-md-4 w-100 d-inline-flex justify-content-start align-items-center mt-2">
                                    @include('shop::products.marketplace.list.price', ['marketplace_product' => $product_marketplace_products->first()])
                                </div>

                                <div class="col-12 col-md-8 add-to-cart w-100 d-inline-flex justify-content-end align-items-center mt-2">
                                    <form action="{{ route('cart.add', $product_marketplace_products->first()->product_id) }}" method="POST" class="info-add-to-cart-form w-100 d-inline-flex justify-content-end align-items-center">
                                        @csrf
                                        <input type="hidden" name="product_id" value="{{ $product_marketplace_products->first()->product_id }}">
                                        <input type="hidden" name="quantity" value="1">
                                        <input type="hidden" name="seller_info[product_id]" value="{{ $product_marketplace_products->first()->id }}">
                                        <input type="hidden" name="seller_info[seller_id]" value="{{ $product_marketplace_products->first()->seller->id }}">
                                        <input type="hidden" name="seller_info[is_owner]" value="0">

                                        <button class="button button-bag" {{ $product_marketplace_products->first()->isSaleable() ? '' : '' }} >
                                            {{ __('shop::app.products.add-to-cart') }}
                                        </button>
                                    </form>
                                    
                                    @auth('customer')
                                        {!! view_render_event('bagisto.shop.products.wishlist.before') !!}
                                        <button
                                            @if ($wishListHelper->getWishlistProduct($product))
                                                class="add-to-wishlist already button button-heart ml-2"
                                                title="{{ __('shop::app.customer.account.wishlist.remove-wishlist-text') }}"
                                                id="wishlist-changer"
                                                href="{{ route('customer.wishlist.add', $product_marketplace_products->first()->product_id) }}"
                                                data-toggle="marketplace">

                                                <i class="fa fa-heart"></i>
                                            @else
                                                class="add-to-wishlist button button-heart ml-2"
                                                title="{{ __('shop::app.customer.account.wishlist.add-wishlist-text') }}"
                                                id="wishlist-changer"
                                                href="{{ route('customer.wishlist.add', $product_marketplace_products->first()->product_id) }}"
                                                data-toggle="marketplace">
                                                
                                                <i class="fa fa-heart" aria-hidden="true"></i>
                                            @endif
                                        </button>
                                        {!! view_render_event('bagisto.shop.products.wishlist.after') !!}
                                    @endauth
                                </div>
                            </div>

                            <div class="row details-condition font-weight-light">
                                @include('shop::products.marketplace.list.condition', ['marketplace_product' => $product_marketplace_products->first()])
                            </div>
                            
                            @else
                            <div class="row">
                                <div class="col-12 col-md-4 w-100 d-inline-flex justify-content-start align-items-center mt-2">
                                    @include('shop::products.list.from-price')
                                </div>

                                <div class="col-12 col-md-8 add-to-cart w-100 d-inline-flex justify-content-end align-items-center mt-2">
                                    <form action="{{ route('shop.productOrCategory.index', $product->url_key) }}" method="GET" class="iinfo-add-to-cart-form w-100 d-inline-flex justify-content-end align-items-center">
                                        <button class="button">
                                            See all for sale
                                        </button>
                                    </form>
                                </div>
                            </div>

                            
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>
