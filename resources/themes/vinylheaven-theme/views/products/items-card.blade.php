@inject ('wishListHelper', 'Webkul\Customer\Helpers\Wishlist')
@php
    $product_link = '';
    $marketProductsCount = $product->product->marketplaceProducts()->count();
    if(isset($has_from_price) || isset($is_catalog)) {
        $product_link = '/' . $product->url_key;
        $product_url_key = $product->url_key;
        $marketplace_link = '/' . $product->url_key;
    } else {
        $product_link = '/marketplace/seller/' . $product->seller->url . '/product/'. $product->product->url_key;
        $product_url_key = $product->product->url_key;
        $marketplace_link = '/marketplace/seller/profile/' . explode("/", $product->seller->url)[0];
    }
@endphp
@inject('reviewRepository', 'Webkul\Marketplace\Repositories\ReviewRepository')

<div class="product-card d-flex flex-row justify-content-center mx-auto">
    <a href="{{ $product_link }}" class="card-info" title="{{ $product->product->name }} - {{ $product->product->getArtistName() }}">
        <div class="info-image">
            <div class="image-holder info-image-{{ $product_url_key }}">
                {!! $product->product->getPrimaryImage() !!}
            </div>
        </div>

        <div class="info-content" title="{{ $product->product->name }} - {{ $product->product->getArtistName() }}">
            <div class="content-title">
                {{ $product->product->name }}
            </div>
            <div class="content-category">
                - {{ $product->product->getArtistName() }}
            </div>
            <div class="info-product font-weight-light">
                @include('shop::products.card.info-product')
            </div>
        </div>
            <div class="row space">
                <div class="info-seller">
                    {{ $marketProductsCount }} for sale of this release
                </div>
            </div>
            <div class="row space-seller">
                <div class="col-12 item-price">
                    For sale from: <span class="text-danger">{{ $product->product->getLowestPrice() }}</span>
                    <button href="#" class="btn" style="" data-toggle="modal" data-target="#basicModal">
                        <span>+ S&H <i class="fas fa-truck"></i></span>
                    </button>
                </div>
            </div>

        <div class="info-price-add-to-cart d-flex justify-content-start align-items-center">
             <div class="info-add-to-cart-form">
                 <button class="add-to-cart-button">
                     {{ 'See all items for sale' }}
                 </button>
                 @auth('customer')
                 {!! view_render_event('bagisto.shop.products.wishlist.before') !!}
                 <button
                     @if ($wishListHelper->getWishlistProduct($product))
                     class="add-to-wishlist already add-to-wishlist-button col-md-9 col-lg-8 col-xl-4"
                     title="{{ __('shop::app.customer.account.wishlist.remove-wishlist-text') }}"
                     @else
                     class="add-to-wishlist add-to-wishlist-button col-md-9 col-lg-8 col-xl-4"
                     title="{{ __('shop::app.customer.account.wishlist.add-wishlist-text') }}"
                     @endif
                     id="wishlist-changer"
                     href="{{ route('customer.wishlist.add', $product->product_id) }}"
                     data-toggle=marketplace>
                     <i class="fa fa-heart" aria-hidden="true"></i>
                 </button>
                 {!! view_render_event('bagisto.shop.products.wishlist.after') !!}
                 @endauth
             </div>
        </div>
    </a>
</div>

<!-- Modal -->
<div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Basic Modal</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Delivery method</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>