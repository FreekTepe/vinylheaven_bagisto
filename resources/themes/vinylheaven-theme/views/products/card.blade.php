@inject('wishListHelper', 'Webkul\Customer\Helpers\Wishlist')

<div class="product-card d-flex flex-row justify-content-center mx-auto">
    <a href="{{ $product->getHref() }}" class="card-info" title="{{ $product->name }} - {{ $product_artist_name }}">
        <div class="info-image">
            <div class="image-holder info-image-{{ $product->url_key }}">
                {!! $product->getPrimaryImage() !!}
            </div>
        </div>

        <div class="info-content" title="{{ $product->name }} - {{ $product_artist_name }}">
            <div class="content-title">
                {{ $product->name }}
            </div>

            <div class="content-category">
                - {{ $product_artist_name }}
            </div>

            <div class="info-product font-weight-light">
                @include('shop::products.card.info')
            </div>
        </div>

        @if ($product_marketplace_products->count() == 1)

            <div class="info-seller">
                @include('shop::products.card.seller')
            </div>
            <div class="row">
                <div class="col-12 col-sm-4 col-md-4 col-lg-4 mb-2 item-condition">
                    @include('shop::products.card.condition')
                </div>
                <div class="col-12 col-sm-8 col-md-8 col-lg-8 item-price">
                    @include('shop::products.card.price')
                </div>
            </div>
                
        @elseif ($product_marketplace_products->count() > 1)
            <div class="info-seller">
            &nbsp<br>
                {{ $product_marketplace_products->count() }} items for sale
            </div>
            <div class="row">
                <div class="col-12 col-sm-4 col-md-4 col-lg-4 mb-2 item-condition">
                    From:
                </div>
                    
                <div class="col-12 col-sm-8 col-md-8 col-lg-8 item-price">
                    @include('shop::products.card.from-price')
				</div>
            </div>
        @endif

        @if ($product_marketplace_products->count() == 1)
            <div class="add-to-cart d-flex justify-content-start">
                <form action="{{ route('cart.add', $product_marketplace_products->first()->product_id) }}" method="POST" class="info-add-to-cart-form">
                    @csrf
                    <input type="hidden" name="product_id" value="{{ $product_marketplace_products->first()->product_id }}">
                    <input type="hidden" name="quantity" value="1">
                    <input type="hidden" name="seller_info[product_id]" value="{{ $product_marketplace_products->first()->id }}">
                    <input type="hidden" name="seller_info[seller_id]" value="{{ $product_marketplace_products->first()->seller->id }}">
                    <input type="hidden" name="seller_info[is_owner]" value="0">

                    <button class="button button-small button-bag button-bag-small" {{ $product_marketplace_products->first()->isSaleable() ? '' : '' }}>
                        {{ __('shop::app.products.add-to-cart') }} 
                    </button>
                    @auth('customer')
                        {!! view_render_event('bagisto.shop.products.wishlist.before') !!}
                            
                            @if ($wishListHelper->getWishlistProduct($product))
                                <button
                                class="add-to-wishlist already button button-small button-heart"
                                title="{{ __('shop::app.customer.account.wishlist.remove-wishlist-text') }}"
                                id="wishlist-changer"
                                href="{{ route('customer.wishlist.add', $product->product_id) }}"
                                data-toggle="marketplace">
                                <i class="fa fa-times" aria-hidden="true"></i>
                                </button>
                            @else
                                <button
                                class="add-to-wishlist button button-small button-heart"
                                title="{{ __('shop::app.customer.account.wishlist.add-wishlist-text') }}"
                                id="wishlist-changer"
                                href="{{ route('customer.wishlist.add', $product->product_id) }}"
                                data-toggle="marketplace">
                                <i class="fa fa-heart" aria-hidden="true"></i>
                                </button>
                            @endif

                        {!! view_render_event('bagisto.shop.products.wishlist.after') !!}
                    @endauth
                </form>
            </div>
        @elseif ($product_marketplace_products->count() > 1)
            <div class="add-to-cart d-flex justify-content-start">
                <form action="{{ route('shop.productOrCategory.index', $product->url_key) }}" method="GET" class="info-add-to-cart-form">
                    <button class="button button-small">
                        Show all items for sale
                    </button>
                </form>
            </div>
        @endif
    </a>
</div>