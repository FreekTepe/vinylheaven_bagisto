@inject ('wishListHelper', 'Webkul\Customer\Helpers\Wishlist')
@php
    $product_link = '';
    $marketProductsCount = $product->product->marketplaceProducts()->count();
    if (isset($has_from_price) || isset($is_catalog)){
        $product_link = '/'. $product->url_key;
    } else {
        $product_link = '/marketplace/seller/' . $seller->url . '/product/'. $product->url_key;
    }
    $reviewRepository = app('Webkul\Marketplace\Repositories\ReviewRepository');
@endphp

<div class="row">
    <div class="col-12">
        <a href="{{ $product_link }}" class="list-item" data-toggle="tooltip" title="{{ $product->product->name }} - {{ $product->product->getArtistName() }}">
            <div class="row">
                <div class="col-12 col-md-4 col-lg-3">
                    <div class="row">
                        <div class="col-12">
                            <div class="item-image">
                                {!! $product->product->getPrimaryImage() !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-8 col-lg-9">
                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-3 col-xl-3">
                            <div class="content-title">
                                <p>
                                    {{ $product->product->name }}
                                </p>
                            </div>
                            <div class="content-category">- {{ $product->product->getArtistName() }}</div>

                            <div class="product-details">
                                @foreach($product->product->formats as $format)
                                    @if ($format !== 0)
                                        {{$format['name']}}
                                        @if ($format['descriptions'])
                                            ({{ $format['descriptions'][0] }})
                                        @endif
                                        | {{ $format['qty'] }} disk
                                    @endif
                                @endforeach

                                @if ($product->product->hasGenre())
                                    <div class="details-info">
                                        {{ $product->product->getGenre() }}
                                    </div>
                                @endif

                                <div class="details-info">
                                    {{ $product->product->release_date }}
                                </div>

                                <div class="details-info">
                                    {{ $product->product->country }}
                                </div>

                                @foreach ($product->product->labels()->get() as $label_index => $label)
                                    @if($label_index == 0)
                                        <div class="details-info">
                                                {{$label->name}} - {{$label->cat_no}} {{$label->label_id}}
                                        </div>
                                    @else
                                        <div class="details-info">
                                                {{$label->name}} - {{$label->cat_no}} {{$label->label_id}}
                                        </div>
                                    @endif
                                @endforeach

                                @if ($product->product->hasCompanyName())
                                    <div class="details-info">
                                        {{ $product->product->getCompanyName() }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-12 col-md-6 col-lg-4 col-xl-5">
                                <div class="text-center">
                                    <div class="font-weight-bold">
                                        <p class="text-con">
                                            {{ $marketProductsCount }} items for sale
                                        </p>
                                    </div>
                                </div>
                        </div>

                        <div class="col-12 col-md-12 col-lg-4 col-xl-4">
                            <div class="row">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="item-price-list col-12 col-md-7 col-lg-11 col-xl-10 text-center">
                                            <div class="col-12 item-price">
                                                <span class="text-con">For sale from:</span>
                                                <span class="text-danger">{{ $product->product->getLowestPrice() }}</span>
                                                <button href="#" class="btn" style="" data-toggle="modal" data-target="#basicModal">
                                                    <span>+ S&H <i class="fas fa-truck"></i></span>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="info-price-add-to-cart-list d-flex justify-content-start align-items-center">
                                            <div class="info-add-to-cart-form">
                                                <button class="see-all-for-sale col-12 text-white">
                                                    {{ 'See all for sale' }}
                                                </button>
                                            </div>
                                        </div>
                                        <div class="w-100"></div>
                                        <div class="info-price-add-to-cart-list d-flex justify-content-start align-items-center">
                                            @auth('customer')
                                                {!! view_render_event('bagisto.shop.products.wishlist.before') !!}
                                                <button
                                                    @if ($wishListHelper->getWishlistProduct($product))
                                                    class="already add-to-wishlist-button col-md-9 col-lg-8 col-xl-4"
                                                    title="{{ __('shop::app.customer.account.wishlist.remove-wishlist-text') }}"
                                                    id="wishlist-changer"
                                                    href="{{ route('customer.wishlist.add', $product->product_id) }}"
                                                    data-toggle="marketplace">
                                                    <i class="far fa-heart"></i>
                                                    @else
                                                        class="add-to-wishlist-button col-md-9 col-lg-8 col-xl-4"
                                                        title="{{ __('shop::app.customer.account.wishlist.add-wishlist-text') }}"
                                                        id="wishlist-changer"
                                                        href="{{ route('customer.wishlist.add', $product->product_id) }}"
                                                        data-toggle="marketplace">
                                                        <i class="fa fa-heart" aria-hidden="true"></i>
                                                    @endif
                                                </button>
                                                {!! view_render_event('bagisto.shop.products.wishlist.after') !!}
                                            @endauth
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Basic Modal</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Delivery method</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>