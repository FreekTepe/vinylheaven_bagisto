<div class="info-product-list">
    {!! $product->getFormatsFormatedHtml() !!}

    @if (!empty($product->country))
        <span class="mx-1">|</span>{{ $product->country }}
    @endif

    @if (!empty($product->getReleaseYear()))
        <span class="mx-1">|</span>{{ $product->getReleaseYear() }}
    @endif
</div>