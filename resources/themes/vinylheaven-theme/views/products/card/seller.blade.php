<div class="pl-1 pt-1">
    <p class="mb-1" title="{{$product->marketplace_product()->first()->seller->shop_title }}">
        {{ $product->marketplace_product()->first()->seller->shop_title }} <br />

        @if ($product->marketplace_product()->first()->seller->getAverageRating() > 0)
            <span class="mr-1">
                @for ($i = 1; $i <= $product->marketplace_product()->first()->seller->getAverageRating(); $i++)
                    <i class="fa fa-star"></i>
                @endfor

                @for ($i = 1; $i <= ($product->marketplace_product()->first()->seller->max_rating - $product->marketplace_product()->first()->seller->getAverageRating()); $i++)
                    <i class="fa fa-star-o"></i>
                @endfor
            </span>

            <span class="mr-1">
                {{ round($product->marketplace_product()->first()->seller->getAverageRating()) }}/{{ $product->marketplace_product()->first()->seller->max_rating }}
            </span>
            
            <span>
                ({{ $product->marketplace_product()->first()->seller->getTotalReviews() }})
            </span>
        @else
            <span class="mr-1">New seller</span>
        @endif
        
    </p>
</div>