<div class="info-price">
    <span class="text-danger">{{ $product->getMarketplaceProductsFromPrice() }}</span>
    <span class="font-weight-light">+ S&H <i class="fa fa-truck"></i></span>
</div>