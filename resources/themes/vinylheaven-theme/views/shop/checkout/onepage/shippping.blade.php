<div class="row">
    <div class="col-12">
        <div class="heading subtitle">
            <h2 class="title">{{ __('shop::app.checkout.onepage.shipping-method') }}</h2>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        


        
        
        <div class="form-row">
            <div class="form-group col">
                <label for="">Last Name</label>
                <input type="text" class="form-control" id="" name="">
            </div>

            <div class="form-group col">
                <label for=""></label>
                <input type="text" class="form-control" id="" name="">
            </div>
        </div>
    </div>
</div>

<div class="row justify-content-end align-items-center">
    <div class="col-auto">
        <button class="button button-continue" data-current-step="#list-item-shipping-toggle" data-next-step="#list-item-payment-toggle" type="button">
            <span>{{ __('shop::app.checkout.onepage.continue') }}</span> <i class="fa fa-check ml-2"></i>
        </button>
    </div>
</div>