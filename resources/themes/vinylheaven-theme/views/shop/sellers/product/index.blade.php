@extends('shop::layouts.master-new')

@php
    $show_other_products = false;
    
    $other_products = $product->product->marketplace_product();

    if ($product_view_config->is_seller_product) {
        
        $other_products->where([['marketplace_seller_id', '!=', $seller->id]]);

        if (empty ($product->product->marketplace_product()->where([['marketplace_seller_id', '!=', $seller->id]]))) {
            $show_other_products = true;
        }
    } else {
        if ($product->product->marketplace_product()->count()) {
            $show_other_products = true;
        }
    }
@endphp

@section('page_title')
    {{ trim($product->meta_title) != "" ? $product->meta_title : $product->name }}
@stop

@section('seo')
    {{--BB: SEO - add a JSON-LD. See https://schema.org/Product :: make sure the JSON-LD is added to the product-page (NOT THE MARKETPLACE PRODUCT PAGE) --}}
    <meta name="description" content="{{ trim($product->meta_description) != "" ? $product->meta_description : str_limit(strip_tags($product->description), 120, '') }}"/>
    <meta name="keywords" content="{{ $product->meta_keywords }}"/>
@stop

@section('content-wrapper')
    <section class="container-fluid container-lg container-xl container-product seller-product">
        <div class="row">
            <div class="col-12 col-lg-6">
                @include('shop::products.view.gallery')
            </div>

            <div class="col-12 col-lg-6 product-details">
                @include('shop::products.view.details')
            </div>
        </div>
        
        @if ($show_other_products)
            <div class="row my-3">
                {{--BB: disbalde the collapse for now, when we have a huge amount of records for sale we might consider adding it again als removed the xxx for sale --}}
                {{-- <div class="col-12 product-other-sellers {{ $product_view_config->classes->product_other_sellers }}" data-toggle="collapse" data-target="#product-other-sellers" aria-controls="product-other-sellers" aria-expanded="{{ $product_view_config->aria_expanded }}" aria-label="Toggle Product Other Sellers"> --}}

                <div class="col-12">
                {{--BB: changed this to a table. Maybe better to make to sortable when we have a huge amount of items --}}
                    <table class="table table-responsive-sm product-sellers-table">
                        <thead>
                            <tr>
                                <th class="align-middle">#</th>
                                <th class="align-middle">Seller</th>
                                <th></th>
                                <th class="align-middle">Media Condition</th>
                                <th class="align-middle">Sleeve Condition</th>
                                <th class="align-middle">Price</th>
                                <th></th>
                            </tr>
                    </thead>    
            
                    <tbody>
                    @foreach ($other_products->get() as $i => $other_product)
                    
                    <tr>
                    <td class="align-middle text-center" scope="row">
                        @if($i == 0 || strlen($i) == 1)
                            {{ '0' . ($i + 1) }}
                        @else
                            {{ ($i + 1) }}
                        @endif
                    </td>
                    <td class="align-middle">
                        {{ $other_product->seller->shop_title }}
                    </td>
                    <td class="align-middle">
                    <a href="{{ route('marketplace.sellers.product.index', ['url' => $other_product->seller->url, 'slug' => $product->product->url_key]) }}">View</a>
                    </td>
                    <td class="align-middle">
                        <span class="condition-grading-circle circle-text">{!! $other_product->getMediaConditionGradingCircle() !!}</span> {{ $other_product->condition }}
                    </td>
                    <td class="align-middle">
                    <span class="condition-grading-circle circle-text">{!! $other_product->getSleeveConditionGradingCircle() !!}</span> {{ $other_product->sleeve_condition }}
                    </td>
                    <td class="align-middle">
                        {{ core()->currency(core()->convertPrice($other_product->price, core()->getBaseCurrencyCode(), 'EUR')) }}
                    </td>
                    <td class="align-right">
                        <form action="{{ route('cart.add', $other_product->product_id) }}" method="POST" class="form-inline">
                            @csrf
                            <input type="hidden" name="product_id" value="{{ $other_product->product_id }}">
                            <input type="hidden" name="quantity" value="1">
                            <input type="hidden" name="seller_info[product_id]" value="{{ $other_product->id }}">
                            <input type="hidden" name="seller_info[seller_id]" value="{{ $other_product->marketplace_seller_id }}">
                            <input type="hidden" name="seller_info[is_owner]" value="0">
                            
                            <button class="button button-bag" {{ $other_product->product->isSaleable() ? '' : '' }}>Add to my bag</button>
                        </form>
                    </td>
                    </tr>
                    
                    @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
            @else
                @if (!$product_view_config->is_seller_product)
                    <div class="row my-3">
                
                    <div class="col-12">
                        <hr>
                        This product is currently not for sale
                    </div>
                @endif
                
            </div>
        @endif
    </section>

    <section class="product-info">
        @include('shop::products.view.info')
    </section>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#product-other-sellers').on('show.bs.collapse', function(){
                if (!$('.product-other-sellers').hasClass('collapse-open')) {
                    $('.product-other-sellers').addClass('collapse-open');
                }
            });

            $('#product-other-sellers').on('hide.bs.collapse', function(){
                if ($('.product-other-sellers').hasClass('collapse-open')) {
                    $('.product-other-sellers').removeClass('collapse-open');
                }
            });
        });
    </script>
@endpush