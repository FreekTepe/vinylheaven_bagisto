@extends('shop::layouts.master-new')

@inject('productRepository', 'Webkul\Marketplace\Repositories\ProductRepository')
@inject('gridHelper', 'VinylHeaven\Tools\Helpers\Grid')
@inject('toolbarHelper', 'VinylHeaven\Tools\Helpers\Toolbar')
@inject('searchHelper', 'VinylHeaven\Search\Helpers\Search')

@section('page_title')
    {{ $seller->shop_title }}
@stop

@section('seo')
    <meta name="description" content="{{ trim($seller->meta_description) != "" ? $seller->meta_description : str_limit(strip_tags($seller->description), 120, '') }}"/>
    <meta name="keywords" content="{{ $seller->meta_keywords }}"/>
@stop

@php
    
@endphp

@section('content-wrapper')
    <section class="container-seller-profile">
        <div class="seller-header">
            <div class="container-fluid container-lg container-xl">
                <div class="row">
                    <div class="col-12">
                        <div class="header-container">
                            <div class="header-logo mb-3 mr-3">
                                <img src="{{ $seller->getLogoUrl() }}" class="logo" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid container-lg container-xl">
            @include('shop::tools.list-grid-view-toolbar', ['toolbar_config' => $toolbar_config])

            @if ($seller->marketplace_products->count())
                <div class="result-grid {{ $toolbarHelper->getGridViewDisplay() }} py-4">
                    <div class="row justify-content-start align-items-center">
                        @foreach ($seller_products as $marketplace_product_index => $marketplace_product)
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-3">
                                @include('shop::products.marketplace.card')
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="result-list {{ $toolbarHelper->getListViewDisplay() }} py-4">
                    @foreach ($seller_products as $marketplace_product_index => $marketplace_product)
                        @include('shop::products.marketplace.list')
                    @endforeach
                </div>

                <div class="row">
                    <div class="col-12">
                        @include('shop::search.pagination', ['products_count' => $seller->marketplace_products->count()])
                    </div>
                </div>
            @endif
        </div>
    </section>
@endsection

@section('seller-profile-banner-logo-css')
    <style>
        .container-seller-profile .seller-header {
            background-image: url('{{ $seller->getBannerUrl() }}');
        }
    </style>
@endsection