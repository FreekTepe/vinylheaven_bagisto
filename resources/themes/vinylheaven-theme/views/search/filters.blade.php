<div class="row tools-filters product-filters d-lg-none collapse py-4" id="tools-filters">
    <div class="filter-filtered-by product-filters-area col-12 mb-3">
        <div class="row">
            <div class="col-12">
                <h4 class="area-title">Filtered by</h4>

                <ul class="area-filtered-by-list d-flex justify-content-start align-items-center p-0 m-0">
                    @foreach ($searchHelper->getFilteredByElements() as $element)
                    <li class="list-item pr-2 pb-2">
                        <a href="{{ $searchHelper->getUrlWithOutFilter($element['label']) }}">
                            <p class="text-center bg-secondary text-white m-0 px-2 py-1">
                                <small>{{ $element['label'] }}:</small> {{ $element['value'] }}
                            </p>
                        </a>
                    </li>
                    @endforeach
                </ul>

                <a href="{{ $searchHelper->getFilteredByClearAllUrl() }}" class="area-clear-all">Clear All</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="area-filter-price area-border-top">
                <h5 class="area-head js-fold-click">Price</h5>

                <div class="area-content js-fold">
                    <div class="form-group">
                        <div class="col-12">
                            <label class="w-100" for="min-price-test">
                                <div class="d-inline-flex justify-content-start align-items-center w-100">
                                    <small>Minimum price:</small>
                                    <span class="ml-auto" id="min-price-test-label-span-mobile">{{ request()->input('price_min', 0) }}</span>
                                </div>
                            </label>

                            <input type="range" class="form-control-range" id="min-price-test-mobile" data-url="{{ $searchHelper->getMinPriceDataUrl() }}" value="{{ request()->input('price_min', 0) }}" min="0" max="500" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-12">
                            <label class="w-100" for="max-price-test">
                                <div class="d-inline-flex justify-content-start align-items-center w-100">
                                    <small>max price:</small>
                                    <span class="ml-auto" id="max-price-test-label-span-mobile">{{ request()->input('price_max', 500) }}</span>
                                </div>
                            </label>

                            <input type="range" class="form-control-range" id="max-price-test-mobile" data-url="{{ $searchHelper->getMaxPriceDataUrl() }}" value="{{ request()->input('price_max', 500) }}" min="0" max="500" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="filter-media-conditions product-filters-area col-6 col-md-4 mb-3">
        <div class="area-filter-price area-border-top">
            <h5 class="area-head js-fold-click hide">Media Condition</h5>

            <div class="area-content js-fold" style="display: none;">
                @foreach ($searchHelper->getConditionFilters() as $condition_index => $condition_filter)
                <div class="content-checkbox">
                    <input class="checkbox" type="checkbox" id="condition-checkbox-{{ $condition_index }}">

                    <label class="checkbox-label filter-condition {{ $condition_filter['label_class'] }}" data-url="{{ $searchHelper->getConditionUrl($condition_filter['grading']) }}" for="condition-checkbox-{{ $condition_index }}">
                        <span class="label-grading {{ $condition_filter['class'] }}">{{ $condition_filter['grading'] }}</span>
                        {{ $condition_filter['text'] }}
                    </label>

                    <a href="#" class="checkbox-info">
                        <i class="fa fa-info-circle" aria-hidden="true"></i>

                        <div class="info-tooltip">
                            The grade of the media based on the Goldmine grading standards.
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="filter-sleeve-conditions product-filters-area col-6 col-md-4 mb-3">
        <div class="area-filter-price area-border-top">
            <h5 class="area-head js-fold-click hide">Sleeve Condition</h5>

            <div class="area-content js-fold" style="display: none;">
                @foreach ($searchHelper->getSleeveConditionFilters() as $condition_index => $condition_filter)
                <div class="content-checkbox">
                    <input class="checkbox" type="checkbox" id="condition-checkbox-{{ $condition_index }}">

                    <label class="checkbox-label filter-sleeve-condition {{ $condition_filter['label_class'] }}" data-url="{{ $searchHelper->getSleeveConditionUrl($condition_filter['grading']) }}" for="condition-checkbox-{{ $condition_index }}">
                        <span class="label-grading {{ $condition_filter['class'] }}">{{ $condition_filter['grading'] }}</span>
                        {{ $condition_filter['text'] }}
                    </label>

                    <a href="#" class="checkbox-info">
                        <i class="fa fa-info-circle" aria-hidden="true"></i>

                        <div class="info-tooltip">
                            The grade of the media based on the Goldmine grading standards.
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="filter-genres product-filters-area col-6 col-md-4 mb-3">
        <div class="area-filter-price area-border-top">
            <h5 class="area-head js-fold-click hide">Genre</h5>

            <div class="area-content js-fold" style="display: none;">
                @foreach ($searchHelper->getGenres() as $genre_index => $genre)
                <div class="content-checkbox">
                    <input class="checkbox" type="checkbox" id="condition-checkbox-{{ $genre_index }}">

                    <label class="checkbox-label filter-genre {{ $genre->label_class }}" data-url="{{ $searchHelper->getGenreUrl($genre->id) }}" for="condition-checkbox-{{ $genre_index }}">
                        {{ $genre->name }} ({{ $genre->products_count }})
                    </label>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="filter-formats product-filters-area col-6 col-md-4 mb-3">
        <div class="area-filter-price area-border-top">
            <h5 class="area-head js-fold-click hide">Formats</h5>

            <div class="area-content js-fold" style="display: none;">
                @foreach ($searchHelper->getFormats() as $format_index => $format)
                <div class="content-checkbox">
                    <input class="checkbox" type="checkbox" id="format-checkbox-{{ $format_index }}">

                    <label class="checkbox-label filter-format {{ $format->label_class }}" data-url="{{ $searchHelper->getFormatUrl($format->slug) }}" for="format-checkbox-{{ $format_index }}">
                        {{ $format->name }}
                    </label>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

<div class="d-none d-lg-block product-filters mt-4">
    <div class="d-inline-block d-md-none product-filters-show-filters js-filter">Show Filters <i class="fa fa-chevron-down" aria-hidden="true"></i></div>

    <div class="d-block product-filters-area js-filter-area">
        <div class="row">
            <div class="col-12">
                <h4 class="area-title">Filtered by</h4>

                <ul class="area-filtered-by-list d-flex justify-content-start align-items-center p-0 m-0">
                    @foreach ($searchHelper->getFilteredByElements() as $element)
                    <li class="list-item pr-2 pb-2">
                        <a href="{{ $searchHelper->getUrlWithOutFilter($element['label']) }}">
                            <p class="text-center bg-secondary text-white m-0 px-2 py-1">
                                <small>{{ $element['label'] }}:</small> {{ $element['value'] }}
                            </p>
                        </a>
                    </li>
                    @endforeach
                </ul>

                <a href="{{ $searchHelper->getFilteredByClearAllUrl() }}" class="area-clear-all">Clear All</a>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="area-filter-price area-border-top">
                    <h5 class="area-head js-fold-click">Price</h5>

                    <div class="area-content js-fold">
                        <div class="form-group">
                            <div class="col-12">
                                <label class="w-100" for="min-price-test">
                                    <div class="d-inline-flex justify-content-start align-items-center w-100">
                                        <small>Minimum price:</small>

                                        <span class="badge badge-secondary rounded-0 ml-auto min-price-label-span" id="min-price-test-label-span">{{ request()->input('price_min', 0) }}</span>
                                    </div>
                                </label>
                                
                                <input type="range" class="form-control-range" id="min-price-test" data-url="{{ $searchHelper->getMinPriceDataUrl() }}" value="{{ request()->input('price_min', 0) }}" min="0" max="500" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-12">
                                <label class="w-100" for="max-price-test">
                                    <div class="d-inline-flex justify-content-start align-items-center w-100">
                                        <small>Maximum price:</small>

                                        <span class="badge badge-secondary rounded-0 ml-auto max-price-label-span" id="max-price-test-label-span">{{ request()->input('price_max', 500) }}</span>
                                    </div>
                                </label>
                                
                                <input type="range" class="form-control-range" id="max-price-test" data-url="{{ $searchHelper->getMaxPriceDataUrl() }}" value="{{ request()->input('price_max', 500) }}" min="0" max="500" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="area-filter-price area-border-top">
                    <h5 class="area-head js-fold-click {{ $searchHelper->getConditionContainerClass() }}">Media Condition</h5>

                    <div class="area-content js-fold" style="{{ $searchHelper->getConditionContainerDisplay() }}">
                        @foreach ($searchHelper->getConditionFilters() as $condition_index => $condition_filter)
                        <div class="content-checkbox">
                            <input class="checkbox" type="checkbox" id="condition-checkbox-{{ $condition_index }}">

                            <label class="checkbox-label filter-condition {{ $condition_filter['label_class'] }}" data-url="{{ $searchHelper->getConditionUrl($condition_filter['grading']) }}" for="condition-checkbox-{{ $condition_index }}">
                                <span class="label-grading {{ $condition_filter['class'] }}">{{ $condition_filter['grading'] }}</span>
                                {{ $condition_filter['text'] }}
                            </label>

                            <a href="#" class="checkbox-info">
                                <i class="fa fa-info-circle" aria-hidden="true"></i>

                                <div class="info-tooltip">
                                    The grade of the media based on the Goldmine grading standards.
                                </div>
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="area-filter-price area-border-top">
                    <h5 class="area-head js-fold-click {{ $searchHelper->getSleeveConditionContainerClass() }}">Sleeve Condition</h5>

                    <div class="area-content js-fold" style="{{ $searchHelper->getSleeveConditionContainerDisplay() }}">
                        @foreach ($searchHelper->getSleeveConditionFilters() as $condition_index => $condition_filter)
                        <div class="content-checkbox">
                            <input class="checkbox" type="checkbox" id="condition-checkbox-{{ $condition_index }}">

                            <label class="checkbox-label filter-sleeve-condition {{ $condition_filter['label_class'] }}" data-url="{{ $searchHelper->getSleeveConditionUrl($condition_filter['grading']) }}" for="condition-checkbox-{{ $condition_index }}">
                                <span class="label-grading {{ $condition_filter['class'] }}">{{ $condition_filter['grading'] }}</span>
                                {{ $condition_filter['text'] }}
                            </label>

                            <a href="#" class="checkbox-info">
                                <i class="fa fa-info-circle" aria-hidden="true"></i>

                                <div class="info-tooltip">
                                    The grade of the media based on the Goldmine grading standards.
                                </div>
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="area-filter-price area-border-top">
                    <h5 class="area-head js-fold-click {{ $searchHelper->getGenreContainerClass() }}">Genre</h5>

                    <div class="area-content js-fold" style="{{ $searchHelper->getGenreContainerDisplay() }}">
                        @foreach ($searchHelper->getGenres() as $genre_index => $genre)
                        <div class="content-checkbox">
                            <input class="checkbox" type="checkbox" id="condition-checkbox-{{ $genre_index }}">

                            <label class="checkbox-label filter-genre {{ $genre->label_class }}" data-url="{{ $searchHelper->getGenreUrl($genre->id) }}" for="condition-checkbox-{{ $genre_index }}">
                                {{ $genre->name }} ({{ $genre->products_count }})
                            </label>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="area-filter-price area-border-top">
                    <h5 class="area-head js-fold-click {{ $searchHelper->getFormatContainerClass() }}">Formats</h5>

                    <div class="area-content js-fold" style="{{ $searchHelper->getFormatContainerDisplay() }}">
                        @foreach ($searchHelper->getFormats() as $format_index => $format)
                        <div class="content-checkbox">
                            <input class="checkbox" type="checkbox" id="format-checkbox-{{ $format_index }}">

                            <label class="checkbox-label filter-format {{ $format->label_class }}" data-url="{{ $searchHelper->getFormatUrl($format->slug) }}" for="format-checkbox-{{ $format_index }}">
                                {{ $format->name }}
                            </label>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#min-price-test').mouseup(function(){
            window.location = $(this).data('url').replace('%23price_min', $(this).val());
        });

        $('#min-price-test').on('input', function(){
            $('#min-price-test-label-span').html($(this).val());
        });

        $('#max-price-test').mouseup(function(){
            window.location = $(this).data('url').replace('%23price_max', $(this).val());
        });

        $('#max-price-test').on('input', function(){
            $('#max-price-test-label-span').html($(this).val());
        });

        $('#min-price-test-mobile').mouseup(function(){
            window.location = $(this).data('url').replace('%23price_min', $(this).val());
        });

        $('#min-price-test-mobile').on('input', function(){
            $('#min-price-test-label-span-mobile').html($(this).val());
        });

        $('#max-price-test-mobile').mouseup(function(){
            window.location = $(this).data('url').replace('%23price_max', $(this).val());
        });

        $('#max-price-test-mobile').on('input', function(){
            $('#max-price-test-label-span-mobile').html($(this).val());
        });
    });
</script>

<script type="text/javascript" src="{{ asset('themes/vinylheaven-theme/assets/dist/js/components/filters.js') }}"></script>
@endpush
