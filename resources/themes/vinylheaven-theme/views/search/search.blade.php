@extends('shop::layouts.master-new')

@inject ('toolbarHelper', 'VinylHeaven\Tools\Helpers\Toolbar')
@inject ('gridHelper', 'VinylHeaven\Tools\Helpers\Grid')
@inject ('searchHelper', 'VinylHeaven\Search\Helpers\Search')

@section('page_title')
    {{ __('shop::app.search.page-title') }}
@endsection

@php
    $view=request()->input('view');
    $is_search = true;
    $is_seller = false;
    $has_from_price = true;
    $toolbar_config = [
        'has_result_counter' => true,
        'has_seller_info' => false
    ];
@endphp

@section('content-wrapper')
    <section class="container-fluid container-lg container-search">
        @include('shop::tools.list-grid-view-toolbar', $toolbar_config)

        <div class="row">
            <div class="col-12 col-lg-4 col-xl-3">
                @include('shop::search.filters')
            </div>

            <div class="col-12 col-lg-8 col-xl-9">
                @if (!$results)
                    <div class="col-12 py-4">
                        <h1>No Results</h1>
                    </div>
                @else
                    <div class="result-grid {{ $toolbarHelper->getGridViewDisplay() }} py-4">
                        <div class="row justify-content-start align-items-center">
                        @if ((empty ($view)) || ($view == 'grid'))
                            @foreach ($results as $product)
                                @if (isset($product->url_key) && !empty($product->url_key))
                                    <div class="col-12 col-sm-6 col-lg-4">
                                        <!-- DEZE MOET HET WORDEN -->
                                        @include('shop::products.card')
                                    </div>
                                @endif
                            @endforeach
                        @endif
                        </div>
                    </div>

                    <div class="result-list {{ $toolbarHelper->getListViewDisplay() }} py-4">
                    @if ($view == 'list')
                        @foreach ($results as $product)
                            @if (isset($product->url_key) && !empty($product->url_key))
                                @include('shop::products.list')
                            @endif
                        @endforeach
                    @endif
                    </div>

                    <div class="col-12">
                        @include('shop::search.pagination')
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection

@section('info-image-background-css')
    <style>
        @if ($results)
            @foreach ($results as $product)
                @if (isset($product->url_key) && !empty($product->url_key))
                    .product-card .card-info .info-image .image-holder.info-image-{{ $product->url_key }} {
                        background-image: url('{{ $product->product->getPrimaryImage() }}');
                    }
                @endif
            @endforeach
        @endif
    </style>
@endsection

@push('scripts')
	<script type="text/javascript">
        (function( $ ) {
            if ($('#vinylheaven-catalog-sorter').length) {
                $('#vinylheaven-catalog-sorter').on('change', function(){
                    window.location = $(this).children('option:selected').val();
                });
            }

            if ($('#vinylheaven-catalog-results-limiter').length) {
                $('#vinylheaven-catalog-results-limiter').on('change', function(){
                    window.location = $(this).children('option:selected').val();
                });
            }
        })(jQuery);
    </script>
@endpush