@php
$product_link = '';
if (isset($has_from_price) || isset($is_catalog)){
    $product_link = '/'. $product->url_key;
} else {
    $product_link = '/marketplace/seller/' . $seller->url . '/product/'. $product->url_key;
}
@endphp

<div class="row">
    <div class="col-12">
        <a href="{{ $product_link }}" class="list-item">
            <div class="row">
                <div class="col-6 col-md-4 col-lg-3">
                    <div class="row">
                        <div class="col-12">
                            <div class="item-image">
                                <img src="{{ $product->product->getPrimaryImage() }}" alt="" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-6 col-md-8 col-lg-9">
                    <div class="row">
                        <div class="col-12">
                            <div class="product-details list-view">
                                <div class="details-title list-view">{{ $product->name }}</div>

                                <div class="details-artist-name list-view">
                                    <span>{{ $product->product->getArtistName() }}</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="product-details">
                                @if (isset($has_from_price) || isset($is_catalog))
                                    <h4 class="details-heading">Other sellers</h4>

                                    <div class="details-info">
                                        {{ $product->product->marketplaceProducts()->count() }}
                                    </div>
                                @endif

                                @if ($product->hasGenre())
                                    <h4 class="details-heading">Genre</h4>

                                    <div class="details-info">
                                        {{ $product->product->getGenre() }}
                                    </div>
                                @endif

                                <h4 class="details-heading">Release date</h4>

                                <div class="details-info">
                                    {{ $product->product->release_date }}
                                </div>

                                <h4 class="details-heading">Country</h4>

                                <div class="details-info">
                                    {{ $product->product->country }}
                                </div>

                                @if ($product->product->hasCompanyName())
                                    <h4 class="details-heading">Company</h4>

                                    <div class="details-info">
                                        {{ $product->product->getCompanyName() }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="col-12 col-md-6">
                            @if (!isset($has_from_price))
                                <div class="row">
                                    <div class="col-12">
                                        <div class="product-details">
                                            <h4 class="details-heading">Condition</h4>

                                            <div class="details-info">
                                                <div class="condition-grading-circle list-view">
                                                    <span class="circle-text {{ $searchHelper->getConditionGradingBackgroundClass($product->product->getConditionGrading($product->condition)) }}">{{ $product->product->getConditionGrading($product->condition) }}</span>
                                                    {{ $product->product->getConditionText($product->condition) }}
                                                </div>
                                            </div>

                                            <h4 class="details-heading">Sleeve Condition</h4>

                                            <div class="details-info">
                                                <div class="condition-grading-circle list-view">
                                                    <span class="circle-text  {{ $searchHelper->getConditionGradingBackgroundClass($product->product->getSleeveConditionGrading($product->sleeve_condition)) }}">{{ $product->product->getSleeveConditionGrading($product->sleeve_condition) }}</span>
                                                    {{ $product->product->getSleeveConditionText($product->sleeve_condition) }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            <div class="row">
                                <div class="col-12">
                                    <div class="d-flex justify-content-center align-items-center pt-3">
                                        @if (isset($has_from_price) || isset($is_catalog))
                                            @include('shop::products.card.from-price')
                                        @else
                                            <div class="col-4 text-center price-list-view">
                                                {{ core()->currency($product->price) }}
                                            </div>

                                            @if(!isset($no_add_to_bag_button))
                                                <form action="{{ route('cart.add', $product->product_id) }}" method="POST" class="col-8">
                                                    @csrf
                                                    <input type="hidden" name="product_id" value="{{ $product->product_id }}">
                                                    <input type="hidden" name="quantity" value="1">
                                                    <input type="hidden" name="seller_info[product_id]" value="{{ $product->id }}">
                                                    <input type="hidden" name="seller_info[seller_id]" value="{{ $product->marketplace_seller_id }}">
                                                    <input type="hidden" name="seller_info[is_owner]" value="0">

                                                    <button class="button button-bag" {{ $product->isSaleable() ? '' : '' }}>{{ __('shop::app.products.add-to-cart') }}</button>
                                                </form>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>