@inject ('gridHelper', 'VinylHeaven\Tools\Helpers\Grid')

<div class="pagination d-flex justify-content-center align-items-center mb-3">
    @if ($gridHelper->getTotalPaginationPages($products_count) > 1)
        <a href="{{ $gridHelper->getPaginationPreviousPageUrl() }}" class="button button-pagination pagination-prev mr-1">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
        </a>

        <a href="{{ $gridHelper->getPaginationFirstPageUrl() }}" class="button button-pagination pagination-first ml-1">
            1
        </a>

        <div class="button button-pagination-numbers ml-2 mr-2">
            @for ($i = $gridHelper->getPageRange()['start'];$i <= $gridHelper->getPageRange()['max']; $i++)
                @if ($i == $gridHelper->getCurrentPage())
                    <a href="#" class="pagination-number is-active ml-2 mr-2">{{ $i }}</a>
                @else
                    <a href="{{ $gridHelper->getPaginationUrl($i) }}" class="pagination-number ml-2 mr-2">{{ $i }}</a>
                @endif
            @endfor
        </div>

        <a href="{{ $gridHelper->getPaginationLastPageUrl($products_count) }}" class="button button-pagination pagination-first mr-1">
            {{ $gridHelper->getTotalPaginationPages($products_count) }}
        </a>

        <a href="{{ $gridHelper->getPaginationNextPageUrl($products_count) }}" class="button button-pagination pagination-first ml-1">
            <i class="fa fa-chevron-right" aria-hidden="true"></i>
        </a>
    @endif
</div>