@extends('shop::layouts.master-new')

@section('page_title')
    {{ __('shop::app.customer.login-form.page-title') }}
@endsection

@section('content-wrapper')
    <section class="container-fluid container-lg container-xl container-login">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <div class="row justify-content-center align-items-center">
                    <div class="col-auto">
                        <div class="mt-3 mb-4 my-md-3 mb-lg-5">
                            {{ __('shop::app.customer.login-text.no_account') }} - <a href="{{ route('customer.register.index') }}">{{ __('shop::app.customer.login-text.title') }}</a>
                        </div>
                    </div>
                </div>

                {!! view_render_event('bagisto.shop.customers.login.before') !!}

                <div class="row">
                    <div class="col-12">
                        <form class="needs-validation sign-in-form" method="post" action="{{ route('customer.session.create') }}" novalidate>
                            {{ csrf_field() }}

                            <div class="form-row">
                                <div class="col-12">
                                    <strong>{{ __('shop::app.customer.login-form.title') }}</strong>
                                </div>
                            </div>

                            {!! view_render_event('bagisto.shop.customers.login_form_controls.before') !!}

                            <div class="form-row">
                                <div class="col-12">
                                    <label></label>
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="{{ __('shop::app.customer.login-form.email') }}" required>
                                    <div class="invalid-feedback">
                                        Please Provide A Valid Email Address
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-12">
                                    <label></label>
                                    <input type="password" class="form-control" name="password" placeholder="{{ __('shop::app.customer.login-form.password') }}" required>
                                    <div class="invalid-feedback">
                                        Please Provide Your Password
                                    </div>
                                </div>
                            </div>

                            <div class="form-row justify-content-start mt-3">
                                <div class="col-auto">
                                    <a href="{{ route('customer.forgot-password.create') }}">{{ __('shop::app.customer.login-form.forgot_pass') }}</a>
                                </div>

                                <div class="col-auto">
                                    @if (Cookie::has('enable-resend'))
                                        @if (Cookie::get('enable-resend') == true)
                                            <a href="{{ route('customer.resend.verification-email', Cookie::get('email-for-resend')) }}">{{ __('shop::app.customer.login-form.resend-verification') }}</a>
                                        @endif
                                    @endif
                                </div>
                            </div>

                            {!! view_render_event('bagisto.shop.customers.login_form_controls.after') !!}

                            <div class="form-row">
                                <div class="col-12 mt-3">
                                    <button type="submit" class="button">{{ __('shop::app.customer.login-form.button_title') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                {!! view_render_event('bagisto.shop.customers.login.after') !!}
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    <script type="text/javascript">
        (function() {
            'use strict';
            window.addEventListener('load', function() {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                var submit_form = false;
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }

                    form.classList.add('was-validated');
                }, false);
                });
            }, false);
        })();
    </script>
@endpush