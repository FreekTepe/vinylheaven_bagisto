<div class="container-fluid container-lg">
    <div class="row">
        <div class="col-12 col-md-6 col-lg-9 mb-4 cart-sellers-container minicart-sellers">
            <div class="row">
                @if (!$cart->items()->count())
                    <div class="col-12 text-center">
                        {{ __('shop::app.checkout.cart.empty') }}
                    </div>
                @endif
                
                @foreach($cart->sellerCarts() as $sellerCart)
                    @php
                        $sellerCart->collectTotals();
                    @endphp

                    <div class="col-12 col-md-6 seller-container minicart-seller">
                        <div class="seller-info">
                            <div class="info-header row justify-content-start align-items-center">
                                <div class="col-auto mr-auto info-name">{{ $sellerCart->seller->shop_title }}</div>

                                <div class="col-auto info-sub-total">{{ core()->currency($sellerCart->base_sub_total) }}</div>
                            </div>
                        </div>

                        <div class="seller-cart-items">
                            @foreach($sellerCart->items()->get() as $item)
                                @php
                                    view_render_event('bagisto.shop.checkout.cart-mini.item.name.before', ['item' => $item]);
                                    view_render_event('bagisto.shop.checkout.cart-mini.item.quantity.before', ['item' => $item]);
                                    view_render_event('bagisto.shop.checkout.cart-mini.item.price.before', ['item' => $item]);
                                @endphp

                                <div class="cart-item d-flex justify-content-start align-items-center">
                                    <div class="item-image col-auto">
                                        <img src="{{ $item->product->getPrimaryImageThumb() }}" alt="{{ $item->name }}" />
                                    </div>

                                    <div class="item-guantity col-auto">
                                        {{ $item->quantity }} X
                                    </div>

                                    <div class="col item-info p-0">
                                        <div class="row align-items-center">
                                            <div class="col-12 info-name">
                                                {{ $item->product->name }}
                                            </div>
                                        </div>

                                        <div class="row align-items-center">
                                            <div class="col-12 info-remove-price">
                                                <div class="row align-items-start">
                                                    <a class="col-auto info-remove" href="{{ route('shop.checkout.cart.remove', $item->id) }}" alt="{{ __('shop::app.checkout.cart.remove-link') }}" onclick="removeLink('{{ __('shop::app.checkout.cart.cart-remove-action') }}')">
                                                        <i class="fa fa-trash remove-icon"></i>
                                                    </a>

                                                    <div class="col-auto ml-auto info-price">
                                                        {{ $item->quantity }} X {{ core()->currency($item->base_price) }} = {{ core()->currency($item->base_total) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @php
                                    view_render_event('bagisto.shop.checkout.cart-mini.item.name.after', ['item' => $item]);
                                    view_render_event('bagisto.shop.checkout.cart-mini.item.quantity.after', ['item' => $item]);
                                    view_render_event('bagisto.shop.checkout.cart-mini.item.price.after', ['item' => $item]);
                                @endphp
                            @endforeach
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="col-12 col-md-6 col-lg-3 minicart-totals">
            <div class="row">
                <div class="col-12 text-center">
                    <strong>Cart totals</strong>
                </div>
            </div>

            <div class="row">
                <div class="col-6 text-left">
                    Subtotal
                </div>

                <div class="col-6 text-right">
                    {{ core()->currency($cart->base_sub_total) }}
                </div>
            </div>

            @if ($cart->selected_shipping_rate)
                <div class="row">
                    <div class="col-6 text-left">
                        {{ __('shop::app.checkout.total.delivery-charges') }}
                    </div>

                    <div class="col-6 text-right">
                        {{ core()->currency($cart->selected_shipping_rate->base_price) }}
                    </div>
                </div>
            @endif

            @if ($cart->base_tax_total && !(true))
                @foreach (Webkul\Tax\Helpers\Tax::getTaxRatesWithAmount($cart, true) as $taxRate => $baseTaxAmount )
                    <div class="row">
                        <div class="col-6 text-left">
                            {{ __('shop::app.checkout.total.tax') }} {{ $taxRate }} %
                        </div>

                        <div class="col-6 text-right">
                            {{ core()->currency($baseTaxAmount) }}
                        </div>
                    </div>
                @endforeach
            @endif

            @if ($cart->base_discount_amount && $cart->base_discount_amount > 0)
                <div class="row">
                    <div class="col-6 text-left">
                        {{ __('shop::app.checkout.total.disc-amount') }}
                    </div>

                    <div class="col-6 text-right">
                        -{{ core()->currency($cart->base_discount_amount) }}
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="col-6 text-left">
                    <strong>{{ __('shop::app.checkout.total.grand-total') }}</strong>
                </div>

                <div class="col-6 text-right">
                    <strong>{{ core()->currency($cart->base_grand_total) }}</strong>
                </div>
            </div>

            <div class="row justify-content-center align-items-center mt-4 minicart-buttons">
                <div class="d-inline-flex justify-content-center align-items-center w-100">
                    <a class="button button-bag" href="{{ route('shop.checkout.onepage.index') }}">{{ __('shop::app.minicart.checkout') }}</a>
                </div>

                <div class="d-inline-flex justify-content-center align-items-center w-100 mt-2">
                    <a class="d-inline-block my-4" href="{{ route('shop.checkout.cart.index') }}">{{ __('shop::app.minicart.view-cart') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>