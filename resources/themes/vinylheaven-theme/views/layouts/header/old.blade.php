@inject('header_helper', 'VinylHeaven\Tools\Helpers\Header')

@php
    $cart = $header_helper->getCart();
    $vinylHeavenCart = $header_helper->getVinylHeavenCart();
@endphp

<header class="header">
    <div class="container-fluid container-lg container-xl">
        <nav class="navbar navbar-expand-lg justify-content-between">
            <button class="navbar-toggler pl-3" type="button" data-toggle="collapse" data-target="#vinylheaven-mobile-navbar" aria-controls="vinylheaven-mobile-navbar" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <a class="navbar-brand logo-container mr-auto ml-2 mr-lg-2" href="/">
                <img src="{{ asset('themes/vinylheaven-theme/assets/img/logo-vinyl-heaven3.png') }}" class="logo d-none d-lg-block" alt="">
                <img src="{{ asset('themes/vinylheaven-theme/assets/img/logo.png') }}" class="logo d-block d-lg-none" alt="">
            </a>

            <div class="navbar-text navbar-menu d-none d-lg-inline-block">
                <span class="navbar-menu-text">Genres</span> <i class="fa fa-chevron-down navbar-menu-icon"></i>

                <div class="navbar-menu-container">
                    <ul class="navbar-menu-list">
                        @foreach ($header_helper->getGenres() as $genre)
                            @if (count($genre->marketplaceProducts) > 0)
                                <li class="list-item list-item-genres">
                                    <a href="/{{ $genre->slug }}">{{ $genre->name }}<sup>{{ count($genre->marketplaceProducts) }}</sup></a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>

            <div class="navbar-text navbar-menu d-inline-block d-xl-none mx-2 ml-lg-auto mr-lg-2">
                <span class="navbar-menu-text"><i class="fa fa-search"></i></span>

                <div class="navbar-menu-container mobile">
                    <div class="navbar-menu-content">
                        <form method="GET" action="/search" id="mobile-navbar-search-form" class="form-inline navbar-search mobile">
                            <div class="input-group">
                                <input type="text" id="mobile-navbar-search-input" class="form-control navbar-search-input" name="term" placeholder="Type and search...." autocomplete="off" data-url="{{ route('shop.search.autocomplete') }}" data-csrf="{{ csrf_token() }}" />
                                
                                <div class="input-group-append">
                                    <button class="btn navbar-search-button" type="submit"></button>
                                </div>
                            </div>

                            <div id="mobile-navbar-search-autocomplete-container" class="navbar-search-autocomplete-container">
                                <div class="autocomplete-content">
                                    <ul class="content-list d-flex flex-column justify-content-start align-items-start">
                                        <li class="list-item">
                                            <div class="item-header text-center">
                                                Releases
                                            </div>

                                            <ul id="mobile-autocomplete-releases-list" class="releases"></ul>
                                        </li>

                                        <li class="list-item">
                                            <div class="item-header text-center">
                                                Artists
                                            </div>

                                            <ul id="mobile-autocomplete-artists-list" class="artists"></ul>
                                        </li>
                                    </ul>

                                    <div id="mobile-loading-results-container" class="loading-results-container">
                                        <div class="row justify-content-center align-items-center">
                                            <div class="col-auto">
                                                <i class="fa fa-spin fa-spinner loading-icon"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <form method="GET" action="/search" id="navbar-search-form" class="form-inline navbar-search d-none d-xl-flex">
                <div class="input-group">
                    <input type="text" id="navbar-search-input" class="form-control navbar-search-input" name="term" placeholder="Type and search...." autocomplete="off" data-url="{{ route('shop.search.autocomplete') }}" data-csrf="{{ csrf_token() }}" />
                    
                    <div class="input-group-append">
                        <button class="btn navbar-search-button" type="submit"></button>
                    </div>
                </div>

                <div id="navbar-search-autocomplete-container" class="navbar-search-autocomplete-container">
                    <div class="autocomplete-content">
                        <ul class="content-list d-flex justify-content-start align-items-start">
                            <li class="list-item">
                                <div class="item-header">
                                    Releases
                                </div>

                                <ul id="autocomplete-releases-list" class="releases"></ul>
                            </li>

                            <li class="list-item">
                                <div class="item-header">
                                    Artists
                                </div>

                                <ul id="autocomplete-artists-list" class="artists"></ul>
                            </li>
                        </ul>

                        <div id="loading-results-container" class="loading-results-container">
                            <div class="row justify-content-center align-items-center">
                                <div class="col-auto">
                                    <i class="fa fa-spin fa-spinner loading-icon"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            {!! view_render_event('bagisto.shop.layout.header.account-item.before') !!}
            
            @guest('customer')
                <div class="navbar-text mx-2 d-none d-md-inline-block">
                    <a href="/customer/login" class="">Login</a> | <a href="/customer/register" class="">Register</a>
                </div>

                <div class="navbar-text navbar-menu navbar-user d-inline-block d-md-none mx-2">
                    <a href="#"><img src="{{ asset('themes/vinylheaven-theme/assets/img/user-icon.png') }}" alt=""></a>

                    <div class="navbar-menu-container">
                        <div class="navbar-menu-content">
                            <div class="d-flex justify-content-center align-items-center w-100">
                                <a href="/customer/login" class="d-inline-block w-auto">Login</a> <span class="d-inline-block mx-2">|</span> <a href="/customer/register" class="d-inline-block w-auto">Register</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endguest

            @auth('customer')
                <div class="navbar-text navbar-user mx-2">
                    <a href="/customer/account/profile" class=""><img src="{{ asset('themes/vinylheaven-theme/assets/img/user-icon.png') }}" alt=""></a>
                </div>
            @endauth

            {!! view_render_event('bagisto.shop.layout.header.account-item.after') !!}

            @auth('customer')
            <div class="navbar-text navbar-menu navbar-shop-wishlist mx-2">
                <span class="navbar-menu-text">
                    <a href="#"><img src="{{ asset('themes/vinylheaven-theme/assets/img/heart-icon.png') }}" alt=""></a>
                </span>
                
                <div class="navbar-menu-container">
                    <ul class="navbar-menu-list">
                        <li class="list-item"><h6>Wishlist</h6></li>
                    </ul>
                </div>
            </div>
            @endauth
            

            @php
            $term = request()->input('term');
            $search_query = '';
            if (! is_null($term)) {
                $search_query = 'term='.request()->input('term') . '&';
            }
            @endphp

            @if (core()->getCurrentChannel()->currencies->count() > 1)
                <div class="navbar-text navbar-currency-switcher dropdown mx-2">
                    <a class="dropdown-toggle" href="#" role="button" id="vinylheaven-currency-switcher" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="navbar-menu-text navbar-currency-current">{{ core()->getCurrentCurrencyCode() }}</span> <i class="fa fa-chevron-down navbar-currency-switcher-icon"></i>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="vinylheaven-currency-switcher">
                        @foreach (core()->getCurrentChannel()->currencies as $currency)
                            <a href="?{{ $search_query }}currency={{ $currency->code }}" class="dropdown-item">{{ $currency->code }}</a>
                        @endforeach
                    </div>
                </div>
            @endif

            {!! view_render_event('bagisto.shop.layout.header.currency-item.after') !!}

            {!! view_render_event('bagisto.shop.layout.header.cart-item.before') !!}

            @if ($cart)
                <div class="navbar-text navbar-menu navbar-shopping-cart ml-2 pr-3">
                    <span class="navbar-menu-text">
                        {!! view_render_event('bagisto.shop.checkout.cart-mini.subtotal.before', ['cart' => $cart]) !!}
                        
                        <a href="#"><img src="{{ asset('themes/vinylheaven-theme/assets/img/cart-icon.png') }}" alt=""> <span class="navbar-shopping-cart-items-count">({{ $cart->items->count() }}) <span class=" d-none d-md-inline">{{ core()->currency($cart->base_sub_total) }}</span></span></a>
                        
                        {!! view_render_event('bagisto.shop.checkout.cart-mini.subtotal.after', ['cart' => $cart]) !!}
                    </span>

                    @if($cart->items->count())
                        <div class="navbar-menu-container">
                            <div class="navbar-menu-content">
                                <div class="row">
                                    <div class="col-12 col-md-6 col-lg-9 cart-sellers-container">
                                        <!-- SELLER CART ITEMS HERE -->
                                        <div class="row">
                                            @foreach ($vinylHeavenCart->getSellerCarts() as $sellerCart)
                                                <div class="col-12 col-md-6">
                                                    <div class="seller-container border-0">
                                                        <div class="seller-info">
                                                            <div class="info-header">
                                                                <div class="d-flex justify-content-start align-items-center">
                                                                    <div class="d-inline-block">{{ $sellerCart->seller->shop_title }}</div>

                                                                    <div class="d-inline-block ml-auto">{{ core()->currency($sellerCart->base_grand_total) }}</div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="seller-items">
                                                            @foreach ($sellerCart->items() as $item)
                                                                @php
                                                                    view_render_event('bagisto.shop.checkout.cart-mini.item.name.before', ['item' => $item]);
                                                                    $item_name = $item->product->name;
                                                                    view_render_event('bagisto.shop.checkout.cart-mini.item.name.after', ['item' => $item]);

                                                                    view_render_event('bagisto.shop.checkout.cart-mini.item.quantity.before', ['item' => $item]);
                                                                    $item_quantity = $item->quantity;
                                                                    view_render_event('bagisto.shop.checkout.cart-mini.item.quantity.after', ['item' => $item]);

                                                                    view_render_event('bagisto.shop.checkout.cart-mini.item.price.before', ['item' => $item]);
                                                                    $item_price = core()->currency($item->base_price);
                                                                    view_render_event('bagisto.shop.checkout.cart-mini.item.price.after', ['item' => $item]);
                                                                @endphp

                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <div class="list-item">
                                                                            <div class="d-flex justify-content-start align-items-center">
                                                                                <div class="item-image-container">
                                                                                    <div class="d-flex justify-content-center align-items-center item-image">
                                                                                        <img src="{{ $item->product->getPrimaryImage() }}" alt="{{ $item_name }}" />
                                                                                    </div>
                                                                                </div>

                                                                                <div class="item-info-container">
                                                                                    <div class="d-flex justify-content-start align-items-center info-quantity-name ml-1">
                                                                                        <div class="d-lg-inline-block item-quantity text-left mr-1">
                                                                                            {{ $item_quantity }} X
                                                                                        </div>

                                                                                        <div class="d-lg-inline-block item-name">
                                                                                            <strong>{{ $item_name }}</strong>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="d-flex justify-content-start align-items-center info-price-remove ml-1">
                                                                                        <div class="d-lg-inline-block item-name mr-auto">
                                                                                            <a class="d-flex justify-content-center align-items-center item-remove" href="{{ route('shop.checkout.cart.remove', $item->id) }}" alt="{{ __('shop::app.checkout.cart.remove-link') }}" onclick="removeLink('{{ __('shop::app.checkout.cart.cart-remove-action') }}')">
                                                                                                <i class="fa fa-trash remove-icon"></i>
                                                                                            </a>
                                                                                        </div>

                                                                                        <div class="d-lg-inline-block ml-2">
                                                                                            {{ $item_quantity }} X {{ $item_price }} = {{ core()->currency($item->base_total) }}
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>

                                    <div class="col-12 col-md-6 col-lg-3">
                                        <!-- TOTALS HERE -->
                                        <div class="row">
                                            <div class="col-12 text-center">
                                                <strong>Cart totals</strong>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-6 text-left">
                                                Subtotal
                                            </div>

                                            <div class="col-6 text-right">
                                                {{ core()->currency($cart->base_sub_total) }}
                                            </div>
                                        </div>

                                        @if ($cart->selected_shipping_rate)
                                            <div class="row">
                                                <div class="col-6 text-left">
                                                    {{ __('shop::app.checkout.total.delivery-charges') }}
                                                </div>

                                                <div class="col-6 text-right">
                                                    {{ core()->currency($cart->selected_shipping_rate->base_price) }}
                                                </div>
                                            </div>
                                        @endif

                                        @if ($cart->base_tax_total)
                                            @foreach (Webkul\Tax\Helpers\Tax::getTaxRatesWithAmount($cart, true) as $taxRate => $baseTaxAmount )
                                                <div class="row">
                                                    <div class="col-6 text-left">
                                                        {{ __('shop::app.checkout.total.tax') }} {{ $taxRate }} %
                                                    </div>

                                                    <div class="col-6 text-right">
                                                        {{ core()->currency($baseTaxAmount) }}
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif

                                        @if ($cart->base_discount_amount && $cart->base_discount_amount > 0)
                                            <div class="row">
                                                <div class="col-6 text-left">
                                                    {{ __('shop::app.checkout.total.disc-amount') }}
                                                </div>

                                                <div class="col-6 text-right">
                                                    -{{ core()->currency($cart->base_discount_amount) }}
                                                </div>
                                            </div>
                                        @endif

                                        <div class="row">
                                            <div class="col-6 text-left">
                                                <strong>{{ __('shop::app.checkout.total.grand-total') }}</strong>
                                            </div>

                                            <div class="col-6 text-right">
                                                <strong>{{ core()->currency($cart->base_grand_total) }}</strong>
                                            </div>
                                        </div>

                                        <div class="row justify-content-center align-items-center mt-4 shopping-mini-cart-buttons">
                                            <div class="col-12 col-lg-auto">
                                                <a class="button button-bag" href="{{ route('shop.checkout.onepage.index') }}">{{ __('shop::app.minicart.checkout') }}</a>
                                            </div>

                                            <div class="col-12 col-lg-auto mt-2">
                                                <a class="pt-4 pb-4" href="{{ route('shop.checkout.cart.index') }}">{{ __('shop::app.minicart.view-cart') }}</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            @endif

            {!! view_render_event('bagisto.shop.layout.header.cart-item.after') !!}

            <div class="collapse navbar-collapse d-lg-none px-3 vinylheaven-mobile-navbar" id="vinylheaven-mobile-navbar">
                <ul class="navbar-nav justify-content-center flex-row flex-wrap align-items-center">
                    @foreach ($header_helper->getGenres() as $genre)
                        <li class="nav-item list-item-genres d-inline-flex px-3 py-2">
                            <a href="/{{ $genre->slug }}" class="nav-item-link d-inline-block">{{ $genre->name }}<sup>{{ $genre->products_count }}</sup></a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </nav>
    </div>
</header>

@push('scripts')
<script type="text/javascript" src="{{ asset('themes/vinylheaven-theme/assets/dist/js/components/search.js') }}"></script>
@endpush