@extends('shop::layouts.master-new')

@section('page_title')
    Home
@stop

@section('content-wrapper')

    @include("shop::home.slider")
    @include("shop::home.newproducts")
    @include("shop::home.genres")

@endsection