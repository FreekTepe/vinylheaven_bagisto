<section class="container-fluid container-lg mb-5 new-records">
    <div class="heading">
        <div class="heading-text medium">
            New Records
        </div>
    </div>

    <div class="row justify-content-center align-items-center">
        @if ($new_marketplace_products->count())
            @foreach ($new_marketplace_products as $marketplace_product)
                <div class="col-6 col-sm-6 col-md-4 col-lg-3 mb-3">
                    @include('shop::products.marketplace.card')
                </div>
            @endforeach
        @endif
    </div>
    
    <hr />

    <div class="row justify-content-end mt-5">
        <div class="col col-md-auto">
            <a href="{{ route('catalog.catalog.index') }}" class="button button-arrow">See All New Records</a>
        </div>
    </div>
</section>
