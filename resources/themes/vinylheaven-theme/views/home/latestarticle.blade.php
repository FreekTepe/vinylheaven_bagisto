<section class="latest-article container">
    <div class="heading">
        <h2>Latest Article</h2>
    </div>

    <div class="latest-article__area">
        <div class="latest-article__image">
            <img src="{{ asset('themes/vinylexpress/assets/src/img/article_image.jpg') }}" alt="" />
        </div>
        <div class="latest-article__holder">
            <div class="latest-article__info">
                <img src="{{ asset('themes/vinylexpress/assets/src/img/avatar.png') }}" alt="" />
                <div class="latest-article__name-add">
                    Ronnie W. <br />
                    August 12, 2016
                </div>
            </div>
            <div class="latest-article__content">
                David Bowie's full £10 million art collection displayed online ahead of Sotheby's auction
            </div>
        </div>

    </div>

    <div class="latest-article__load">
        <a href="#" class="button button--arrow">See All Articles</a>
    </div>

</section>