
@extends('shop::layouts.master-new')

@section('page_title', __('Not Found'))

@section('content-wrapper')
    <section class="container-fluid container-lg container-xl container-404">
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="row justify-content-center align-items-center">
                    <div class="col-auto">
                        <div class="error-record d-flex justify-content-center align-items-center">
                            <h1>404</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-6 pt-5">
                <div class="row justify-content-center align-items-center">
                    <div class="col-auto">
                        <h3>Page not found</h3>
                    </div>
                </div>

                <div class="row justify-content-center align-items-center">
                    <div class="col-auto text-center">
                        <h4>The Page you are looking for does not exist or have been moved.</h4>
                    </div>
                </div>

                <div class="row justify-content-center align-items-center">
                    <div class="col-auto text-center mt-2 py-2">
                        <p>Below some suggestions where you might find what you are looking for.</p>

                        <p class="d-none d-lg-inline-block">Or try to search via the searchbar in the top of your screen</p>
                    </div>
                </div>

                <div class="row justify-content-center align-items-center d-lg-none">
                    <div class="col-12 navbar-search py-2">
                        <form method="GET" action="{{ route('shop.search.index') }}" class="form search-form">
                            <div class="form-control-group">
                                <div class="input-group">
                                    <input type="text" class="form-control search-input border-right-width-0" name="term" placeholder="Type and search...." autocomplete="off" />
                                    
                                    <div class="input-group-append">
                                        <button class="btn search-button border-left-width-0 border-radius-0" type="submit">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="row justify-content-center align-items-center mt-2 py-2">
                    <div class="col text-center">
                        <a href="{{ route('shop.search.index') }}?term=">Checkout our catalog</a>
                    </div>

                    <div class="col text-center">
                        <a href="{{ route('shop.home.index') }}">Go to our homepage</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection