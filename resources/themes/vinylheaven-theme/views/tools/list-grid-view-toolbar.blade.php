@inject('toolbarHelper', 'VinylHeaven\Tools\Helpers\Toolbar')

<div class="list-grid-view-toolbar">
    <div class="row justify-content-start align-items-center">
        @if($toolbar_config['has_result_counter'])
            <div class="toolbar-button auto-width col-auto d-lg-none mr-2">
                <i class="fa fa-calculator mr-2"></i>{{ $products_count }}
            </div>

            <div class="toolbar-button auto-width col-auto d-lg-none mr-2">
                <i class="fa fa-search mr-2"></i>{{ isset($_GET['term']) ? $_GET['term'] : '' }}
            </div>

            <div class="toolbar-result-counter col-auto d-none d-lg-inline-block px-0 mr-2 ml-0">
                <div class="row align-items-center">
                    <div class="col-auto counter-text">
                        {{ $products_count }} Results For:
                    </div>
                </div>

                <div class="row align-items-center">
                    <div class="col-auto counter-term">
                        {{ isset($_GET['term']) ? $_GET['term'] : '' }}
                    </div>
                </div>
            </div>
        @endif

        @if($toolbar_config['has_seller_info'])
            @include('marketplace::shop.sellers.profile.info')
        @endif

        @if ($toolbarHelper->toolbarHasFiltersToggler())
            <div class="toolbar-button toolbar-filters-toggler col-auto d-lg-none px-0 ml-auto mr-2" data-toggle="collapse" data-target="#tools-filters" aria-controls="tools-filters" aria-expanded="false" aria-label="Toggle Mobile Filters">
                <i class="fa fa-filter"></i>
            </div>
        @endif

        <div class="toolbar-button toolbar-mobile-limits-toggler col-auto d-lg-none px-0 mx-2" data-toggle="collapse" data-target="#toolbar-mobile-limits" aria-controls="toolbar-mobile-limits" aria-expanded="false" aria-label="Toggle Mobile Limits">
            <i class="fa fa-sort-amount-asc"></i>
        </div>

        <div class="toolbar-limits toolbar-select col-auto d-none d-lg-inline-flex justify-content-start align-items-center px-0 ml-auto mr-2">
            <div class="col-auto select-label p-0 mr-2">
                Per page:
            </div>

            <div class="col-auto px-0 ml-2">
                <select class="toolbar-limits-select select-dropdown">
                    @foreach ($toolbarHelper->getAvailableLimits() as $limit_key => $limit_value)
                        @if ($toolbarHelper->isLimitCurrent($limit_value))
                            <option value="{{ $toolbarHelper->getLimitUrl($limit_value) }}" selected>{{ $limit_value }}</option>
                        @else
                            <option value="{{ $toolbarHelper->getLimitUrl($limit_value) }}">{{ $limit_value }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>

        <div class="toolbar-order toolbar-select col-auto d-none d-lg-inline-flex justify-content-end align-items-center px-0 ml-2 mr-5">
            <div class="col-auto select-label p-0 mr-2">
                Sort:
            </div>

            <div class="col-auto px-0 ml-2">
                <select class="toolbar-order-select select-dropdown">
                    @foreach ($toolbarHelper->getAvailableOrders() as $order_key => $order_value)
                        @if ($toolbarHelper->isOrderCurrent($order_key))
                            <option value="{{ $toolbarHelper->getOrderUrl($order_key) }}" selected>{{ $order_value }}</option>
                        @else
                            <option value="{{ $toolbarHelper->getOrderUrl($order_key) }}">{{ $order_value }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>

        <div class="toolbar-button {{ $toolbarHelper->getGridModeClass() }} toolbar-grid-list-view-button col-auto px-0 mx-2" data-url="{{ $toolbarHelper->getGridViewUrl() }}">
            <img src="{{ asset('themes/vinylexpress/assets/src/img/grid-icon.png') }}" alt="Grid view" />
        </div>

        <div class="toolbar-button {{ $toolbarHelper->getListModeClass() }} toolbar-grid-list-view-button col-auto px-0 ml-2" data-url="{{ $toolbarHelper->getListViewUrl() }}">
            <img src="{{ asset('themes/vinylexpress/assets/src/img/list-Icon.png') }}" alt="List view" />
        </div>
    </div>

    <div class="toolbar-mobile-limits collapse" id="toolbar-mobile-limits">
        <div class="row toolbar-select justify-content-start align-items-center my-3">
            <div class="select-label col-auto ml-0 pl-0 pr-1">
                Per Page:
            </div>

            <div class="col-auto pl-0">
                <select class="toolbar-limits-select select-dropdown">
                    @foreach ($toolbarHelper->getAvailableLimits() as $limit_key => $limit_value)
                        @if ($toolbarHelper->isLimitCurrent($limit_value))
                            <option value="{{ $toolbarHelper->getLimitUrl($limit_value) }}" selected>{{ $limit_value }}</option>
                        @else
                            <option value="{{ $toolbarHelper->getLimitUrl($limit_value) }}">{{ $limit_value }}</option>
                        @endif
                    @endforeach
                </select>
            </div>

            <div class="select-label col-auto pl-0 pr-1 ml-auto">
                Sort:
            </div>
            
            <div class="col-auto px-0">
                <select class="toolbar-order-select select-dropdown">
                    @foreach ($toolbarHelper->getAvailableOrders() as $order_key => $order_value)
                        @if ($toolbarHelper->isOrderCurrent($order_key))
                            <option value="{{ $toolbarHelper->getOrderUrl($order_key) }}" selected>{{ $order_value }}</option>
                        @else
                            <option value="{{ $toolbarHelper->getOrderUrl($order_key) }}">{{ $order_value }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>


@push('scripts')
<script type="text/javascript" src="{{ asset('themes/vinylheaven-theme/assets/dist/js/components/tools/list-grid-view-toolbar.js') }}"></script>
@endpush
