@extends('marketplace::shop.layouts.account')

@section('page_title')
    {{ __('marketplace::app.shop.sellers.account.catalog.products.title') }}
@endsection

@section('content')

    <div class="account-layout">

        <div class="account-head mb-10">
            <span class="account-heading">
                {{ __('marketplace::app.shop.sellers.account.catalog.products.title') }}
            </span>
            <div class="account-action">
                <a href="{{ route('marketplace.account.products.search') }}" class="btn btn-primary btn-lg">
                    {{ __('marketplace::app.shop.sellers.account.catalog.products.create') }}
                </a>
            </div>

            <div class="horizontal-rule"></div>
        </div>




        <ul class="nav nav-tabs mb-4" id="myTab" role="tablist">
            <li class="nav-item">
            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Catalog
            </a>
            </li>
            <li class="nav-item">
                {{-- NTS: pending count even wat netter afvangen --}}
            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Pending <span class="badge badge-pill badge-primary">14</span></a> 
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="home" role="tabpanel" aria-labelledby="home-tab">
                {!! view_render_event('marketplace.sellers.account.catalog.products.list.before') !!}

                <div class="account-items-list">
        
                    {!! app('Webkul\Marketplace\DataGrids\Shop\ProductDataGrid')->render() !!}
        
                </div>
        
                {!! view_render_event('marketplace.sellers.account.catalog.products.list.after') !!}
            </div>
            <div class="tab-pane" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                {{-- Pending products --}}
                {{-- 
                    
                    - a list of the csv rows we have found matches on
                    - per csv row we can click to expand the matched releases (reuse other components)
                    - now the user can choose to sell a result -> form will open -> seller ads the product
                    
                    --}}

                    <pending-products></pending-products>
                </div>
            </div>


    </div>

@endsection