@extends('marketplace::shop.layouts.account')

@section('page_title')
    {{ __('marketplace::app.shop.sellers.account.reviews.title') }}
@endsection

@section('content')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-like2 icon-gradient bg-tempting-azure"></i>
            </div>
            <div>{{ __('marketplace::app.shop.sellers.account.reviews.title') }}
                <div class="page-title-subheading">An overview of customer reviews.</div>
            </div>
        </div>
      
    </div>
</div>   

{!! view_render_event('marketplace.sellers.account.reviews.list.before') !!}

    {!! app('Webkul\Marketplace\DataGrids\Shop\ReviewDataGrid')->render() !!}

{!! view_render_event('marketplace.sellers.account.reviews.list.after') !!}

@endsection