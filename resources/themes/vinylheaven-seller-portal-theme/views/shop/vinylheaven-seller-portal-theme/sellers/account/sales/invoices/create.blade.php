@extends('marketplace::shop.layouts.account')

@section('page_title')
    {{ __('marketplace::app.shop.sellers.account.sales.invoices.create-title') }}
@endsection

@section('content')

<form method="POST" action="{{ route('marketplace.account.invoices.store', $sellerOrder->order_id) }}" @submit.prevent="onSubmit">
    @csrf

    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-plus icon-gradient bg-tempting-azure"></i>
                </div>
                <div>{{ __('marketplace::app.shop.sellers.account.sales.invoices.create-title') }}
                    <div class="page-title-subheading">An overview of orders made at your shop</div>
                </div>
            </div>

            <div class="page-title-actions">
                <div class="d-inline-block">
                    <button type="submit" style="color:white;" class="btn-shadow btn btn-info">
                        {{ __('marketplace::app.shop.sellers.account.sales.invoices.create') }}
                    </button>
                </div>
            </div>
        </div>
    </div>

        {!! view_render_event('marketplace.sellers.account.sales.invoices.create.before', ['sellerOrder' => $sellerOrder]) !!}

    <div class="main-card mb-2 card">
        <div class="card-body">
            <div class="row">
                <div class="col-6">
                    <ul class="list-group">
                        <li class="list-group-item"><b>{{ __('marketplace::app.shop.sellers.account.sales.invoices.order-id') }}:</b> <span class="float-right ml-3"><a href="{{ route('marketplace.account.orders.view', $sellerOrder->order_id) }}">#{{ $sellerOrder->order_id }}</a></span></li>
                        <li class="list-group-item"><b>{{ __('marketplace::app.shop.sellers.account.sales.orders.placed-on') }}:</b> <span class="float-right ml-3">{{ core()->formatDate($sellerOrder->created_at, 'd M Y') }}</span></li>
                        <li class="list-group-item"><b>{{ __('marketplace::app.shop.sellers.account.sales.orders.status') }}:</b> <span class="float-right ml-3">{{ $sellerOrder->status_label }}</span></li>
                        <li class="list-group-item"><b>{{ __('marketplace::app.shop.sellers.account.sales.orders.customer-name') }}:</b> <span class="float-right ml-3">{{ $sellerOrder->order->customer_full_name }}</span></li>

                        <li class="list-group-item"><b>{{ __('marketplace::app.shop.sellers.account.sales.orders.email') }}:</b> <span class="float-right ml-3">{{ $sellerOrder->order->customer_email }}</span></li>
                        <li class="list-group-item"><b>{{ __('marketplace::app.shop.sellers.account.sales.orders.customer-name') }}:</b> <span class="float-right ml-3">{{ $sellerOrder->order->customer_full_name }}</span></li>

                    </ul>
                </div>
            </div>
        </div>
    </div>


    <div class="main-card mb-2 card">
        <div class="card-body">
            <h5 class="card-title">{{ __('marketplace::app.shop.sellers.account.sales.invoices.create-title') }}</h5>

            <div class="table table-responsive">
                <table class="table mt-3">
                    <thead>
                    <tr>
                        <th>{{ __('marketplace::app.shop.sellers.account.sales.invoices.product-name') }}</th>
                        <th>{{ __('marketplace::app.shop.sellers.account.sales.invoices.qty-ordered') }}</th>
                        <th>{{ __('marketplace::app.shop.sellers.account.sales.invoices.qty-to-invoice') }}</th>
                    </tr>
                    </thead>

                    <tbody>

                    @foreach ($sellerOrder->items as $sellerOrderItem)
                        <tr>
                            <td>
                                {{ $sellerOrderItem->item->name }}

                                @if (isset($sellerOrderItem->additional['attributes']))
                                    <div class="item-options">

                                        @foreach ($sellerOrderItem->additional['attributes'] as $attribute)
                                            <b>{{ $attribute['attribute_name'] }} : </b>{{ $attribute['option_label'] }}</br>
                                        @endforeach

                                    </div>
                                @endif
                            </td>
                            <td>{{ $sellerOrderItem->item->qty_ordered }}</td>
                            <td>
                                <div class="control-group" :class="[errors.has('invoice[items][{{ $sellerOrderItem->order_item_id }}]') ? 'has-error' : '']">
                                    <input type="text" v-validate="'required|numeric|min:0'" class="control" id="invoice[items][{{ $sellerOrderItem->order_item_id }}]" name="invoice[items][{{ $sellerOrderItem->order_item_id }}]" value="{{ $sellerOrderItem->qty_to_invoice }}" data-vv-as="&quot;{{ __('admin::app.sales.invoices.qty-to-invoice') }}&quot;"/>

                                    <span class="control-error" v-if="errors.has('invoice[items][{{ $sellerOrderItem->order_item_id }}]')">
                                                        @verbatim
                                            {{ errors.first('invoice[items][<?php echo $sellerOrderItem->order_item_id ?>]') }}
                                        @endverbatim
                                                    </span>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-3">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <div class="widget-content p-0">
                                        <div class="widget-content-wrapper">
                                            <div class="widget-content-left">
                                                <div class="widget-heading">{{ __('shop::app.customer.account.order.view.shipping-address') }}</div>
                                                <hr>
                                                <div class="widget-subheading">{{ $sellerOrder->order->billing_address->name }}</div>
                                                <div class="widget-subheading">{{ $sellerOrder->order->billing_address->address1 }}</div>
                                                <div class="widget-subheading">{{ $sellerOrder->order->billing_address->city }}</div>
                                                <div class="widget-subheading">{{ $sellerOrder->order->billing_address->state }}</div>
                                                <div class="widget-subheading">{{ core()->country_name($sellerOrder->order->billing_address->country) }} {{ $sellerOrder->order->billing_address->postcode }}</div>
                                                <div class="widget-subheading">{{ __('shop::app.checkout.onepage.contact') }} : {{ $sellerOrder->order->billing_address->phone }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-3">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <div class="widget-content p-0">
                                        <div class="widget-content-wrapper">
                                            <div class="widget-content-left">
                                                <div class="widget-heading">{{ __('shop::app.customer.account.order.view.billing-address') }}</div>
                                                <hr>
                                                <div class="widget-subheading">{{ $sellerOrder->order->shipping_address->name }}</div>
                                                <div class="widget-subheading">{{ $sellerOrder->order->shipping_address->address1 }}</div>
                                                <div class="widget-subheading">{{ $sellerOrder->order->shipping_address->city }}</div>
                                                <div class="widget-subheading">{{ $sellerOrder->order->shipping_address->state }}</div>
                                                <div class="widget-subheading">{{ core()->country_name($sellerOrder->order->shipping_address->country) }} {{ $sellerOrder->order->shipping_address->postcode }}</div>
                                                <div class="widget-subheading">{{ __('shop::app.checkout.onepage.contact') }} : {{ $sellerOrder->order->shipping_address->phone }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-3">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <div class="widget-content p-0">
                                        <div class="widget-content-wrapper">
                                            <div class="widget-content-left">
                                                <div class="widget-heading">{{ __('shop::app.customer.account.order.view.shipping-method') }}</div>
                                                <hr>
                                                <div class="widget-subheading">{{ $sellerOrder->order->shipping_title }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-3">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <div class="widget-content p-0">
                                        <div class="widget-content-wrapper">
                                            <div class="widget-content-left">
                                                <div class="widget-heading">{{ __('shop::app.customer.account.order.view.payment-method') }}</div>
                                                <hr>
                                                <div class="widget-subheading">{{ core()->getConfigData('sales.paymentmethods.' . $sellerOrder->order->payment->method . '.title') }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>
{!! view_render_event('marketplace.sellers.account.sales.invoices.create.after', ['sellerOrder' => $sellerOrder]) !!}


@endsection