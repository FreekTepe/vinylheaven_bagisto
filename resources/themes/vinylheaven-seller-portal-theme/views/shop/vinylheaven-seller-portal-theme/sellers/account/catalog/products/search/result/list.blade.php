<ul class="list-group list-group-flush">
    @foreach ($releases as $release_index => $release)
        <li class="pt-3"  id="release-{{ $release['sku'] }}-sellect-container">
            <div class="card">
                <div class="card-header">
                    <h5>{{ $releases_additional[$release_index]['artist_name'] }} - {{ $release['name'] }}</h5>
                    
                    <div class="btn-actions-pane-right">
                        <button class="btn btn-shadow btn-info release-sell-button" data-release-id="{{ $release['sku'] }}" style="margin-left: 1em">Sell</button>
                    </div>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-md-3">
                            {!! $releases_additional[$release_index]['primary_image'] !!}
                        </div>

                        <div class="col-12 col-md-9">
                            <div class="d-flex flex-column flex-sm-row justify-content-between w-100 mb-3">
                                <div class="text-muted">
                                    Labels:
                                    <small>{!! $releases_additional[$release_index]['release_labels'] !!}</small>
                                </div>

                                <div class="text-muted">
                                    Country:
                                    <small>{{ $release['country'] }}, {{ $release['release_date'] }}</small>
                                </div>

                                <div class="text-muted">
                                    Formats:
                                    <small>{!! $releases_additional[$release_index]['release_formats'] !!}</small>
                                </div>
                            </div>

                            <div class="d-flex justify-content-start mb-3">
                                <div class="w-100">
                                    <ul class="nav nav-pills" id="release-{{ $release['sku'] }}-information-tabs" role="tablist">
                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link active" id="release-{{ $release['sku'] }}-tracklist-tab" data-toggle="tab" href="#release-{{ $release['sku'] }}-tracklist" role="tab" aria-controls="release-{{ $release['sku'] }}-tracklist" aria-selected="true">
                                                Tracks
                                            </a>
                                        </li>

                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link" id="release-{{ $release['sku'] }}-artists-tab" data-toggle="tab" href="#release-{{ $release['sku'] }}-artists" role="tab" aria-controls="release-{{ $release['sku'] }}-artists" aria-selected="true">
                                                Artists
                                            </a>
                                        </li>

                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link" id="release-{{ $release['sku'] }}-labels-tab" data-toggle="tab" href="#release-{{ $release['sku'] }}-labels" role="tab" aria-controls="release-{{ $release['sku'] }}-labels" aria-selected="true">
                                                Labels
                                            </a>
                                        </li>

                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link" id="release-{{ $release['sku'] }}-companies-tab" data-toggle="tab" href="#release-{{ $release['sku'] }}-companies" role="tab" aria-controls="release-{{ $release['sku'] }}-companies" aria-selected="true">
                                                Companies
                                            </a>
                                        </li>

                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link" id="release-{{ $release['sku'] }}-identifiers-tab" data-toggle="tab" href="#release-{{ $release['sku'] }}-identifiers" role="tab" aria-controls="release-{{ $release['sku'] }}-identifiers" aria-selected="true">
                                                Identifiers
                                            </a>
                                        </li>
                                    </ul>

                                    <div class="tab-content" id="release-{{ $release['sku'] }}-information-tabs-content">
                                        <div class="tab-pane fade show active" id="release-{{ $release['sku'] }}-tracklist" role="tabpanel" aria-labelledby="release-{{ $release['sku'] }}-tracklist-tab">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th style="width:10%;">Position</th>
                                                        <th style="width:60%;">Title</th>
                                                        <th style="width:20%;">Duration</th>
                                                        <th style="width:10%;"></th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    @foreach ($release['tracklist'] as $track)
                                                        <tr>
                                                            <td>{{ $track['position'] }}</td>
                                                            <td>{{ $track['title'] }}</td>
                                                            <td>{{ $track['duration'] }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="tab-pane fade" id="release-{{ $release['sku'] }}-artists" role="tabpanel" aria-labelledby="release-{{ $release['sku'] }}-artists-tab">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Artist id</th>
                                                        <th scope="col">Name</th>
                                                    </tr>
                                                </thead>
                                                
                                                <tbody>
                                                    @foreach ($release['artists'] as $artist)
                                                        <tr>
                                                            <td>
                                                                <b>{{ $artist['artist_id'] }}</b>
                                                            </td>

                                                            <td>{{ $artist['name'] }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="tab-pane fade" id="release-{{ $release['sku'] }}-labels" role="tabpanel" aria-labelledby="release-{{ $release['sku'] }}-labels-tab">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Label id</th>
                                                        <th scope="col">Name</th>
                                                        <th scope="col">Cat Nr.</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    @foreach ($release['labels'] as $label)
                                                        <tr>
                                                            <td>
                                                                <b>{{ $label['label_id'] }}</b>
                                                            </td>

                                                            <td>{{ $label['name'] }}</td>
                                                            <td>{{ $label['cat_no'] }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="tab-pane fade" id="release-{{ $release['sku'] }}-companies" role="tabpanel" aria-labelledby="release-{{ $release['sku'] }}-companies-tab">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">Name</th>
                                                        <th scope="col">Cat no</th>
                                                        <th scope="col">Entity type</th>
                                                        <th scope="col">Entity type name</th>
                                                        <th scope="col">Resource Url</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    @if ($release['companies'])
                                                        @foreach ($release['companies'] as $company)
                                                            <tr>
                                                                <td>
                                                                <b>{{ $company['id'] }}</b>
                                                                </td>
                                                                <td>{{ $company['name'] }}</td>
                                                                <td>{{ $company['catno'] }}</td>
                                                                <td>{{ $company['entity_type'] }}</td>
                                                                <td>{{ $company['entity_type_name'] }}</td>
                                                                <td>{{ $company['resource_url'] }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="tab-pane fade" id="release-{{ $release['sku'] }}-identifiers" role="tabpanel" aria-labelledby="release-{{ $release['sku'] }}-identifiers-tab">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Description</th>
                                                        <th scope="col">Type</th>
                                                        <th scope="col">Value</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    @if ($release['identifiers'])
                                                        @foreach($release['identifiers'] as $identifier)
                                                            <tr>
                                                                <td>{{ $identifier['description'] }}</td>
                                                                <td>{{ $identifier['type'] }}</td>
                                                                <td>{{ $identifier['value'] }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </li>

        <li class="pt-3 d-none" id="release-{{ $release['sku'] }}-sell-container">
            <div class="card">
                <div class="card-header">
                    <h5>{{ $releases_additional[$release_index]['artist_name'] }} - {{ $release['name'] }}</h5>

                    <div class="btn-actions-pane-right d-inline-flex">
                        <div id="result-message-container-{{ $release['sku'] }}" class="d-inline-flex align-items-center d-none mr-2"></div>

                        <button id="release-add-to-store-button-{{ $release['sku'] }}" class="btn-icon btn-icon-only btn btn-info btn-sm btn-shadow release-add-to-store-button mr-2" data-release-id="{{ $release['sku'] }}">
                            <i id="loading-icon-{{ $release['sku'] }}" class="fas fa-compact-disc fa-spin d-none"></i> 
                            Add To Store
                        </button>

                        <button id="release-sell-cancel-button-{{ $release['sku'] }}" class="btn-icon btn-icon-only btn btn-danger btn-sm btn-shadow release-sell-cancel-button" data-release-id="{{ $release['sku'] }}" style="color: white;">
                            <i class="fas fa-times btn-icon-wrapper"></i>
                        </button>
                    </div>
                </div>

                <div id="card-body-{{ $release['sku'] }}" class="card-body">
                    <input type="hidden" id="release-{{ $release['sku'] }}-json-string" value="{{ json_encode($release) }}" />
                    <input type="hidden" id="release-additional-{{ $release['sku'] }}-json-string" value="{{ json_encode($releases_additional[$release_index]) }}" />

                    <div class="row">
                        <div class="col-12 col-md-3">
                            {!! $releases_additional[$release_index]['primary_image'] !!}
                        </div>

                        <div class="col-12 col-md-9">
                            <div class="d-flex flex-column flex-sm-row justify-content-between w-100 mb-3">
                                <div class="text-muted">
                                    Labels:
                                    <small>{!! $releases_additional[$release_index]['release_labels'] !!}</small>
                                </div>

                                <div class="text-muted">
                                    Country:
                                    <small>{{ $release['country'] }}, {{ $release['release_date'] }}</small>
                                </div>

                                <div class="text-muted">
                                    Formats:
                                    <small>{!! $releases_additional[$release_index]['release_formats'] !!}</small>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label class="required">Media condition <span style="color:red;">*</span></label>

                                        <select class="form-control" id="media-condition-{{ $release['sku'] }}">
                                            <option disabled value="null">Please select a condition</option>
                                            <option value="Mint (M)">Mint (M)</option>
                                            <option value="Near Mint (NM or M-)">Near Mint (NM or M-)</option>
                                            <option value="Very Good Plus (VG+)">Very Good Plus (VG+)</option>
                                            <option value="Very Good (VG)">Very Good (VG)</option>
                                            <option value="Good Plus (G+)">Good Plus (G+)</option>
                                            <option value="Good (G)">Good (G)</option>
                                            <option value="Fair (F)">Fair (F)</option>
                                            <option value="Poor (P)">Poor (P)</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label class="required">Sleeve condition <span style="color:red;">*</span></label>

                                        <select class="form-control" id="sleeve-condition-{{ $release['sku'] }}">
                                            <option disabled value="null">Please select a condition</option>
                                            <option value="Mint (M)">Mint (M)</option>
                                            <option value="Near Mint (NM or M-)">Near Mint (NM or M-)</option>
                                            <option value="Very Good Plus (VG+)">Very Good Plus (VG+)</option>
                                            <option value="Very Good (VG)">Very Good (VG)</option>
                                            <option value="Good Plus (G+)">Good Plus (G+)</option>
                                            <option value="Good (G)">Good (G)</option>
                                            <option value="Fair (F)">Fair (F)</option>
                                            <option value="Poor (P)">Poor (P)</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-12 col-md-6">
                                    <label for="price" class="required">Price <span style="color:red;">*</span></label>

                                    <input type="text" class="form-control" id="price-{{ $release['sku'] }}" />
                                </div>

                                <div class="col-12 col-md-6">
                                    <label for="description">Description</label>

                                    <textarea class="form-control autosize-input" id="description-{{ $release['sku'] }}"></textarea>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col-12 col-md-6">
                                    <label for="private_comments">Private comments</label>

                                    <textarea class="form-control autosize-input" id="private-comments-{{ $release['sku'] }}"></textarea>
                                </div>

                                <div class="col-12 col-md-6">
                                    <label for="storage_location">Storage location</label>

                                    <textarea class="form-control autosize-input" id="storage-location-{{ $release['sku'] }}"></textarea>
                                </div>
                            </div>

                            <div class="d-flex justify-content-start mb-3">
                                <div class="w-100">
                                    <ul class="nav nav-pills" id="release-{{ $release['sku'] }}-information-tabs-sell" role="tablist">
                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link active" id="release-{{ $release['sku'] }}-tracklist-tab-sell" data-toggle="tab" href="#release-{{ $release['sku'] }}-tracklist-sell" role="tab" aria-controls="release-{{ $release['sku'] }}-tracklist-sell" aria-selected="true">
                                                Tracks
                                            </a>
                                        </li>

                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link" id="release-{{ $release['sku'] }}-artists-tab-sell" data-toggle="tab" href="#release-{{ $release['sku'] }}-artists-sell" role="tab" aria-controls="release-{{ $release['sku'] }}-artists-sell" aria-selected="true">
                                                Artists
                                            </a>
                                        </li>

                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link" id="release-{{ $release['sku'] }}-labels-tab-sell" data-toggle="tab" href="#release-{{ $release['sku'] }}-labels-sell" role="tab" aria-controls="release-{{ $release['sku'] }}-labels-sell" aria-selected="true">
                                                Labels
                                            </a>
                                        </li>

                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link" id="release-{{ $release['sku'] }}-companies-tab-sell" data-toggle="tab" href="#release-{{ $release['sku'] }}-companies-sell" role="tab" aria-controls="release-{{ $release['sku'] }}-companies-sell" aria-selected="true">
                                                Companies
                                            </a>
                                        </li>

                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link" id="release-{{ $release['sku'] }}-identifiers-tab-sell" data-toggle="tab" href="#release-{{ $release['sku'] }}-identifiers-sell" role="tab" aria-controls="release-{{ $release['sku'] }}-identifiers-sell" aria-selected="true">
                                                Identifiers
                                            </a>
                                        </li>
                                    </ul>

                                    <div class="tab-content" id="release-{{ $release['sku'] }}-information-tabs-content-sell">
                                        <div class="tab-pane fade show active" id="release-{{ $release['sku'] }}-tracklist-sell" role="tabpanel" aria-labelledby="release-{{ $release['sku'] }}-tracklist-tab-sell">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th style="width:10%;">Position</th>
                                                        <th style="width:60%;">Title</th>
                                                        <th style="width:20%;">Duration</th>
                                                        <th style="width:10%;"></th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    @foreach ($release['tracklist'] as $track)
                                                        <tr>
                                                            <td>{{ $track['position'] }}</td>
                                                            <td>{{ $track['title'] }}</td>
                                                            <td>{{ $track['duration'] }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="tab-pane fade" id="release-{{ $release['sku'] }}-artists-sell" role="tabpanel" aria-labelledby="release-{{ $release['sku'] }}-artists-tab-sell">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Artist id</th>
                                                        <th scope="col">Name</th>
                                                    </tr>
                                                </thead>
                                                
                                                <tbody>
                                                    @foreach ($release['artists'] as $artist)
                                                        <tr>
                                                            <td>
                                                                <b>{{ $artist['artist_id'] }}</b>
                                                            </td>

                                                            <td>{{ $artist['name'] }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="tab-pane fade" id="release-{{ $release['sku'] }}-labels-sell" role="tabpanel" aria-labelledby="release-{{ $release['sku'] }}-labels-tab-sell">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Label id</th>
                                                        <th scope="col">Name</th>
                                                        <th scope="col">Cat Nr.</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    @foreach ($release['labels'] as $label)
                                                        <tr>
                                                            <td>
                                                                <b>{{ $label['label_id'] }}</b>
                                                            </td>

                                                            <td>{{ $label['name'] }}</td>
                                                            <td>{{ $label['cat_no'] }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="tab-pane fade" id="release-{{ $release['sku'] }}-companies-sell" role="tabpanel" aria-labelledby="release-{{ $release['sku'] }}-companies-tab-sell">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">Name</th>
                                                        <th scope="col">Cat no</th>
                                                        <th scope="col">Entity type</th>
                                                        <th scope="col">Entity type name</th>
                                                        <th scope="col">Resource Url</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    @if ($release['companies'])
                                                        @foreach ($release['companies'] as $company)
                                                            <tr>
                                                                <td>
                                                                <b>{{ $company['id'] }}</b>
                                                                </td>
                                                                <td>{{ $company['name'] }}</td>
                                                                <td>{{ $company['catno'] }}</td>
                                                                <td>{{ $company['entity_type'] }}</td>
                                                                <td>{{ $company['entity_type_name'] }}</td>
                                                                <td>{{ $company['resource_url'] }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="tab-pane fade" id="release-{{ $release['sku'] }}-identifiers-sell" role="tabpanel" aria-labelledby="release-{{ $release['sku'] }}-identifiers-tab-sell">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Description</th>
                                                        <th scope="col">Type</th>
                                                        <th scope="col">Value</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    @if ($release['identifiers'])
                                                        @foreach($release['identifiers'] as $identifier)
                                                            <tr>
                                                                <td>{{ $identifier['description'] }}</td>
                                                                <td>{{ $identifier['type'] }}</td>
                                                                <td>{{ $identifier['value'] }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    @endforeach
</ul>