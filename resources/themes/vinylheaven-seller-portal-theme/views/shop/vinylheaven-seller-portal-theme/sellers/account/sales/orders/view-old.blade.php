<div class="row">
    <div class="col-12">
        <h6 >{{ __('shop::app.customer.account.order.view.individual-invoice', ['invoice_id' => $sellerInvoice->invoice_id]) }}</h6>

        <a href="{{ route('marketplace.account.invoices.print', $sellerInvoice->marketplace_order_id ) }}" class="pull-right btn btn-sm">
            {{ __('shop::app.customer.account.order.view.print') }}
        </a>
    </div>

</div>



<div class="row">
    <div class="col-12">
        <div class="table table-responsive">
            <table>
                <thead>
                <tr>
                    <th>{{ __('shop::app.customer.account.order.view.product-name') }}</th>
                    <th>{{ __('shop::app.customer.account.order.view.price') }}</th>
                    <th>{{ __('shop::app.customer.account.order.view.qty') }}</th>
                    <th>{{ __('shop::app.customer.account.order.view.subtotal') }}</th>
                    <th>{{ __('shop::app.customer.account.order.view.tax-amount') }}</th>
                    <th>{{ __('shop::app.customer.account.order.view.discount') }}</th>
                    <th>{{ __('shop::app.customer.account.order.view.grand-total') }}</th>
                </tr>
                </thead>

                <tbody>

                @foreach ($sellerInvoice->items as $sellerInvoiceItem)
                    <?php $baseInvoiceItem = $sellerInvoiceItem->item; ?>
                    <tr>
                        <td data-value="{{ __('shop::app.customer.account.order.view.product-name') }}">{{ $baseInvoiceItem->name }}</td>

                        <td data-value="{{ __('shop::app.customer.account.order.view.price') }}">
                            {{ core()->formatPrice($baseInvoiceItem->price, $sellerOrder->order->order_currency_code) }}
                        </td>

                        <td data-value="{{ __('shop::app.customer.account.order.view.qty') }}">{{ $baseInvoiceItem->qty }}</td>

                        <td data-value="{{ __('shop::app.customer.account.order.view.subtotal') }}">
                            {{ core()->formatPrice($baseInvoiceItem->total, $sellerOrder->order->order_currency_code) }}
                        </td>

                        <td data-value="{{ __('shop::app.customer.account.order.view.tax-amount') }}">
                            {{ core()->formatPrice($baseInvoiceItem->tax_amount, $sellerOrder->order->order_currency_code) }}
                        </td>

                        <td data-value="{{ __('shop::app.customer.account.order.view.discount') }}">
                            {{ core()->formatPrice($baseInvoiceItem->discount_amount, $sellerOrder->order->order_currency_code) }}
                        </td>

                        <td data-value="{{ __('shop::app.customer.account.order.view.grand-total') }}">
                            {{ core()->formatPrice($baseInvoiceItem->total + $baseInvoiceItem->tax_amount - $sellerOrder->discount_amount, $sellerOrder->order->order_currency_code) }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
</div>