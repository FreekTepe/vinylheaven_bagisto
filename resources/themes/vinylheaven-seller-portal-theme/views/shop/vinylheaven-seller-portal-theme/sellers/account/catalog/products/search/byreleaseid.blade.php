<div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control" id="release_id" placeholder="Release id" />

                            <div class="input-group-append">
                                <button type="button" id="release-search-by-id-button" class="btn btn-info btn-shadow datepicker-trigger-btn" >
                                    <span class="btn-icon-wrapper pr-2 opacity-7">
                                        <i class="fa fa-search fa-w-20"></i>
                                    </span>SEARCH
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>