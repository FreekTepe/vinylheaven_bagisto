<div>
    <div class="card">
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="title">Titles</label>

                    <input type="text" class="form-control jquery-autocomplete" id="release_title" name="title" autocomplete="off" data-token="{{ csrf_token() }}" data-query-key="release_title" />

                    <i class="fas fa-compact-disc fa-spin autocomplete-loading-icon text-primary d-none"></i>
                    <i class="fas fa-question autocomplete-question-icon text-warning d-none"></i>
                    <i class="fas fa-check autocomplete-check-icon text-success d-none"></i>
                </div>

                <div class="form-group col-md-4">
                    <label for="artist">Artist</label>

                    <input type="text" class="form-control jquery-autocomplete" id="artist_name" name="artist" autocomplete="off" data-token="{{ csrf_token() }}" data-query-key="artist_name" />

                    <i class="fas fa-compact-disc fa-spin autocomplete-loading-icon text-primary d-none"></i>
                    <i class="fas fa-question autocomplete-question-icon text-warning d-none"></i>
                    <i class="fas fa-check autocomplete-check-icon text-success d-none"></i>
                </div>

                <div class="form-group col-md-4">
                    <label for="label">Label</label>

                    <input type="text" class="form-control jquery-autocomplete" id="label" name="label" autocomplete="off" data-token="{{ csrf_token() }}" data-query-key="label" />

                    <i class="fas fa-compact-disc fa-spin autocomplete-loading-icon text-primary d-none"></i>
                    <i class="fas fa-question autocomplete-question-icon text-warning d-none"></i>
                    <i class="fas fa-check autocomplete-check-icon text-success d-none"></i>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="catalog">Catalog #</label>
                    
                    <input type="text" class="form-control jquery-autocomplete" id="cat_no" name="cat_no" autocomplete="off" data-token="{{ csrf_token() }}" data-query-key="cat_no" />
                    
                    <i class="fas fa-compact-disc fa-spin autocomplete-loading-icon text-primary d-none"></i>
                    <i class="fas fa-question autocomplete-question-icon text-warning d-none"></i>
                    <i class="fas fa-check autocomplete-check-icon text-success d-none"></i>
                </div>

                <div class="form-group col-md-4">
                    <label for="barcode">Barcode</label>
                    
                    <input type="text" class="form-control jquery-autocomplete" id="barcode" name="barcode" autocomplete="off" data-token="{{ csrf_token() }}" data-query-key="barcode" />
                    
                    <i class="fas fa-compact-disc fa-spin autocomplete-loading-icon text-primary d-none"></i>
                    <i class="fas fa-question autocomplete-question-icon text-warning d-none"></i>
                    <i class="fas fa-check autocomplete-check-icon text-success d-none"></i>
                </div>

                <div class="form-group col-md-4">
                    <label for="year">Year</label>
                    
                    <input type="text" class="form-control jquery-autocomplete" id="released" name="released" autocomplete="off" data-token="{{ csrf_token() }}" data-query-key="released" />
                    
                    <i class="fas fa-compact-disc fa-spin autocomplete-loading-icon text-primary d-none"></i>
                    <i class="fas fa-question autocomplete-question-icon text-warning d-none"></i>
                    <i class="fas fa-check autocomplete-check-icon text-success d-none"></i>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-4" id="release_formats">
                    <label>Formats</label><br />

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="release_format" value="vinyl">
                        <label class="form-check-label">Vinyl</label>
                    </div>

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="release_format" value="cd">
                        <label class="form-check-label">Cd</label>
                    </div>

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="release_format" value="mc">
                        <label class="form-check-label">Cassette</label>
                    </div>

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="release_format" value="other">
                        <label class="form-check-label">Other</label>
                    </div>
                </div>

                <div class="form-group col-md-4">
                    <label>Country</label>

                    <select id="country" class="form-control">
                        <option value="null">Select a country</option>
                        <option>Abkhazia</option>
                        <option>Afghanistan</option>
                        <option>Africa</option>
                        <option>Albania</option>
                        <option>Algeria</option>
                        <option>Andorra</option>
                        <option>Angola</option>
                        <option>Anguilla</option>
                        <option>Antigua & Barbuda</option>
                        <option>Antigua & Barbuda</option>
                        <option>Argentina</option>
                        <option>Armenia</option>
                        <option>Aruba</option>
                        <option>Asia</option>
                        <option>Australasia</option>
                        <option>Australia</option>
                        <option>Australia & New Zealand</option>
                        <option>Austria</option>
                        <option>Austria-Hungary</option>
                        <option>Azerbaijan</option>
                        <option>Bahamas, The</option>
                        <option>Bahrain</option>
                        <option>Bangladesh</option>
                        <option>Barbados</option>
                        <option>Belarus</option>
                        <option>Belgium</option>
                        <option>Belize</option>
                        <option>Benelux</option>
                        <option>Benin</option>
                        <option>Bermuda</option>
                        <option>Bhutan</option>
                        <option>Bohemia</option>
                        <option>Bolivia</option>
                        <option>Bosnia & Herzegovina</option>
                        <option>Botswana</option>
                        <option>Brazil</option>
                        <option>British Virgin Islands</option>
                        <option>Brunei</option>
                        <option>Bulgaria</option>
                        <option>Burkina Faso</option>
                        <option>Burma</option>
                        <option>Cambodia</option>
                        <option>Cameroon</option>
                        <option>Canada</option>
                        <option>Cape Verde</option>
                        <option>Cayman Islands</option>
                        <option>Central African Republic</option>
                        <option>Central America</option>
                        <option>Chad</option>
                        <option>Chile</option>
                        <option>China</option>
                        <option>Colombia</option>
                        <option>Comoros</option>
                        <option>Congo, Democratic Republic of the</option>
                        <option>Congo, Republic of the</option>
                        <option>Cook Islands</option>
                        <option>Costa Rica</option>
                        <option>Croatia</option>
                        <option>Cuba</option>
                        <option>Curaçao</option>
                        <option>Cyprus</option>
                        <option>Czech Republic</option>
                        <option>Czech Republic & Slovakia</option>
                        <option>Czechoslovakia</option>
                        <option>Dahomey</option>
                        <option>Denmark</option>
                        <option>Djibouti</option>
                        <option>Dominica</option>
                        <option>Dominican Republic</option>
                        <option>Dutch East Indies</option>
                        <option>East Timor</option>
                        <option>Ecuador</option>
                        <option>Egypt</option>
                        <option>El Salvador</option>
                        <option>Equatorial Guinea</option>
                        <option>Eritrea</option>
                        <option>Estonia</option>
                        <option>Ethiopia</option>
                        <option>Europe</option>
                        <option>Falkland Islands</option>
                        <option>Faroe Islands</option>
                        <option>Fiji</option>
                        <option>Finland</option>
                        <option>France</option>
                        <option>France & Benelux</option>
                        <option>French Guiana</option>
                        <option>French Polynesia</option>
                        <option>Gabon</option>
                        <option>Gambia, The</option>
                        <option>Gaza Strip</option>
                        <option>Georgia</option>
                        <option>German Democratic Republic (GDR)</option>
                        <option>Germany</option>
                        <option>Germany & Switzerland</option>
                        <option>Germany, Austria, & Switzerland</option>
                        <option>Ghana</option>
                        <option>Gibraltar</option>
                        <option>Greece</option>
                        <option>Greenland</option>
                        <option>Grenada</option>
                        <option>Guadeloupe</option>
                        <option>Guam</option>
                        <option>Guatemala</option>
                        <option>Guernsey</option>
                        <option>Guinea</option>
                        <option>Guinea-Bissau</option>
                        <option>Gulf Cooperation Council</option>
                        <option>Guyana</option>
                        <option>Haiti</option>
                        <option>Honduras</option>
                        <option>Hong Kong</option>
                        <option>Hungary</option>
                        <option>Iceland</option>
                        <option>India</option>
                        <option>Indochina</option>
                        <option>Indonesia</option>
                        <option>Iran</option>
                        <option>Iraq</option>
                        <option>Ireland</option>
                        <option>Israel</option>
                        <option>Italy</option>
                        <option>Ivory Coast</option>
                        <option>Jamaica</option>
                        <option>Japan</option>
                        <option>Jersey</option>
                        <option>Jordan</option>
                        <option>Kazakhstan</option>
                        <option>Kenya</option>
                        <option>Korea (pre-1945)</option>
                        <option>Kosovo</option>
                        <option>Kuwait</option>
                        <option>Kyrgyzstan</option>
                        <option>Laos</option>
                        <option>Latvia</option>
                        <option>Lebanon</option>
                        <option>Lesotho</option>
                        <option>Liberia</option>
                        <option>Libya</option>
                        <option>Liechtenstein</option>
                        <option>Lithuania</option>
                        <option>Luxembourg</option>
                        <option>Macau</option>
                        <option>Macedonia</option>
                        <option>Madagascar</option>
                        <option>Malawi</option>
                        <option>Malaysia</option>
                        <option>Maldives</option>
                        <option>Mali</option>
                        <option>Malta</option>
                        <option>Man, Isle of</option>
                        <option>Marshall Islands</option>
                        <option>Martinique</option>
                        <option>Mauritania</option>
                        <option>Mauritius</option>
                        <option>Mayotte</option>
                        <option>Mexico</option>
                        <option>Micronesia, Federated States of</option>
                        <option>Middle East</option>
                        <option>Moldova, Republic of</option>
                        <option>Monaco</option>
                        <option>Mongolia</option>
                        <option>Montenegro</option>
                        <option>Montserrat</option>
                        <option>Morocco</option>
                        <option>Mozambique</option>
                        <option>Namibia</option>
                        <option>Nauru</option>
                        <option>Nepal</option>
                        <option>Netherlands</option>
                        <option>Netherlands Antilles</option>
                        <option>New Caledonia</option>
                        <option>New Zealand</option>
                        <option>Nicaragua</option>
                        <option>Niger</option>
                        <option>Nigeria</option>
                        <option>Norfolk Island</option>
                        <option>North America (inc Mexico)</option>
                        <option>North Korea</option>
                        <option>Northern Mariana Islands</option>
                        <option>Norway</option>
                        <option>Oman</option>
                        <option>Ottoman Empire</option>
                        <option>Pakistan</option>
                        <option>Palau</option>
                        <option>Panama</option>
                        <option>Papua New Guinea</option>
                        <option>Paraguay</option>
                        <option>Peru</option>
                        <option>Philippines</option>
                        <option>Poland</option>
                        <option>Portugal</option>
                        <option>Protectorate of Bohemia and Moravia</option>
                        <option>Puerto Rico</option>
                        <option>Qatar</option>
                        <option>Reunion</option>
                        <option>Rhodesia</option>
                        <option>Romania</option>
                        <option>Russia</option>
                        <option>Rwanda</option>
                        <option>Saint Kitts and Nevis</option>
                        <option>Saint Lucia</option>
                        <option>Saint Vincent and the Grenadines</option>
                        <option>Samoa</option>
                        <option>San Marino</option>
                        <option>Sao Tome and Principe</option>
                        <option>Saudi Arabia</option>
                        <option>Scandinavia</option>
                        <option>Senegal</option>
                        <option>Serbia</option>
                        <option>Serbia and Montenegro</option>
                        <option>Seychelles</option>
                        <option>Sierra Leone</option>
                        <option>Singapore</option>
                        <option>Singapore &amp; Malaysia</option>
                        <option>Singapore, Malaysia & Hong Kong</option>
                        <option>Sint Maarten</option>
                        <option>Slovakia</option>
                        <option>Slovenia</option>
                        <option>Solomon Islands</option>
                        <option>Somalia</option>
                        <option>South Africa</option>
                        <option>South America</option>
                        <option>South East Asia</option>
                        <option>South Georgia and the South Sandwich Islands</option>
                        <option>South Korea</option>
                        <option>South Pacific</option>
                        <option>South Vietnam</option>
                        <option>South West Africa</option>
                        <option>Southern Rhodesia</option>
                        <option>Southern Sudan</option>
                        <option>Spain</option>
                        <option>Sri Lanka</option>
                        <option>Sudan</option>
                        <option>Suriname</option>
                        <option>Swaziland</option>
                        <option>Sweden</option>
                        <option>Switzerland</option>
                        <option>Syria</option>
                        <option>Taiwan</option>
                        <option>Tajikistan</option>
                        <option>Tanzania</option>
                        <option>Thailand</option>
                        <option>Togo</option>
                        <option>Tonga</option>
                        <option>Trinidad & Tobago</option>
                        <option>Tunisia</option>
                        <option>Turkey</option>
                        <option>Turkmenistan</option>
                        <option>Turks and Caicos Islands</option>
                        <option>UK</option>
                        <option>UK & Europe</option>
                        <option>UK & France</option>
                        <option>UK & Ireland</option>
                        <option>UK & US</option>
                        <option>UK, Europe & Israel</option>
                        <option>UK, Europe & Japan</option>
                        <option>UK, Europe & US</option>
                        <option>US</option>
                        <option>USA & Canada</option>
                        <option>USA & Europe</option>
                        <option>USA, Canada & Europe</option>
                        <option>USA, Canada & UK</option>
                        <option>USSR</option>
                        <option>Uganda</option>
                        <option>Ukraine</option>
                        <option>United Arab Emirates</option>
                        <option>Upper Volta</option>
                        <option>Uruguay</option>
                        <option>Uzbekistan</option>
                        <option>Vanuatu</option>
                        <option>Vatican City</option>
                        <option>Venezuela</option>
                        <option>Vietnam</option>
                        <option>Virgin Islands</option>
                        <option>West Bank</option>
                        <option>Yemen</option>
                        <option>Yugoslavia</option>
                        <option>Zaire</option>
                        <option>Zambia</option>
                        <option>Zimbabwe</option>
                    </select>
                </div>

                <div class="form-group col-md-4">
                    <label>Matrix / Runout</label>
                    <input type="text" class="form-control jquery-autocomplete" id="matrix" name="matrix" autocomplete="off" data-token="{{ csrf_token() }}" data-query-key="matrix" />

                    <i class="fas fa-compact-disc fa-spin autocomplete-loading-icon text-primary d-none"></i>
                    <i class="fas fa-question autocomplete-question-icon text-warning d-none"></i>
                    <i class="fas fa-check autocomplete-check-icon text-success d-none"></i>
                </div>

                <div class="row justify-content-start align-items-center">
                    <div class="col-auto">
                        <button id="release-search-button" class="btn btn-block btn-primary">Find</button>
                    </div>

                    <div id="release-searching-icon" class="col-auto p-0 d-none">
                        <i class="fas fa-compact-disc fa-spin text-primary h4 m-0"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="search-release-result-list" class="d-block mt-5"></div>

@push('scripts')
    <script type="text/javascript" src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/components/autocomplete.js') }}"></script>
    <script type="text/javascript" src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/seller-portal/release.search.js') }}"></script>
@endpush