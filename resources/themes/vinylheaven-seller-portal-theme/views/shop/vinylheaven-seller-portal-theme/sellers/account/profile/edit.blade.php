@extends('marketplace::shop.layouts.account')

@section('page_title')
    {{ __('marketplace::app.shop.sellers.account.profile.edit-title') }}
@endsection

@section('content')

<form method="post" action="{{ route('marketplace.account.seller.update', $seller->id) }}" @submit.prevent="onSubmit" enctype="multipart/form-data">

    {!! view_render_event('marketplace.sellers.account.profile.edit.before', ['seller' => $seller]) !!}

    @csrf()
    <input type="hidden" name="_method" value="PUT">

    
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="fas fa-store-alt icon-gradient bg-tempting-azure"></i>
                </div>
                <div>{{ __('marketplace::app.shop.sellers.account.profile.edit-title') }}
                    <div class="page-title-subheading">Here you can edit your store information.</div>
                </div>
            </div>
            <div class="page-title-actions">

                <div class="d-inline-block dropdown">
                    <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn-shadow dropdown-toggle btn btn-dark" style="color:white;">
                        <span class="btn-icon-wrapper pr-2 opacity-7">
                            <i class="far fa-eye fa-w-20"></i>
                        </span>
                        View Shop
                    </button>
                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu-rounded dropdown-menu dropdown-menu-right">
                        <ul class="nav flex-column">
                            <li class="nav-item">
                                <a href="{{ route('marketplace.products.index', $seller->url) }}" target="_blank" class="nav-link">
                                    <span>
                                        Collection Page
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('marketplace.seller.show', $seller->url) }}" target="_blank" class="nav-link">
                                    <span>
                                        Seller Page
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="d-inline-block">
                    
                    <button  type="submit" style="color:white;" class="btn-shadow btn btn-info">
                        Save Details
                    </button>
                </div>
            </div>    
        </div>
    </div>   


    {{-- FORM CONTENT --}}


    <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">

        <li class="nav-item">
            <a role="tab" class="nav-link active" id="tab-0" data-toggle="tab" href="#tab-content-0" aria-selected="true">
                <span>General</span>
                {{-- General information + media + About shop --}}
            </a>
        </li>
        <li class="nav-item">
            <a role="tab" class="nav-link" id="tab-1" data-toggle="tab" href="#tab-content-1" aria-selected="true">
                <span>Payments</span>
                {{-- Payment options for a shop --}}
            </a>
        </li>
        <li class="nav-item">
            <a role="tab" class="nav-link" id="tab-3" data-toggle="tab" href="#tab-content-3" aria-selected="false">
                <span>Policies</span>
                {{-- Policies --}}
            </a>
        </li>
    </ul>




    {{-- Content --}}
    <div class="container-fluid mt-4">
        <div class="tab-content">
            {{-- Discogs CSV --}}
            <div class="tab-pane tabs-animation fade active show" id="tab-content-0" role="tabpanel">
                <div class="row">
                    <div class="col-md-12">
                        <div class="main-card mb-2 card">
                            <div class="card-body">
                                <h5 class="card-title">Shop Title</h5>

                                    {{-- shop_title --}}
                                    <div class="position-relative row form-group" :class="[errors.has('shop_title') ? 'has-error' : '']">
                                        <label for="shop_title" class="col-sm-2 col-form-label required">{{ __('marketplace::app.shop.sellers.account.profile.shop_title') }}</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" :class="errors.has('shop_title') ? 'is-invalid'  : ''" name="shop_title" v-validate="'required'" data-vv-as="&quot;{{ __('marketplace::app.shop.sellers.account.profile.shop_title') }}&quot;" value="{{ old('shop_title') ?: $seller->shop_title }}" >
                                        </div>
                                        {{-- <em id="firstname-error" class="error invalid-feedback">Please enter your firstname</em> --}}
                                        {{-- <span class="control-error" v-if="errors.has('shop_title')">@{{ errors.first('shop_title') }}</span> --}}
                                    </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="main-card mb-2 card">
                            <div class="card-body">

                                


                                <h5 class="card-title">Media</h5>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="control-group">
                                            <label>{{ __('marketplace::app.shop.sellers.account.profile.logo') }}</label>
                                            <custom-image-picker 
                                                :button-label="'{{ __('marketplace::app.shop.sellers.account.profile.add-image-btn-title') }}'" 
                                                input-name="logo" 
                                                :multiple="false" 
                                                :images='"{{ $seller->logo_url }}"'
                                            ></custom-image-picker>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="control-group">
                                            <label>{{ __('marketplace::app.shop.sellers.account.profile.banner') }}</label>
                                            <custom-image-picker 
                                                :button-label="'{{ __('marketplace::app.shop.sellers.account.profile.add-image-btn-title') }}'"  
                                                input-name="banner"  
                                                :multiple="false"  
                                                :images='"{{ $seller->banner_url }}"'  
                                            ></custom-image-wrapper>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="main-card mb-2 card">
                            <div class="card-body">
                                <h5 class="card-title">General Info</h5>


                                {{-- shop_enabled --}}
                                <div class="position-relative row form-group" :class="[errors.has('shop_enabled') ? 'has-error' : '']">
                                    <label for="shop_enabled" class="col-sm-2 col-form-label required">Go Live</label>
                                    <div class="col-sm-10">
                                        <input @if($seller->shop_enabled) checked @endif  name="shop_enabled" type="checkbox" value="1" data-toggle="toggle" class="pull-right" data-size="small">
                                    </div>
                                    {{-- <span class="control-error" v-if="errors.has('shop_title')">@{{ errors.first('shop_title') }}</span> --}}
                                </div>

                                {{-- legal_status (Business or Private) --}}
                                <div class="position-relative row form-group" :class="[errors.has('legal_status') ? 'has-error' : '']">
                                    <label for="legal_status" class="col-sm-2 col-form-label required">Legal Status</label>
                                    <div class="col-sm-10">
                                        <select name="legal_status" id="legal_status" class="form-control">
                                            <option value="business" @if($seller->legal_status == "business") selected @endif >Business</option>
                                            <option value="private" @if($seller->legal_status == "private") selected @endif >Private</option>
                                        </select>
                                    </div>
                                </div>

                                {{-- tax_vat --}}
                                <div class="position-relative row form-group">
                                    <label for="tax_vat" class="col-sm-2 col-form-label">{{ __('marketplace::app.shop.sellers.account.profile.tax_vat') }}</label>
                                    <div class="col-sm-10">
                                        <input name="tax_vat" type="text" class="form-control"  @if($seller->legal_status == "private") value="" @else value="{{ old('tax_vat') ?: $seller->tax_vat }}" @endif @if($seller->legal_status == "private") disabled @endif>
                                    </div>
                                </div>

                                {{-- address1 --}}
                                <div class="position-relative row form-group" :class="[errors.has('address1') ? 'has-error' : '']">
                                    <label for="address1" class="col-sm-2 col-form-label required">{{ __('marketplace::app.shop.sellers.account.profile.address1') }}</label>
                                    <div class="col-sm-10">
                                        <input name="address1" type="text" class="form-control" :class="errors.has('address1') ? 'is-invalid'  : ''" v-validate="'required'" data-vv-as="&quot;{{ __('marketplace::app.shop.sellers.account.profile.address1') }}&quot;" value="{{ old('address1') ?: $seller->address1 }}">
                                    </div>
                                </div>

                                {{-- address2 --}}
                                <div class="position-relative row form-group">
                                    <label for="address2" class="col-sm-2 col-form-label">{{ __('marketplace::app.shop.sellers.account.profile.address2') }}</label>
                                    <div class="col-sm-10">
                                        <input name="address2" type="text" class="form-control" value="{{ old('address2') ?: $seller->address2 }}">
                                    </div>
                                </div>

                                {{-- postcode --}}
                                <div class="position-relative row form-group" :class="[errors.has('postcode') ? 'has-error' : '']">
                                    <label for="postcode" class="col-sm-2 col-form-label required">{{ __('marketplace::app.shop.sellers.account.profile.postcode') }}</label>
                                    <div class="col-sm-10">
                                        <input name="postcode" type="text" class="form-control" :class="errors.has('postcode') ? 'is-invalid'  : ''" v-validate="'required'" data-vv-as="&quot;{{ __('marketplace::app.shop.sellers.account.profile.postcode') }}&quot;" value="{{ old('postcode') ?: $seller->postcode }}">
                                    </div>
                                </div>

                                {{-- city --}}
                                <div class="position-relative row form-group" :class="[errors.has('city') ? 'has-error' : '']">
                                    <label for="city" class="col-sm-2 col-form-label required">{{ __('marketplace::app.shop.sellers.account.profile.city') }}</label>
                                    <div class="col-sm-10">
                                        <input name="city" type="text" class="form-control" :class="errors.has('city') ? 'is-invalid'  : ''" v-validate="'required'" data-vv-as="&quot;{{ __('marketplace::app.shop.sellers.account.profile.city') }}&quot;" value="{{ old('city') ?: $seller->city }}">
                                    </div>
                                </div>

                                {{-- NTS: template this snippet --}}
                                @include ('shop::customers.account.address.country-state', ['countryCode' => old('country') ?? $seller->country, 'stateCode' => old('state') ?? $seller->state])


                                {{-- url --}}
                                {{-- <div class="position-relative row form-group" :class="[errors.has('url') ? 'has-error' : '']">
                                    <label for="url" class="col-sm-2 col-form-label required">{{ __('marketplace::app.shop.sellers.account.profile.url') }}</label>
                                    <div class="col-sm-10">
                                        <input name="url" type="text" class="form-control" :class="errors.has('url') ? 'is-invalid'  : ''" v-validate="'required'" data-vv-as="&quot;{{ __('marketplace::app.shop.sellers.account.profile.url') }}&quot;" value="{{ old('url') ?: $seller->url }}">
                                    </div>
                                </div> --}}


                                {{-- phone --}}
                                <div class="position-relative row form-group" :class="[errors.has('phone') ? 'has-error' : '']">
                                    <label for="phone" class="col-sm-2 col-form-label required">{{ __('marketplace::app.shop.sellers.account.profile.phone') }}</label>
                                    <div class="col-sm-10">
                                        <input name="phone" type="text" class="form-control" :class="errors.has('phone') ? 'is-invalid'  : ''" v-validate="'required'" data-vv-as="&quot;{{ __('marketplace::app.shop.sellers.account.profile.phone') }}&quot;" value="{{ old('phone') ?: $seller->phone }}">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                    {{-- About shop --}}
                    <div class="col-md-12">
                        <div class="main-card mb-2 card">
                            <div class="card-body">
                                <h5 class="card-title">{{ __('marketplace::app.shop.sellers.account.profile.about') }}</h5>

                                <div class="position-relative row form-group" >
                                    <div class="col-sm-12">
                                        <textarea class="form-control" id="description" name="description">{{ old('description') ?: $seller->description }}</textarea>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    
                    {{-- SOCIAL MEDIA --}}
                    <div class="col-md-12">
                        <div class="main-card card">
                            <div class="card-body">
                                <div class="row">
                                    {{-- SEO --}}
                                    <div class="col-md-12">

                                        <h5 class="card-title">{{ __('marketplace::app.shop.sellers.account.profile.social_links') }}</h5>

                                        {{-- Twitter --}}
                                        <div class="position-relative row form-group">
                                            <label for="twitter" class="col-sm-2 col-form-label">{{ __('marketplace::app.shop.sellers.account.profile.twitter') }}</label>
                                            <div class="col-sm-10">
                                                <input name="twitter" type="text" class="form-control" value="{{ old('twitter') ?: $seller->twitter }}">
                                            </div>
                                        </div>

                                        {{-- Facebook --}}
                                        <div class="position-relative row form-group">
                                            <label for="facebook" class="col-sm-2 col-form-label">{{ __('marketplace::app.shop.sellers.account.profile.facebook') }}</label>
                                            <div class="col-sm-10">
                                                <input name="facebook" type="text" class="form-control" value="{{ old('facebook') ?: $seller->facebook }}">
                                            </div>
                                        </div>


                                        {{-- Youtube --}}
                                        {{-- <div class="position-relative row form-group">
                                            <label for="youtube" class="col-sm-2 col-form-label">{{ __('marketplace::app.shop.sellers.account.profile.youtube') }}</label>
                                            <div class="col-sm-10">
                                                <input name="youtube" type="text" class="form-control" value="{{ old('youtube') ?: $seller->youtube }}">
                                            </div>
                                        </div> --}}
                                        
                                        {{-- Instagram --}}
                                        <div class="position-relative row form-group">
                                            <label for="instagram" class="col-sm-2 col-form-label">{{ __('marketplace::app.shop.sellers.account.profile.instagram') }}</label>
                                            <div class="col-sm-10">
                                                <input name="instagram" type="text" class="form-control" value="{{ old('instagram') ?: $seller->instagram }}">
                                            </div>
                                        </div>
                
                                        {{-- Skype --}}
                                        {{-- <div class="position-relative row form-group">
                                            <label for="skype" class="col-sm-2 col-form-label">{{ __('marketplace::app.shop.sellers.account.profile.skype') }}</label>
                                            <div class="col-sm-10">
                                                <input name="skype" type="text" class="form-control" value="{{ old('skype') ?: $seller->skype }}">
                                            </div>
                                        </div> --}}
                
                                        {{-- Linked_in --}}
                                        {{-- <div class="position-relative row form-group">
                                            <label for="linked_in" class="col-sm-2 col-form-label">{{ __('marketplace::app.shop.sellers.account.profile.linked_in') }}</label>
                                            <div class="col-sm-10">
                                                <input name="linked_in" type="text" class="form-control" value="{{ old('linked_in') ?: $seller->linked_in }}">
                                            </div>
                                        </div> --}}
                
                                        {{-- Pinterest --}}
                                        {{-- <div class="position-relative row form-group">
                                            <label for="pinterest" class="col-sm-2 col-form-label">{{ __('marketplace::app.shop.sellers.account.profile.pinterest') }}</label>
                                            <div class="col-sm-10">
                                                <input name="pinterest" type="text" class="form-control" value="{{ old('pinterest') ?: $seller->pinterest }}">
                                            </div>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane tabs-animation fade" id="tab-content-1" role="tabpanel">
                <div class="row">
                    <div class="col-md-12">
                        <div class="main-card mb-2 card">
                            <div class="card-header">
                                <h5 class="card-title mt-1">Paypal</h5>
                                <div class="btn-actions-pane-right">
                                    <input @if($seller->paypal_allowed) checked @endif  name="paypal_allowed" type="checkbox" value="1" data-toggle="toggle" class="pull-right" data-size="small">
                                </div>
                            </div>
                            <div class="card-body">
                                <p>
                                    <small>
                                    Before you accept payments with paypal you will have to add your Paypal account to your profile.
                                    You can do that here. You will need a VERIFIED account to accept Paypal Payments.
                                    </small>
                                </p>

                                {{-- Paypal email --}}
                                <div class="position-relative row form-group">
                                    <label for="youtube" class="col-sm-2 col-form-label">
                                        Paypal Email</label>
                                    <div class="col-sm-10">
                                        <input value="{{ $seller->paypal_account }}" name="paypal_account" type="text" class="form-control">
                                    </div>
                                </div>

                            </div>
                        </div>
                        

                        <div class="main-card mb-2 card">
                            <div class="card-header">
                                <h5 class="card-title mt-1">Bank Transfer</h5>
                                <div class="btn-actions-pane-right">
                                    <input @if($seller->bank_allowed) checked @endif name="bank_allowed" type="checkbox" value="1" data-toggle="toggle" class="pull-right" data-size="small">

                                    {{-- <input @if($seller->bank_allowed) checked @endif  value="{{ $seller->bank_allowed }}" name="bank_allowed" type="checkbox" data-toggle="toggle" class="pull-right" data-size="small"> --}}
                                </div>
                            </div>
                            <div class="card-body">
                                <p>
                                    <small>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius fugiat ea eveniet consequatur nobis perferendis ullam non labore blanditiis numquam aspernatur, voluptatum voluptatem officia, est sequi aut fugit alias. Quam?
                                    </small>
                                </p>

                                {{-- Bank Account Name --}}
                                <div class="position-relative row form-group">
                                    <label for="youtube" class="col-sm-2 col-form-label">
                                        Name</label>
                                    <div class="col-sm-10">
                                        <input value="{{ $seller->bank_account_holder_name }}" name="bank_account_holder_name" type="text" class="form-control">
                                    </div>
                                </div>

                                {{-- Iban --}}
                                <div class="position-relative row form-group">
                                    <label for="youtube" class="col-sm-2 col-form-label">
                                        Iban</label>
                                    <div class="col-sm-10">
                                        <input value="{{ $seller->bank_iban }}" name="bank_iban" type="text" class="form-control">
                                    </div>
                                </div>

                                {{-- Bic / Swift --}}
                                <div class="position-relative row form-group">
                                    <label for="youtube" class="col-sm-2 col-form-label">
                                        BIC / SWIFT</label>
                                    <div class="col-sm-10">
                                        <input value="{{ $seller->bank_bic_swift }}" name="bank_bic_swift" type="text" class="form-control">
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="main-card mb-2 card">
                            <div class="card-header">
                                <h5 class="card-title mt-1">Cash</h5>
                                <div class="btn-actions-pane-right">
                                    <input @if($seller->cash_allowed) checked @endif name="cash_allowed" type="checkbox" value="1" data-toggle="toggle" class="pull-right" data-size="small">
                                </div>
                            </div>
                            <div class="card-body">
                                <p>
                                    <small>
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius fugiat ea eveniet consequatur nobis perferendis ullam non labore blanditiis numquam aspernatur, voluptatum voluptatem officia, est sequi aut fugit alias. Quam?
                                    </small>
                                </p>
                            </div>
                        </div>

                        <div class="main-card mb-2 card">
                            <div class="card-header">
                                <h5 class="card-title mt-1">Local Pickup</h5>
                                <div class="btn-actions-pane-right">
                                    <input @if($seller->pickup_allowed) checked @endif name="pickup_allowed" type="checkbox" value="1" data-toggle="toggle" class="pull-right" data-size="small">
                                </div>
                            </div>
                            <div class="card-body">
                                
                                <p>
                                    <small>
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius fugiat ea eveniet consequatur nobis perferendis ullam non labore blanditiis numquam aspernatur, voluptatum voluptatem officia, est sequi aut fugit alias. Quam?
                                    </small>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        
            <div class="tab-pane tabs-animation fade" id="tab-content-2" role="tabpanel">
                <div class="row">
                    {{-- SEO --}}
                    <div class="col-md-12">
                        <div class="main-card mb-2 card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="card-title">{{ __('marketplace::app.shop.sellers.account.profile.seo') }}</h5>

                                        <div class="position-relative row form-group" >
                                            <div class="col-sm-12">
                                                <label for="meta_description">{{ __('marketplace::app.shop.sellers.account.profile.meta_description') }}</label>

                                                <textarea class="form-control" id="meta_description" name="meta_description">{{ old('meta_description') ?: $seller->meta_description }}</textarea>
                                            </div>
                                        </div>
                                        

                                        <div class="position-relative row form-group" >
                                            <div class="col-sm-12">
                                                <label for="meta_keywords">{{ __('marketplace::app.shop.sellers.account.profile.meta_keywords') }}</label>
                                                <textarea  class="form-control" id="meta_keywords" name="meta_keywords">{{ old('meta_keywords') ?: $seller->meta_keywords }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                </div>
            </div>

            <div class="tab-pane tabs-animation fade" id="tab-content-3" role="tabpanel">
                <div class="row">
                    <div class="col-md-12">
                        <div class="main-card mb-3 mt-3 card">
                            <div class="card-body">
                                <div class="row">
                                    {{-- Policies --}}
                                    <div class="col-md-12">
                                        <h5 class="card-title">{{ __('marketplace::app.shop.sellers.account.profile.policies') }}</h5>

                                        <div class="position-relative row form-group">
                                            <label class="ml-3" for="return_policy">{{ __('marketplace::app.shop.sellers.account.profile.return_policy') }}</label>
                                            <div class="col-sm-12">
                                                <textarea class="form-control" id="return_policy" name="return_policy">{{ old('return_policy') ?: $seller->return_policy }}</textarea>
                                            </div>
                                        </div>

                                        <div class="position-relative row form-group">
                                            <label class="ml-3" for="shipping_policy">{{ __('marketplace::app.shop.sellers.account.profile.shipping_policy') }}</label>
                                            <div class="col-sm-12">
                                                <textarea class="form-control" id="shipping_policy" name="shipping_policy">{{ old('shipping_policy') ?: $seller->shipping_policy }}</textarea>
                                            </div>
                                        </div>

                                        <div class="position-relative row form-group">
                                            <label class="ml-3" for="privacy_policy">{{ __('marketplace::app.shop.sellers.account.profile.privacy_policy') }}</label>
                                            <div class="col-sm-12">
                                                <textarea class="form-control" id="privacy_policy" name="privacy_policy">{{ old('privacy_policy') ?: $seller->privacy_policy }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>


    {!! view_render_event('marketplace.sellers.account.profile.edit.after', ['seller' => $seller]) !!}

</form>


@endsection

@push('scripts')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <script src="{{ asset('vendor/webkul/admin/assets/js/tinyMCE/tinymce.min.js') }}"></script>

    <script>
        $(document).ready(function () {
            tinymce.init({
                selector: 'textarea#description,textarea#return_policy,textarea#shipping_policy,textarea#privacy_policy',
                height: 200,
                width: "100%",
                plugins: 'image imagetools media wordcount save fullscreen code',
                toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | code',
                image_advtab: true,
                valid_elements : '*[*]',
                templates: [
                    { title: 'Test template 1', content: 'Test 1' },
                    { title: 'Test template 2', content: 'Test 2' }
                ],
            });

            function toggle(id) {
                var val = $('#'+id).value()
                $('#'+id).value(!val)
                console.log(id);

            }
        });
    </script>
@endpush