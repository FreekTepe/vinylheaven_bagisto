@extends('marketplace::shop.layouts.account')

@section('page_title')
    {{ __('marketplace::app.shop.sellers.account.catalog.products.search-title') }}
@endsection
@section('content')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="fas fa-box-open icon-gradient bg-tempting-azure"></i>
            </div>
            <div>Product search
                <div class="page-title-subheading">Here you can search products and add them to your store.</div>
            </div>
        </div>
    </div>
</div>   


<ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">
    <li class="nav-item">
        <a role="tab" class="nav-link active show" id="tab-0" data-toggle="tab" href="#tab-content-0" aria-selected="true">
            <span>Search</span>
        </a>
    </li>
    <li class="nav-item">
        <a role="tab" class="nav-link show" id="tab-1" data-toggle="tab" href="#tab-content-1" aria-selected="false">
            <span>Discogs id</span>
        </a>
    </li>
</ul>

{!! view_render_event('marketplace.sellers.account.catalog.products.search.before') !!}

<div class="tab-content mt-4">
    {{-- Discogs CSV --}}
    <div class="tab-pane tabs-animation fade active show" id="tab-content-0" role="tabpanel">
        @include('marketplace::shop.sellers.account.catalog.products.search.release')
    </div>
    
    <div class="tab-pane tabs-animation fade" id="tab-content-1" role="tabpanel">
    @include('marketplace::shop.sellers.account.catalog.products.search.byreleaseid')
    </div>
</div>

{!! view_render_event('marketplace.sellers.account.catalog.products.search.after') !!}

@endsection

@push('scripts')

    

@endpush