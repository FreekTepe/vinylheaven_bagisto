@extends('marketplace::shop.layouts.account')

@section('page_title')
    {{ __('marketplace::app.shop.sellers.account.catalog.products.assing-edit-title') }}
@endsection

@section('content')

<form method="POST" action="" enctype="multipart/form-data" @submit.prevent="onSubmit">

    @csrf()

    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="fas fa-box-open icon-gradient bg-tempting-azure"></i>
                </div>
                <div>{{ __('marketplace::app.shop.sellers.account.catalog.products.assing-edit-title') }}
                    <div class="page-title-subheading">Here you can edit your product details.</div>
                </div>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block">
                    <button type="submit" style="color:white;" class="btn-shadow btn btn-info">
                        {{ __('marketplace::app.shop.sellers.account.catalog.products.save-btn-title') }}
                    </button>
                </div>
            </div>
        </div>
    </div>


    {{-- <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav ml-2">

        <li class="nav-item">
            <a role="tab" class="nav-link active " id="tab-0" data-toggle="tab" href="#tab-content-0" aria-selected="true">
                <span>Edit</span>
            </a>
        </li>
        <li class="nav-item">
            <a role="tab" class="nav-link" id="tab-1" data-toggle="tab" href="#tab-content-1" aria-selected="false">
                <span>Metrics</span>
            </a>
        </li>
    </ul> --}}


    <div class="tab-content">
        {{-- Discogs CSV --}}
        <div class="tab-pane tabs-animation fade active show" id="tab-content-0" role="tabpanel">

    {!! view_render_event('marketplace.sellers.account.catalog.product.edit-assign.before') !!}

        <div class="row">
          <div class="col mb-3">
            <div class="card">
              <div class="card-body">
                <div class="e-profile">
                  <div class="row">

                    {{-- Image --}}
                    <div class="col-12 col-sm-auto mb-3">
                      <div class="mx-auto" style="width: 140px;">
                        <div class="d-flex justify-content-center align-items-center rounded" style="height: 140px; background-color: rgb(233, 236, 239);">
                            {!! $product->product->getPrimaryImage() !!}
                        </div>
                      </div>
                    </div>

                    {{-- Small details next to image --}}
                    <div class="col d-flex flex-column flex-sm-row justify-content-between mb-3">
                      <div class="text-center text-sm-left mb-2 mb-sm-0">
                        <a href="{{ url()->to('/').'/products/'.$product->product->url_key }}" target="_blank" title="{{ $product->product->name }}">
                            <h4 class="pt-sm-2 pb-1 mb-0 text-nowrap">{{ $product->product->name }}</h4>
                        </a>
                        <p class="mb-0">{{ $product->seller->shop_title }}</p>
                        <div class="text-muted"><small>Updated {{ $product->updated_at->diffForHumans() }}</small></div>
                        <div class="mt-2">
                          <button class="btn btn-shadow btn-info" type="button">
                            <i class="fa fa-fw fa-camera"></i>
                            <span>Change Photo</span>
                          </button>
                        </div>
                      </div>
                      <div class="text-center text-sm-right">
                        <div class="text-muted"><small>Created {{ $product->created_at->diffForHumans() }}</small></div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                <input name="_method" type="hidden" value="PUT">

                <div class="col-md-12">
                    <h5 class="card-title">{{ __('marketplace::app.shop.sellers.account.catalog.products.general') }}</h5>
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="position-relative form-group" :class="[errors.has('condition') ? 'has-error' : '']">
                                <label class="required">Media Condition</label>
                                <?php $selectedOption = old('condition') ?: $product->condition ?>
                                <select class="form-control"
                                        v-validate="'required'"
                                        name="condition"
                                        class="control"
                                        id="condition">

                                        <option disabled :value="null">Please select a condition</option>
                                        <option value="Mint (M)" {{ $selectedOption == 'Mint (M)' ? 'selected' : '' }}>Mint (M)</option>
                                        <option value="Near Mint (NM or M-)" {{ $selectedOption == 'Near Mint (NM or M-)' ? 'selected' : '' }}>Near Mint (NM or M-)</option>
                                        <option value="Very Good Plus (VG+)" {{ $selectedOption == 'Very Good Plus (VG+)' ? 'selected' : '' }}>Very Good Plus (VG+)</option>
                                        <option value="Very Good (VG)" {{ $selectedOption == 'Very Good (VG)' ? 'selected' : '' }}>Very Good (VG)</option>
                                        <option value="Good Plus (G+)" {{ $selectedOption == 'Good Plus (G+)' ? 'selected' : '' }}>Good Plus (G+)</option>
                                        <option value="Good (G)" {{ $selectedOption == 'Good (G)' ? 'selected' : '' }}>Good (G)</option>
                                        <option value="Fair (F)" {{ $selectedOption == 'Fair (F)' ? 'selected' : '' }}>Fair (F)</option>
                                        <option value="Poor (P)" {{ $selectedOption == 'Poor (P)' ? 'selected' : '' }}>Poor (P)</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="position-relative form-group" :class="[errors.has('condition') ? 'has-error' : '']">
                                <label for="sleeve_condition" class="required">Sleeve Condition</label>
                                <?php $selectedOption = old('sleeve_condition') ?: $product->sleeve_condition ?>
                                <select class="form-control"
                                        v-validate="'required'"
                                        name="sleeve_condition"
                                        class="control"
                                        id="sleeve_condition">

                                        <option disabled :value="null">Please select a condition</option>
                                        <option value="Generic"{{ $selectedOption == 'Generic' ? 'selected' : '' }}>Generic</option>
                                        <option value="Not Graded"{{ $selectedOption == 'Not Graded' ? 'selected' : '' }}>Not Graded</option>
                                        <option value="No Cover"{{ $selectedOption == 'No Cover' ? 'selected' : '' }}>No Cover</option>
                                        <option value="Mint (M)" {{ $selectedOption == 'Mint (M)' ? 'selected' : '' }}>Mint (M)</option>
                                        <option value="Near Mint (NM or M-)" {{ $selectedOption == 'Near Mint (NM or M-)' ? 'selected' : '' }}>Near Mint (NM or M-)</option>
                                        <option value="Very Good Plus (VG+)" {{ $selectedOption == 'Very Good Plus (VG+)' ? 'selected' : '' }}>Very Good Plus (VG+)</option>
                                        <option value="Very Good (VG)" {{ $selectedOption == 'Very Good (VG)' ? 'selected' : '' }}>Very Good (VG)</option>
                                        <option value="Good Plus (G+)" {{ $selectedOption == 'G+' ? 'selected' : '' }}>Good Plus (G+)</option>
                                        <option value="Good (G)" {{ $selectedOption == 'Good (G)' ? 'selected' : '' }}>Good (G)</option>
                                        <option value="Fair (F)" {{ $selectedOption == 'Fair (F)' ? 'selected' : '' }}>Fair (F)</option>
                                        <option value="Poor (P)" {{ $selectedOption == 'Poor (P)' ? 'selected' : '' }}>Poor (P)</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="control-group form-row mb-3" :class="[errors.has('price') ? 'has-error' : '']">
                        <div class="col-md-6">

                            <label for="price" class="required">{{ __('marketplace::app.shop.sellers.account.catalog.products.price') }}</label>
                            <input type="text" class="form-control" v-validate="'required'" :class="errors.has('price') ? 'is-invalid'  : ''" id="price" name="price" value="{{ old('price') ?: $product->price }}" data-vv-as="&quot;{{ __('marketplace::app.shop.sellers.account.catalog.products.price') }}&quot;" {{ $product->product->type == 'configurable' ? 'disabled' : '' }}/>
                        </div>

                        <div class="col-md-6">

                            <label for="shipping_price" class="required">Shipping price</label>
                            <input type="text" class="form-control" id="shipping_price" name="shipping_price" value="{{ old('shipping_price') ?: $product->shipping_price }}" data-vv-as="&quot;shipping_price&quot;" {{ $product->product->type == 'configurable' ? 'disabled' : '' }}/>
                        </div>
                    </div>

                    <div class="control-group form-row mb-3">
                        <div class="col-md-12">
                            <label for="description">{{ __('marketplace::app.shop.sellers.account.catalog.products.description') }}</label>
                            <textarea  id="description" name="description"  data-vv-as="&quot;{{ __('marketplace::app.shop.sellers.account.catalog.products.description') }}&quot;" class="form-control autosize-input" style="height: 77px;">{{ old('description') ?: $product->description }}</textarea>
                        </div>
                    </div>

                    {{-- Private comments --}}
                    <div class="control-group form-row mb-3">
                        <div class="col-md-12">
                            <label for="private_comments">Private Comments</label>
                            <textarea id="private_comments" name="private_comments" class="form-control autosize-input"  style="height: 77px;">{{ old('private_comments') ?: $product->private_comments }}</textarea>
                        </div>
                    </div>

                    {{-- Storage Location --}}
                    <div class="control-group form-row mb-3">
                        <div class="col-md-12">

                        <label for="storage_location">Storage Location</label>
                        <textarea id="storage_location" name="storage_location" class="form-control autosize-input" style="height: 77px;">{{ old('storage_location') ?: $product->storage_location }}</textarea>
                        </div>
                    </div>

                    <div class="control-group form-row mb-3" :class="[errors.has('price') ? 'has-error' : '']">
                        <div class="col-md-3">
                            <label for="storage_location">Width</label>
                            <input type="text" class="form-control" name="width" value="{{ $product->width }}" />
                        </div>
                        <div class="col-md-3">
                            <label for="storage_location">Height</label>
                            <input type="text" class="form-control" name="height" value="{{ $product->height }}" />
                        </div>
                        <div class="col-md-3">
                            <label for="storage_location">Length</label>
                            <input type="text" class="form-control" name="length" value="{{ $product->length }}" />
                        </div>
                        <div class="col-md-3">
                            <label for="storage_location">Weight</label>
                            <input type="text" class="form-control" name="weight" value="{{ $product->weight }}" />
                        </div>
                    </div>

                    <div class="control-group form-row mb-3" :class="[errors.has('price') ? 'has-error' : '']">
                        <div class="col-md-6">
                            <label for="storage_location">Inventory Quantity</label>
                            <input type="number" class="form-control" name="qty" value="{{ old('qty') ?: $product->qty }}" />
                        </div>
                    </div>
                </div>
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>


         {{-- {!! view_render_event('marketplace.sellers.account.catalog.product.edit-assign.after') !!} --}}

        </div>

            {{-- Metrics tab --}}
        <div class="tab-pane tabs-animation fade" id="tab-content-1" role="tabpanel">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body"><h5>metrics</h5></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>

@endsection






{{-- Confusing script stuff --}}
@if ($product->product->type == 'configurable')
@push('scripts')
    @parent

    <script type="text/x-template" id="variant-list-template">
        <div class="table" style="margin-top: 20px; overflow-x: unset;">
            <table>

                <thead>
                    <tr>
                        <th class=""></th>

                        <th>{{ __('admin::app.catalog.products.name') }}</th>

                        <th class="qty">{{ __('admin::app.catalog.products.qty') }}</th>

                        @foreach ($product->product->super_attributes as $attribute)
                            <th class="{{ $attribute->code }}" style="width: 150px">{{ $attribute->admin_name }}</th>
                        @endforeach

                        <th class="price" style="width: 100px;">{{ __('admin::app.catalog.products.price') }}</th>
                    </tr>
                </thead>

                <tbody>

                    <variant-item v-for='(variant, index) in variants' :variant="variant" :key="index" :index="index"></variant-item>

                </tbody>

            </table>
        </div>
    </script>

    <script type="text/x-template" id="variant-item-template">
        <tr>
            <td>
                <span class="checkbox">
                    <input type="checkbox" :id="variant.id" name="selected_variants[]" :value="variant.id" v-model="selected_variant">
                    <label :for="variant.id" class="checkbox-view"></label>
                </span>
            </td>

            <td data-value="{{ __('admin::app.catalog.products.name') }}">
                @{{ variant.name }}
            </td>

            <td data-value="{{ __('admin::app.catalog.products.qty') }}">
                <button style="width: 100%;" type="button" class="dropdown-btn dropdown-toggle" :disabled="!selected_variant">
                    @{{ totalQty }}
                    <i class="icon arrow-down-icon"></i>
                </button>

                <div class="dropdown-list">
                    <div class="dropdown-container">
                        <ul>
                            <li v-for='(inventorySource, index) in inventorySources'>
                                <div class="control-group" :class="[errors.has(variantInputName + '[inventories][' + inventorySource.id + ']') ? 'has-error' : '']">
                                    <label>@{{ inventorySource.name }}</label>
                                    <input type="text" v-validate="'numeric|min:0'" :name="[variantInputName + '[inventories][' + inventorySource.id + ']']" v-model="inventories[inventorySource.id]" class="control" v-on:keyup="updateTotalQty()" :data-vv-as="'&quot;' + inventorySource.name  + '&quot;'"/>
                                    <span class="control-error" v-if="errors.has(variantInputName + '[inventories][' + inventorySource.id + ']')">@{{ errors.first(variantInputName + '[inventories][' + inventorySource.id + ']') }}</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </td>

            <td v-for='(attribute, index) in superAttributes' :data-value="attribute.admin_name">
                @{{ optionName(variant[attribute.code]) }}
            </td>

            <td data-value="{{ __('admin::app.catalog.products.price') }}">
                <div class="control-group" :class="[errors.has(variantInputName + '[price]') ? 'has-error' : '']">
                    <input type="text" v-validate="'required'" :name="[variantInputName + '[price]']" class="control" :value="price" data-vv-as="&quot;{{ __('admin::app.catalog.products.price') }}&quot;" :disabled="!selected_variant"/>
                    <span class="control-error" v-if="errors.has(variantInputName + '[price]')">@{{ errors.first(variantInputName + '[price]') }}</span>
                </div>
            </td>
        </tr>
    </script>

    <script>
        var super_attributes = @json(app('\Webkul\Product\Repositories\ProductRepository')->getSuperAttributes($product->product));
        var variants = @json($product->product->variants()->with(['inventories'])->get());
        var assignVariants = @json($product->variants);

        Vue.component('variant-list', {

            template: '#variant-list-template',

            inject: ['$validator'],

            data: () => ({
                variants: variants,
                assignVariants: assignVariants,
                superAttributes: super_attributes
            }),

            created () {
                this_this = this;

                this.variants.forEach(function(variant) {
                    this_this.assignVariants.forEach(function(assignVariant) {
                        if (variant.id == assignVariant.product_id) {
                            variant.assignVariant = assignVariant;
                        }
                    });
                });
            },
        });

        Vue.component('variant-item', {

            template: '#variant-item-template',

            props: ['index', 'variant'],

            inject: ['$validator'],

            data: () => ({
                inventorySources: @json($inventorySources),
                inventories: {},
                totalQty: 0,
                superAttributes: super_attributes,
                selected_variant: false
            }),

            created () {
                var this_this = this;
                this.inventorySources.forEach(function(inventorySource) {
                    this_this.inventories[inventorySource.id] = this_this.sourceInventoryQty(inventorySource.id)
                    this_this.totalQty += parseInt(this_this.inventories[inventorySource.id]);
                })

                if (this.variant.assignVariant) {
                    this.selected_variant = this.variant.assignVariant.id;
                }
            },

            computed: {
                variantInputName () {
                    return "variants[" + this.variant.id + "]";
                },

                price () {
                    if (this.variant.assignVariant) {
                        return this.variant.assignVariant.price;
                    }

                    return 0;
                }
            },

            methods: {
                optionName (optionId) {
                    var optionName = '';

                    this.superAttributes.forEach(function(attribute) {
                        attribute.options.forEach(function(option) {
                            if (optionId == option.id) {
                                optionName = option.admin_name;
                            }
                        });
                    })

                    return optionName;
                },

                sourceInventoryQty (inventorySourceId) {
                    var inventories = this.variant.inventories.filter(function(inventory) {
                        return inventorySourceId === inventory.inventory_source_id && inventory.vendor_id == "{{ $product->marketplace_seller_id }}";
                    })

                    if (inventories.length)
                        return inventories[0]['qty'];

                    return 0;
                },

                updateTotalQty () {
                    this.totalQty = 0;
                    for (var key in this.inventories) {
                        this.totalQty += parseInt(this.inventories[key]);
                    }
                }
            }

        });
    </script>
@endpush
@endif