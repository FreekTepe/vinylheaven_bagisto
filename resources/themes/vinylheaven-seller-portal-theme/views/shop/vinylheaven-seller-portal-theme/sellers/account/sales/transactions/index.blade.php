@extends('marketplace::shop.layouts.account')

@section('page_title')
    {{ __('marketplace::app.shop.sellers.account.sales.transactions.title') }}
@endsection

@section('content')


<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-cash icon-gradient bg-tempting-azure"></i>
            </div>
            <div>{{ __('marketplace::app.shop.sellers.account.sales.transactions.title') }}
                <div class="page-title-subheading">An transaction overview.</div>
            </div>
        </div>
      
    </div>
</div>   


{!! view_render_event('marketplace.sellers.account.sales.transactions.list.before') !!}

<div class="row mb-3">
    <div class="col-md-4">
        <div class="card mb-2 widget-chart widget-chart2 text-left">
            <div class="widget-chart-content">
                <div class="widget-chart-flex">
                    <div class="widget-title">
                        {{ __('marketplace::app.shop.sellers.account.sales.transactions.total-sale') }}
                    </div>
                </div>
                <div class="widget-chart-flex">
                    <div class="widget-numbers text-primary">
                        <span>{{ core()->formatBasePrice($statistics['total_sale']) }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="card mb-2 widget-chart widget-chart2 text-left">
            <div class="widget-chart-content">
                <div class="widget-chart-flex">
                    <div class="widget-title">
                        {{ __('marketplace::app.shop.sellers.account.sales.transactions.total-payout') }}
                    </div>
                </div>
                <div class="widget-chart-flex">
                    <div class="widget-numbers text-primary">
                        <span>{{ core()->formatBasePrice($statistics['total_payout']) }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="card mb-2 widget-chart widget-chart2 text-left">
            <div class="widget-chart-content">
                <div class="widget-chart-flex">
                    <div class="widget-title">
                        {{ __('marketplace::app.shop.sellers.account.sales.transactions.remaining-payout') }}
                    </div>
                </div>
                <div class="widget-chart-flex">
                    <div class="widget-numbers text-primary">
                        <span>{{ core()->formatBasePrice($statistics['remaining_payout']) }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- 
<div class="row">
    <div class="col-md-12 mt-3"> --}}
        {{-- <div class="card">
            <div class="card-body"> --}}
                {{-- NTS: insert a transactions datatable --}}
                {{-- OLD Bagisto Datagrid--}}
                {!! app('Webkul\Marketplace\DataGrids\Shop\TransactionDataGrid')->render() !!}
            {{-- </div>
        </div> --}}
    {{-- </div>
</div> --}}


{!! view_render_event('marketplace.sellers.account.sales.transactions.list.after') !!}

{{-- ... --}}

@endsection