@extends('marketplace::shop.layouts.account')

@section('page_title')
    {{ __('marketplace::app.shop.sellers.account.sales.orders.view-title', ['order_id' => $sellerOrder->order_id]) }}
@endsection

@section('content')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="far fa-clipboard icon-gradient bg-tempting-azure"></i>
            </div>
            <div><strong>{{ __('marketplace::app.shop.sellers.account.sales.orders.view-title', ['order_id' => $sellerOrder->order_id]) }}</strong>
                <div class="page-title-subheading">Manage your order</div>
            </div>
        </div>
        <div class="page-title-actions">
        </div>
    </div>
</div>

{!! view_render_event('marketplace.sellers.account.sales.orders.view.before', ['sellerOrder' => $sellerOrder]) !!}


<div class="main-card mb-2 card">
    <div class="card-body">
        <h5 class="card-title">{{ __('marketplace::app.shop.sellers.account.sales.orders.info') }}</h5>

        <div class="row">

            <div class="col-4">
                <ul class="list-group">
                    <li class="list-group-item"><b>Order date:</b> <span class="float-right ml-3">{{ core()->formatDate($sellerOrder->created_at, 'd M Y') }}</span></li>
                    <li class="list-group-item"><b>Order status:</b> <span class="float-right ml-3">{{ $sellerOrder->status_label }}</span></li>
                    <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.payment-method') }}</b><span class="float-right ml-3">{{ core()->getConfigData('sales.paymentmethods.' . $sellerOrder->order->payment->method . '.title') }}</span></li>
                    <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.shipping-method') }}</b><span class="float-right ml-3">{{ $sellerOrder->order->shipping_title }}</span></li>
                    <li class="list-group-item"><span class="float-right ml-3">
                        @if (core()->getConfigData('marketplace.settings.general.can_cancel_order') && $sellerOrder->canCancel())
                            <a  href="{{ route('marketplace.account.orders.cancel', $sellerOrder->order_id) }}" v-alert:message="'{{ __('admin::app.sales.orders.cancel-confirm-msg') }}'" style="color:white;" class="btn-shadow btn btn-info">
                                {{ __('admin::app.sales.orders.cancel-btn-title') }}
                            </a>
                        @endif

                        @if (core()->getConfigData('marketplace.settings.general.can_create_invoice') && $sellerOrder->canInvoice())
                            <a  href="{{ route('marketplace.account.invoices.create', $sellerOrder->order_id) }}" style="color:white;" class="btn-shadow btn btn-info">
                                {{ __('admin::app.sales.orders.invoice-btn-title') }}
                            </a>
                        @endif

                        @if (core()->getConfigData('marketplace.settings.general.can_create_shipment') && $sellerOrder->canShip())
                            <a  href="{{ route('marketplace.account.shipments.create', $sellerOrder->order_id) }}" style="color:white;" class="btn-shadow btn btn-info">
                                {{ __('admin::app.sales.orders.shipment-btn-title') }}
                            </a>
                        @endif
                    </span></li>
                </ul>
            </div>

            <div class="col-4">
                <ul class="list-group">
                    @if ($sellerOrder->shipments->count())
                        <li class="list-group-item"><b>{{ __('marketplace::app.shop.sellers.account.sales.orders.shipments') }}:</b> 
                            <span class="float-right ml-3">
                                @foreach ($sellerOrder->shipments as $sellerShipment)
                                    {{ __('shop::app.customer.account.order.view.individual-shipment', ['shipment_id' => $sellerShipment->shipment_id]) }}
                                    <br>{{ $sellerShipment->shipment->carrier_title }}
                                    <br>{{ $sellerShipment->shipment->track_number }}
                                @endforeach
                            </span>
                        </li>
                    @endif

                    @if ($sellerOrder->refunds->count())
                        <li class="list-group-item"><b>{{ __('marketplace::app.shop.sellers.account.sales.orders.refunds') }}:</b> 
                            <span class="float-right ml-3">
                               Refund send
                            </span>
                        </li>
                    @endif
                </ul>
            </div>

            <div class="col-4">
                <ul class="list-group">
                
                    <li class="list-group-item"><b>{{ __('marketplace::app.shop.sellers.account.sales.orders.customer-name') }}:</b> <span class="float-right ml-3">{{ $sellerOrder->order->customer_full_name }}</span></li>
                    <li class="list-group-item"><b>{{ __('marketplace::app.shop.sellers.account.sales.orders.email') }}:</b> <span class="float-right ml-3">{{ $sellerOrder->order->customer_email }}</span></li>
                    <li class="list-group-item"><b>Shipping address:</b><span class="float-right ml-3">{{ $sellerOrder->order->billing_address->address1 }}
                                                                                                        <br>{{ $sellerOrder->order->billing_address->city }}
                                                                                                        <br>{{ $sellerOrder->order->billing_address->postcode }}
                                                                                                        <br>{{ $sellerOrder->order->billing_address->state }}
                                                                                                        <br>{{ core()->country_name($sellerOrder->order->billing_address->country) }} 
                                                                                                        <br>{{ $sellerOrder->order->billing_address->phone }}</span></li>                   
                </ul>
            </div>

        </div>

    </div>
</div>

<div class="main-card mb-2 card">
    <div class="card-body">
        <h5 class="card-title">Itmes</h5>
        <div class="table table-responsive">
            <table class="table mt-3">
            <thead>
                <tr>
                    <th>Item ID</th>
                    <th>Product Name</th>
                    <th>{{ __('shop::app.customer.account.order.view.item-status') }}</th>
                    <th></th>
                    <th>{{ __('shop::app.customer.account.order.view.price') }}</th>
                    <th>Commission</th>
                </tr>
            </thead>
            <tbody>

            @foreach ($sellerOrder->items as $sellerOrderItem)
                <tr>
                    <td data-value="{{ __('shop::app.customer.account.order.view.id') }}">
                        {{ $sellerOrderItem->item->type == 'configurable' ? $sellerOrderItem->item->child->id : $sellerOrderItem->item->id }}
                    </td>
                    <td data-value="{{ __('shop::app.customer.account.order.view.product-name') }}">
                        {{ $sellerOrderItem->item->name }}
                        <br>We need also: Artist / item condtion / seller notes

                        @if (isset($sellerOrderItem->additional['attributes']))
                            <div class="item-options">

                                @foreach ($sellerOrderItem->additional['attributes'] as $attribute)
                                    <b>{{ $attribute['attribute_name'] }} : </b>{{ $attribute['option_label'] }}</br>
                                @endforeach

                            </div>
                        @endif
                    </td>
                    
                    <td data-value="{{ __('shop::app.customer.account.order.view.item-status') }}">
                        <span class="qty-row">
                            {{ __('shop::app.customer.account.order.view.item-ordered', ['qty_ordered' => $sellerOrderItem->item->qty_ordered]) }}
                        </span>

                        <span class="qty-row">
                            {{ $sellerOrderItem->item->qty_invoiced ? __('shop::app.customer.account.order.view.item-invoice', ['qty_invoiced' => $sellerOrderItem->item->qty_invoiced]) : '' }}
                        </span>

                        <span class="qty-row">
                            {{ $sellerOrderItem->item->qty_refunded ? __('admin::app.sales.orders.item-refunded', ['qty_refunded' => $sellerOrderItem->item->qty_refunded]) : '' }}
                        </span>

                        <span class="qty-row">
                            {{ $sellerOrderItem->item->qty_shipped ? __('shop::app.customer.account.order.view.item-shipped', ['qty_shipped' => $sellerOrderItem->item->qty_shipped]) : '' }}
                        </span>

                        <span class="qty-row">
                            {{ $sellerOrderItem->item->qty_canceled ? __('shop::app.customer.account.order.view.item-canceled', ['qty_canceled' => $sellerOrderItem->item->qty_canceled]) : '' }}
                        </span>
                    </td>
                    <td></td>
                    <td data-value="{{ __('shop::app.customer.account.order.view.price') }}">{{ core()->formatPrice($sellerOrderItem->item->price, $sellerOrder->order->order_currency_code) }}</td>
                    <td data-value="{{ __('marketplace::app.shop.sellers.account.sales.orders.admin-commission') }}">{{ core()->formatPrice($sellerOrderItem->commission, $sellerOrder->order->order_currency_code) }}</td>
                </tr>
            @endforeach
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>{{ __('shop::app.customer.account.order.view.subtotal') }}</td>
                    <td>{{ core()->formatPrice($sellerOrder->sub_total, $sellerOrder->order->order_currency_code) }}</td>
                    <td>{{ core()->formatPrice($sellerOrder->commission, $sellerOrder->order->order_currency_code) }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>{{ __('shop::app.customer.account.order.view.shipping-handling') }}</td>
                    <td>{{ core()->formatPrice($sellerOrder->shipping_amount, $sellerOrder->order->order_currency_code) }}</td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>{{ __('shop::app.customer.account.order.view.grand-total') }}</td>
                    <td>{{ core()->formatPrice($sellerOrder->grand_total, $sellerOrder->order->order_currency_code) }}</td>
                    <td>{{ core()->formatPrice($sellerOrder->commission, $sellerOrder->order->order_currency_code) }}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>




    @if ($sellerOrder->invoices->count())
    {{-- Move invoice to a modal & add two button: view invoce & view pdf in the --}}
        <div class="main-card mb-2 card" >
            <div class="card-body">
                <h5 class="card-title">{{ __('marketplace::app.shop.sellers.account.sales.orders.invoices') }}</h5>

                @foreach ($sellerOrder->invoices as $sellerInvoice)


                        <div class="row p-2">
                            <div class="col-10">
                                <h6 >{{ __('shop::app.customer.account.order.view.individual-invoice', ['invoice_id' => $sellerInvoice->invoice_id]) }}</h6>
                            </div>
                            <div class="col-2">
                                <a href="{{ route('marketplace.account.invoices.print', $sellerInvoice->marketplace_order_id ) }}" class="pull-right btn-icon btn-icon-only btn btn-info btn-sm btn-shadow" style="color:white;">
                                    <i class="lnr lnr-printer btn-icon-wrapper"></i>
                                </a>
                            </div>
                        </div>

                        <div class="table table-responsive">
                            <table class="table mt-3">
                                <thead>
                                <tr>
                                    <th>{{ __('shop::app.customer.account.order.view.product-name') }}</th>
                                    <th>{{ __('shop::app.customer.account.order.view.price') }}</th>
                                    <th>{{ __('shop::app.customer.account.order.view.qty') }}</th>
                                    <th>{{ __('shop::app.customer.account.order.view.subtotal') }}</th>
                                    <th>{{ __('shop::app.customer.account.order.view.tax-amount') }}</th>
                                    <th>{{ __('shop::app.customer.account.order.view.discount') }}</th>
                                    <th>{{ __('shop::app.customer.account.order.view.grand-total') }}</th>
                                </tr>
                                </thead>

                                <tbody>

                                @foreach ($sellerInvoice->items as $sellerInvoiceItem)
                                    <?php $baseInvoiceItem = $sellerInvoiceItem->item; ?>
                                    <tr>
                                        <td data-value="{{ __('shop::app.customer.account.order.view.product-name') }}">{{ $baseInvoiceItem->name }}</td>

                                        <td data-value="{{ __('shop::app.customer.account.order.view.price') }}">
                                            {{ core()->formatPrice($baseInvoiceItem->price, $sellerOrder->order->order_currency_code) }}
                                        </td>

                                        <td data-value="{{ __('shop::app.customer.account.order.view.qty') }}">{{ $baseInvoiceItem->qty }}</td>

                                        <td data-value="{{ __('shop::app.customer.account.order.view.subtotal') }}">
                                            {{ core()->formatPrice($baseInvoiceItem->total, $sellerOrder->order->order_currency_code) }}
                                        </td>

                                        <td data-value="{{ __('shop::app.customer.account.order.view.tax-amount') }}">
                                            {{ core()->formatPrice($baseInvoiceItem->tax_amount, $sellerOrder->order->order_currency_code) }}
                                        </td>

                                        <td data-value="{{ __('shop::app.customer.account.order.view.discount') }}">
                                            {{ core()->formatPrice($baseInvoiceItem->discount_amount, $sellerOrder->order->order_currency_code) }}
                                        </td>

                                        <td data-value="{{ __('shop::app.customer.account.order.view.grand-total') }}">
                                            {{ core()->formatPrice($baseInvoiceItem->total + $baseInvoiceItem->tax_amount - $sellerOrder->discount_amount, $sellerOrder->order->order_currency_code) }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="row">
                            <div class="ml-auto mr-4">
                                <ul class="list-group">
                                    <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.subtotal') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($sellerInvoice->sub_total, $sellerOrder->order->order_currency_code) }}</span></li>
                                    <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.shipping-handling') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($sellerInvoice->shipping_amount, $sellerOrder->order->order_currency_code) }}</span></li>
                                    <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.tax') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($sellerInvoice->tax_amount, $sellerOrder->order->order_currency_code) }}</span></li>
                                    <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.discount') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($sellerInvoice->discount_amount, $sellerOrder->order->order_currency_code) }}</span></li>
                                    <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.grand-total') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($sellerOrder->grand_total, $sellerOrder->order->order_currency_code) }}</span></li>
                                </ul>
                            </div>
                        </div>

                @endforeach
            </div>
        </div>
    </div>
    @endif

{!! view_render_event('marketplace.sellers.account.sales.orders.view.after', ['sellerOrder' => $sellerOrder]) !!}


@endsection