@extends('marketplace::shop.layouts.account')

@section('page_title')
    {{ __('marketplace::app.shop.sellers.account.catalog.products.title') }}
@endsection

@section('modal')
<!-- Destroy all products modal -->
<div class="modal fade" id="deleteAllProductsModal" tabindex="-1" role="dialog" aria-labelledby="deleteAllProductsModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Be carefull!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                You are about to delete all products from your store.
                <br>
                Are you sure you want to procceed?
                <hr>
                <input type="checkbox" id="deletecheck" onchange="document.getElementById('deleteButton').disabled = !this.checked;" >
                <label for="deletecheck">Yes i want to delete all products</label>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <form method="POST" action="{{ route('mpproducts.delete.all' ) }}">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button id="deleteButton" type="submit" disabled class="btn btn-danger">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')



        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-icon">
                    <i class="fas fa-box-open icon-gradient bg-tempting-azure"></i>
                </div>

                <div class="page-title-heading">
                    <div><h1>Products</h1>
                        <div class="page-title-subheading">Here you can manage your product catalog.<br> You have XXXX products in your inventory<br>We need to have multiple tabs here: For sale / Sold / Draft</div>
                    </div>
                </div>

                <div class="d-inline-flex justify-content-start align-items-center ml-auto">
                    <a href="{{ route('marketplace.account.products.search') }}" style="color:white;" class="btn-shadow btn btn-info mr-1">
                        Add new products <i class="fas fa-plus btn-icon-wrapper"></i>
                    </a>

                    <a target="_bank" href="{{ route('mpproducts.download') }}" style="color:white;" class="btn-shadow btn btn-warning mx-1">
                        Download catalog <i class="fas fa-file-download btn-icon-wrapper"></i>
                    </a>

                    <button data-toggle="modal" data-target="#deleteAllProductsModal" class="btn-icon btn-icon-only btn btn-danger btn-shadow ml-1" style="color: white;" value="Delete user"> Delete catalog <i class="far fa-trash-alt btn-icon-wrapper"></i>
                </div>
            </div>
        </div>


        {!! view_render_event('marketplace.sellers.account.catalog.products.list.before') !!}

        {{-- <div class="account-items-list"> --}}

            {{-- ProductDataGrid renders table --}}
            {!! app('VinylHeaven\Product\DataGrids\ProductDataGrid')->render() !!}

        {{-- </div> --}}

        {!! view_render_event('marketplace.sellers.account.catalog.products.list.after') !!}





@endsection