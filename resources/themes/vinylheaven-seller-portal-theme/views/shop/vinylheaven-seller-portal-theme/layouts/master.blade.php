@extends('shop::layouts.master')

@push('css')
    <link rel="stylesheet" href="{{ bagisto_asset('css/marketplace.css') }}">
@endpush

@push('scripts')
    <!--Toggle Switch -->
    
    <script type="text/javascript" src="{{ bagisto_asset('js/marketplace.js') }}"></script>
    <script src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/js/architect-js/vendors/form-components/toggle-switch.js') }}"></script>
@endpush