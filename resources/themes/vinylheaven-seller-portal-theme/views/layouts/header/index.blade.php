<div class="app-header">
    {{-- Left logo --}}
    <div class="app-header__logo">
        <div class="header__pane  ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>

    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>

    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>   


    {{-- Dropdowns --}}
    <div class="app-header__content">
        <div class="app-header-left">



            {{-- <ul class="search-container">
                <li class="search-group">
                    <form role="search" action="{{ route('shop.search.index') }}" method="GET" style="display: inherit;">
                        <input type="search" name="term" class="search-field" placeholder="{{ __('shop::app.header.search-text') }}" required>

                        <div class="search-icon-wrapper">
                            <button class="" class="background: none;">
                                <i class="icon icon-search"></i>
                            </button>
                        </div>
                    </form>
                </li>
            </ul> --}}

            {{-- Searchbox (for searching products) --}}
            {{-- <div class="search-wrapper">
                <div class="input-holder">
                    <form role="search" action="{{ route('shop.search.index') }}" method="GET" style="display: inherit;">
                        <input type="text" class="search-input" placeholder="{{ __('shop::app.header.search-text') }}" required>
                        <button class="search-icon"><span></span></button>
                    </form>
                </div>
                <button class="close"></button>
            </div> --}}

                {{-- Currency selector --}}



        {{-- <li class="currency-switcher">
            <span class="dropdown-toggle">
                {{ core()->getCurrentCurrencyCode() }}

                <i class="icon arrow-down-icon"></i>
            </span>

            <ul class="dropdown-list currency">
                @foreach (core()->getCurrentChannel()->currencies as $currency)
                    <li>
                        @if (isset($serachQuery))
                            <a href="?{{ $serachQuery }}&currency={{ $currency->code }}">{{ $currency->code }}</a>
                        @else
                            <a href="?currency={{ $currency->code }}">{{ $currency->code }}</a>
                        @endif
                    </li>
                @endforeach
            </ul>
        </li> --}}



            <a href="{{ route('shop.home.index') }}" class="header-logo">
                <img class="logo" src="{{ asset('themes/vinylheaven-theme/assets/img/logo-vinyl-heaven-def.png') }}" />
            </a>
        </div>

        <div class="app-header-right">        
            <div class="header-btn-lg pr-0">
                {{-- Currency selector --}}

                {!! view_render_event('bagisto.shop.layout.header.currency-item.before') !!}

                @if (core()->getCurrentChannel()->currencies->count() /* > 1 */) {{-- NTS: uncomment "> 1" on live --}}

                <ul class="header-megamenu nav">
                    
                    <li class="nav-item">
                        <a href="javascript:void(0);" data-placement="bottom" rel="popover-focus" data-offset="300" data-toggle="popover-custom" class="nav-link">
                            <i class="nav-link-icon fas fa-gift"> </i>
                            {{ core()->getCurrentCurrencyCode() }}
                            <i class="fa fa-angle-down ml-2 opacity-5"></i>
                        </a>
                        <div class="rm-max-width">
                            <div class="d-none popover-custom-content">
                                <div class="dropdown-mega-menu">
                                    <div class="no-gutters row">
                                        <div class="col-sm-6 col-xl-4">
                                            <ul class="nav flex-column">
                                                <li class="nav-item-header nav-item">
                                                    Currencies
                                                </li>
                                                @foreach (core()->getCurrentChannel()->currencies as $currency)

                                                    <li class="nav-item">
                                                        @if (isset($serachQuery))
                                                            <a href="?{{ $serachQuery }}&currency={{ $currency->code }}" class="nav-link">
                                                                <i class="nav-link-icon fas fa-inbox"></i>
                                                                <span>{{ $currency->code }}</span>
                                                            </a>
                                                        @else
                                                            <a href="?currency={{ $currency->code }}" class="nav-link">
                                                                <i class="nav-link-icon fas fa-inbox"></i>
                                                                <span>{{ $currency->code }}</span>
                                                            </a>
                                                        @endif
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>  

                @endif

                {!! view_render_event('bagisto.shop.layout.header.currency-item.after') !!}
            </div>

            <div class="header-btn-lg pr-0">

                {!! view_render_event('bagisto.shop.layout.header.account-item.before') !!}

                @php
                    $seller = VinylHeaven\Seller\Models\Seller::where('customer_id', auth()->guard('customer')->user()->id)->first();
                @endphp

                <div class="widget-content p-0">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left">
                            <div class="btn-group">
                                <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                                    <img width="42" class="rounded-circle" src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/images/avatars/1.jpg') }}" alt="">
                                    <i class="fa fa-angle-down ml-2 opacity-8"></i>
                                </a>
                                <div tabindex="-1" role="menu" aria-hidden="true" class="rm-pointers dropdown-menu-lg dropdown-menu dropdown-menu-right">
                                    <div class="dropdown-menu-header">
                                        <div class="dropdown-menu-header-inner bg-info">
                                            <div class="menu-header-image opacity-2" style="background-image: url('{{ asset('themes/vinylheaven-seller-portal-theme/assets/images/dropdown-header/city3.jpg') }};"></div>
                                            <div class="menu-header-content text-left">
                                                <div class="widget-content p-0">
                                                    <div class="widget-content-wrapper">
                                                        <div class="widget-content-left mr-3">
                                                            <img width="42" class="rounded-circle"
                                                                    src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/images/avatars/1.jpg') }}"
                                                                    alt="">
                                                        </div>
                                                        <div class="widget-content-left">
                                                            <div class="widget-heading">{{ auth()->guard('customer')->user()->first_name }}
                                                            </div>
                                                            @if ($seller)
                                                                <div class="widget-subheading opacity-8">Vinyl heaven seller</div>
                                                            @else
                                                                <div class="widget-subheading opacity-8">Customer</div>
                                                            @endif
                                                        </div>
                                                        <div class="widget-content-right mr-2">
                                                            {{-- <a href="{{ route('customer.session.destroy') }}">{{ __('shop::app.header.logout') }}</a> --}}
                                                            <a class="btn-pill btn-shadow btn-shine btn btn-focus" href="{{ route('customer.session.destroy') }}">
                                                                {{ __('shop::app.header.logout') }}
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="scroll-area-xs" style="height: 300px;">
                                        <div class="scrollbar-container ps">
                                            <ul class="nav flex-column">
                                                <li class="nav-item-header nav-item">My Account
                                                </li>

                                                <li class="nav-item">
                                                    <a href="{{ route('customer.profile.index') }}" class="nav-link">{{ __('shop::app.header.profile') }}</a>
                                                </li>

                                                <li class="nav-item">
                                                    <a href="{{ route('customer.wishlist.index') }}" class="nav-link">{{ __('shop::app.header.wishlist') }}</a>
                                                </li>
            
                                                <li class="nav-item">
                                                    <a href="{{ route('shop.checkout.cart.index') }}" class="nav-link">{{ __('shop::app.header.cart') }}</a>
                                                </li>

                                                @if ($seller)
                                                    <li class="nav-item-header nav-item">Market Place
                                                    </li>
                                                    <li class="nav-item">
                                                        <a href="javascript:void(0);" class="nav-link">Orders
                                                            <div class="ml-auto badge badge-success">5
                                                            </div>
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a href="javascript:void(0);" class="nav-link">Products
                                                            <div class="ml-auto badge badge-warning">512
                                                            </div>
                                                        </a>
                                                    </li>
                                                @else
                                                    <li class="nav-item">
                                                        <a href="{{ route('marketplace.account.seller.create') }}" class="nav-link">Become a seller</a>
                                                    </li>
                                                @endif

                                            </ul>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                        </div>

                        <div class="widget-content-left  ml-3 header-user-info">
                            <div class="widget-heading">
                                {{ auth()->guard('customer')->user()->first_name }}
                            </div>
                            <div class="widget-subheading">
                            </div>
                        </div>
                    </div>
                </div>

                {!! view_render_event('bagisto.shop.layout.header.account-item.after') !!}

            </div> 
        </div>
    </div>
</div>    
