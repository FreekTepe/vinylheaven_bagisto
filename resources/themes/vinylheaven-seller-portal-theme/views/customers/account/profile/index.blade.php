@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.customer.account.profile.index.title') }}
@endsection

@section('content-wrapper')


<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="far fa-id-card icon-gradient bg-tempting-azure"></i>
            </div>
            <div>{{ __('shop::app.customer.account.profile.index.title') }}
                <div class="page-title-subheading">Your profile.</div>
            </div>
        </div>
    </div>
</div>   

<div class="col-md-8 mx-auto ">
    <div class="card-shadow-primary card-border mb-3 card mt-5">
        <div class="dropdown-menu-header">
            <div class="dropdown-menu-header-inner bg-info text-white">
                <div class="menu-header-content">
                     <div>
                        <h5 class="menu-header-title">{{ auth()->guard('customer')->user()->first_name }} {{ auth()->guard('customer')->user()->last_name }}</h5>
                        <h6 class="menu-header-subtitle"></h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">

            {!! view_render_event('bagisto.shop.customers.account.profile.view.before', ['customer' => $customer]) !!}

            <ul class="list-group list-group-flush">

                {{-- First Name --}}
                <li class="list-group-item">
                    <div class="widget-content p-0">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="widget-heading">{{ __('shop::app.customer.account.profile.fname') }}</div>
                                <div class="widget-subheading">{{ isset($customer->first_name) ? $customer->first_name : '...' }}</div>
                            </div>
                        </div>
                    </div>
                </li>

                {!! view_render_event( 'bagisto.shop.customers.account.profile.view.table.before', ['customer' => $customer]) !!}


                {{-- Last Name --}}
                <li class="list-group-item">
                    <div class="widget-content p-0">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="widget-heading">{{ __('shop::app.customer.account.profile.lname') }}</div>
                                <div class="widget-subheading">{{ isset($customer->last_name) ? $customer->last_name : '...' }}</div>
                            </div>
                        </div>
                    </div>
                </li>

                {!! view_render_event('bagisto.shop.customers.account.profile.view.table.last_name.after') !!}


                {{-- Gender --}}
                {{-- BB: removed this 
                <li class="list-group-item">
                    <div class="widget-content p-0">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="widget-heading">{{ __('shop::app.customer.account.profile.gender') }}</div>
                                <div class="widget-subheading">{{ isset($customer->gender) ? $customer->gender : '...' }}</div>
                            </div>
                        </div>
                    </div>
                </li>
                {!! view_render_event('bagisto.shop.customers.account.profile.view.table.gender.after') !!}
                --}}


                {{-- Date of birth --}}
                {{-- BB: removed this 
                <li class="list-group-item">
                    <div class="widget-content p-0">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="widget-heading">{{ __('shop::app.customer.account.profile.dob') }}</div>
                                <div class="widget-subheading">{{ isset($customer->date_of_birth) ? $customer->date_of_birth : '...' }}</div>
                            </div>
                        </div>
                    </div>
                </li>
                {!! view_render_event('bagisto.shop.customers.account.profile.view.table.date_of_birth.after') !!}
                --}}

                {{-- Email --}}
                <li class="list-group-item">
                    <div class="widget-content p-0">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="widget-heading">{{ __('shop::app.customer.account.profile.email') }}</div>
                                <div class="widget-subheading">{{ isset($customer->email) ? $customer->email : '...' }}</div>
                            </div>
                        </div>
                    </div>
                </li>
                {!! view_render_event('bagisto.shop.customers.account.profile.view.table.after', ['customer' => $customer]) !!}


            </ul>

            {!! view_render_event('bagisto.shop.customers.account.profile.view.table.after', ['customer' => $customer]) !!}
        </div>
        <div class="d-block text-right card-footer">
            <a href="{{ route('customer.profile.edit') }}" class="btn-icon btn-icon-only btn btn-info btn-sm btn-shadow"><i class="far fa-edit btn-icon-wrapper"></i></a>
            <button class="btn-icon btn-icon-only btn btn-danger btn-sm btn-shadow" data-toggle="modal" data-target="#deleteAccountModal"><i class="far fa-trash-alt btn-icon-wrapper"></i></button>
        </div>
    </div>
</div>

{!! view_render_event('bagisto.shop.customers.account.profile.view.after', ['customer' => $customer]) !!}
@endsection

@section('modal')
<!-- Delete account modal -->
<div class="modal fade" id="deleteAccountModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="POST" action="{{ route('customer.profile.destroy') }}" @submit.prevent="onSubmit">
            @csrf

        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ __('shop::app.customer.account.address.index.enter-password') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p style="color:#656B70;">
                    Are you sure you want to delete your account?
                    You can proceed by filling in your password and pressing the delete button.
                </p>
                <div class="position-relative row form-group" :class="[errors.has('password') ? 'has-error' : '']">
                    <label for="url" class="col-sm-2 col-form-label required">{{ __('admin::app.users.users.password') }}</label>
                    <div class="col-sm-10">
                        <input type="password" name="password" id="password" class="form-control" :class="errors.has('password') ? 'is-invalid'  : ''" v-validate="'required|min:6|max:18'" data-vv-as="&quot;{{ __('admin::app.users.users.password') }}&quot;">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger">{{ __('shop::app.customer.account.address.index.delete') }}</button>
            </div>
        </div>

        </form>
    </div>
</div>
@endsection
