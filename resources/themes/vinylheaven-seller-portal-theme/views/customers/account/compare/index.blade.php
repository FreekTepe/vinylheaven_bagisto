@extends('shop::layouts.master')

@include('shop::guest.compare.compare-products')

@section('page_title')
    {{ __('velocity::app.customer.compare.compare_similar_items') }}
@endsection

@section('content-wrapper')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="far fa-eye icon-gradient bg-tempting-azure"></i>
            </div>
            <div>Compare products
                <div class="page-title-subheading">Compare similar products.</div>
            </div>
        </div>
    </div>
</div>   

    <div class="account-content">

        <div class="account-layout">
            {!! view_render_event('bagisto.shop.customers.account.comparison.list.before') !!}

            <div class="account-items-list">
                <div class="account-table-content">
                    <compare-product></compare-product>
                </div>
            </div>

            {!! view_render_event('bagisto.shop.customers.account.comparison.list.after') !!}

        </div>

    </div>

@endsection
