@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.customer.account.address.index.page-title') }}
@endsection

@section('content-wrapper')


<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="fas fa-map-marked-alt icon-gradient bg-tempting-azure"></i>
            </div>
            <div>{{ __('shop::app.customer.account.address.index.title') }}
                <div class="page-title-subheading">Manage your delivery address.</div>
            </div>
        </div>
        <div class="page-title-actions">
            <div class="d-inline-block">
                <a href="{{ route('customer.address.create') }}" class="btn-shadow btn btn-info" style="color: white;">
                    {{ __('shop::app.customer.account.address.index.add') }}
                </a>
            </div>
        </div>
    </div>
</div>  


{!! view_render_event('bagisto.shop.customers.account.address.list.before', ['addresses' => $addresses]) !!}

@if ($addresses->isEmpty())
        <div>{{ __('shop::app.customer.account.address.index.empty') }}</div>
        <br/>
        <a href="{{ route('customer.address.create') }}">{{ __('shop::app.customer.account.address.index.add') }}</a>
    @else
        <div class="row">
            @foreach ($addresses as $address)


                <div class="col-md-6 mx-auto ">
                    <div class="card-shadow-primary card-border mb-3 card mt-5">
                        <div class="dropdown-menu-header">
                            <div class="dropdown-menu-header-inner bg-info text-white">
                                <div class="menu-header-content">
                                    <div class="avatar-icon-wrapper mb-3 avatar-icon-xl">
                                        <div class="avatar-icon"><img  src="{{ asset('themes/vinylheaven-seller-portal-theme/assets/images/avatars/1.jpg') }}" alt="Avatar 5"></div>
                                    </div>
                                    <div>
                                        <h5 class="menu-header-title">{{ auth()->guard('customer')->user()->name }}</h5>
                                        <h6 class="menu-header-subtitle"></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">

                            <ul class="list-group list-group-flush">

                                {{-- First Name --}}
                                <li class="list-group-item">
                                    <div class="widget-content p-0">
                                        <div class="widget-content-wrapper">
                                            <div class="widget-content-left">
                                                <div class="widget-heading">Company Name</div>
                                                <div class="widget-subheading">{{ $address->company_name }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                                {{-- First Name --}}
                                <li class="list-group-item">
                                    <div class="widget-content p-0">
                                        <div class="widget-content-wrapper">
                                            <div class="widget-content-left">
                                                <div class="widget-heading">First name</div>
                                                <div class="widget-subheading">{{ $address->first_name }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                                {{-- Last name --}}
                                <li class="list-group-item">
                                    <div class="widget-content p-0">
                                        <div class="widget-content-wrapper">
                                            <div class="widget-content-left">
                                                <div class="widget-heading">Last name</div>
                                                <div class="widget-subheading">{{ $address->last_name }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                                {{-- Address --}}
                                <li class="list-group-item">
                                    <div class="widget-content p-0">
                                        <div class="widget-content-wrapper">
                                            <div class="widget-content-left">
                                                <div class="widget-heading">Address</div>
                                                <div class="widget-subheading">{{ $address->address1 }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                                {{-- City --}}
                                <li class="list-group-item">
                                    <div class="widget-content p-0">
                                        <div class="widget-content-wrapper">
                                            <div class="widget-content-left">
                                                <div class="widget-heading">City</div>
                                                <div class="widget-subheading">{{ $address->city }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                                {{-- State --}}
                                <li class="list-group-item">
                                    <div class="widget-content p-0">
                                        <div class="widget-content-wrapper">
                                            <div class="widget-content-left">
                                                <div class="widget-heading">State</div>
                                                <div class="widget-subheading">{{ $address->state }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                                {{-- State --}}
                                <li class="list-group-item">
                                    <div class="widget-content p-0">
                                        <div class="widget-content-wrapper">
                                            <div class="widget-content-left">
                                                <div class="widget-heading">Country and Zip</div>
                                                <div class="widget-subheading">{{ core()->country_name($address->country) }} {{ $address->postcode }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                                {{-- Phone --}}
                                <li class="list-group-item">
                                    <div class="widget-content p-0">
                                        <div class="widget-content-wrapper">
                                            <div class="widget-content-left">
                                                <div class="widget-heading">{{ __('shop::app.customer.account.address.index.contact') }}</div>
                                                <div class="widget-subheading">{{ $address->phone }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                                
                                           
                                
                            </ul>
                        </div>
                        <div class="d-block text-right card-footer">
                            <a href="{{ route('customer.address.edit', $address->id) }}" class="btn-icon btn-icon-only btn btn-info btn-sm btn-shadow" style="color:white;">
                                <i class="far fa-edit btn-icon-wrapper"></i>
                            </a>
                            <a 
                                href="{{ route('address.delete', $address->id) }}" 
                                onclick="deleteAddress('{{ __('shop::app.customer.account.address.index.confirm-delete') }}')"
                                class="btn-icon btn-icon-only btn btn-danger btn-sm btn-shadow" style="color:white;">
                                <i class="far fa-trash-alt btn-icon-wrapper"></i>
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
            {{-- {{ $items->links('vendor.pagination.architect')  }} --}}
        @endif

        {!! view_render_event('bagisto.shop.customers.account.address.list.after', ['addresses' => $addresses]) !!}

@endsection

@push('scripts')
    <script>
        function deleteAddress(message) {
            if (!confirm(message))
                event.preventDefault();
        }
    </script>
@endpush
