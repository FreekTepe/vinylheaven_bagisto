@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.customer.account.address.create.page-title') }}
@endsection

@section('content-wrapper')


{!! view_render_event('bagisto.shop.customers.account.address.create.before') !!}

<form id="customer-address-form" method="post" action="{{ route('customer.address.create') }}" @submit.prevent="onSubmit">
@csrf

{!! view_render_event('bagisto.shop.customers.account.address.create_form_controls.before') !!}

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="fas fa-plus icon-gradient bg-tempting-azure"></i>
            </div>
            <div>{{ __('shop::app.customer.account.address.create.title') }}
                <div class="page-title-subheading">Create a delivery address.</div>
            </div>
        </div>
        <div class="page-title-actions">
            <div class="d-inline-block">
                <input type="submit" class="btn-shadow btn btn-info" style="color:white;" value="{{ __('shop::app.customer.account.address.create.submit') }}">
            </div>
        </div>    
    </div>
</div>  


<div class="col-md-12">
     <div class="main-card mb-2 card">
        {!! view_render_event('bagisto.shop.customers.account.address.create_form_controls.before') !!}

        <div class="card-body">
            <h5 class="card-title">{{ __('shop::app.customer.account.profile.edit-profile.title') }}</h5>

            {{-- company_name --}}
            <div class="position-relative row form-group" :class="[errors.has('company_name') ? 'has-error' : '']">
                <label for="company_name" class="col-sm-2 col-form-label required">{{ __('shop::app.customer.account.address.create.company_name') }}</label>
                <div class="col-sm-10">
                    <input 
                        value="{{ old('company_name') }}" 
                        type="text" 
                        class="form-control" :class="errors.has('company_name') ? 'is-invalid'  : ''" 
                        name="company_name" 
                        v-validate="'required'" 
                        data-vv-as="&quot;{{ __('shop::app.customer.account.address.create.company_name') }}&quot;" 
                    >
                </div>
            </div>
            {!! view_render_event('bagisto.shop.customers.account.address.create_form_controls.company_name.after') !!}

            {{-- first_name --}}
            <div class="position-relative row form-group" :class="[errors.has('first_name') ? 'has-error' : '']">
                <label for="first_name" class="col-sm-2 col-form-label required">{{ __('shop::app.customer.account.address.create.first_name') }}</label>
                <div class="col-sm-10">
                    <input 
                        value="{{ old('first_name') }}" 
                        type="text" 
                        class="form-control" :class="errors.has('first_name') ? 'is-invalid'  : ''" 
                        name="first_name" 
                        v-validate="'required'" 
                        data-vv-as="&quot;{{ __('shop::app.customer.account.address.create.first_name') }}&quot;" 
                    >
                </div>
            </div>
            {!! view_render_event('bagisto.shop.customers.account.address.create_form_controls.first_name.after') !!}

            {{-- last_name --}}
            <div class="position-relative row form-group" :class="[errors.has('last_name') ? 'has-error' : '']">
                <label for="last_name" class="col-sm-2 col-form-label required">{{ __('shop::app.customer.account.address.create.last_name') }}</label>
                <div class="col-sm-10">
                    <input 
                        value="{{ old('last_name') }}" 
                        type="text" 
                        class="form-control" :class="errors.has('last_name') ? 'is-invalid'  : ''" 
                        name="last_name" 
                        v-validate="'required'" 
                        data-vv-as="&quot;{{ __('shop::app.customer.account.address.create.last_name') }}&quot;" 
                    >
                </div>
            </div>
            {!! view_render_event('bagisto.shop.customers.account.address.create_form_controls.last_name.after') !!}

            {{-- vat_id --}}
            <div class="position-relative row form-group" :class="[errors.has('vat_id') ? 'has-error' : '']">
                <label for="vat_id" class="col-sm-2 col-form-label required">{{ __('shop::app.customer.account.address.create.vat_id') }}</label>
                <div class="col-sm-10">
                    <input 
                        value="{{ old('vat_id') }}" 
                        type="text" 
                        class="form-control" :class="errors.has('vat_id') ? 'is-invalid'  : ''" 
                        name="vat_id" 
                        v-validate="'required'" 
                        data-vv-as="&quot;{{ __('shop::app.customer.account.address.create.vat_id') }}&quot;" 
                    >
                    <small class="form-text text-muted">{{ __('shop::app.customer.account.address.create.vat_help_note') }}</small>
                </div>
            </div>
            {!! view_render_event('bagisto.shop.customers.account.address.create_form_controls.vat_id.after') !!}


            {{-- address1 --}}
            <div class="position-relative row form-group" :class="[errors.has('address1[]') ? 'has-error' : '']">
                <label for="address1" class="col-sm-2 col-form-label required">{{ __('shop::app.customer.account.address.create.street-address') }}</label>
                <div class="col-sm-10">
                    <input 
                        value="{{ old('address1') }}"
                        type="text" 
                        class="form-control" :class="errors.has('last_name') ? 'is-invalid'  : ''" 
                        name="address1[]" 
                        v-validate="'required'" 
                        data-vv-as="&quot;{{ __('shop::app.customer.account.address.create.street-address') }}&quot;" 
                    >
                </div>
            </div>

            @if (core()->getConfigData('customer.settings.address.street_lines') && core()->getConfigData('customer.settings.address.street_lines') > 1)
                <div class="position-relative row form-group" :class="[errors.has('address1[]') ? 'has-error' : '']">
                    <label for="address1" class="col-sm-2 col-form-label required">{{ __('shop::app.customer.account.address.create.street-address') }}</label>
                    <div class="col-sm-10">
                        @for ($i = 1; $i < core()->getConfigData('customer.settings.address.street_lines'); $i++)
                            <input 
                                type="text" 
                                class="form-control" :class="errors.has('vat_id') ? 'is-invalid'  : ''" 
                                name="address1[{{ $i }}]" 
                                id="address_{{ $i }}"
                            >
                        @endfor
                    </div>
                </div>
            @endif

            {!! view_render_event('bagisto.shop.customers.account.address.create_form_controls.street-address.after') !!}

            @include ('shop::customers.account.address.country-state', ['countryCode' => old('country'), 'stateCode' => old('state')])

            {!! view_render_event('bagisto.shop.customers.account.address.create_form_controls.country-state.after') !!}

            {{-- city --}}
            <div class="position-relative row form-group" :class="[errors.has('city') ? 'has-error' : '']">
                <label for="city" class="col-sm-2 col-form-label required">{{ __('shop::app.customer.account.address.create.city') }}</label>
                <div class="col-sm-10">
                    <input 
                        value="{{ old('city') }}" 
                        type="text" 
                        class="form-control" :class="errors.has('city') ? 'is-invalid'  : ''" 
                        name="city" 
                        v-validate="'required'" 
                        data-vv-as="&quot;{{ __('shop::app.customer.account.address.create.city') }}&quot;" 
                    >
                </div>
            </div>

            {!! view_render_event('bagisto.shop.customers.account.address.create_form_controls.city.after') !!}


             {{-- postcode --}}
             <div class="position-relative row form-group" :class="[errors.has('postcode') ? 'has-error' : '']">
                <label for="postcode" class="col-sm-2 col-form-label required">{{ __('shop::app.customer.account.address.create.postcode') }}</label>
                <div class="col-sm-10">
                    <input 
                        value="{{ old('postcode') }}" 
                        type="text" 
                        class="form-control" :class="errors.has('postcode') ? 'is-invalid'  : ''" 
                        name="postcode" 
                        v-validate="'required'" 
                        data-vv-as="&quot;{{ __('shop::app.customer.account.address.create.postcode') }}&quot;" 
                    >
                </div>
            </div>

            {!! view_render_event('bagisto.shop.customers.account.address.create_form_controls.postcode.after') !!}
           

             {{-- phone --}}
             <div class="position-relative row form-group" :class="[errors.has('phone') ? 'has-error' : '']">
                <label for="phone" class="col-sm-2 col-form-label required">{{ __('shop::app.customer.account.address.create.phone') }}</label>
                <div class="col-sm-10">
                    <input 
                        value="{{ old('phone') }}" 
                        type="text" 
                        class="form-control" :class="errors.has('phone') ? 'is-invalid'  : ''" 
                        name="phone" 
                        v-validate="'required'" 
                        data-vv-as="&quot;{{ __('shop::app.customer.account.address.create.phone') }}&quot;" 
                    >
                </div>
            </div>

            {!! view_render_event('bagisto.shop.customers.account.address.create_form_controls.phone.after') !!}
        
        </div>
        {!! view_render_event('bagisto.shop.customers.account.address.create_form_controls.after') !!}
    </div> 
</div>
</form>

{!! view_render_event('bagisto.shop.customers.account.address.create.after') !!}
{{--  --}}



@endsection