<div class="app-sidebar sidebar-shadow">

    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>    
    
    
    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu mt-4">

                @php
                    $seller = VinylHeaven\Seller\Models\Seller::where('customer_id', auth()->guard('customer')->user()->id)->first();
                @endphp

                @foreach ($menu->items as $menuItem)

                @if($menuItem['key'] == 'account')
                    <li class="app-sidebar__heading">{{ trans($menuItem['name']) }}</li>
                    @foreach ($menuItem['children'] as $subMenuItem)
                        @if (!in_array($subMenuItem['key'], config('exclude-from-dashboard-menu'))  )
                            <li>                            
                                <a href="{{ $subMenuItem['url'] }}" class="<?php if($menu->getActive($subMenuItem)){ echo 'mm-active'; } ?>" >
                                    @if (isset($subMenuItem['icon']))
                                        <i class="metismenu-icon {{ $subMenuItem['icon'] }}">
                                    @else
                                        ?
                                    @endif
                                    </i>{{ trans($subMenuItem['name']) }}
                                </a>
                            </li>
                        @endif
                    @endforeach 
                @elseif($menuItem['key'] == 'marketplace')
                    @if ($seller)
                        <li class="app-sidebar__heading">{{ trans($menuItem['name']) }}</li>
                        @foreach ($menuItem['children'] as $subMenuItem)
                            @if (!in_array($subMenuItem['key'], config('exclude-from-dashboard-menu'))  )
                                <li>                            
                                    <a href="{{ $subMenuItem['url'] }}" class="<?php if($menu->getActive($subMenuItem)){ echo 'mm-active'; } ?>" >
                                        @if (isset($subMenuItem['icon']))
                                            <i class="metismenu-icon {{ $subMenuItem['icon'] }}">
                                        @else
                                            ?
                                        @endif
                                        </i>{{ trans($subMenuItem['name']) }}
                                    </a>
                                </li>
                            @endif
                        @endforeach
                    @endif
                @endif
                @endforeach

            </ul>
        </div>
    </div>
</div>


@push('scripts')
<script>
    $(document).ready(function() {
        $(".icon.icon-arrow-down.right").on('click', function(e){
            var currentElement = $(e.currentTarget);
            if (currentElement.hasClass('icon-arrow-down')) {
                $(this).parents('.menu-block').find('.menubar').show();
                currentElement.removeClass('icon-arrow-down');
                currentElement.addClass('icon-arrow-up');
            } else {
                $(this).parents('.menu-block').find('.menubar').hide();
                currentElement.removeClass('icon-arrow-up');
                currentElement.addClass('icon-arrow-down');
            }
        });
    });
</script>
@endpush