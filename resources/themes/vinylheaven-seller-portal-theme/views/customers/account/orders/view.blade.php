@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.customer.account.order.view.page-tile', ['order_id' => $order->increment_id]) }}
@endsection

@section('content-wrapper')
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="far fa-edit icon-gradient bg-tempting-azure"></i>
                </div>
                <div>{{ __('shop::app.customer.account.order.view.page-tile', ['order_id' => $order->increment_id]) }}
                    <div class="page-title-subheading">Follow your order status</div>
                </div>
            </div>
        </div>
    </div>

    {!! view_render_event('bagisto.shop.customers.account.orders.view.before', ['order' => $order]) !!}

    <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">

        {{-- Order Information --}}
        <li class="nav-item">
            <a role="tab" id="tab-0" data-toggle="tab" href="#tab-content-0" aria-selected="true" class="nav-link active">
                <span>{{ __('shop::app.customer.account.order.view.info') }}</span>
            </a>
        </li>

        {{-- Invoices --}}
        @if ($order->invoices->count())
            <li class="nav-item">
                <a role="tab" id="tab-1" data-toggle="tab" href="#tab-content-1" aria-selected="false" class="nav-link">
                    <span>{{ __('shop::app.customer.account.order.view.invoices') }}</span>
                </a>
            </li>
        @endif

        {{-- Shipments --}}
        @if ($order->shipments->count())
            <li class="nav-item">
                <a role="tab" id="tab-2" data-toggle="tab" href="#tab-content-2" aria-selected="false" class="nav-link">
                    <span>{{ __('shop::app.customer.account.order.view.shipments') }}</span>
                </a>
            </li>
        @endif

        {{-- Refunds --}}
        @if ($order->refunds->count())
            <li class="nav-item">
                <a role="tab" id="tab-2" data-toggle="tab" href="#tab-content-3" aria-selected="false" class="nav-link">
                    <span>{{ __('shop::app.customer.account.order.view.refunds') }}</span>
                </a>
            </li>
        @endif

    </ul>

    {{-- Tab content sections --}}
    <div class="tab-content">

        {{-- Order Information Tab --}}
        <div id="tab-content-0" role="tabpanel" class="tab-pane tabs-animation fade active show">

            <div class="main-card mb-2 card">
                <div class="card-body">
                    <h5 class="card-title">Information</h5>
                    <div class="row">
                        <div class="col-6">
                            <ul class="list-group">
                                <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.placed-on') }}:</b> <span class="float-right ml-3">{{ core()->formatDate($order->created_at, 'd M Y') }}</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main-card mb-2 card">
                <div class="card-body">
                    <h5 class="card-title">{{ __('shop::app.customer.account.order.view.products-ordered') }}</h5>

                    <div class="table table-responsive">
                        <table class="table mt-3">
                            <thead>
                            <tr>
                                <th>{{ __('shop::app.customer.account.order.view.SKU') }}</th>
                                <th>{{ __('shop::app.customer.account.order.view.product-name') }}</th>
                                <th>{{ __('shop::app.customer.account.order.view.price') }}</th>
                                <th>{{ __('shop::app.customer.account.order.view.item-status') }}</th>
                                <th>{{ __('shop::app.customer.account.order.view.subtotal') }}</th>
                                <th>{{ __('shop::app.customer.account.order.view.tax-percent') }}</th>
                                <th>{{ __('shop::app.customer.account.order.view.tax-amount') }}</th>
                                <th>{{ __('shop::app.customer.account.order.view.grand-total') }}</th>
                            </tr>
                            </thead>

                            <tbody>

                            @foreach ($order->items as $item)
                                <tr>
                                    <td data-value="{{ __('shop::app.customer.account.order.view.SKU') }}">
                                        {{ $item->getTypeInstance()->getOrderedItem($item)->sku }}
                                    </td>

                                    <td data-value="{{ __('shop::app.customer.account.order.view.product-name') }}">
                                        {{ $item->name }}

                                        @if (isset($item->additional['attributes']))
                                            <div class="item-options">

                                                @foreach ($item->additional['attributes'] as $attribute)
                                                    <b>{{ $attribute['attribute_name'] }} : </b>{{ $attribute['option_label'] }}</br>
                                                @endforeach

                                            </div>
                                        @endif
                                    </td>

                                    <td data-value="{{ __('shop::app.customer.account.order.view.price') }}">
                                        {{ core()->formatPrice($item->price, $order->order_currency_code) }}
                                    </td>

                                    <td data-value="{{ __('shop::app.customer.account.order.view.item-status') }}">
                                                        <span class="qty-row">
                                                            {{ __('shop::app.customer.account.order.view.item-ordered', ['qty_ordered' => $item->qty_ordered]) }}
                                                        </span>

                                        <span class="qty-row">
                                                            {{ $item->qty_invoiced ? __('shop::app.customer.account.order.view.item-invoice', ['qty_invoiced' => $item->qty_invoiced]) : '' }}
                                                        </span>

                                        <span class="qty-row">
                                                            {{ $item->qty_shipped ? __('shop::app.customer.account.order.view.item-shipped', ['qty_shipped' => $item->qty_shipped]) : '' }}
                                                        </span>

                                        <span class="qty-row">
                                                            {{ $item->qty_refunded ? __('shop::app.customer.account.order.view.item-refunded', ['qty_refunded' => $item->qty_refunded]) : '' }}
                                                        </span>

                                        <span class="qty-row">
                                                            {{ $item->qty_canceled ? __('shop::app.customer.account.order.view.item-canceled', ['qty_canceled' => $item->qty_canceled]) : '' }}
                                                        </span>
                                    </td>

                                    <td data-value="{{ __('shop::app.customer.account.order.view.subtotal') }}">
                                        {{ core()->formatPrice($item->total, $order->order_currency_code) }}
                                    </td>

                                    <td data-value="{{ __('shop::app.customer.account.order.view.tax-percent') }}">
                                        {{ number_format($item->tax_percent, 2) }}%
                                    </td>

                                    <td data-value="{{ __('shop::app.customer.account.order.view.tax-amount') }}">
                                        {{ core()->formatPrice($item->tax_amount, $order->order_currency_code) }}
                                    </td>

                                    <td data-value="{{ __('shop::app.customer.account.order.view.grand-total') }}">
                                        {{ core()->formatPrice($item->total + $item->tax_amount - $item->discount_amount, $order->order_currency_code) }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row">

                        <div class="ml-auto mr-4">
                            <ul class="list-group">
                                <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.subtotal') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($order->sub_total, $order->order_currency_code) }}</span></li>
                                @if ($order->haveStockableItems())
                                    <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.shipping-handling') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($order->shipping_amount, $order->order_currency_code) }}</span></li>
                                @endif
                                @if ($order->base_discount_amount > 0)
                                    <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.discount') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($order->discount_amount, $order->order_currency_code) }}</span></li>
                                @endif
                                <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.tax') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($order->tax_amount, $order->order_currency_code) }}</span></li>
                                <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.grand-total') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($order->grand_total, $order->order_currency_code) }}</span></li>
                                <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.total-paid') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($order->grand_total_invoiced, $order->order_currency_code) }}</span></li>
                                <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.total-refunded') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($order->grand_total_refunded, $order->order_currency_code) }}</span></li>
                                <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.total-due') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($order->total_due, $order->order_currency_code) }}</span></li>

                            </ul>
                        </div>

                    </div>
                </div>
            </div>


            {{-- Timeline --}}
            <div class="main-card mb-2 card">
                <div class="card-body">
                    <div class="card-header">
                        <i class="header-icon fas fa-users icon-gradient bg-tempting-azure"> </i>
                        <span class="mt-1">Timeline</span>
                        {{-- <div class="btn-actions-pane-right actions-icon-btn">
                            <div role="group" class="btn-group-sm nav btn-group">
                                <a data-toggle="tab" href="#tab-eg3-0" class="btn-shadow btn btn-info active">Invoiced</a>
                                <a data-toggle="tab" href="#tab-eg3-1" class="btn-shadow btn btn-info active">Payed</a>
                                <a data-toggle="tab" href="#tab-eg3-2" class="btn-shadow  btn btn-info">Shipped</a>
                            </div>
                        </div> --}}
                    </div>
                    {{-- <h5 class="card-title">Timeline</h5> --}}
                    
                        <div class="table-responsive">
                            <div class="chat-wrapper">
    
    
                                @foreach ($order->messages()->orderBy('created_at')->get() as $message)
    
                                    @if ($message->sender == "seller")
                                        {{-- Seller chatbox --}}
                                        <div class="chat-box-wrapper">
                                            <div>
                                                <div class="avatar-icon-wrapper mr-1">
                                                    <div class="badge badge-bottom btn-shine badge-success badge-dot badge-dot-lg">
                                                    </div>
                                                    <div class="avatar-icon avatar-icon-lg rounded">
                                                        <img src="https://demo.dashboardpack.com/architectui-html-pro/assets/images/avatars/1.jpg" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                {!! $message->render() !!}
                                            </div>
                                        </div>
                                    @elseif($message->sender == "customer")
                                        {{-- Customer chatbox --}}
    
                                        <div class="float-right">
                                            <div class="chat-box-wrapper chat-box-wrapper-right">
                                                <div>
                                                    {!! $message->render() !!}
                                                </div>
                                                <div>
                                                    <div class="avatar-icon-wrapper ml-1">
                                                        <div class="badge badge-bottom btn-shine badge-success badge-dot badge-dot-lg">
                                                        </div>
                                                        <div class="avatar-icon avatar-icon-lg rounded">
                                                            <img src="https://demo.dashboardpack.com/architectui-html-pro/assets/images/avatars/2.jpg" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    <div class="clearfix"></div>

                                @endforeach         
                            </div>

                            @php
                                $sellerOrder =  Webkul\Marketplace\Models\Order::where('order_id', $order->id)->first();
                            @endphp
      
                            <hr>

                                <div class="app-inner-layout__bottom-pane d-block text-center">
                                    <form action="{{route('messages.store')}}" method="post">
                                        @csrf


                                        <input type="hidden" name="seller_id" value="{{$sellerOrder->seller->id}}">
                                       
                                        <input type="hidden" name="customer_id" value="{{$order->customer->id}}">
                                       
                                        <input type="hidden" name="order_id" value="{{$order->id}}">
                                        <input type="hidden" name="sender" value="customer">
                                        <input type="hidden" name="status_change" value="0">
                                    <div class="mb-0 position-relative row form-group mt-2">
                                        <div class="col-sm-10">
                                            <input type="text" name="message" placeholder="Type your message.." class="form-control-lg form-control">
                                        </div>
                                        <div class="col-sm-2">
                                            <button type="submit" class="btn btn-shadow btn-info btn-block btn-round form-control-lg form-control">Send</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                </div>
            </div>


        {{-- Invoices --}}
        <div id="tab-content-1" role="tabpanel" class="tab-pane tabs-animation fade">
            <div class="main-card mb-2 card">
                <div class="card-body">
                    <h5 class="card-title">{{ __('shop::app.customer.account.order.view.invoices') }}</h5>
                    @foreach ($order->invoices as $invoice)

                        <div class="row p-2">
                            <div class="col-10">
                                <h6>{{ __('shop::app.customer.account.order.view.individual-invoice', ['invoice_id' => $invoice->id]) }}</h6>
                            </div>
                            <div class="col-2">
                                <a href="{{ route('customer.orders.print', $invoice->id) }}" class="pull-right btn-icon btn-icon-only btn btn-info btn-sm btn-shadow" style="color:white;">
                                    <i class="fas fa-print btn-icon-wrapper"></i>
                                </a>
                            </div>
                        </div>


                                <div class="table table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>{{ __('shop::app.customer.account.order.view.SKU') }}</th>
                                            <th>{{ __('shop::app.customer.account.order.view.product-name') }}</th>
                                            <th>{{ __('shop::app.customer.account.order.view.price') }}</th>
                                            <th>{{ __('shop::app.customer.account.order.view.qty') }}</th>
                                            <th>{{ __('shop::app.customer.account.order.view.subtotal') }}</th>
                                            <th>{{ __('shop::app.customer.account.order.view.tax-amount') }}</th>
                                            <th>{{ __('shop::app.customer.account.order.view.grand-total') }}</th>
                                        </tr>
                                        </thead>

                                        <tbody>

                                        @foreach ($invoice->items as $item)
                                            <tr>
                                                <td data-value="{{ __('shop::app.customer.account.order.view.SKU') }}">
                                                    {{ $item->getTypeInstance()->getOrderedItem($item)->sku }}
                                                </td>

                                                <td data-value="{{ __('shop::app.customer.account.order.view.product-name') }}">
                                                    {{ $item->name }}
                                                </td>

                                                <td data-value="{{ __('shop::app.customer.account.order.view.price') }}">
                                                    {{ core()->formatPrice($item->price, $order->order_currency_code) }}
                                                </td>

                                                <td data-value="{{ __('shop::app.customer.account.order.view.qty') }}">
                                                    {{ $item->qty }}
                                                </td>

                                                <td data-value="{{ __('shop::app.customer.account.order.view.subtotal') }}">
                                                    {{ core()->formatPrice($item->total, $order->order_currency_code) }}
                                                </td>

                                                <td data-value="{{ __('shop::app.customer.account.order.view.tax-amount') }}">
                                                    {{ core()->formatPrice($item->tax_amount, $order->order_currency_code) }}
                                                </td>

                                                <td data-value="{{ __('shop::app.customer.account.order.view.grand-total') }}">
                                                    {{ core()->formatPrice($item->total + $item->tax_amount, $order->order_currency_code) }}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                                <div class="row">
                                    <div class="ml-auto mr-4">
                                        <ul class="list-group">
                                            <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.subtotal') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($invoice->sub_total, $order->order_currency_code) }}</span></li>
                                            <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.shipping-handling') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($invoice->shipping_amount, $order->order_currency_code) }}</span></li>
                                            @if ($order->base_discount_amount > 0)
                                            <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.discount') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($order->discount_amount, $order->order_currency_code) }}</span></li>
                                            @endif
                                            <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.tax') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($invoice->tax_amount, $order->order_currency_code) }}</span></li>
                                            <li class="list-group-item"><b>{{ __('shop::app.customer.account.order.view.grand-total') }}</b> <span class="float-right ml-3">{{ core()->formatPrice($invoice->grand_total, $order->order_currency_code) }}</span></li>
                                        </ul>
                                    </div>
                                </div>

                    @endforeach
                </div>
            </div>
        </div>

        {{-- Shipments --}}
        <div id="tab-content-2" role="tabpanel" class="tab-pane tabs-animation fade">
            <div class="main-card mb-2 card">
                <div class="card-body">
                    <h5 class="card-title">{{ __('shop::app.customer.account.order.view.shipments') }}</h5>
                    @foreach ($order->shipments as $shipment)

                            <span>{{ __('shop::app.customer.account.order.view.individual-shipment', ['shipment_id' => $shipment->id]) }}</span>

                            <div class="table table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>{{ __('shop::app.customer.account.order.view.SKU') }}</th>
                                        <th>{{ __('shop::app.customer.account.order.view.product-name') }}</th>
                                        <th>{{ __('shop::app.customer.account.order.view.qty') }}</th>
                                    </tr>
                                    </thead>

                                    <tbody>

                                    @foreach ($shipment->items as $item)

                                        <tr>
                                            <td data-value="{{  __('shop::app.customer.account.order.view.SKU') }}">{{ $item->sku }}</td>
                                            <td data-value="{{  __('shop::app.customer.account.order.view.product-name') }}">{{ $item->name }}</td>
                                            <td data-value="{{  __('shop::app.customer.account.order.view.qty') }}">{{ $item->qty }}</td>
                                        </tr>

                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                    @endforeach
                </div>
            </div>
        </div>


        {{-- Refunds --}}
        <div id="tab-content-3" role="tabpanel" class="tab-pane tabs-animation fade">
            <div class="main-card mb-2 card">
                <div class="card-body">
                    <h5 class="card-title">{{ __('shop::app.customer.account.order.view.refunds') }}</h5>

                    @foreach ($order->refunds as $refund)

                                <span>{{ __('shop::app.customer.account.order.view.individual-refund', ['refund_id' => $refund->id]) }}</span>

                                <div class="table table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>{{ __('shop::app.customer.account.order.view.SKU') }}</th>
                                            <th>{{ __('shop::app.customer.account.order.view.product-name') }}</th>
                                            <th>{{ __('shop::app.customer.account.order.view.price') }}</th>
                                            <th>{{ __('shop::app.customer.account.order.view.qty') }}</th>
                                            <th>{{ __('shop::app.customer.account.order.view.subtotal') }}</th>
                                            <th>{{ __('shop::app.customer.account.order.view.tax-amount') }}</th>
                                            <th>{{ __('shop::app.customer.account.order.view.grand-total') }}</th>
                                        </tr>
                                        </thead>

                                        <tbody>

                                        @foreach ($refund->items as $item)
                                            <tr>
                                                <td data-value="{{ __('shop::app.customer.account.order.view.SKU') }}">{{ $item->child ? $item->child->sku : $item->sku }}</td>
                                                <td data-value="{{ __('shop::app.customer.account.order.view.product-name') }}">{{ $item->name }}</td>
                                                <td data-value="{{ __('shop::app.customer.account.order.view.price') }}">{{ core()->formatPrice($item->price, $order->order_currency_code) }}</td>
                                                <td data-value="{{ __('shop::app.customer.account.order.view.qty') }}">{{ $item->qty }}</td>
                                                <td data-value="{{ __('shop::app.customer.account.order.view.subtotal') }}">{{ core()->formatPrice($item->total, $order->order_currency_code) }}</td>
                                                <td data-value="{{ __('shop::app.customer.account.order.view.tax-amount') }}">{{ core()->formatPrice($item->tax_amount, $order->order_currency_code) }}</td>
                                                <td data-value="{{ __('shop::app.customer.account.order.view.grand-total') }}">{{ core()->formatPrice($item->total + $item->tax_amount, $order->order_currency_code) }}</td>
                                            </tr>
                                        @endforeach

                                        @if (! $refund->items->count())
                                            <tr>
                                                <td class="empty" colspan="7">{{ __('admin::app.common.no-result-found') }}</td>
                                            <tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>

                                <div class="totals">
                                    <table class="sale-summary">
                                        <tr>
                                            <td>{{ __('shop::app.customer.account.order.view.subtotal') }}</td>
                                            <td>-</td>
                                            <td>{{ core()->formatPrice($refund->sub_total, $order->order_currency_code) }}</td>
                                        </tr>

                                        @if ($refund->shipping_amount > 0)
                                            <tr>
                                                <td>{{ __('shop::app.customer.account.order.view.shipping-handling') }}</td>
                                                <td>-</td>
                                                <td>{{ core()->formatPrice($refund->shipping_amount, $order->order_currency_code) }}</td>
                                            </tr>
                                        @endif

                                        @if ($refund->discount_amount > 0)
                                            <tr>
                                                <td>{{ __('shop::app.customer.account.order.view.discount') }}</td>
                                                <td>-</td>
                                                <td>{{ core()->formatPrice($order->discount_amount, $order->order_currency_code) }}</td>
                                            </tr>
                                        @endif

                                        @if ($refund->tax_amount > 0)
                                            <tr>
                                                <td>{{ __('shop::app.customer.account.order.view.tax') }}</td>
                                                <td>-</td>
                                                <td>{{ core()->formatPrice($refund->tax_amount, $order->order_currency_code) }}</td>
                                            </tr>
                                        @endif

                                        <tr>
                                            <td>{{ __('shop::app.customer.account.order.view.adjustment-refund') }}</td>
                                            <td>-</td>
                                            <td>{{ core()->formatPrice($refund->adjustment_refund, $order->order_currency_code) }}</td>
                                        </tr>

                                        <tr>
                                            <td>{{ __('shop::app.customer.account.order.view.adjustment-fee') }}</td>
                                            <td>-</td>
                                            <td>{{ core()->formatPrice($refund->adjustment_fee, $order->order_currency_code) }}</td>
                                        </tr>

                                        <tr class="bold">
                                            <td>{{ __('shop::app.customer.account.order.view.grand-total') }}</td>
                                            <td>-</td>
                                            <td>{{ core()->formatPrice($refund->grand_total, $order->order_currency_code) }}</td>
                                        </tr>
                                    </table>
                                </div>

                    @endforeach

                </div>
            </div>
        </div>





    </div>





                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-3">
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">
                                                <div class="widget-content p-0">
                                                    <div class="widget-content-wrapper">
                                                        <div class="widget-content-left">
                                                            <div class="widget-heading">{{ __('shop::app.customer.account.order.view.shipping-address') }}</div>
                                                            <hr>
                                                            <div class="widget-subheading">{{ $order->shipping_address->name }}</div>
                                                            <div class="widget-subheading">{{ $order->shipping_address->address1 }}</div>
                                                            <div class="widget-subheading">{{ $order->shipping_address->city }}</div>
                                                            <div class="widget-subheading">{{ $order->shipping_address->state }}</div>
                                                            <div class="widget-subheading">{{ core()->country_name($order->shipping_address->country) }} {{ $order->shipping_address->postcode }}</div>
                                                            <div class="widget-subheading">{{ __('shop::app.checkout.onepage.contact') }} : {{ $order->shipping_address->phone }}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-3">
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">
                                                <div class="widget-content p-0">
                                                    <div class="widget-content-wrapper">
                                                        <div class="widget-content-left">
                                                            <div class="widget-heading">{{ __('shop::app.customer.account.order.view.billing-address') }}</div>
                                                            <hr>
                                                            <div class="widget-subheading">{{ $order->billing_address->name }}</div>
                                                            <div class="widget-subheading">{{ $order->billing_address->address1 }}</div>
                                                            <div class="widget-subheading">{{ $order->billing_address->city }}</div>
                                                            <div class="widget-subheading">{{ $order->billing_address->state }}</div>
                                                            <div class="widget-subheading">{{ core()->country_name($order->billing_address->country) }} {{ $order->billing_address->postcode }}</div>
                                                            <div class="widget-subheading">{{ __('shop::app.checkout.onepage.contact') }} : {{ $order->billing_address->phone }}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-3">
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">
                                                <div class="widget-content p-0">
                                                    <div class="widget-content-wrapper">
                                                        <div class="widget-content-left">
                                                            <div class="widget-heading">{{ __('shop::app.customer.account.order.view.shipping-method') }}</div>
                                                            <hr>
                                                            <div class="widget-subheading">{{ $order->shipping_title }}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-3">
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">
                                                <div class="widget-content p-0">
                                                    <div class="widget-content-wrapper">
                                                        <div class="widget-content-left">
                                                            <div class="widget-heading">{{ __('shop::app.customer.account.order.view.payment-method') }}</div>
                                                            <hr>
                                                            <div class="widget-subheading">{{ core()->getConfigData('sales.paymentmethods.' . $order->payment->method . '.title') }}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



            {!! view_render_event('bagisto.shop.customers.account.orders.view.after', ['order' => $order]) !!}



@endsection