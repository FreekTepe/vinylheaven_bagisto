@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.customer.account.order.index.page-title') }}
@endsection

@section('content-wrapper')

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="fas fa-shopping-cart icon-gradient bg-tempting-azure"></i>
            </div>
            <div>{{ __('shop::app.customer.account.order.index.title') }}
                <div class="page-title-subheading">Your orders.</div>
            </div>
        </div>
    </div>
</div>   


    {!! view_render_event('bagisto.shop.customers.account.orders.list.before') !!}

            {!! app('Webkul\Shop\DataGrids\OrderDataGrid')->render() !!}

    {!! view_render_event('bagisto.shop.customers.account.orders.list.after') !!}


@endsection