(function ($) {
    $.AutoComplete = function(element, options) {
        //stores the passed element as a property of the created instance.
        //This way we can access it later
        this.element = (element instanceof $) ? element : $(element);
        
        var root = this;

        var data = {
            results: [],
            key: null
        };

        var autoCompleteOptions = {
            selectors: {
                template: '#MyDefaultAutoCompleteTemplate'
            },
            elements: {
                container: null
            },
            minQueryTermCharacterCount: 1
        };

        var isAutoCompleteContainerShown = function()
        {
            return $(autoCompleteOptions.elements.container).hasClass('show');
        };

        var showAutoCompleteContainer = function()
        {
            if (!$(autoCompleteOptions.elements.container).hasClass('show')) {
                $(autoCompleteOptions.elements.container).addClass('show');
            }
        };

        var showAutoCompleteLoader = function(parent_element) {
            if ($(parent_element).find('.autocomplete-loading-icon').hasClass('d-none')) {
                $(parent_element).find('.autocomplete-loading-icon').removeClass('d-none');
            }
        };

        var showAutoCompleteQuestion = function(parent_element) {
            if ($(parent_element).find('.autocomplete-question-icon').hasClass('d-none')) {
                $(parent_element).find('.autocomplete-question-icon').removeClass('d-none');
            }
        };

        var showAutoCompleteCheck = function(parent_element) {
            if ($(parent_element).find('.autocomplete-check-icon').hasClass('d-none')) {
                $(parent_element).find('.autocomplete-check-icon').removeClass('d-none');
            }
        };

        var hideAutoCompleteLoader = function(parent_element) {
            if (!$(parent_element).find('.autocomplete-loading-icon').hasClass('d-none')) {
                $(parent_element).find('.autocomplete-loading-icon').addClass('d-none');
            }
        };

        var hideAutoCompleteQuestion = function(parent_element) {
            if (!$(parent_element).find('.autocomplete-question-icon').hasClass('d-none')) {
                $(parent_element).find('.autocomplete-question-icon').addClass('d-none');
            }
        };

        var hideAutoCompleteCheck = function(parent_element) {
            if (!$(parent_element).find('.autocomplete-check-icon').hasClass('d-none')) {
                $(parent_element).find('.autocomplete-check-icon').addClass('d-none');
            }
        };

        var hideAutoCompleteContainer = function()
        {
            if ($(autoCompleteOptions.elements.container).hasClass('show')) {
                $(autoCompleteOptions.elements.container).removeClass('show');
            }
        };

        var setAutoCompleteContainerContent = function(content, autocomplete_input) {
            $(autoCompleteOptions.elements.container).html(content);

            $.each($('.autocomplete-value'), function(auto_index, auto) {
                $(this).off('click');

                $(this).click(function(event){
                    $(autocomplete_input).val($(this).attr('data-value'));

                    hideAutoCompleteContainer();
                    showAutoCompleteCheck($(autocomplete_input).parent());
                });
            });
        };

        var getJwtToken = function () {
            var x = document.querySelector('meta[name="jwt"]');

            if (x) {
                return x.getAttribute("content");
            } else {
                return false;
            }
        };

        var OnKeyUp = function(event) {
            var key = event.which;
            var searchQueryTerm = $(this).val();
            var searchQueryKey = $(this).data("queryKey");
            var searchQueryToken = $(this).data("token");

            if (key == 13) {
                event.preventDefault();
                event.stopPropagation();
            }

            if ($(this).val().length >= autoCompleteOptions.minQueryTermCharacterCount) {
                showAutoCompleteLoader($(this).parent());
                showAutoCompleteContainer();
            } else {
                hideAutoCompleteContainer();
            }

            search(searchQueryTerm, searchQueryKey, searchQueryToken, $(this));
        };

        var OnBlur = function(event) {
            hideAutoCompleteContainer();
        };

        var OnFocus = function() {
            if ($(this).val().length) {
                showAutoCompleteContainer();
            }
        };

        var getReleaseTitleAutoCompleteTemplate = function() {
            var template = $('<div class="container-fluid"></div>')
            
            for (var i = 0; i < data.results.length; i++) {
                var new_data_row = $('<div class="row"></div>');
                var new_data_column = $('<div class="col-12 autocomplete-col autocomplete-value"></div>');

                var name = data.results[i].name;
                var released = data.results[i].release_date;
                var artist = 'unknown';

                if (data.results[i].artists.length) {
                    artist = data.results[i].artists[0].name;
                }

                $(new_data_column).attr('data-value', name)
                    .html('<strong>Title:</strong> ' + name);

                $(new_data_row).append($(new_data_column));

                $(template).append($(new_data_row));
            }

            return $(template);
        };

        var getArtistNameAutoCompleteTemplate = function() {
            var template = $('<div class="container-fluid"></div>');

            for (var i = 0; i < data.results.length; i++) {
                var name = data.results[i].name;
                var new_data_row = $('<div class="row"></div>');
                var new_data_column = $('<div class="col-12 autocomplete-col autocomplete-value"></div>');
                

                $(new_data_column)
                        .attr('data-value', name)
                        .html('<strong>Name:</strong> ' + name);

                $(new_data_row).append($(new_data_column));

                $(template).append($(new_data_row));
            }

            return $(template);
        };

        var getLabelAutoCompleteTemplate = function() {
            var template = $('<div class="container-fluid"></div>');

            for (var i = 0; i < data.results.length; i++) {
                var new_data_row = $('<div class="row"></div>');
                var new_data_column = $('<div class="col-12 autocomplete-col autocomplete-value"></div>');

                var name = data.results[i].name;

                $(new_data_column).attr('data-value', name)
                    .html('<strong>Label:</strong> ' + name);

                $(new_data_row).append($(new_data_column));

                $(template).append($(new_data_row));
            }

            return $(template);
        };

        var getReleasedAutoCompleteTemplate = function() {
            var template = $('<div class="container-fluid"></div>')
            
            for (var i = 0; i < data.results.length; i++) {
                var new_data_row = $('<div class="row"></div>');
                var new_data_column = $('<div class="col-12 autocomplete-col autocomplete-value"></div>');

                var released = data.results[i].release_date;

                $(new_data_column).attr('data-value', released)
                    .html('<strong>Released:</strong> ' + released);

                $(new_data_row).append($(new_data_column));

                $(template).append($(new_data_row));
            }

            return $(template);
        };

        var getCatNoAutoCompleteTemplate = function() {
            var template = $('<div class="container-fluid"></div>');

            for (var i = 0; i < data.results.length; i++) {
                var catno = data.results[i].catno;
                var new_data_row = $('<div class="row"></div>');
                var new_data_column = $('<div class="col-12 autocomplete-col autocomplete-value"></div>');
                

                $(new_data_column)
                        .attr('data-value', catno)
                        .html('<strong>cat. nr:</strong> ' + catno);

                $(new_data_row).append($(new_data_column));

                $(template).append($(new_data_row));
            }

            return $(template);
        };

        var getBarcodeAutoCompleteTemplate = function() {
            var template = $('<div class="container-fluid"></div>');

            for (var i = 0; i < data.results.length; i++) {
                var barcode = data.results[i].value;
                var new_data_row = $('<div class="row"></div>');
                var new_data_column = $('<div class="col-12 autocomplete-col autocomplete-value"></div>');
                

                $(new_data_column)
                        .attr('data-value', barcode)
                        .html('<strong>barcode:</strong> ' + barcode);

                $(new_data_row).append($(new_data_column));

                $(template).append($(new_data_row));
            }

            return $(template);
        };

        var getMatrixAutoCompleteTemplate = function() {
            var template = $('<div class="container-fluid"></div>');

            for (var i = 0; i < data.results.length; i++) {
                var matrix = data.results[i].value;
                var new_data_row = $('<div class="row"></div>');
                var new_data_column = $('<div class="col-12 autocomplete-col autocomplete-value"></div>');
                

                $(new_data_column)
                        .attr('data-value', matrix)
                        .html('<strong>matrix:</strong> ' + matrix);

                $(new_data_row).append($(new_data_column));

                $(template).append($(new_data_row));
            }

            return $(template);
        };

        var setAutoCompleteTemplate = function(autocomplete_input, autocomplete_input_parent) {
            console.log(data);
            var autocomplete_container_content = $('<div></div>');
            var template_content = null;
            

            switch(data.key) {
                case "release_title":
                    template_content = getReleaseTitleAutoCompleteTemplate();
                    break;
                case "artist_name":
                    template_content = getArtistNameAutoCompleteTemplate();
                    break;
                case "label":
                    template_content = getLabelAutoCompleteTemplate();
                    break;
                case "released":
                    template_content = getReleasedAutoCompleteTemplate();
                    break;
                case "cat_no":
                    template_content = getCatNoAutoCompleteTemplate();
                    break;
                case "barcode":
                    template_content = getBarcodeAutoCompleteTemplate();
                    break;
                case "matrix":
                    template_content = getMatrixAutoCompleteTemplate();
                    break;
                default:
                    template_content = $('<div>Unkown Data Key</div>');
                    showAutoCompleteQuestion($(autocomplete_input_parent));
            }

            $(autocomplete_container_content).append($(template_content));

            setAutoCompleteContainerContent($(autocomplete_container_content), autocomplete_input);
            hideAutoCompleteLoader($(autocomplete_input_parent));
        };

        var setAutoCompleteValue = function(autocomplete_input, data) {
            switch ($(autocomplete_input).data("queryKey")) {
                case "release_title":
                    $(autocomplete_input).val(data.results[0].name);
                    break;
                case "released":
                    $(autocomplete_input).val(data.results[0].release_date);
                    break;
                case "artist_name":
                    $(autocomplete_input).val(data.results[0].name);
                    break;
                case "barcode":
                    $(autocomplete_input).val(data.results[0].value);
                    break;
                case "matrix":
                    $(autocomplete_input).val(data.results[0].value);
                    break;
                case "label":
                    $(autocomplete_input).val(data.results[0].name);
                    break;
                case "cat_no":
                    $(autocomplete_input).val(data.results[0].catno);
                    break;
            }
        };

        var search = function(searchQueryTerm, searchQueryKey, searchQueryToken, autocomplete_input) {
            hideAutoCompleteQuestion($(autocomplete_input).parent());
            hideAutoCompleteCheck($(autocomplete_input).parent());
            
            var post_data = {
                term: searchQueryTerm,
                key: searchQueryKey,
                token: searchQueryToken
            };
            var post_headers = {
                Authorization: "Bearer " + getJwtToken()
            };
            var post_settings = {
                url: window.location.origin + '/api/release/autocomplete',
                data: post_data,
                headers: post_headers
            };
            
            $.post(
                post_settings
            )
            .then(function(result){
                // ajax post call successfull set the data object and create autocomplete content
                data.results = result.suggestions;
                data.key = searchQueryKey;
                
                console.log('AJAX POST CALL FOR SUGGESTIONS RESULTS!!!!!');
                // console.log(result);

                if (data.results.length == 0) {
                    showAutoCompleteQuestion($(autocomplete_input).parent());
                    setAutoCompleteTemplate($(autocomplete_input), $(autocomplete_input).parent());
                } else if (data.results.length == 1) {
                    showAutoCompleteCheck($(autocomplete_input).parent());
                    setAutoCompleteTemplate($(autocomplete_input), $(autocomplete_input).parent());
                    hideAutoCompleteContainer();

                    setAutoCompleteValue($(autocomplete_input), data);
                } else {
                    setAutoCompleteTemplate($(autocomplete_input), $(autocomplete_input).parent());
                }
            })
            .fail(function(error){
                console.log('WE HAVE ERRORS ON THE AJAX CALLLLL!!!!!!');
                console.log(error);

                hideAutoCompleteLoader($(autocomplete_input).parent());
            });

        };

        var createAutoCompleteContainer = function()
        {
            var parentElement = $(root.element).parent();
            autoCompleteOptions.elements.container = $("<div></div>");

            $(autoCompleteOptions.elements.container).addClass('jquery-auto-complete-container');

            $(parentElement).append($(autoCompleteOptions.elements.container));
        }

        var init = function() {
            // set the events
            $(root.element).keyup(OnKeyUp);
            $(root.element).focus(OnFocus);
            // $(root.element).blur(OnBlur);

            // set the template id
            if ($(root.element).data('template')) {
                autoCompleteOptions.selectors.template = $(root.element).data('template');
            }
            
            // Create the Auto Complete Container
            createAutoCompleteContainer();
        };

        $.extend(autoCompleteOptions , options);
        init();

        console.log("AUTOCOMPLETE INITIALIZED");
    };

    $.AutoComplete.prototype = {
        InitEvents: function() {
            var that = this;


        },

        search: function(searchQueryTerm, searchQueryKey, searchQueryData = []) {
            // Check if Search Query Params are of the correct type
            if (searchQueryTerm == null || !typeof searchQueryTerm === "string" || !searchQueryTerm instanceof String) {
                throw new Exception("The Search Query Term, cannot be null and has to be typeof/instanceof string.");
            }

            if (searchQueryKey == null || !typeof searchQueryKey === "string" || !searchQueryKey instanceof String) {
                throw new Exception("The Search Query Key, cannot be null and has to be typeof/instanceof string.");
            }

            // Do search stuff

        }
    };
}(jQuery))