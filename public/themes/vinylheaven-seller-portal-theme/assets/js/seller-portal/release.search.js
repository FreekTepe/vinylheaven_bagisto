$(document).ready(function(){
    var autocomplete;

    var getSearchJwtToken = function () {
        var x = document.querySelector('meta[name="jwt"]');

        if (x) {
            return x.getAttribute("content");
        } else {
            return false;
        }
    };

    var addReleaseToSore = function(){
        var release_id = $(this).data('releaseId');

        if ($('#loading-icon-' + release_id).hasClass('d-none')) {
            $('#loading-icon-' + release_id).removeClass('d-none');
        }

        var post_headers = {
            Authorization: "Bearer " + getSearchJwtToken()
        };

        var post_data = JSON.parse($('#release-' + release_id + '-json-string').val());
        var release_additional = JSON.parse($('#release-additional-' + release_id + '-json-string').val())
console.log("HELLLOOOOOOOOOOOOO!!!!!111");
console.log(post_data);
        post_data.seller = {
            price: $('#price-' + release_id).val(),
            is_owner: 0,
            shipping_price: 0,
            description: $('#description-' + release_id).val(),
            private_comments: $('#private-comments-' + release_id).val(),
            storage_location: $('#storage-location-' + release_id).val(),
            condition: $('#media-condition-' + release_id).val(), // is the media condition
            sleeve_condition: $('#sleeve-condition-' + release_id).val(),
            inventories: 1
        };

        post_data.seller_id = release_additional.seller_id;
        post_data.channel = release_additional.channel;
        post_data.price = release_additional.price;
        post_data.seller_id = release_additional.seller_id;
        post_data.status = release_additional.status;
        post_data.type = release_additional.type;
        post_data.visible_individually = release_additional.visible_individually;
        post_data.weight = release_additional.weight;
        post_data.locale = release_additional.locale;

        
        if (post_data.is_main_release == "true") {
            post_data.is_main_release = true;
        } else {
            post_data.is_main_release = false;
        }

        var post_settings = {
            url: window.location.origin + '/api/release/import',
            data: post_data,
            headers: post_headers
        };

console.log("YOEHOEEEEEEE");
        console.log(post_data);
        
        $.post(
            post_settings
        )
        .then(function(result){
            console.log("GOT RESULTSSSSS!!!!!!!");
            console.log(result);

            if (!$('#loading-icon-' + release_id).hasClass('d-none')) {
                $('#loading-icon-' + release_id).addClass('d-none');
            }

            if (result.swal.type == 'success') {
                var success_message = result.swal.title + " " + result.swal.text;

                if (!$('#card-body-' + release_id).hasClass('d-none')) {
                    $('#card-body-' + release_id).addClass('d-none');
                }

                $('#result-message-container-' + release_id).html(success_message);

                if (!$('#result-message-container-' + release_id).hasClass('text-success')) {
                    $('#result-message-container-' + release_id).addClass('text-success');
                }

                if ($('#result-message-container-' + release_id).hasClass('d-none')) {
                    $('#result-message-container-' + release_id).removeClass('d-none');
                }

                if (!$('#release-add-to-store-button-' + release_id).hasClass('d-none')) {
                    $('#release-add-to-store-button-' + release_id).addClass('d-none');
                }

                if (!$('#release-sell-cancel-button-' + release_id).hasClass('d-none')) {
                    $('#release-sell-cancel-button-' + release_id).addClass('d-none');
                }
            }

            ///marketplace/account/catalog/products release-add-to-store-button release-sell-cancel-button
        })
        .fail(function(error){
            console.log('WE HAVE ERRORS ON THE AJAX CALLLLL!!!!!!');
            console.log(error);
        });
    };

    var searchReleases = function(queryParams) {
        if ($('#release-searching-icon').hasClass('d-none')) {
            $('#release-searching-icon').removeClass('d-none');
        }

        var post_headers = {
            Authorization: "Bearer " + getSearchJwtToken()
        };
        var post_settings = {
            url: window.location.origin + '/api/release/search',
            data: queryParams,
            headers: post_headers
        };
        
        $.post(
            post_settings
        )
        .then(function(result){
            // ajax post call successfull set the data object and create autocomplete content
            console.log("GOT RESULTSSSSS!!!!!!!");
            console.log(result);
            
            if (!$('#release-searching-icon').hasClass('d-none')) {
                $('#release-searching-icon').addClass('d-none');
            }

            $('#search-release-result-list').html(result.view);

            $.each($('.release-sell-button'), function(sell_button_index, sell_button){
                $(sell_button).off();
                $(sell_button).click(function(){
                    if ($('#release-' + $(this).data('releaseId') + '-sell-container').hasClass('d-none')) {
                        $('#release-' + $(this).data('releaseId') + '-sell-container').removeClass('d-none');
                    }

                    if (!$('#release-' + $(this).data('releaseId') + '-sellect-container').hasClass('d-none')) {
                        $('#release-' + $(this).data('releaseId') + '-sellect-container').addClass('d-none');
                    }
                });
            });
        
            $.each($('.release-sell-cancel-button'), function(sell_button_index, sell_button){
                $(sell_button).off();
                $(sell_button).click(function(){
                    if ($('#release-' + $(this).data('releaseId') + '-sellect-container').hasClass('d-none')) {
                        $('#release-' + $(this).data('releaseId') + '-sellect-container').removeClass('d-none');
                    }

                    if (!$('#release-' + $(this).data('releaseId') + '-sell-container').hasClass('d-none')) {
                        $('#release-' + $(this).data('releaseId') + '-sell-container').addClass('d-none');
                    }
                });
            });

            $.each($('.release-add-to-store-button'), function(add_to_store_button_index, add_to_store_button){
                $(add_to_store_button).click(addReleaseToSore);
            });
        })
        .fail(function(error){
            console.log('WE HAVE ERRORS ON THE AJAX CALLLLL!!!!!!');
            console.log(error);

            // alert('THERE ARE ERRORSSSS!!!!!!');
        });
            
    };

    var searchReleaseById = function(queryParams) {
        var post_headers = {
            Authorization: "Bearer " + getSearchJwtToken()
        };
        var post_settings = {
            url: window.location.origin + '/api/release/find/' + queryParams.release_id,
            data:{},
            headers: post_headers
        };
        
        $.post(
            post_settings
        )
        .then(function(result){
            // ajax post call successfull set the data object and create autocomplete content
            // data.results = result.suggestions;
            
            console.log("GOT RESULTSSSSS!!!!!!!");
            console.log(result);
            
            // $('#search-release-result-list').html(result.view);

            alert('GOT RESULTSSSS!!!!!');
            
        })
        .fail(function(error){
            console.log('WE HAVE ERRORS ON THE AJAX CALLLLL!!!!!!');
            console.log(error);

            alert('THERE ARE ERRORSSSS!!!!!!');
        });
    };

    $.each($("input.jquery-autocomplete"), function(jquery_autocomplete_index, jquery_autocomplete) {
        autocomplete = $.AutoComplete($(jquery_autocomplete));
    });

    $('#release-search-button').click(function(){
        
        var queryParams = {
            release_title: $('#release_title').val(),
            artist_name: $('#artist_name').val(),
            released: $('#released').val(),
            barcode: $('#barcode').val(),
            cat_no: $('#cat_no').val(),
            label: $('#label').val(),
            country: "",
            matrix: $('#matrix').val(),
            format: ""
        };

        if ($('#country').val() != "null") {
            queryParams.country = $('#country').val();
        }

        if ($('#release_formats input[type="radio"]:checked').val() != undefined) {
            queryParams.format = $('#release_formats input[type="radio"]:checked').val()
        }

        searchReleases(queryParams);
    });

    $('#release-search-by-id-button').click(function(){
        var queryParams = {
            release_id: $('#release_id').val()
        };
        searchReleaseById(queryParams);

        alert("button works!!!!");
    });
});