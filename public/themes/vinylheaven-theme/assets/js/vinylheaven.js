var VinylHeavenTheme = {
    selectors: {
        alertContainer: "#alert-container"
    },

    createAlert: function(alertTypeClass, message) {
        var alertCreated = $(
            "<div class='alert " +
                alertTypeClass +
                " alert-dismissible fade show' role='alert'><div class='container-fluid container-lg'><div class='row justify-content-center align-items-center'><div class='col-12'>" +
                message +
                "</div></div></div><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
        );

        $(this.selectors.alertContainer).append(alertCreated);
    },

    putAlert: function(type, message) {
        var alertType = "";
        switch (type) {
            case "success":
                alertType = "alert-success";
                break;
            case "error":
                alertType = "alert-danger";
                break;
        }
        this.createAlert(alertType, message);
    }
};

$(document).ready(function(){
    // Support for AJAX loaded modal window.
    // Focuses on first input textbox after it loads the window.
    $('[data-toggle=modal]').click(function(event) {
        event.preventDefault();
    });

    // initialize the jquery breakpoints plugin for detecting in jQuery the breakpoints
    bsBreakpoints.init();
});