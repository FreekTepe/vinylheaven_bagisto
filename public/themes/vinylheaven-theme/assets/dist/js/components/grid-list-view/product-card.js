        <div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-3">
            <div class="product-card d-flex flex-row justify-content-center mx-auto">
                <a href="{{:href}}" class="card-info" title="{{:name}}">
                    <div class="info-image">
                        <div class="image-holder info-image-{{:url_key}}">
                            
                        </div>
                    </div>

                    <div class="info-content" title="{{:name}}">
                        <div class="content-title">
                            {{:name}}
                        </div>

                        <div class="content-category">
                            - artist name
                        </div>

                        <div class="info-product font-weight-light">
                            
                        </div>
                    </div>
                </a>
            </div>
        </div>