$(document).ready(function(){
    $('[data-component="slider"]').each(function () {
        $(this).slick({ rows: 0, arrows: !1, dots: !1, infinite: !0, speed: 300, slidesToShow: 1, slidesToScroll: 1, autoplay: !0, autoplaySpeed: 3e3 });
    });
});
