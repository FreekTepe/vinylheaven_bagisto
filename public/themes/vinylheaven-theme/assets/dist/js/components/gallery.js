$(document).ready(function() {
    var productGallery = $('#product-gallery');
    var productGalleryFilmRollHeight = $('#product-gallery-film-roll').prop('scrollHeight');
    var filmRollHeight = $('.gallery-film-roll').height();
    var timesMovedUp = 0;

    var setProductGalleryFilmRol = function(breakpoint) {
        switch (breakpoint) {
            case "xSmall":
            case "small":
            case "medium":
                if (!$('#product-gallery-film-rol').hasClass('list-group-horizontal')) {
                    $('#product-gallery-film-rol').addClass('list-group-horizontal');
                }
                break;
            default:
                if ($('#product-gallery-film-rol').hasClass('list-group-horizontal')) {
                    $('#product-gallery-film-rol').removeClass('list-group-horizontal');
                }
        }

        
    };

    var OnNewBsBreakpoint = function (event) {
        setProductGalleryFilmRol(event.breakpoint);
    };

    var initGalleryFilmRol = function () {
        productGalleryFilmRollHeight = $('#product-gallery-film-roll').prop('scrollHeight');
        $(window).on('new.bs.breakpoint', OnNewBsBreakpoint);

        setProductGalleryFilmRol(bsBreakpoints.detectBreakpoint());

        $.each($('#product-gallery-film-roll .list-group-item'), function(list_group_item_index, list_group_item) {
            $(list_group_item).click(function(event) {
                $('#product-gallery').carousel(parseInt($(this).data('slide')));
            });
        });

        $('.film-roll-move-up').click(function(event){
            event.preventDefault();
            event.stopPropagation();

            
            if (timesMovedUp > 0) {
                timesMovedUp = timesMovedUp - 1;
                $('#product-gallery-film-roll').css('top', '-' + (timesMovedUp * filmRollHeight) + 'px');
            }
        });

        $('.film-roll-move-down').click(function(event){
            event.preventDefault();
            event.stopPropagation();

            if (((timesMovedUp + 1) * filmRollHeight) <= productGalleryFilmRollHeight) {
                timesMovedUp = timesMovedUp + 1;
                $('#product-gallery-film-roll').css('top', '-' + (timesMovedUp * filmRollHeight) + 'px');
            }
        });
    };

    var initGallery = function () {
        var productGalleryOptions = {
            pause: true,
            ride: false,
            touch: true
        };
        $('#product-gallery').carousel(productGalleryOptions);
        initGalleryFilmRol();
    }

    var init = function () {
        initGallery();
    }

    $(window).on('load', function(){
        init();
    });
});
