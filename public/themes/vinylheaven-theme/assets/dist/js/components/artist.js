$(document).ready(function(){
    $.each($('.collapse.has-transition'), function(collapse_index, collapse){
        $(collapse).on("show.bs.collapse", function() {
            if (!$($(collapse).data("trigger")).hasClass("open")) {
                $($(collapse).data("trigger")).addClass("open");
            }
        });

        $(collapse).on("hide.bs.collapse", function() {
            if ($($(collapse).data("trigger")).hasClass("open")) {
                $($(collapse).data("trigger")).removeClass("open");
            }
        });
    });
});