(function() {
    "use strict";

    var order_form = document.getElementById("checkout-order-form");

    var getOrderSelectedPaymentOptions = function(seller_id) {
        if ($("#payment-selected-options-" + seller_id).html() == 0) {
            return false;
        }

        return $("#payment-selected-options-" + seller_id).html();
    };

    var getOrderSelectedShippingOptions = function(seller_id) {
        if ($("#shipping-selected-options-" + seller_id).html() == 0) {
            return false;
        }

        return $("#shipping-selected-options-" + seller_id).html();
    };

    var onClickShippingMethod = function(event) {
        if ($(order_form).hasClass("was-validated")) {
            if (
                $(
                    "#header-shipping-options-" +
                        $(this).data("sellerId") +
                        " .header-text"
                ).hasClass("text-danger")
            ) {
                $(
                    "#header-shipping-options-" +
                        $(this).data("sellerId") +
                        " .header-text"
                ).removeClass("text-danger");
            }

            if (
                !$(
                    "#header-shipping-options-" +
                        $(this).data("sellerId") +
                        " .header-text"
                ).hasClass("text-success")
            ) {
                $(
                    "#header-shipping-options-" +
                        $(this).data("sellerId") +
                        " .header-text"
                ).addClass("text-success");
            }
        }

        $("#shipping-selected-options-" + $(this).data("sellerId")).html("1");
    };

    var onClickPaymentMethod = function(event) {
        if ($(order_form).hasClass("was-validated")) {
            if (
                $(
                    "#header-payment-options-" +
                        $(this).data("sellerId") +
                        " .header-text"
                ).hasClass("text-danger")
            ) {
                $(
                    "#header-payment-options-" +
                        $(this).data("sellerId") +
                        " .header-text"
                ).removeClass("text-danger");
            }

            if (
                !$(
                    "#header-payment-options-" +
                        $(this).data("sellerId") +
                        " .header-text"
                ).hasClass("text-success")
            ) {
                $(
                    "#header-payment-options-" +
                        $(this).data("sellerId") +
                        " .header-text"
                ).addClass("text-success");
            }
        }

        $("#payment-selected-options-" + $(this).data("sellerId")).html("1");
    };

    var onClickSubmitOrderButton = function(event) {
        event.preventDefault();
        event.stopPropagation();

        var order_for_seller = $(this).data("sellerId");
        if (order_form.checkValidity() == false) {
            if ($(order_form).hasClass("was-validated") == false) {
                order_form.classList.add("was-validated");
            }

            if (
                !getOrderSelectedPaymentOptions(order_for_seller) &&
                !getOrderSelectedShippingOptions(order_for_seller)
            ) {
                $("#checkout-shipping-options-" + order_for_seller).removeAttr(
                    "data-parent"
                );
                $("#checkout-payment-options-" + order_for_seller).removeAttr(
                    "data-parent"
                );
                //#seller-checkout-accordion-6
                //#checkout-shipping-options-6
                //#checkout-payment-options-6
            }

            if (
                !getOrderSelectedPaymentOptions(order_for_seller) &&
                !$(
                    "#header-payment-options-" +
                        order_for_seller +
                        " .header-text"
                ).hasClass("text-danger")
            ) {
                if (
                    $(
                        "#header-payment-options-" +
                            order_for_seller +
                            " .header-text"
                    ).hasClass("text-success")
                ) {
                    $(
                        "#header-payment-options-" +
                            order_for_seller +
                            " .header-text"
                    ).removeClass("text-success");
                }

                $(
                    "#header-payment-options-" +
                        order_for_seller +
                        " .header-text"
                ).addClass("text-danger");
            }

            if (
                getOrderSelectedPaymentOptions(order_for_seller) &&
                !$(
                    "#header-payment-options-" +
                        $(this).data("sellerId") +
                        " .header-text"
                ).hasClass("text-success")
            ) {
                $(
                    "#header-payment-options-" +
                        $(this).data("sellerId") +
                        " .header-text"
                ).addClass("text-success");
            }

            if (
                !getOrderSelectedShippingOptions(order_for_seller) &&
                !$(
                    "#header-shipping-options-" +
                        order_for_seller +
                        " .header-text"
                ).hasClass("text-danger")
            ) {
                if (
                    $(
                        "#header-shipping-options-" +
                            order_for_seller +
                            " .header-text"
                    ).hasClass("text-success")
                ) {
                    $(
                        "#header-shipping-options-" +
                            order_for_seller +
                            " .header-text"
                    ).removeClass("text-success");
                }

                $(
                    "#header-shipping-options-" +
                        order_for_seller +
                        " .header-text"
                ).addClass("text-danger");
            }

            if (
                getOrderSelectedShippingOptions(order_for_seller) &&
                !$(
                    "#header-shipping-options-" +
                        $(this).data("sellerId") +
                        " .header-text"
                ).hasClass("text-success")
            ) {
                $(
                    "#header-shipping-options-" +
                        $(this).data("sellerId") +
                        " .header-text"
                ).addClass("text-success");
            }

            if (
                !getOrderSelectedShippingOptions(order_for_seller) &&
                !$("#checkout-shipping-options-" + order_for_seller).hasClass(
                    "show"
                )
            ) {
                $(
                    "#header-shipping-options-" +
                        order_for_seller +
                        " .header-text"
                ).click();
            }

            if (
                !getOrderSelectedPaymentOptions(order_for_seller) &&
                !$("#checkout-payment-options-" + order_for_seller).hasClass(
                    "show"
                )
            ) {
                $(
                    "#header-payment-options-" +
                        order_for_seller +
                        " .header-text"
                ).click();
            }
        } else {
            $("#checkout-order-for-seller").val(order_for_seller);
            $(order_form).submit();
        }
    };

    var TabSwitch = function(pane_to_show) {
        $(".switcher-pane.show").removeClass("show");
        $(pane_to_show).addClass("show");
    };

    var TabClicked = function(event) {
        $(".switcher-tab.active").removeClass("active");
        $(this).addClass("active");

        TabSwitch($(this).data("target"));
    };

    var UseForShipping = function() {
        if ($("#billing_use_for_shipping").is(":checked")) {
            if ($(".switcher-tabs").hasClass("show")) {
                $(".switcher-tabs").removeClass("show");

                $("#shipping_address1").prop("required", false);
                $("#shipping_city").prop("required", false);
                $("#shipping_country").prop("required", false);
                $("#shipping_state").prop("required", false);
                $("#shipping_postcode").prop("required", false);

                $(".tab-billing").click();
            }
        } else {
            if (!$(".switcher-tabs").hasClass("show")) {
                $(".switcher-tabs").addClass("show");
                $(".tab-shipping").click();

                $("#shipping_address1").prop("required", true);
                $("#shipping_city").prop("required", true);
                $("#shipping_country").prop("required", true);
                $("#shipping_state").prop("required", true);
                $("#shipping_postcode").prop("required", true);
            }
        }
    };

    $(document).ready(function() {
        $(".switcher-tab").click(TabClicked);

        $("#billing_use_for_shipping").click(function(event) {
            UseForShipping();
        });

        $.each($(".button-submit-order"), function(button_index, button) {
            $(button).click(onClickSubmitOrderButton);
        });

        $.each($(".payment-method"), function(method_index, method) {
            $(method).click(onClickPaymentMethod);
        });

        $.each($(".shipping-method"), function(method_index, method) {
            $(method).click(onClickShippingMethod);
        });
    });
})();
