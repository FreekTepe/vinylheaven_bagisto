var SearchAutoComplete = function(url, data) {
    $("#search-autocomplete").collapse("show");

    $.post(url, data)
        .then(function(response) {
            $("#autocomplete-releases").html("");
            $("#autocomplete-artists").html("");
            $.each(response.result.releases, function(release_index, release) {
                $("#autocomplete-releases").append(
                    $(
                        "<div class='col-12 align-items-center mb-2'><a href='" +
                            release.url +
                            "' class='d-block'><div class='row align-items-center'><div class='col-2'><img src='" +
                            release.primary_image +
                            "' class='release-image' /></div><div class='col-10'><strong class='item-name'>" +
                            release.name +
                            "</strong></div></div></a></div>"
                    )
                );
            });

            $.each(response.result.artists, function(artist_index, artist) {
                $("#autocomplete-artists").append(
                    $(
                        "<div class='col-12 align-items-center mb-2'><a href='" +
                            artist.url +
                            "' class='d-block'><div class='row align-items-center'><div class='col-2'><img src='" +
                            artist.primary_image +
                            "' class='release-image' /></div><div class='col-10'><strong class='item-name'>" +
                            artist.name +
                            "</strong></div></div></a></div>"
                    )
                );
            });
        })
        .catch(function(error) {
            $("#search-autocomplete").collapse("hide");

            VinylHeavenTheme.putAlert(
                "error",
                error.responseJSON.exception +
                    " | " +
                    error.responseJSON.message
            );
        });
};

$(document).ready(function() {
    var SearchAutoCompleteCollapseOptions = {
        toggle: false
    };
    $("#search-autocomplete").collapse(SearchAutoCompleteCollapseOptions);

    $("#mobile-search-input").keyup(function(event) {
        event.preventDefault();
        event.stopPropagation();

        if (event.which == 13) {
            $("#mobile-search-form").submit();
        } else {
            if ($("#mobile-search-input").val().length >= 3) {
                var url = $("#mobile-search-input").data("url");
                var data = {
                    _token: $("#mobile-search-input").data("csrf"),
                    autocomplete_search_term: $("#mobile-search-input").val()
                };

                SearchAutoComplete(url, data);
            } else {
                $("#search-autocomplete").collapse("hide");
            }
        }
    });

    $("#search-input").keyup(function(event) {
        event.preventDefault();
        event.stopPropagation();

        if (event.which == 13) {
            $("#search-form").submit();
        } else {
            if ($("#search-input").val().length >= 3) {
                var url = $("#search-input").data("url");
                var data = {
                    _token: $("#search-input").data("csrf"),
                    autocomplete_search_term: $("#search-input").val()
                };

                SearchAutoComplete(url, data);
            } else {
                $("#search-autocomplete").collapse("hide");
            }
        }
    });

    $("#search-autocomplete").mouseleave(function(event) {
        $("#search-autocomplete").collapse("hide");
    });

    $("#navbar-search-input").keyup(function(event) {
        event.preventDefault();
        event.stopPropagation();

        if (event.which == 13) {
            $("#navbar-search-form").submit();
        } else {
            if ($("#navbar-search-input").val().length >= 3) {
                var url = $("#navbar-search-input").data("url");
                var data = {
                    _token: $("#navbar-search-input").data("csrf"),
                    autocomplete_search_term: $("#navbar-search-input").val()
                };

                SearchAutoComplete(url, data);
            } else {
                if (
                    $("#navbar-search-autocomplete-container").hasClass("show")
                ) {
                    $("#navbar-search-autocomplete-container").removeClass(
                        "show"
                    );
                }
            }
        }
    });
});
