<?php

namespace App\Console\Commands;

use DateTime;
use Illuminate\Console\Command;

class MigrateProductsToElastic extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vhelastic:migrate-products-to-elastic';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add the bagsito products to te elastic search';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $product_model = app('VinylHeaven\Product\Models\Product');

        // $products = $product_model->getProductsForCatalog(0);
        $products = $product_model->with(['variants', 'variants.inventories'])->get();
        $products_saved = 0;
        $bar = $this->output->createProgressBar($products->count());
        
        $this->line('Start migrating the products to the elasticsearch stack');

        $bar->start();

        foreach ($products as $product_index => $product) {
            try {
                if ($product->push()) {
                    $date = new DateTime('now');
                    $sku = $product->sku;
                    $product->sku = $date->format('YmdHisu');
                    $product->save();

                    $product->sku = $sku;
                    $product->save();

                    $products_saved++;

                    $bar->advance();
                } else {
                    throw new Exception('Poduct: ' . $product->id . ' cannot be pushed!');
                }
            } catch (\Exception $e) {
                report($e);
                continue;
            }
        }

        $bar->finish();

        $this->info('Total products found: ' . $products->count() . ' Total products indexed in the elasticsearch node: ' . $products_saved);
    }
}
