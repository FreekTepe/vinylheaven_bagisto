<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use VinylHeaven\Product\Models\FetchedImageRelease as FetchedImageRelease;
use VinylHeaven\Response\Http\Resources\ResponseResource;
use GuzzleHttp\Client;

class FetchingImageDiscogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetchingImageDiscogs:fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fetchedImageRelease = new FetchedImageRelease;;
        // Retrieve database info where we left off with retrieve images
        $lastId = $fetchedImageRelease::select('active', 1)->max('release_id');

        echo "last release id fetched image is : id " . $lastId . "\n";

        // This is the function that wil make the api call with
        try {
            // params for the api endpoint
            $amount = 10;

            // construct the client for making the api call
            $client = new Client();

            // make the request to the api and add the options we need for this api request
            //@params | $lastId => the start id from which release id to start from(greater then),
            //			$amount => the amount of ids we want to return(Keep in mind that we cannot get more then 10000 ids at ones)
            $result = $client->request(
                'GET', // type of api call(POST|PUT|GET|DELETE) in our case a GET
                env('LUMEN_API') . "/releases/ids/start/" . $lastId . "/amount/" . $amount, // the api endpoint
                []
            );

            $status = $result->getStatusCode();

            if (!$status == 200) {
                abort($status);
            }

            $contents = json_decode($result->getBody()->getContents(), true);

            // all of the release ids, as an array, that you can use to make the call to the discogs api(not our lumen api)
            $release_ids = $contents['release_ids'];
        } catch (\Throwable $th) {
            dd($th);
            return (new ResponseResource('error', 'Oops...', $th->getMessage(), 3000, true))->response()->setStatusCode($this->status_code($th));
        }

        echo "List following release ids before picking up the cover art\n";

        // This is the function that wil make the Discogs api call with
        try {

            $this->saveImages($release_ids, $fetchedImageRelease);

        } catch (\Throwable $th) {
            dd($th);
            return (new ResponseResource('error', 'Oops...', $th->getMessage(), 3000, true))->response()->setStatusCode($this->status_code($th));
        }
    }

    public function saveImages($release_ids, $fetchedImageRelease)
	{

        $client = new Client();

        foreach($release_ids as $release_id) {

            sleep(1);

            try {

                $result = $client->request('GET', env('DISCOGS_API') . "/releases/" . $release_id . "?token=" . env('DISCOGS_TOKEN'), []);
                $status = $result->getStatusCode();
                if (!$status == 200) {
                    abort($status);
                }

                $results = json_decode($result->getBody()->getContents());

                $images = $results->images;

            } catch (\Exception $e) {
                return $e->getMessage();
            }

            if(isset($images)){
                foreach($images as $image){

                    $file_name = explode("/", $image->resource_url);

                    $data =
                    [
                        'url' => $image->resource_url,
                        'file_name' => end($file_name),
                        'height' => $image->height,
                        'width' => $image->width,
                        'image_type' => $image->type,
                        'release_id' => $release_id,
                    ];

                    dump($data);

                    $fetchedImageRelease::updateOrCreate($data);
                }
            }

        }

        echo "last release id inserted " . $release_id . "\n";

        return $data;

	}
}
