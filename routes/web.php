<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::delete('mpproducts/delete', 'MarketplaceProductController@destroy_all')->name('mpproducts.delete.all');
Route::get('mpproducts/download', 'MarketplaceProductController@download_csv')->name('mpproducts.download');

Route::post('messages', 'MessageController@store')->name('messages.store');


Route::group(['middleware' => ['web', 'locale', 'theme', 'currency']], function () {
    Route::post('/search/autocomplete', '\VinylHeaven\Search\Http\Controllers\SearchController@autocomplete')->name('shop.search.autocomplete');

    Route::prefix('marketplace')->group(function () {
		Route::get('seller/{url}/product/{slug}', 'Webkul\Marketplace\Http\Controllers\Shop\ProductController@sellerProduct')->defaults('_config', [
            'view' => 'marketplace::shop.sellers.product.index'
        ])->name('marketplace.sellers.product.index');
	});
});

Route::get('/testdiscogsapi', '\VinylHeaven\Search\Http\Controllers\SearchController@discogsapitest');

