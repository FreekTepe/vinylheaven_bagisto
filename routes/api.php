<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('releaseImage/{release_id}', 'ReleaseImageController@image')->name('releaseImage.get');

Route::post('statistics/orderprices/{seller_id}', 'Api\StatisticsController@orderprices')->name('statistics.orderprices');